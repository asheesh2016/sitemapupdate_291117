function categoryslug(product){
    pdata = Products.findOne({_id:product,category: { $exists: true, $ne: [] }});
    id = "";
    if(pdata){
        id = pdata.category[0];
    }else{
        id = "";
    }
    if(pdata){
	
        sid = pdata.subcategory[0];
		
		
		if(sid){
			if(sid != undefined){
            return Categories.findOne({_id:id}).slug +'/'+ SubCategories.findOne({_id:sid}).slug+'/';
			}
			
        }else{
			if(id != undefined){
            return Categories.findOne({_id:id}).slug + '/';
			}
			
        }
		
	
    }else{
            return " ";
        }
	
}


sitemaps.add('/sitemap.xml', function() {
    var out = [], products = Products.find().fetch(),pages = Pages.find().fetch(),categories = Categories.find().fetch(),subcategory = SubCategories.find().fetch(); 
      out.push({
		  page:'/',
		  priority:1.00
	  });
    _.each(categories,function(cat){
		if(cat._id=="boJKLzQB4cPZWEMLf"){
			out.push({
            page : '/category/' + cat.slug,
            lastmod : new Date(),
			priority:0.6
        });
		}else{
			out.push({
            page : '/category/' + cat.slug,
            lastmod : new Date(),
			priority:0.8
        });
		}
        
		
    });
	 _.each(subcategory,function(cat){
		
        out.push({
            page : '/subcat/' + cat.slug,
            lastmod : new Date(),
			priority:0.8
        });
		
    });
    _.each(products, function(product) {
        out.push({
            page : '/product/' +categoryslug(product._id)+ product.slug,
            lastmod : new Date(),
			priority:0.8
        });
    }); 
    _.each(pages, function(page) {
        out.push({
            page : '/' + page.slug,
            lastmod : new Date(),
			priority:0.6
        });
    });
	 
    return out;
});

sitemaps.config('rootUrl', 'https://www.wingreensfarms.com/')
