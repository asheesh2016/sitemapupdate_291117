/**
 * Created by tushardhara on 23/05/16.
 */
var PayU = Npm.require('payu'); 

var payu = new PayU('q0BEE9', 'hBw9gyix', 'https://info.payu.in/merchant/postservice.php?form=2');

// payu.get_transaction_info('2017-03-18 00:00:00','2017-03-18 12:00:00',function(err, response){
//     console.log(response);
// });

// payu.verify_payment('Pto2jn3yg9FGwquNy',function(err, response){
//     console.log(response);
// });


Accounts.emailTemplates.resetPassword.html = function(user, url) {
   url = url.replace('#/','');
   
   var html =
`
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Wingreen</title>
    </head>
    <body style="background-color:#eaebf4; padding:0; margin:0;">
        <table width="600" cellpadding="0" cellspacing="0" style="background-color: #fff; border:1px solid #dddddd; margin:0 auto; font-size:12px; padding:0; font-family:Arial, Helvetica, sans-serif;">
            <tbody>
                <tr>
                    <td height="4" bgcolor="#7bb100"></td>
                </tr>
                <tr>
                    <td height="51" style="padding:5px 15px; border-bottom:1px solid #c1c1c1;"><a href="index.html"><img src="http://www.wingreensfarms.com/client/images/wingreen_logo.png" width="177" height="80" alt="wingreen Logo"></a></td>
                </tr>
                <tr>
                    <td>
                        <h5 style="font-size:15px; line-height:24px; padding:15px 15px 0 15px; margin:5px 0; color:#7bb100; font-weight:bold; text-transform:uppercase;">Dear ${user.profile.name},</h5>
                        <p style="font-size:15px; line-height:24px; padding:0 15px; color:#232323; margin-bottom:30px;">Click this link to reset your password: <a href="${url}" target="_blank" style="color:#7bb100; text-decoration:underline;">Link</a></p>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding-top:30px;">
                        <p> <a href="https://twitter.com/WingreensFarms"><img src="http://www.wingreensfarms.com/client/images/tw_icon.png"></a> <a href="https://www.facebook.com/wingreens/"><img src="http://www.wingreensfarms.com/client/images/fb_icon.png"></a> <a href="https://www.instagram.com/wingreensfarms/"><img src="http://www.wingreensfarms.com/client/images/insta_icon.png"></a> </p>
                        <p>For Enquiry: <strong>(0) 88007 93642</strong> | <a href="mailto:contact@wingreensfarms.com" style="color:#7bb100; text-decoration:underline;">contact@wingreensfarms.com</a></p>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>`
    return html;
}
Meteor.startup(function(){
    if(Meteor.users.find().count() == 0){
        let id = Accounts.createUser({
            username: 'admin',
            profile: {
                name: 'admin',
                fname: 'admin',
                lname: 'admin',
            },
            email: 'admin@wingreen.com',
            password: '123123'
        });
        Roles.addUsersToRoles(id,['admin']);
    }
    if(Footer.find().count() == 0){
        let id = Footer.insert({tag:'footer',content:'footer'});
    }
})

ServiceConfiguration.configurations.remove({
    service: "facebook"
});
ServiceConfiguration.configurations.insert({
    service: "facebook",
    appId: "134727616926860",
    secret: "49cc1ff221b4efef00896e43d8bc0e2b"
});
ServiceConfiguration.configurations.remove({
  service: "google"
});
ServiceConfiguration.configurations.insert({
  service: "google",
  clientId: "728671475903-5vgbfalup14c6m7diq57ump8b4l4lthe.apps.googleusercontent.com",
  secret: "GtHejr2r20Qg8lJj-JvEeOf4"
});

Slingshot.createDirective('myDefinedDirective', Slingshot.S3Storage, {
    bucket:"wingreens",
    maxSize: 10 * 1024 * 1024,
    acl: "public-read",
    region: "ap-southeast-1",
    AWSAccessKeyId: "AKIAJ4TUKQCTVDU2EXOQ",
    AWSSecretAccessKey: "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",
    allowedFileTypes: ['image/png', 'image/jpeg', 'image/gif'],
    authorize: function() {
        var message;
        if (!this.userId) {
            message = 'Please login before posting files';
            throw new Meteor.Error('Login Required', message);
        }
        return true;
    },
    key: function(file) {
        // admin would be the folder and file would be saved with a timestamp
        return "admin"+'/' + Date.now() + file.name;
    }
});

Meteor.startup(function() {
    //process.env.MAIL_URL="smtp://postmaster@igenero.in:9f097858603793b4a3d70fbc31a93f00@smtp.mailgun.org:587";
    process.env.MAIL_URL="smtp://postmaster@mg.wingreensfarms.com:1678a42093ec16114e243396ab6ca969@smtp.mailgun.org:587";
});


Accounts.emailTemplates.siteName = "Wingreen";
Accounts.emailTemplates.from = "Wingreen <no-reply@wingreen.com>";

SyncedCron.add({
  name: 'Check From PayU transaction is successfully or not',
  schedule: function(parser) {
    // parser is a later.parse object
    return parser.text('every 2 minutes');
  },
  job: function() {
    orders = Orders.find({ cart: { $exists: true},address:{$exists:true},status:0,createdAt : { $gte : new Date(moment().startOf('day')) }}).fetch()
    if(!_.isEmpty(orders)){
        _.each(_.pluck(orders,'_id'),function(id){
            payu.verify_payment(id,Meteor.bindEnvironment(function(err, response){
                var toJsonResponse = JSON.parse(response);
                if(!_.isUndefined(toJsonResponse.transaction_details[id])){
                    if(toJsonResponse.transaction_details[id].status == 'success'){
                        var orderId = id;
                        var order = Orders.find({'_id':orderId},{limit:1,sort:{createdAt : -1}}).fetch()[0];
                        cid =  Orders.update({'_id':orderId},{$set:{status:2}});
                        cpayu = Orders.update({'_id':orderId},{$set:{payu:toJsonResponse.transaction_details[id]}});
                        cid = Carts.remove({'_id':order.cart._id});
                        _.each(order.cart.items,function (item,index) {
                            up = Products.update({_id:item.id},{$inc :{quantity: -1 * (item.count)}});
                            console.log('reduced',up);
                        });
                        var user = {
                            name : order.name
                        }
                        Meteor.defer(function(){
                            Email.send({
                                to: order.email,
                                from: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                                bcc: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                                subject: 'Order Confirmation - Your Order No. '+orderId+ ' & payment with wingreensfarms.com has been successfully received',
                                html: html(user,orderId,2)
                            },function(err,result){
                                if(err) {
                                    console.log(err)
                                }else{
                                    //console.log(result)
                                } 
                            });
                        });
                        Meteor.defer(function(){
                            var message = "Dear "+order.name+", Thank you for placing your order with Wingreens Farms! It will be dispatched within the next business day. For customer support please dial +91-8800793642";
                            var encodeedMessage = encodeURIComponent(message);
                            console.log(encodeedMessage);
                            HTTP.call('POST','http://api.textlocal.in/send',{
                                params : {
                                    username : 'armaan@wingreens.in',
                                    hash : '29aba3f6d5c61f94c5dc11cee1b96177e2dd865e',
                                    sender : 'WINGRN',
                                    numbers : order.phone,
                                    message : encodeedMessage
                                }
                            }, function(err,result){
                                if(err) {
                                    console.log(err)
                                }else{
                                    //console.log(result)
                                }
                                
                            });
                        });
                    }else{
                        var orderId = id;
                        var order = Orders.find({'_id':orderId},{limit:1,sort:{createdAt : -1}}).fetch()[0];
                        cpayu = Orders.update({'_id':orderId},{$set:{payu:toJsonResponse.transaction_details[id]}});
                    }
                }
            }));
        })
    }
    var numbersCrunched = orders;
    return numbersCrunched;
  }
});

SyncedCron.start();

