Meteor.publish('Categories',function(){
    return [Categories.find({},{ sort:{createdAt : -1}})];
})

Meteor.publish('SubCategories',function(id){
    return [SubCategories.find({cat_id:id},{ sort:{createdAt : -1}})];
})

Meteor.publish('SubCategoriesList',function(id){
    product = Products.findOne({_id:id});
    if(product){
        cat = product.category; 
        return [SubCategories.find({cat_id:cat},{ sort:{createdAt : -1}})];
    }
})
Meteor.publish('SubCategoriesHome',function(id){
    return [SubCategories.find()]
})
Meteor.publish('ProductSlug',function () {
    return [Products.find({},{fields : {slug:1}})];
})

Meteor.publish('ProductList',function () {
    return [Products.find({}),Categories.find({})];
})

Meteor.publish('SingleProduct',function (id) {
    return [Products.find({_id:id})]
})

Meteor.publish('ProductListInCat',function (slug) {
    if(Categories.find({slug:slug}).count() > 0){
        cat_id = Categories.findOne({slug:slug})._id;
        return [Products.find({category:cat_id})]
    }
})

Meteor.publish('SubCatList',function (slug) {
    if(SubCategories.find({slug:slug}).count() > 0){
        cat_id = SubCategories.findOne({slug:slug})._id;
        return [Products.find({subcategory:cat_id})]
    }
})

Meteor.publish('Carts',function(session){
    if(this.userId){
        return [Carts.find({'userId':this.userId})]
    }else{
        return [Carts.find({'userId':session})]
    }
})

Meteor.publish('CartItems',function(session) {
    if(this.userId){
        let CartCol =  Carts.findOne({'userId':this.userId});
        if(!_.isUndefined(CartCol) && !_.isUndefined(CartCol.items)){
            return [Products.find({}),Carts.find({'userId':this.userId})]
        }
    }else{
        let CartCol =  Carts.findOne({'userId':session});
        if(!_.isUndefined(CartCol) && !_.isUndefined(CartCol.items)){
            return [Products.find({}),Carts.find({'userId':session})]
        }
    }
})

Meteor.publish('CouponsList',function () {
    return [Coupons.find()]
})

Meteor.publish('singleCoupon',function (id) {
    if(this.userId){
        return [Coupons.find({_id:id})]
    }
})

Meteor.publish('singleStore',function (id) {
    if(this.userId){
        return [Stores.find({_id:id})]
    }
})

Meteor.publish('PageSlug',function (slug) {
    return [Pages.find({},{fields : {slug:1}})]
})

Meteor.publish('SinglePage',function (slug) {
    return [Pages.find({slug:slug})]
})

Meteor.publish('pageWithId',function (id) {
    if(this.userId){
        return [Pages.find({_id:id})]
    }
})

Meteor.publish('Pages',function (slug) {
    if(this.userId) {
        return [Pages.find({})]
    }
})

Meteor.publish('OrdersData',function (id) {
    if(this.userId){
        return [Orders.find({_id:id})]
    }else{
        return [Orders.find({_id:id})]
    }
})

Meteor.publish('ShippingDetails',function () {
    if(this.userId){
        return [Shipping.find({userId:this.userId})]
    }
})

Meteor.publish('ProductListWithSearch',function (search) {
    if (Products.find({title:buildRegExp(search)}).count()) {
        return [ Products.find({title:buildRegExp(search)}) ]
    } else {
        var categories = Categories.find({name:buildRegExp(search)}).fetch();
        var searchArray = _.pluck(categories, '_id');
        return Products.find({category: {$in: searchArray}})
    }
})

Meteor.publish('Sliders',function () {
        return [Sliders.find({})]
})

Meteor.publish('Seo',function (url) {
    return [Seo.find({url:url})]
})

Meteor.publish('SingleSlider',function (id) {
    if(this.userId){
        return [Sliders.find({_id:id})]
    }
})

Meteor.publish('Header',function () {
        return [Header.find({})]
})

Meteor.publish('Footer',function () {
        return [Footer.find({tag:'footer'})]
})
Meteor.publish('Background',function(){
    return [Background.find({tag:'bg'})]
})
function buildRegExp(searchText) {
    var parts = searchText.trim().split(/[ \-\:]+/);
    return new RegExp("(" + parts.join('|') + ")", "ig");
}

Meteor.publishComposite('SingleProductSlug',function (slug) {
    return {
        find: function(){
            return Products.find({slug:slug})
        },
        children: [
            {
                find: function(){
                    return Products.find({slug: {$nin:[slug]}},{limit:5})
                }
            }
        ]
    }
})

Meteor.publish('All',function(){
    return [
        Meteor.users.find({"roles":'user'},{fields:{'_id':1,roles:1}}),
        Products.find({},{fields:{'_id':1}}),
        Orders.find({}),
        Categories.find({},{fields:{'_id':1}}),
        Pages.find({},{fields:{'_id':1}}),
        Coupons.find({},{fields:{'_id':1}})
    ]
})

Meteor.publish('userslist', function (role){
    return Meteor.users.find({"roles":role});
});

Meteor.publish('OrderHistoryUser',function () {
    if(this.userId){
        return [Orders.find({createdBy:this.userId,$or :[{status:2},{status:3},{status:4},{status:5}]}),Products.find({})];
    }
})

Meteor.publish('OrderHistoryAdmin',function () {
    if(this.userId){
        return [Orders.find({$or :[{status:2},{status:3},{status:4},{status:5}]}),Products.find({})];
    }
})

Meteor.publish('OrderHistoryFailedAdmin',function () {
    if(this.userId){
        return [Orders.find({$or :[{status:0}]}),Products.find({})];
    }
})

Meteor.publish('OrderHistoryAdminWithID',function (id) {
    if(this.userId){
        return [Orders.find({_id:id,$or :[{status:0},{status:2},{status:3},{status:4},{status:5}]}),Products.find({})];
    }
});

Meteor.publish('RatingByUser',function () {
    if(this.userId){
        return [Reviews.find({userId:this.userId})];
    }
});

Meteor.publish('RatingByAdmin',function(){
    if(this.userId){
        return [Reviews.find({}),Meteor.users.find({}),Products.find({})];
    }
})

Meteor.publishComposite('RatingProduct',function(slug){
    return {
        find: function(){
            product = Products.findOne({slug:slug});
            var id = '';
            if(!_.isUndefined(product)){
                id = product._id;
            }
            return Reviews.find({productId:id})
        },
        children: [
            {
                find: function(review){
                    return Meteor.users.find({_id:review.userId})
                }
            }
        ]
    }
})

Meteor.publish('Reports',function(){
    if(this.userId){  
        return [Orders.find({$or :[{status:4}]})];
    }
})

Meteor.publish(null, function() {
    if(this.userId){ 
        return Meteor.users.find({
            _id: this.userId
        }, 
        {
            fields: {
                email: true,
                profile: true,
                roles: true,
                services: true,
            }
        })
    }
})

Meteor.publish('StoreList',function(){
    return Stores.find({});
})

Meteor.publish('ShippingAll',function(){
    return Shipping.find({});
})
