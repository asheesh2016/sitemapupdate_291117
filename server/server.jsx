var bodyParser = Npm.require('body-parser'); 
Picker.middleware(bodyParser.urlencoded({ extended: false }));
Picker.middleware(bodyParser.json());
Picker.route('/payment/success', function(params, req, res, next) {
  if(!_.isUndefined(req.body.status)){
        var order = Orders.find({'_id':req.body.txnid},{limit:1,sort:{createdAt : -1}}).fetch()[0];
        id =  Orders.update({'_id':order._id},{$set:{status:2}});
        payu = Orders.update({'_id':order._id},{$set:{payu:req.body}});
        id = Carts.remove({'_id':order.cart._id});
        _.each(order.cart.items,function (item,index) {
            up = Products.update({_id:item.id},{$inc :{quantity: -1 * (item.count)}});
            console.log('reduced',up);
        });
        var user = {
            name : order.name
        }
        Meteor.defer(function(){
            Email.send({
                to: order.email,
                from: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                bcc: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                subject: 'Order Confirmation - Your Order No. '+order._id+ ' & payment with wingreensfarms.com has been successfully received',
                html: html(user,order._id,2)
            },function(err,result){
                if(err) {
                    console.log(err)
                }else{
                    //console.log(result)
                } 
            });
        });
        Meteor.defer(function(){
            var message = "Dear "+order.name+", Thank you for placing your order with Wingreens Farms! It will be dispatched within the next business day. For customer support please dial +91-8800793642";
            var encodeedMessage = encodeURIComponent(message);
            console.log(encodeedMessage);
            HTTP.call('POST','http://api.textlocal.in/send',{
                params : {
                    username : 'armaan@wingreens.in',
                    hash : '29aba3f6d5c61f94c5dc11cee1b96177e2dd865e',
                    sender : 'WINGRN',
                    numbers : order.phone,
                    message : encodeedMessage
                }
            }, function(err,result){
                if(err) {
                    console.log(err)
                }else{
                    //console.log(result)
                }
                
            });
        });
  }
  res.writeHead(301, {Location: "/payments/success"});
  res.end();
});
Picker.route('/payment/error', function(params, req, res, next) {
  if(!_.isUndefined(req.body)){
        var order = Orders.find({'_id':req.body.txnid},{limit:1,sort:{createdAt : -1}}).fetch()[0];
        payu = Orders.update({'_id':order._id},{$set:{payu:req.body}});
    }
  res.writeHead(301, {Location: "/payments/error"});
  res.end();
});
Picker.route('/payment/cancel', function(params, req, res, next) {
  if(!_.isUndefined(req.body)){
        var order = Orders.find({'_id':req.body.txnid},{limit:1,sort:{createdAt : -1}}).fetch()[0];
        payu = Orders.update({'_id':order._id},{$set:{payu:req.body}});
    }
  res.writeHead(301, {Location: "/payments/cancel"});
  res.end();
});

Meteor.methods({
    categoryslug(product){
        pdata = Products.findOne({slug:product});
        //console.log(pdata);
        id = "";
        if(pdata){
            id = pdata.category[0];
        }else{
            id = "";
        }
        if(pdata){
            sid = pdata.subcategory[0]
            if(sid){
                return Categories.findOne({_id:id}).slug +'/'+ SubCategories.findOne({_id:sid}).slug+'/';
            }else{
                return Categories.findOne({_id:id}).slug + '/';
            }
        }else{
            return "";
        }
    },
    createAccount: function (data) {
        //Meteor.unblock();
        try {
            id = Accounts.createUser({
                password: data.password,
                email: data.email,
                profile: {}
            });
            Roles.addUsersToRoles(id, ['user'])
        } catch (e) {
            throw new Meteor.Error(e);
        }
    },
    createManagerAccount: function (data) {
        //Meteor.unblock();
        try {
            id = Accounts.createUser({
                username: data.username,
                password: data.password,
                email: data.email,
                profile: {
                    name : data.name,
                    phone : data.phone
                }
            });
            Roles.addUsersToRoles(id, ['manager'])
            Meteor.users.update({_id: id}, {
                $set: {
                    'profile.name': data.name,
                    'profile.phone': data.phone,
                }
            })
        } catch (e) {
            throw new Meteor.Error(e);
        }
    },
    updateProfile: function (data) {
        if (this.userId) {
            if (_.isUndefined(data.avatar)) {
                Meteor.users.update({_id: this.userId}, {
                    $set: {
                        'profile.fname': data.fname,
                        'profile.lname': data.lname,
                    }
                })
            } else {
                Meteor.users.update({_id: this.userId}, {
                    $set: {
                        'profile.fname': data.fname,
                        'profile.lname': data.lname,
                        'profile.avatar': data.avatar
                    }
                })
            }
        }
    },
    deleteAccount : function(data){
        if (this.userId) {
            try {
                Meteor.users.remove({_id: data});
            } catch (e) {
                throw new Meteor.Error(e);
            }
        }
    },
    updateUserProfile: function (data) {
        if (this.userId) {
            Meteor.users.update({_id: this.userId}, {
                $set: {
                    'profile.name': data.name,
                    'profile.dob': data.dob,
                    'profile.phone': data.phone
                }
            })
        }
    },
    updateUserBillProfile: function (data) {
        if (this.userId) {
            Meteor.users.update({_id: this.userId}, {
                $set: {
                    'profile.name': data.name,
                    'profile.phone': data.phone
                }
            })
        }
    },
    AddCategory: function (name) {
        if (this.userId) {
            try {
                let convertToSlug = (Text) => {
                    return Text
                        .toLowerCase()
                        .replace(/ /g, '-')
                        .replace(/[^\w-]+/g, '');
                }
                if (Categories.find({slug: convertToSlug(name)}).count() == 0) {
                    id = Categories.insert({name: name, slug: convertToSlug(name), createdAt: new Date()});
                    console.log(id);
                } else {
                    throw new Meteor.Error("slug exist", "category slug is already there");
                }
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    EditCategory: function (data) {
        if (this.userId) {
            try {
                Categories.update({_id: data.id}, {$set: data.set});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    EditSubCategory: function (data) {
        if (this.userId) {
            try {
                SubCategories.update({_id: data.id}, {$set: data.set});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    AddSubCategory: function (data) {
        if (this.userId) {
            try {
                let convertToSlug = (Text) => {
                    return Text
                        .toLowerCase()
                        .replace(/ /g, '-')
                        .replace(/[^\w-]+/g, '');
                }
                if (SubCategories.find({cat_id: data.cat_id, slug: convertToSlug(data.name)}).count() == 0) {
                    id = SubCategories.insert({
                        name: data.name,
                        slug: convertToSlug(data.name),
                        cat_id: data.cat_id,
                        createdAt: new Date()
                    });
                    console.log(id);
                } else {
                    throw new Meteor.Error("slug exist", "category slug is already there");
                }
            } catch (e) {
                console.log(e);
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    DeleteCategory: function (id) {
        if (this.userId) {
            try {
                Categories.remove({_id: id});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    DeleteSubCategory: function (id) {
        if (this.userId) {
            try {
                SubCategories.remove({_id: id});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    AddProduct: function (data) {
        if (this.userId) {
            try {
                id = Products.insert(data);
                console.log('productId', id);
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    deleteProduct: function (id) {
        if (this.userId) {
            try {
                Products.remove({_id: id});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    EditProduct: function (data) {
        if (this.userId) {
            try {
                Products.update({_id: data.id}, {$set: data.set});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    AddToCart: function (data) {
        if (this.userId) {
            var find = Carts.find({'userId': this.userId}).count();
            if (find == 0) {
                Carts.insert({
                    userId: this.userId,
                    items: [{
                        id: data.id,
                        count: 1,
                    }]
                });
            } else {
                Carts.update({"userId": this.userId}, {$addToSet: {"items": {id: data.id, count: 1}}});
            }
        }else{
            var find = Carts.find({'userId': data.session}).count();
            if (find == 0) {
                Carts.insert({
                    userId: data.session,
                    items: [{
                        id: data.id,
                        count: 1,
                    }]
                });
            } else {
                Carts.update({"userId": data.session}, {$addToSet: {"items": {id: data.id, count: 1}}});
            }
        }
    },
    modifyCart : function(session){
        if(this.userId){
            var find = Carts.find({'userId':this.userId}).count();
            if(find == 0){
                var sfind = Carts.find({'userId':session}).count();
                if(sfind > 0){
                    cart_data = Carts.findOne({userId:session});
                    Carts.insert({
                        userId : this.userId,
                        items: cart_data.items
                    });
                }
            }else{
                var userCart_data = Carts.findOne({'userId':this.userId});  
                var sfind = Carts.find({'userId':session}).count();
                if(sfind > 0){
                    cart_data = Carts.findOne({userId:session});
                    //items  = _.union(_.pluck(userCart_data.items,'id'),_.pluck(cart_data.items,'id'))
                    //if(_.size(userCart_data.items) != _.size(cart_data.items)){
                    items = [];
                    _.each(userCart_data.items,function(val){
                        items.push(val)
                    })
                    var itemId = _.union(_.pluck(userCart_data.items,'id'));
                    _.each(cart_data.items,function(val){
                        if(!_.contains(_.pluck(items,'id'),val.id)){
                            _.each(itemId,function(val1,index){
                                if(val1 == val1.id){
                                    val.count = val.count + userCart_data.items[index].count
                                }
                            })
                            items.push(val)
                        }
                    })
                    Carts.update({"userId":this.userId},{$set : {"items":items}})
                    //}
                }
            }
        }
    },
    pullItemCart: function (data) {
        if (this.userId) {
            Carts.update({"userId": this.userId, 'items.id': data.id}, {$pull: {"items": {id: data.id}}});
        }else{
            Carts.update({"userId": data.session, 'items.id': data.id}, {$pull: {"items": {id: data.id}}});
        }
    },
    changeItemCart: function (data) {
        if (this.userId) {
            Carts.update({"userId": this.userId, 'items.id': data.id}, {$set: {"items.$.count": data.count}});
        }else{
            Carts.update({"userId": data.session, 'items.id': data.id}, {$set: {"items.$.count": data.count}});
        }
    },
    createCoupon: function (data) {
        if (this.userId) {
            var find = Coupons.find({'code': (data.code).toUpperCase()}).count();
            if (find == 0) {
                id = Coupons.insert({
                    code: (data.code).toUpperCase(),
                    type: data.type,
                    amount: data.amount,
                    status: data.status,
                    createdAt: new Date()
                });
                console.log('coupon created:', id)
            } else {
                throw new Meteor.Error("Coupon already exist", "coupon error");
            }
        }
    },
    deleteCoupon: function (id) {
        if (this.userId) {
            try {
                Coupons.remove({_id: id});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    editCoupon: function (data) {
        if (this.userId) {
            try {
                Coupons.update({_id: data.id}, {$set: data.set});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    AddPage: function (data) {
        try {
            id = Pages.insert(data);
            console.log('pageId', id);
        } catch (e) {
            throw new Meteor.Error("Insert Error", "Insert Error");
        }
    },
    deletePage: function (id) {
        console.log(id);
        try {
            Pages.remove({_id: id});
        } catch (e) {
            throw new Meteor.Error("logged-out", "You must be logged in to delte a Page.");
        }
    },
    editPage: function (data) {
        if (this.userId) {
            try {
                Pages.update({_id: data.id}, {$set: data.set});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    editSeo : function (data) {
        if (this.userId) {
            try {
                Seo.update({url: data.url}, {$set: data.set});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    createOrder: function (data) {
        if (this.userId) {
            try {
                // data[orderNumber] = "WG" + Number(Orders.find().count() + 1000);
                // console.log(data);
                id = Orders.insert(data);
                return id;
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }else{
            try {
                // data[orderNumber] = "WG" + Number(Orders.find().count() + 1000);
                // console.log(data);
                id = Orders.insert(data);
                return id;
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    createShippingAddress: function (data) {
        if (this.userId) {
            try {
                id = Shipping.insert(data);
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }else{
            try {
                id = Shipping.insert(data);
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    deleteShippingAddress: function (id) {
        if (this.userId) {
            try {
                id = Shipping.remove({_id: id});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    editShippingAddress: function (data) {
        if (this.userId) {
            try {
                id = Shipping.update({_id: data.id},{$set : data.set});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    addSlider: function (data) {
        try {
            id = Sliders.insert(data);
            console.log('Id', id);
        } catch (e) {
            throw new Meteor.Error("logged-out", "You must be logged in to Add a Slider.");
        }
    },
    deleteSlider: function (id) {
        try {
            Sliders.remove({_id: id});
        } catch (e) {
            throw new Meteor.Error("logged-out", "You must be logged in to Delete a Slider.");
        }
    },
    editSlider: function (data) {
        if (this.userId) {
            try {
                Sliders.update({_id: data.id}, {$set: data.set});
            } catch (e) {
                throw new Meteor.Error("logged-out", "You must be logged in to edit a Slider.");
            }
        }
    },
    AddRemoveHeader: function (data) {
        if (this.userId) {
            try {
                count = Header.find({slug: data.slug}).count();
                if (count == 0) {
                    Header.insert(data);
                } else {
                    Header.remove({slug: data.slug});
                }
            } catch (e) {
                throw new Meteor.Error("logged-out", "You must be logged in to edit a Slider.");
            }
        }
    },
    footerContent: function (data) {
        if (this.userId) {
            try {
                count = Footer.find({}).count();
                if (count == 0) {
                    Footer.insert({content: data, tag: 'footer'});
                } else {
                    Footer.update({_id: data.id}, {content: data.content, tag: 'footer'});
                }
            } catch (e) {
                throw new Meteor.Error("logged-out", "You must be logged in to edit Footer.");
            }
        }
    },
    backgroundContent: function (data) {
        if (this.userId) {
            try {
                count = Background.find({}).count();
                if (count == 0) {
                    Background.insert({featuredImage: data, tag: 'bg'});
                } else {
                    Background.update({_id: data.id}, {featuredImage: data.featuredImage, tag: 'bg'});
                }
            } catch (e) {
                throw new Meteor.Error("logged-out", "You must be logged in to edit Background.");
            }
        }
    },
    adminOrder : function(data){
        if(this.userId){
            try {
                id = Orders.insert(data);
                var user = {
                    name : data.name
                }
                this.unblock();
                Email.send({
                    to: data.email,
                    from: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                    bcc: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                    subject: 'Your order is being processed - '+id,
                    html: html(user,id,2)
                });
                console.log(id);
            } catch (e) {
                console.log(e);
                throw new Meteor.Error("logged-out", "You must be logged in to edit Footer.");
            }
        }
    },
    deleteOrder : function(id){
        try {
            Orders.remove({_id: id});
        } catch (e) {
            throw new Meteor.Error("logged-out", "You must be logged in to Delete a Slider.");
        }
    },
    proceedPayment : function (data) {
        if(this.userId){
            try {
                id = Orders.update({_id:data.id},{$set :{name:data.name,phone:data.phone,email:data.email,address:data.address}});
                id = Orders.update({_id:data.id},{$set :{'cart.userId':this.userId,'createdBy':this.userId}});
                console.log("this",id);
            } catch (e) {
                console.log(e);
                throw new Meteor.Error("logged-out", e);
            }
        }else{
            try {
                id = Orders.update({_id:data.id},{$set :{name:data.name,phone:data.phone,email:data.email,address:data.address}});
                console.log(id);
            } catch (e) {
                console.log(e);
                throw new Meteor.Error("logged-out", e);
            }
        }
    },
    successPayment : function(data){
        if(this.userId){
            try{
                order = Orders.find({'cart.userId':this.userId},{limit:1,sort:{createdAt : -1}}).fetch()[0];
                id =  Orders.update({_id:order._id},{$set:{status:2}});
                console.log(order,id);
                id = Carts.remove({_id:order.cart._id});
                
                // id =  Orders.update({_id:order._id},{$set:{status:2}});
                // console.log(order,id);
                // id = Carts.remove({userId:this.userId});
                console.log(id);
                _.each(order.cart.items,function (item,index) {
                    up = Products.update({_id:item.id},{$inc :{quantity: -1 * (item.count)}});
                    console.log('reduced',up);
                });
                user = {
                    name : Meteor.users.findOne({_id:this.userId}).profile.name
                }
                this.unblock();
                Email.send({
                    to: Meteor.users.findOne({_id:this.userId}).emails[0].address,
                    from: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                    bcc: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                    subject: 'Order Confirmation - Your Order No. '+order._id+ ' & payment with wingreensfarms.com has been successfully received',
                    html: html(user,order._id,2)
                });
            } catch(e) {
                console.log(e);
                throw new Meteor.Error("logged-out","you must logged in");
            }
        }else{
           try{
                order = Orders.find({'cart.userId':data.session},{limit:1,sort:{createdAt : -1}}).fetch()[0];
                id =  Orders.update({_id:order._id},{$set:{status:2}});
                console.log(order,id);
                id = Carts.remove({userId:data.session});
                console.log(id);
                _.each(order.cart.items,function (item,index) {
                    up = Products.update({_id:item.id},{$inc :{quantity: -1 * (item.count)}});
                    console.log('reduced',up);
                });
                var user = {
                    name : order.name
                }
                this.unblock();
                Email.send({
                    to: order.email,
                    from: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                    bcc: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                    subject: 'Order Confirmation - Your Order No. '+order._id+ ' & payment with wingreensfarms.com has been successfully received',
                    html: html(user,order._id,2)
                });
            } catch(e) {
                console.log(e);
                throw new Meteor.Error("logged-out","you must logged in");
            } 
        }
    },
    changeStatus : function (data) {
        if(this.userId){
            try {
                id = Orders.update({_id:data.id},{$set :{status:data.status}});
                order = Orders.findOne({_id:data.id});
                user  = {
                    name : order.name
                }
                email = order.email;
                console.log(email); 
                this.unblock();
                if(data.status == 2) {
                    Email.send({
                        to: email,
                        from: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                        bcc: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                        subject: 'Order Confirmation - Your Order No. '+data.id+ ' & payment with wingreensfarms.com has been successfully received',
                        html: html(user,data.id,data.status)
                    });
                }else if(data.status == 3){
                    Email.send({
                        to: email,
                        from: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                        bcc: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                        subject: 'Order Dispatched - Your Order No. '+data.id+ ' has been successfully shipped out by wingreensfarms.com',
                        html: html(user,data.id,data.status)
                    });
                }else if(data.status == 4){
                    Email.send({
                        to: email,
                        from: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                        bcc: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                        subject: 'Your Order No. '+data.id+ ' has been successfully delivered by wingreensfarms.com',
                        html: html(user,data.id,data.status)
                    });
                }else if(data.status == 5){
                    Email.send({
                        to: email,
                        from: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                        bcc: 'Wingreens Farms Team <orders@wingreensfarms.com>',
                        subject: 'Order Cancelled - Your Order No. '+data.id+ ' has been successfully cancelled out by wingreensfarms.com',
                        html: html(user,data.id,data.status)
                    });
                }

           }catch (e) {
                console.log(e);
                throw new Meteor.Error("logged-out", "You must be logged in to edit Footer.");
           }
        }
    },
    changeToAll : function (data) {
        if(this.userId){
            try {
                id = Orders.update({_id:data.id},{$set :{
                    'name' : data.name,
                    'email' :data.email,
                    'phone' : data.phone,
                    'address.address' : data.address.address,
                    'address.city' : data.address.city,
                    'address.state' : data.address.state,
                    'address.country' : data.address.country,
                    'address.zipcode' :  data.address.zipcode
                }});
           }catch (e) {
                console.log(e);
                throw new Meteor.Error("logged-out", "You must be logged in to edit Footer.");
           }
        }
    },
    changeTacking : function (data) {
        if(this.userId){
            try {
                id = Orders.update({_id:data.id},{$set :{
                    'tracking_mumber' : data.tracking_mumber,
                    'tracking_mumber_slug' : data.tracking_mumber_slug,
                }});
                order = Orders.findOne({_id:data.id});
                var result = HTTP.call('POST','https://api.aftership.com/v4/trackings',{
                    data : {
                        "tracking": {
                            "tracking_number": order.tracking_mumber,
                            "slug" : order.tracking_mumber_slug,
                            "title": "Wingreen Order ID " + order._id,
                            "smses": '+91'+ order.phone.replace(/^[0]+|^(\+91)+/, ''),
                            "emails": order.email,
                            "order_id": order._id,
                            "customer_name" : order.name
                        }
                    },
                    headers : {
                        "aftership-api-key" : "889f838e-a2ad-4bba-a68e-56fa79172d82",
                    }
                });
                //console.log(result);
                return result.data;
           }catch (e) {
                //console.log(e);
                throw new Meteor.Error(e);
           }
        }
    },
    addReview : function (data) {
        if(this.userId){
            try {
                id = Reviews.insert(data);
                productIdCount = Reviews.find({productId:data.productId}).count();
                productRatingSum = _.reduce(_.pluck(Reviews.find({productId:data.productId}).fetch(),'rating'),function (memo, num) {
                  return num + memo
                },0);
                avg = productRatingSum/productIdCount;
                console.log(avg);
                id = Products.update({'_id':data.productId},{$set:{rating:avg}});
                console.log(id);
            }catch (e) {
                console.log(e);
                throw new Meteor.Error("logged-out", "You must be logged in to edit Footer.");
            }
        }
    },
    deleteReview : function(id){
        if(this.userId){
            try{
                id = Reviews.remove({_id:id});
                console.log(id);
            }catch(e){
                console.log(e);
                throw new Meteor.Error("logged-out","You must be loggerd");
            }
        }
    },
    createStore: function (data) {
        if (this.userId) {
            id = Stores.insert({
                address: data.address,
                city: data.city,
                lat: data.lat,
                lng: data.lng,
                createdAt: new Date()
            });
            console.log('Store created:', id)
        }else {
            throw new Meteor.Error("Store already exist", "Store error");
        }
    },
    editStore: function (data) {
        if (this.userId) {
            try {
                Stores.update({_id: data.id}, {$set: data.set});
            } catch (e) {
                throw new Meteor.Error("logged-out", "The user must be logged in to post a comment.");
            }
        }
    },
    deleteStore :  function(id){
        if(this.userId){
            try{
                id = Stores.remove({_id:id});
                console.log(id);
            }catch(e){
                console.log(e);
                throw new Meteor.Error("logged-out","You must be loggerd");
            }
        }
    }
})

function html(user,orderId,status){
    var status_text;
    if(status == 2){
        status_text = "Your order is Confirmed"
    }else if(status == 3){
        status_text = "Your order is Dispatched"
    }else if(status == 4){
        status_text = "Your order is Delivered"
    }else if(status == 5){
        status_text = "Your order is Cancelled"
    }

    var products='';
    order = Orders.findOne({_id:orderId});
    _.each(order.cart.items,function(val){
        products +=` 
            <tr height="40">
                <td style="border-bottom:1px solid #b9b9b9; vertical-align:middle; padding:15px;">
                    <h4 style="font-size:15px; line-height:24px; color:#000; font-weight:bold; margin:0;">${Products.findOne(val.id).title}</h4>
                </td>
                <td align="center" style="border-bottom:1px solid #b9b9b9;">${val.count}</td>
                <td align="center" style="border-bottom:1px solid #b9b9b9;">Rs ${Products.findOne(val.id).price}</td>
            </tr>`
    })
var html =
`
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Wingreen</title>
    </head>
    <body style="background-color:#eaebf4; padding:0; margin:0;">
        <table width="600" cellpadding="0" cellspacing="0" style="background-color: #fff; border:1px solid #dddddd; margin:0 auto; font-size:12px; padding:0; font-family:Arial, Helvetica, sans-serif;">
            <tbody>
                <tr>
                    <td height="4" bgcolor="#7bb100"></td>
                </tr>
                <tr>
                    <td height="51" style="padding:5px 15px; border-bottom:1px solid #c1c1c1;"><a href="index.html"><img src="http://www.wingreensfarms.com/client/images/wingreen_logo.png" width="177" height="80" alt="wingreen Logo"></a></td>
                </tr>
                <tr>
                    <td>
                        <h5 style="font-size:15px; line-height:24px; padding:15px 15px 0 15px; margin:5px 0; color:#7bb100; font-weight:bold; text-transform:uppercase;">Dear ${user.name},</h5>
                        <p style="font-size:15px; line-height:24px; padding:0 15px; color:#232323;">Thank you for your purchase at <a href="https://www.wingreensfarms.com">wingreensfarms.com</a>
                        </p>
                        <p style="font-size:15px; line-height:24px; padding:0 15px; color:#232323; margin-bottom:30px;">${status_text}</p>
                    </td>
                </tr>
                <tr>
                    <td style="padding:0 15px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#f4f4f4; border:0px solid #ccc; font-size:13px;">
                            <tbody>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
                                            <tbody>
                                                <tr height="40" style="background-color:#6f9812; color:#fff;">
                                                    <td style="border-bottom:1px solid #b9b9b9; padding:0 15px;">Product</td>
                                                    <td align="center" style="text-transform:uppercase; border-bottom:1px solid #b9b9b9; padding:0 15px;">Qty</td>
                                                    <td width="40%" align="center" style="text-transform:uppercase; border-bottom:1px solid #b9b9b9; padding:0 15px;">Price</td>
                                                </tr>
                                                ${products}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#f4f4f4; margin:0px auto; font-size:13px; padding:0 15px;">
                                            <tbody>
                                                <tr height="100">
                                                    <td style="">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="11%" style="border-bottom:1px solid #b9b9b9;"></td>
                                                                    <td width="11%" style="border-bottom:1px solid #b9b9b9;"></td>
                                                                    <td width="11%" style="border-bottom:1px solid #b9b9b9;"></td>
                                                                    <td width="12%" style="border-bottom:1px solid #b9b9b9;"></td>
                                                                    <td align="right" width="32%" style="border-bottom:1px solid #b9b9b9; line-height:30px;">
                                                                        Sub Total<br>
                                                                        Shipping<br>
                                                                        Discount 
                                                                    </td>
                                                                    <td align="center" width="13%" style="line-height:30px; border-bottom:1px solid #b9b9b9;">
                                                                        Rs ${order.subtotal}<br>
                                                                        Rs ${order.ship}<br>
                                                                        Rs ${order.discount}
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr height="60">
                                                    <td>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr width="100%">
                                                                    <td width="60%">
                                                                        <p style="font-size:13px; line-height:16px; color:#232323; margin:0; padding:15px 0; width:100%;"><strong style=" line-height:30px;">Shipping Address</strong><br>
                                                                            ${order.address.address}<br>
                                                                            ${order.address.city},<br>
                                                                            ${order.address.state}<br>
                                                                            Pincode:- ${order.address.zipcode}
                                                                        </p>
                                                                    </td>
                                                                    <td width="30%" style="font-size:15px; line-height:24px; color:#232323; text-transform:uppercase;">Total <span style="font-size:19px; line-height:24px; padding:0 15px; margin:20px 0; color:#000; font-weight:bold;">Rs ${order.total}</span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding-top:30px;">
                        <p> <a href="https://twitter.com/WingreensFarms"><img src="http://www.wingreensfarms.com/client/images/tw_icon.png"></a> <a href="https://www.facebook.com/wingreens/"><img src="http://www.wingreensfarms.com/client/images/fb_icon.png"></a> <a href="https://www.instagram.com/wingreensfarms/"><img src="http://www.wingreensfarms.com/client/images/insta_icon.png"></a> </p>
                        <p>For Enquiry: <strong>(0) 88007 93642</strong> | <a href="mailto:contact@wingreensfarms.com" style="color:#7bb100; text-decoration:underline;">contact@wingreensfarms.com</a></p>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>`
return html;
}
Accounts.onCreateUser(function(options, user) {
    if (user.services) {
        var service = _.keys(user.services)[0];
        var email = user.services[service].email;
        if (!user.profile){
            user.profile = {}
        }
        if(service == 'google' || service == 'facebook'){
            user["emails"] = [];
            user["emails"].push({address : email,verified : false});
            user.profile["name"] = options.profile.name;
            user["roles"] = [];
            user["roles"].push('user');
        }
        if (!email)
            return user;

        var existingUser = Meteor.users.findOne({'emails.address': email});
        if (!existingUser)
            return user;

        // precaution, these will exist from accounts-password if used
        if (!existingUser.services)
            existingUser.services = { resume: { loginTokens: [] }};
        if (!existingUser.services.resume)
            existingUser.services.resume = { loginTokens: [] };

        // copy across new service info
        existingUser.services[service] = user.services[service];
        existingUser.profile["name"] = options.profile.name;
        //console.log(user.services.resume.loginTokens[0])
        //existingUser.services.resume.loginTokens.push(user.services.resume.loginTokens[0]);

        // even worse hackery
        Meteor.users.remove({_id: existingUser._id}); // remove existing record
        return existingUser;                          // record is re-inserted
    }
});

Meteor.publish(null,() => {
    return Meteor.roles.find({})
})


Meteor.methods({
    'createAccountJson' : function(){
        var data = [{"name":"Vipul Sharma","phone":"8800793642","email":"vipulsharma.9229@gmail.com"},
        {"name":"j j","phone":"8373900448","email":"jaydjordan@yahoo.com"},
        {"name":"Vikramaditya Chaudhri","phone":"9560540051","email":"vikramadityac@gmail.com"},
        {"name":"Anju C Srivastava","phone":"919871672038","email":"anju@wingreens.in"},
        {"name":"Arpita Sen","phone":"9999053682","email":"senj82@gmail.com"},
        {"name":"mrjones3k","phone":"","email":"mrjones3k@yahoo.com"},
        {"name":"vikram","phone":"9810115847","email":"vikram@wingreens.in"},
        {"name":"Hitesh Gakhreja","phone":"9953467069","email":"hiteshgakhreja1234@gmail.com"},
        {"name":"Simar","phone":"9654989733","email":"punihani.simar@gmail.com"},
        {"name":"Dr Roopa","phone":"9845194372","email":"roop_76@yahoo.com"},
        {"name":"Vivek Prakash","phone":"7042169988","email":"vivek604@gmail.com"},
        {"name":"ghanshyam","phone":"","email":"ghanshyam@zepo.in"},
        {"name":"Vibha Paul Rishi","phone":"9650920779","email":"Vibhapaul.rishi@gmail.com"},
        {"name":"Mrinalini Mirchandani","phone":"9820606634","email":"mrinalini.mirchandani@gmail.com"},
        {"name":"Archish Kapoor","phone":"09769241020","email":"archish15@gmail.com"},
        {"name":"A Joosten","phone":"+911244069950","email":"a-joosten@hotmail.com"},
        {"name":"Navin","phone":"09949055202","email":"navin1506@gmail.com"},
        {"name":"skjsrhgfsakjegfwa","phone":"99887763748","email":"vipulsharma9229@yahoo.com"},
        {"name":"Simar","phone":"9654989733","email":"spunihani@hotmail.com"},
        {"name":"sumeet","phone":"","email":"sumeet@zepo.in"},
        {"name":"sumeet","phone":"","email":"sumeet@bijness.in"},
        {"name":"reachmonica","phone":"","email":"reachmonica@gmail.com"},
        {"name":"babin.tdr","phone":"","email":"babin.tdr@gmail.com"},
        {"name":"sumeet.gadodia","phone":"","email":"sumeet.gadodia@gmail.com"},
        {"name":"Sabina pathak","phone":"9717117413","email":"rasda25@gmail.com"},
        {"name":"vipul","phone":"+918800793642","email":"vipul.sharma@wingreens.in"},
        {"name":"Vishal Verma","phone":"9930494219","email":"vvrockstar08@hotmail.com"},
        {"name":"aayush wasu","phone":"01352744999","email":"freshneasydehradun@gmail.com"},
        {"name":"Eesh kakkar","phone":"9167313135","email":"eesh.kakkar@gmail.com"},
        {"name":"Fyrd","phone":"268882","email":"Abc@gmail.com"},
        {"name":"Trisha Chaudhary ","phone":"7838477703","email":"trishac701@gmail.com"},
        {"name":"Dr sugandh","phone":"9650322077","email":"sugandhg82@yahoo.com"},
        {"name":"Kathakoli Dasgupta","phone":"9811312347","email":"kathakoli.dasgupta@gmail.com"},
        {"name":"Sridhar Vijendra","phone":"9686744005","email":"rahdirsv@yahoo.com"},
        {"name":"Parthasengupta15","phone":"","email":"Parthasengupta15@gmail.com"},
        {"name":"TSHEGYAL TASHI","phone":"8588992544","email":"tanishajain221@gmail.com"},
        {"name":"musicalogy7","phone":"","email":"musicalogy7@gmail.com"},
        {"name":"Aamana","phone":"","email":"aamana.singh@gmail.com"},
        {"name":"Shubh oswal","phone":"9876300400","email":"shubhoswal@gmail.com"},
        {"name":"Shagun Malhotra","phone":"9910880021","email":"shagun_uv@hotmail.com"},
        {"name":"Bhavna Vohra","phone":"8800793642","email":"Bhavna.Vohra@egonzehnder.com"},
        {"name":"Mickey Bardava","phone":"9829134431","email":"mickeybardava@gmail.com"},
        {"name":"Tannyverma","phone":"","email":"Tannyverma@hotmail.com"},
        {"name":"zakir nizam","phone":"9891540204","email":"zakirnnizam@yahoo.in"},
        {"name":"Harika Rudra","phone":"8861522993","email":"anilkumar.manchikatla@gmail.com"},
        {"name":"Kshitij","phone":"9619196628","email":"stuli1989@gmail.com"},
        {"name":"Sathya Peri","phone":"7032397980","email":"sathya.peri@gmail.com"},
        {"name":"Nitin Sumitran","phone":"","email":"nitinsumitran@hotmail.com"},
        {"name":"Lakshay","phone":"09025806394","email":"a.lakshayjain@gmail.com"},
        {"name":"Niteesh Sharma","phone":"9899895554","email":"niteesh26@yahoo.com"},
        {"name":"Sarah ahmad","phone":"02172341619","email":"sarah.chishti@gmail.com"},
        {"name":"Shilp Verma","phone":"9725503615","email":"shilpv@gmail.com"},
        {"name":"akriti goel","phone":"9811598838","email":"akriti_goel@hotmail.com"},
        {"name":"ashokchavhan.007","phone":"","email":"ashokchavhan.007@rediffmail.com"},
        {"name":"emmanuel","phone":"7548954964","email":"emmanuel97@gmail.com"},
        {"name":"ayush verma","phone":"8010019732","email":"ayushverma80@gmail.com"},
        {"name":"surabhi","phone":"9818898699","email":"surabhivohra@gmail.com"},
        {"name":"SuryaRajawat ","phone":"9828130303","email":"suryarajawat@gmail.com"},
        {"name":"Linda Johnson","phone":"","email":"lindia60@gmail.com"},
        {"name":"gaurav goel","phone":"9818520889","email":"gaur1987goel@gmail.com"},
        {"name":"Harsh Karki","phone":"9968066684","email":"hkarki078@gmail.com"},
        {"name":"Lopah Mudra Bhattacharrya","phone":"9820318469","email":"Lopamudra.b@gmail.com"},
        {"name":"Suparna Mohindra","phone":"9810069188","email":"suparna.mohindra@gmail.com"},
        {"name":"Shivya Nath","phone":"08527141626","email":"shivyanath@gmail.com"},
        {"name":"Sumit Rajliwal","phone":"9716816873","email":"sumitraj644@gmail.com"},
        {"name":"kehea","phone":"9873147840","email":"keshav.madhav@live.com"},
        {"name":"yuehhsin","phone":"","email":"yuehhsin@msn.com"},
        {"name":"nagavkerdavid","phone":"","email":"nagavkerdavid@yahoo.co.in"},
        {"name":"Rajiv Dhandia","phone":"9820033327","email":"dhandia1@gmail.com"},
        {"name":"dipankar21","phone":"","email":"dipankar21@gmail.com"},
        {"name":"Abhijit kulkarni","phone":"9177526280","email":"kingkong.ak@gmail.com"},
        {"name":"Manushi Parikh","phone":"9998054138","email":"manushi.parikh4@gmail.com"},
        {"name":"Shivam Srivastava","phone":"9999281931","email":"bravoshivam@gmail.com"},
        {"name":"Chitta Ranjan Sahu","phone":"9437228841","email":"thekillerx.sahu@gmail.com"},
        {"name":"Yashhraaj Barman","phone":"9724477011","email":"yashhraaj@yahoo.com"},
        {"name":"Puru Singh","phone":"9957787822","email":"aps.stormcatcher@gmail.com"},
        {"name":"mohammedsohail4","phone":"","email":"mohammedsohail4@yahoo.com"},
        {"name":"Kunal Kothari","phone":"9924000014","email":"leo1682@gmail.com"},
        {"name":"Bianca","phone":"","email":"biancavtoness@gmail.com"},
        {"name":"Alisha Madhok","phone":"9918401663","email":"Madhok.alisha@hotmail.com"},
        {"name":"Amarjit Singh","phone":"9855796175","email":"amardhiman39@gmail.com"},
        {"name":"vishwas kumar","phone":"9873737478","email":"vishuk_18@yahoo.co.in"},
        {"name":"VANDANA SAIGAL","phone":"9811023723","email":"vandana@aspirationsintl.com"},
        {"name":"shivi gupta","phone":"22873633","email":"gupta.shv@gmail.com"},
        {"name":"tushrnagalia","phone":"","email":"tushrnagalia@gmail.com"},
        {"name":"Tushar Nagalia","phone":"9818851515","email":"tusharnagalia@gmail.com"},
        {"name":"Seema Prasad","phone":"8105876074","email":"richa.prasad4@gmail.com"},
        {"name":"Neha chauhan","phone":"9923066669","email":"nehamanushanu@gmail.com"},
        {"name":"Khushali","phone":"9825868081","email":"Khush101suraria@yahoo.com"},
        {"name":"Umesh kumar Dotania","phone":"9999228807","email":"umesh.kumar1606@gmail.com"},
        {"name":"Prarthana khaitan","phone":"9687976994","email":"prarthana_khaitan@rediffmail.com"},
        {"name":"Shubhra","phone":"09423174608","email":"srkanaujia@gmail.com"},
        {"name":"mona","phone":"","email":"mbr1967@yaho.com"},
        {"name":"Piyush diwan","phone":"9899328360","email":"khanna.ridhima85@gmail.com"},
        {"name":"Sarah Chick","phone":"+919805072724","email":"sarahchick24@gmail.com"},
        {"name":"S Kapoor","phone":"9811050178","email":"spkapr@gmail.com"},
        {"name":"Anuj Gupta","phone":"9837005898","email":"anuj1973.ag@gmail.com"},
        {"name":"ddivya0721","phone":"","email":"ddivya0721@gmail.com"},
        {"name":"Manish Garg","phone":"9911435559","email":"manish@gargmanish.com"},
        {"name":"Saonli","phone":"","email":"saoni.choudhury@egonzehnder.com"},
        {"name":"Rhea Bhatia","phone":"","email":"rhea.bhatia@ymail.com"},
        {"name":"Abhinav","phone":"9560785009","email":"priyanka.goel13@gmail.com"},
        {"name":"PAVNI SHARMA","phone":"9643021721","email":"sharma.pavni@gmail.com"},
        {"name":"Lucsindelkova.lucie","phone":"","email":"Lucsindelkova.lucie@gmail.com"},
        {"name":"Benoit Couaillier","phone":"9871904488","email":"sindelkova.lucie@gmail.com"},
        {"name":"Ankita Joshi","phone":"9880528360","email":"ankita42002@gmail.com"},
        {"name":"Roopalisharma4","phone":"","email":"Roopalisharma4@gmail.com"},
        {"name":"Gaurav Malhotra","phone":"9810022340","email":"gmalhotra@pobox.com"},
        {"name":"Vaibhav Singh","phone":"8003990297","email":"vaibhavsingh@modernschool.ac.in"},
        {"name":"ketan dhruv","phone":"8000439210","email":"kketan658@gmail.com"},
        {"name":"nihit nakra","phone":"9611706902","email":"NIHIT28@GMAIL.COM"},
        {"name":"Archish Kapoor","phone":"919811553489","email":"archish_kapoor@yahoo.com"},
        {"name":"m.chakrapani","phone":"","email":"m.chakrapani@yahoo.co.in"},
        {"name":"shaunaq singh","phone":"9731038297","email":"shaunaqsingh@gmail.com"},
        {"name":"ADITI RAGHAVAN","phone":"9840255422","email":"aditiraghavan@Gmail.com"},
        {"name":"mridu.r","phone":"","email":"mridu.r@gmail.com"},
        {"name":"Pooja Bagrodia","phone":"9810174303","email":"p.bagrodia@gmail.com"},
        {"name":"Ronak Shah","phone":"9819021013","email":"ronak_123@hotmail.com"},
        {"name":"RITU DUBEY","phone":"9811204681","email":"ritu_dubey@hotmail.com"},
        {"name":"Bank of Baroda","phone":"9810115847","email":"praveensharmabob@gmail.com"},
        {"name":"test","phone":"9267476625","email":"yohan@zepo.in"},
        {"name":"Kavita Saraf","phone":"9374554474","email":"kavitasaraf@in.com"},
        {"name":"Sudha Maheshwari","phone":"9999318181","email":"sudhais@gmail.com"},
        {"name":"Vector Marketing","phone":"9717127676","email":"mamtachadha100@gmail.com"},
        {"name":"Abhinav Bathula","phone":"09969061942","email":"abhinav4598@gmail.com"},
        {"name":"Gautam Aggarwal","phone":"9873654320","email":"gautam235@gmail.com"},
        {"name":"Ankur Ecoste ","phone":"9643331140","email":"info@ecoste.in"},
        {"name":"Suparna Mohindra","phone":"01244638000","email":"suparna.mohindra@egonzehnder.com"},
        {"name":"Sudeshna Batra","phone":"9811478911","email":"sudeshna.batra@gmail.com"},
        {"name":"Deepali Gupta","phone":"9873687062","email":"deepaligupta18@gmail.com"},
        {"name":"Anuradha Bhavnani - Shell India Private Limited","phone":"8800298456","email":"anuradha.bhavnani@shell.com"},
        {"name":"Aarti Joshi","phone":"9810884596","email":"aartijos@gmail.com"},
        {"name":"Pooja Mahajan","phone":"9871902740","email":"pooja.mahajan9@gmail.com"},
        {"name":"Sakshi Malik","phone":"9811807215","email":"sakshiikalra@hotmail.com"},
        {"name":"Ineke Bezembinder","phone":"9818346771","email":"ineke@womenonwings.com"},
        {"name":"amararpharma","phone":"","email":"amararpharma@yahoo.co.in"},
        {"name":"Amita Mohan","phone":"9810146834","email":"amitascollections@gmail.com"},
        {"name":"Dr Prodipta Sen","phone":"9810041063","email":"ps@alphagcorp.com"},
        {"name":"Mridhu malhotra","phone":"9999281133","email":"mridhu.malhotra@gmail.com"},
        {"name":"Ishan Guliani","phone":"9811011881","email":"ishanguliani@hotmail.com"},
        {"name":"Shivika suri","phone":"9953337387","email":"Shivikaasuri@ymail.com"},
        {"name":"Ramashankar Priyadarshi","phone":"919711161249","email":"r_priyadarshi@in.com"},
        {"name":"Lalit Khanna","phone":"9811413657","email":"lkhanna@me.com"},
        {"name":"shivika8","phone":"","email":"shivika8@yahoo.co.in"},
        {"name":"Devbrat Goenka","phone":"9836000035","email":"devbrat.goenka@gmail.com"},
        {"name":"Vicky Rohl","phone":"9999069367","email":"vicky1rohl@gmail.com"},
        {"name":"ANUPAMA PUROHIT","phone":"9899102441","email":"anupamapurohit@hotmail.com"},
        {"name":"Koanrk Chhabra","phone":"9810822340","email":"ginu21@gmail.com"},
        {"name":"Shashi Bahl","phone":"9560707999","email":"sbahl@kpmg.com"},
        {"name":"Mukul Sharma","phone":"9958111320","email":"mukulsharma_ddn@yahoo.com"},
        {"name":"Siddhartha Datta","phone":"8800536016","email":"siddatta@hotmail.com"},
        {"name":"Naseem  Balsara ","phone":"9825943877","email":"balsaranaseem14@gmail.com"},
        {"name":"Chandhiok and Associates","phone":"9811077115","email":"karan.chandhiok@chandhiok.com"},
        {"name":"Daksh Trivedi","phone":"9899738403","email":"daksht@gmail.com"},
        {"name":"Priyanka tiwari","phone":"9910277713","email":"priyanka_90@yahoo.com"},
        {"name":"preet singh","phone":"9873336617","email":"preetsinghleo26sheru@gmail.com"},
        {"name":"dhariwalvaibhav","phone":"","email":"dhariwalvaibhav@gmail.com"},
        {"name":"Siddharth Sethi","phone":"7032920614","email":"siddharth.sethi12@gmail.com"},
        {"name":"Monica Godinho","phone":"9811899928","email":"monica.godinho@egonzehnder.com"},
        {"name":"tanya_1590","phone":"","email":"tanya_1590@yahoo.com"},
        {"name":"PRIME MARKETING","phone":"09825110074","email":"shahisupari@rediffmail.com"},
        {"name":"saloni sahaya","phone":"9988990605","email":"salonisahaya@gmail.com"},
        {"name":"Shaily Saluja","phone":"9717056789","email":"shailysaluja1@gmail.com"},
        {"name":"sanyam agarwal","phone":"9935429441","email":"sanyam_ag@yahoo.co.in"},
        {"name":"suhasini1952","phone":"","email":"suhasini1952@gmail.com"},
        {"name":"kuranifamily","phone":"","email":"kuranifamily@gmail.com"},
        {"name":"Swatantra Gautam","phone":"9811799691","email":"chefswatantra@gmail.com"},
        {"name":"Rohit","phone":"9811433442","email":"sareen.rohit@hotmail.com"},
        {"name":"Ravi Narula","phone":"9818678124","email":"narulr@yahoo.com"},
        {"name":"soma surana","phone":"8130432999","email":"soma4july@gmail.com"},
        {"name":"Karthik Chandrasekar","phone":"9873775366","email":"karthik@chandrasekar.com"},
        {"name":"Nanhi Singh","phone":"9818504062","email":"Nksawhney@gmail.com"},
        {"name":"Sivakumar.harini","phone":"","email":"Sivakumar.harini@gmail.com"},
        {"name":"G SENTHILKUMAR","phone":"8883969646","email":"gchitra98@gmail.com"},
        {"name":"Hitesh Jain","phone":"8053777066","email":"hitesh.jain1988@gmail.com"},
        {"name":"agl.neha","phone":"","email":"agl.neha@gmail.com"},
        {"name":"vaibhav suranaa","phone":"8130432999","email":"vaibhus@gmail.com"},
        {"name":"garodia.rohit","phone":"","email":"garodia.rohit@gmail.com"},
        {"name":"Deepak","phone":"9940190572","email":"resumeservices4all@gmail.com"},
        {"name":"RRN","phone":"8860406189","email":"satn.paperfly666@gmail.com"},
        {"name":"Manish Gupta","phone":"9919501378","email":"manish_genesis@yahoo.com"},
        {"name":"Nandita Mehta","phone":"9971077599","email":"nandita.nikki@gmail.com"},
        {"name":"Santhosh","phone":"8552988899","email":"keshwarsan@gmail.com"},
        {"name":"Andreson Pang Tze Schen","phone":"9611921073","email":"andre_warvec@yahoo.com"},
        {"name":"Amrita","phone":"9676260562","email":"amritagngly@yahoo.co.in"},
        {"name":"priya sawhney","phone":"9818493981","email":"priyasawhney@gmail.com"},
        {"name":"Varun kalra","phone":"9896040037","email":"varunkalra37@yahoo.co.in"},
        {"name":"K Hashwitha Reddy","phone":"09866933600","email":"hashwitha@janapriya.com"},
        {"name":"Chaitanya","phone":"8008779976","email":"Chaitanyareddy1315@gmail.com"},
        {"name":"payal  agarwal  ","phone":"9831638336","email":"payal@jmddiagnstics.in"},
        {"name":"Keerti","phone":"9885048817","email":"d.keerti@gmail.com"},
        {"name":"mohi shukla","phone":"9743068618","email":"mohishukla.92@gmail.com"},
        {"name":"Sushil","phone":"9841728338","email":"sushil24us@yahoo.com"},
        {"name":"Smrithi Talwar","phone":"9818001082","email":"smrithi_talwar@hotmail.com"},
        {"name":"Natasha yadav","phone":"9662122333","email":"Narasha_gyl@yahoo.co.uk"},
        {"name":"Eshani","phone":"9717952250","email":"esh_k@hotmail.com"},
        {"name":"sirisha","phone":"8971955559","email":"sirisnowywhite@yahoo.co.in"},
        {"name":"Bidisha Bhattacharya","phone":"9836088003","email":"bidishabhattacharya90@gmail.com"},
        {"name":"Saumya","phone":"9648888892","email":"Saumya.princess23@gmail.com"},
        {"name":"kanchan.bhatia","phone":"","email":"kanchan.bhatia@gmail.com"},
        {"name":"khooshrow","phone":"","email":"khooshrow@gmail.com"},
        {"name":"Kamlesh ","phone":"9221602477","email":"powerman27@rediff.com"},
        {"name":"amrita ghulati","phone":"9811470179","email":"amritaghulati@yahoo.co.in"},
        {"name":"fhb","phone":"919866191127","email":"Sagi.rishi97@gmail.com"},
        {"name":"shah.nayan","phone":"","email":"shah.nayan@hotmail.com"},
        {"name":"Dinesh j naidu","phone":"9677058600","email":"Djanarthanan2911@gmail.com"},
        {"name":"gorgvc","phone":"","email":"gorgvc@gmail.com"},
        {"name":"Ashwin Coutinho","phone":"9011670780","email":"coutinho7ashwin@gmail.com"},
        {"name":"Dr J S Kohli","phone":"9872687200","email":"drjskohli@yahoo.co.in"},
        {"name":"sjsingh86","phone":"","email":"sjsingh86@hotmail.com"},
        {"name":"nimishha","phone":"","email":"nimishha@gmail.com"},
        {"name":"dhruv.sn","phone":"","email":"dhruv.sn@facebook.com"},
        {"name":"kav2310","phone":"","email":"kav2310@gmail.com"},
        {"name":"Raghav Maheshwari","phone":"9818988218","email":"rock_raghav@yahoo.com"},
        {"name":"SANJOY GHOSH","phone":"9949482972","email":"ghoshsanjoy@gmail.com"},
        {"name":"Sanjukta Ray","phone":"9811599139","email":"raysanjukta@yahoo.com"},
        {"name":"sehraj singh","phone":"9873711968","email":"sehrajsingh@barkatfab.com"},
        {"name":"Vasundhra Jain","phone":"9910087608","email":"Vasundhra.j@gmail.com"},
        {"name":"Rachna Yadalam","phone":"7892054999","email":"rdundoo@hotmail.com"},
        {"name":"Ayushi Ganotra","phone":"9899814771","email":"ayushi.ganotra@gmail.com"},
        {"name":"Chandana poddar","phone":"9831153111","email":"c.poddar@hotmail.com"},
        {"name":"Arika Chopra","phone":"9899555152","email":"arikachopra86@yahoo.co.in"},
        {"name":"Nagendra D Pai","phone":"9844043030","email":"nagendrapai@yahoo.com"},
        {"name":"Tanyaakhurana","phone":"","email":"Tanyaakhurana@gmail.com"},
        {"name":"Sidharth Dassani","phone":"9866045755","email":"sdassani@gmail.com"},
        {"name":"pritikabhrgv2","phone":"","email":"pritikabhrgv2@gmail.com"},
        {"name":"Kanika khanna","phone":"9810931705","email":"supagreat@gmail.com"},
        {"name":"Mona Arthur","phone":"9818210794","email":"arthurmona@gmail.com"},
        {"name":"SINDHU CHANDRA","phone":"8500091910","email":"raju.edla@gmail.com"},
        {"name":"karishma jain ","phone":"9885544402","email":"karishjain@gmail.com"},
        {"name":"Himanshu Gupta","phone":"919996022260","email":"himanshu.hsr@gmail.com"},
        {"name":"Servesh Sharma","phone":"9829038200","email":"servesharma@yahoo.co.in"},
        {"name":"Yashwin S","phone":"9004935883","email":"yashwinss@gmail.com"},
        {"name":"Nirmala Dundoo","phone":"7799011345","email":"ndundoo@gmail.com"},
        {"name":"Aamird95","phone":"","email":"Aamird95@gmail.com"},
        {"name":"Vartika Bhartiya","phone":"9650992114","email":"chandansax294@gmail.com"},
        {"name":"s1.kothari","phone":"","email":"s1.kothari@yahoo.com"},
        {"name":"Ankit Anand","phone":"8800478064","email":"ankitanand25@gmail.com"},
        {"name":"Gerhard Schanz","phone":"8527481343","email":"gerhard-schanz@t-online.de"},
        {"name":"sonal sharma","phone":"7738874286","email":"sharma.sonal731@gmail.com"},
        {"name":"sml401tp","phone":"","email":"sml401tp@gmail.com"},
        {"name":"vaibhav surana","phone":"8130432999","email":"sml401tp@gmail.con"},
        {"name":"Saravanan N","phone":"9742687136","email":"snmail555@gmail.com"},
        {"name":"niteshji","phone":"","email":"niteshji@outlook.com"},
        {"name":"Basina Srinivasa Rao","phone":"9717795067","email":"basinasriniwas@yahoo.com"},
        {"name":"david nagavker","phone":"23745381","email":"nagavkerdavid@rediffmail.com"},
        {"name":"hriday","phone":"9582229167","email":"hriday7@gmail.com"},
        {"name":"selvamseer","phone":"","email":"selvamseer@gmail.com"},
        {"name":"rituraj055","phone":"","email":"rituraj055@gmail.com"},
        {"name":"poonam","phone":"9650366218","email":"poonamkh86@gmail.com"},
        {"name":"Chetna Singhal","phone":"7738311148","email":"livestrong19@gmail.com"},
        {"name":"Rachit Bansal","phone":"9811810800","email":"rachitbansal@live.com"},
        {"name":"Krishnan Manoharan S R","phone":"9036091958","email":"sophia.krishnan@gmail.com"},
        {"name":"srikant agrawal","phone":"9818386737","email":"srikantagrawal@hotmail.com"},
        {"name":"ankushkaushal23455","phone":"","email":"ankushkaushal23455@gmail.com"},
        {"name":"Shrinivasan Ganesh","phone":"9714800344","email":"shri.bru.in@gmail.com"},
        {"name":"Aditya","phone":"9910305697","email":"anayan@savelifefoundation.org"},
        {"name":"Rajat Bhalla","phone":"9999270572","email":"rajat84bhalla@gmail.com"},
        {"name":"Pria Devi","phone":"9911178363","email":"priadevi@gmail.com"},
        {"name":"vimoha bagla","phone":"9910559183","email":"vimoha@gmail.com"},
        {"name":"Anuraag Goyal","phone":"9416044535","email":"anurag.goel1@gmail.com"},
        {"name":"Surbhi gupta ","phone":"9810065580","email":"sur.gupta@gmail.com"},
        {"name":"Anuj Mathur","phone":"9686190830","email":"anujmathurhere@gmail.com"},
        {"name":"anupa","phone":"+918800329500","email":"anupa.r23@gmail.com"},
        {"name":"Bala Ramachandran","phone":"9177222670","email":"baladude@hotmail.com"},
        {"name":"SANDEEP THAKUR","phone":"09915855783","email":"sandeep.thakur.advocate@gmail.com"},
        {"name":"Natasha ","phone":"9662122333","email":"natasha_gyl@yahoo.co.uk"},
        {"name":"Vidushi kanoria","phone":"09860922206","email":"Vidushikanoria@gmail.com"},
        {"name":"niharika chawla","phone":"9953037167","email":"niharika162@gmail.com"},
        {"name":"harsha","phone":"9953442788","email":"harshita.usa@gmail.com"},
        {"name":"snehilsuman18","phone":"","email":"snehilsuman18@gmail.com"},
        {"name":"nipun handa","phone":"9910776463","email":"nipunhanda322@gmail.com"},
        {"name":"Sanovar Nair","phone":"9741125075","email":"sanovar2002@gmail.com"},
        {"name":"Vaibhav Gulia ","phone":"9818649297","email":"vaibhav2810@gmail.com"},
        {"name":"Himmat Singh","phone":"9810820414","email":"himmat.1989@gmail.com"},
        {"name":"Aparna Kejriwal","phone":"9839077355","email":"aparnakejriwal@outlook.com"},
        {"name":"Ahmed","phone":"9900217740","email":"Ahmed@samgypsum.com"},
        {"name":"priyanka sharma","phone":"09999399986","email":"priyankasharma20@gmail.com"},
        {"name":"Ambar Vaid","phone":"9810895820","email":"ambar.vaid@gmail.com"},
        {"name":"Tanya","phone":"7773032224","email":"Tanyaakhurana@hotmail.com"},
        {"name":"Sunil Toshniwal","phone":"9810058655","email":"sunil@toshniwaldelhi.com"},
        {"name":"Radhika Jain","phone":"+919994598744","email":"radhikaj@quibusresources.com"},
        {"name":"Gaurav Sharma","phone":"9871944755","email":"gaurav.cyril@gmail.com"},
        {"name":"Farhad Vania","phone":"9899110005","email":"fvelinks@gmail.com"},
        {"name":"Ritu Khura","phone":"9810095525","email":"Khurana_ritu@yahoo.com"},
        {"name":"ashutoshsatpathy_2007","phone":"","email":"ashutoshsatpathy_2007@rediffmail.com"},
        {"name":"mailmevijay1988","phone":"","email":"mailmevijay1988@gmail.com"},
        {"name":"M Pandy","phone":"09600720690","email":"adityapandian@gmail.com"},
        {"name":"Shruti jain","phone":"9654450410","email":"shrutjain84@gmail.com"},
        {"name":"Sidharth Dassani","phone":"9866045755","email":"dassani.sidharth@gmail.com"},
        {"name":"drmaliknidhi","phone":"","email":"drmaliknidhi@gmail.com"},
        {"name":"Rajinthebox","phone":"","email":"Rajinthebox@gmail.com"},
        {"name":"Namrata Goyal","phone":"7698387066","email":"mail.namratagoyal@gmail.com"},
        {"name":"Neha Srivastava","phone":"9807159078","email":"nehasrivastavaindian@gmail.com"},
        {"name":"nehasrivastavaindian","phone":"","email":"nehasrivastavaindian@gmail.com"},
        {"name":"anuj katiyar","phone":"9935035427","email":"anujkatiyar64@gmail.com"},
        {"name":"Pradeep Sedha","phone":"9821348200","email":"praad@ymail.com"},
        {"name":"amrita bakshi","phone":"9953777230","email":"amritabakshi92@gmail.com"},
        {"name":"Ashutosh Garg","phone":"9871030403","email":"perfectgarg@gmail.com"},
        {"name":"Haresh Gurnani","phone":"9821345555","email":"geetagurnani@gmail.com"},
        {"name":"karswam.1980","phone":"","email":"karswam.1980@gmail.com"},
        {"name":"aldinisultan","phone":"","email":"aldinisultan@gmail.com"},
        {"name":"Samruddhi MULYE","phone":"9833716925","email":"samruddhi.mulye@aavishkaar.in"},
        {"name":"anikasunda","phone":"","email":"anikasunda@gmail.com"},
        {"name":"Probodh Palit","phone":"9867640811","email":"ppalit@gmail.com"},
        {"name":"Ankur Jain","phone":"9810014153","email":"itsankur@outlook.com"},
        {"name":"pallavi srivastava","phone":"9811380477","email":"bunsandmuffins@gmail.com"},
        {"name":"Jagdish chhabra ","phone":"9826210301","email":"Jimmychhabra1@gmail.con"},
        {"name":"anas","phone":"8801308847","email":"anasasrar3@gmail.com"},
        {"name":"Vyshak Upadhya ","phone":"9980411411","email":"Vyshak@msn.com"},
        {"name":"Kanishk Mehta","phone":"9408703902","email":"kanishk.m7@gmail.com"},
        {"name":"Patel mihir","phone":"9624529888","email":"Chiragpatel2988@yahoo.com"},
        {"name":"Pavan Kumar","phone":"9959992050","email":"pavan.austin@gmail.com"},
        {"name":"Arjunkashyap66","phone":"","email":"Arjunkashyap66@gmail.com"},
        {"name":"ALOK PALIWAL","phone":"9811058130","email":"decor03@gmail.com"},
        {"name":"Vinati Kastia","phone":"+919810798315","email":"Vinati.kastia@azbpartners.com"},
        {"name":"Vinati Kastia","phone":"+919810798315","email":"Vinati.kastia@gmail.com"},
        {"name":"Nitika manjhu","phone":"9999727999","email":"Nitika_manjhu@yahoo.co.in"},
        {"name":"richa bishnoi","phone":"9873000029","email":"bishnoi4455@gmail.com"},
        {"name":"kunal chauhan","phone":"9650798678","email":"koolkunalchauhan18@gmail.com"},
        {"name":"Supriya Sinha","phone":"9810729280","email":"supriya.sinha@gmail.com"},
        {"name":"Ganesh raja","phone":"8870049928","email":"gachu07@gmail.com"},
        {"name":"Arindom Mondal","phone":"7501520478","email":"xyz.unwanted@gmail.com"},
        {"name":"shweta khanna","phone":"9873640254","email":"Shwetakh@live.com"},
        {"name":"majumder.moumita4","phone":"","email":"majumder.moumita4@gmail.com"},
        {"name":"Prerak Sethi","phone":"9711197066","email":"preraksethi@gmail.com"},
        {"name":"Raag Khandelwal","phone":"8852078697","email":"khandelwalraag@gmail.com"},
        {"name":"varsha sharma","phone":"8812942566","email":"varshukhandal@gmail.com"},
        {"name":"Raag Khandelwal","phone":"8852078697","email":"khandelwal.hyd@gmail.com"},
        {"name":"Vishal Desai","phone":"9920877790","email":"vishaldesai4@gmail.com"},
        {"name":"Lalit Sethia","phone":"9490323566","email":"lalit.89@live.com"},
        {"name":"hem143","phone":"","email":"hem143@gmail.com"},
        {"name":"Radhika singh","phone":"01244362280","email":"ramanika@hotmail.com"},
        {"name":"girijeshwarsingh","phone":"","email":"girijeshwarsingh@rediffmail.com"},
        {"name":"rajiv kakeri","phone":"900404533","email":"rgk22@rediffmail.com"},
        {"name":"Ashok Choudhury","phone":"9861310407","email":"ashok.oreda@gmail.com"},
        {"name":"bikram uberoi","phone":"9818294940","email":"bik5000@gmail.com"},
        {"name":"devkipatel13","phone":"","email":"devkipatel13@gmail.com"},
        {"name":"Shivani Anand","phone":"9805729089","email":"sanand1681@yahoo.co.in"},
        {"name":"puneet lakra","phone":"8802861309","email":"lakra.roky@yahoo.co.in"},
        {"name":"SOWMIYA","phone":"9788091200","email":"sowmiaarthi7@gmail.com"},
        {"name":"Anandita Bishnoi","phone":"9899100410","email":"ananditabishnoi@hotmail.com"},
        {"name":"Tobias Dorr","phone":"08860910878","email":"tfdorr@me.com"},
        {"name":"Sagar Chawla","phone":"9810229189","email":"sagar.chawla8@gmail.com"},
        {"name":"VENKATARAMADASK","phone":"9440156645","email":"vramadask2010@ymail.com"},
        {"name":"ankita shah","phone":"9049849666","email":"anki_babes89@yahoo.com"},
        {"name":"tahmeedhussain","phone":"","email":"tahmeedhussain@gmail.com"},
        {"name":"sumit","phone":"9716816873","email":"sumitrajliwal26@gmail.com"},
        {"name":"kukki bhatia","phone":"9336813280","email":"kukkibhatia@gmail.com"},
        {"name":"Suprin Ahluwalia","phone":"919873024966","email":"suprinahluwalia@gmail.com"},
        {"name":"Ananya Bhalla","phone":"9999869177","email":"ananyabhalla2@gmail.com"},
        {"name":"SUMI KIM","phone":"8052233377","email":"india_ren666@msn.com"},
        {"name":"mohita tandi","phone":"9953775821","email":"shikharias.mohita26@gmail.com"},
        {"name":"saurabh jain","phone":"9910772279","email":"saurabh.j9@gmail.com"},
        {"name":"dzfsdfsdfsd","phone":"","email":"dzfsdfsdfsd@dfdf.com"},
        {"name":"Divya Bhardwaj","phone":"9810403674","email":"divyabhardwaj2003@gmail.com"},
        {"name":"Ankush","phone":"9999958349","email":"Ankush.malhotra1992@gmail.com"},
        {"name":"Jay soni","phone":"9811067878","email":"Jay_dhanak@yahoo.com"},
        {"name":"reha khan","phone":"9220060115","email":"reha.khan545@gmail.com"},
        {"name":"amandp818","phone":"","email":"amandp818@gmail.com"},
        {"name":"urvi jani","phone":"9820727464","email":"u.sanghvi85@gmail.com"},
        {"name":"Prateek Goyal","phone":"9999991379","email":"prateekgoyal17@gmail.com"},
        {"name":"critica","phone":"9811328859","email":"critika_lunial@yahoo.com"},
        {"name":"Stanley","phone":"8451842484","email":"lobo.stanley@gmail.com"},
        {"name":"hem143","phone":"","email":"karthickvelumani@gmail.com"},
        {"name":"Mohit Tomar","phone":"8095909555","email":"apaar.mahajan@oba.co.uk"},
        {"name":"Kaahish goel","phone":"9999419076","email":"goel.kashish71@rocketmail.com"},
        {"name":"Nidhi Chanana","phone":"9779900568","email":"nidhichanana@ymail.com"},
        {"name":"Pooja Khemani ","phone":"+919970701303","email":"khemani.p@gmail.com"},
        {"name":"Jonathan Jacob","phone":"9845163623","email":"jonathan@presidency.com"},
        {"name":"dshobhalatha","phone":"","email":"dshobhalatha@yahoo.com"},
        {"name":"SARAH LEE","phone":"7829608485","email":"sarah.lee@live.com.my"},
        {"name":"khyati chaddha","phone":"8800440202","email":"midas.intl@rediffmail.com"},
        {"name":"Mahima kumar","phone":"9711343286","email":"Mahimakumar95@gmail.com"},
        {"name":"vermaarchna","phone":"","email":"vermaarchna@gmail.com"},
        {"name":"Nandita Dasgupta","phone":"8335001191","email":"avdasgupta@yahoo.com"},
        {"name":"anupama_kanakpura","phone":"","email":"anupama_kanakpura@yahoo.com"},
        {"name":"grajhasekar","phone":"","email":"grajhasekar@gmail.com"},
        {"name":"Ritu Dhanuka","phone":"09910071181","email":"knowritz@gmail.com"},
        {"name":"Jay Vaishnav","phone":"9811614113","email":"jay.vaishnav@gmail.com"},
        {"name":"Srishti Kapoor","phone":"9833756760","email":"kapoor.srishti40@gmail.com"},
        {"name":"yukta anand","phone":"9999304427","email":"yuktaanand@yahoo.co.in"},
        {"name":"kunal bahri ","phone":"9810456780","email":"swati308@gmail.com"},
        {"name":"Aashana kochar ","phone":"9619147361","email":"aashanapatawari@yahoo.in"},
        {"name":"Isha Gupta","phone":"9811865233","email":"isha_gupta93@hotmail.com"},
        {"name":"Nidhi Jha","phone":"7838210806","email":"nidhi23aug@gmail.com"},
        {"name":"Markus Meier","phone":"9920013810","email":"mjmeier@gmx.net"},
        {"name":"Prachi Jindal","phone":"9711757555","email":"prachi.gibs@gmail.com"},
        {"name":"ANIRUDH JINDAL","phone":"9711767666","email":"vc.gibs@gmail.com"},
        {"name":"vikas mittal","phone":"8447369858","email":"vikas.mittal10010@gmail.com"},
        {"name":"Manya Malhotra","phone":"9971559966","email":"manya.malh@gmail.com"},
        {"name":"Manav Khanna","phone":"9811508040","email":"manavkhanna@hotmail.com"},
        {"name":"Gyanu Mishra","phone":"9873746767","email":"gyanu_m@yahoo.com"},
        {"name":"piyush vyas","phone":"9377593855","email":"khushee999@gmail.com"},
        {"name":"P.kadam1","phone":"","email":"P.kadam1@gmail.com"},
        {"name":"Jayesh Patel","phone":"8888888888","email":"jaypatel@gmail.com"},
        {"name":"jha.arsh3","phone":"","email":"jha.arsh3@gmail.com"},
        {"name":"Krishna  Kishore","phone":"9581477634","email":"vangavolu.kanna@gmail.com"},
        {"name":"charmaine paul","phone":"09431353323","email":"paulcharmaine@gmail.com"},
        {"name":"Sahil Siddiqui","phone":"9810066176","email":"Sahil.sid.qui@gmail.com"},
        {"name":"saral b","phone":"9873646070","email":"saral.bhatnagar@gmail.com"},
        {"name":"Kishan Shetty","phone":"9820004277","email":"kishanshetty@hotmail.com"},
        {"name":"tdhairya","phone":"","email":"tdhairya@gmail.com"},
        {"name":"Tasneem Saiyed","phone":"9703298292","email":"tas_saiyed@yahoo.com"},
        {"name":"ishita luthra","phone":"8879041227","email":"ishitaluthra@gmail.com"},
        {"name":"Test","phone":"45677878889","email":"xya@gmail.com"},
        {"name":"KMP","phone":"7845122356","email":"AMZ@GMAIL.COM"},
        {"name":"Saurav Kankaria","phone":"+919831247999","email":"saurav@mkgroupindia.biz"},
        {"name":"G  Sanddep Reddy","phone":"9290231336","email":"gsandeep1711@gmail.com"},
        {"name":"sunanda","phone":"9829168790","email":"sunanda@talcindia.com"},
        {"name":"Gurmiit Grover","phone":"9759000019","email":"marketingexpresshrd@gmail.com"},
        {"name":"Sanya Jain","phone":"9650263625","email":"sannyajain@gmail.com"},
        {"name":"jayachaturvedi.nls","phone":"","email":"jayachaturvedi.nls@gmail.com"},
        {"name":"Anish Beri","phone":"9899472617","email":"anishberi@yahoo.com"},
        {"name":"amitluthra69","phone":"","email":"amitluthra69@gmail.com"},
        {"name":"Parth Ojha","phone":"9871096363","email":"parth10@gmail.com"},
        {"name":"mani.tasha","phone":"","email":"mani.tasha@gmail.com"},
        {"name":"Rahat Dhawan","phone":"9878277913","email":"rahatdhawan1993@gmail.com"},
        {"name":"Rahul Das","phone":"9739765009","email":"voidr2win@gmail.com"},
        {"name":"Courtney Al Moreno","phone":"9582380648","email":"calmoreno@aes.ac.in"},
        {"name":"chefmanojnagi","phone":"","email":"chefmanojnagi@live.com"},
        {"name":"varun vasant","phone":"9821376861","email":"varunmvasant@gmail.com"},
        {"name":"Gaurav","phone":"9871636717","email":"gaurav11.malik@yahoo.com"},
        {"name":"MANISHA SHAH","phone":"9873097200","email":"dr_mshah@yahoo.in"},
        {"name":"sudhanshu_goel","phone":"","email":"sudhanshu_goel@outlook.com"},
        {"name":"Amruta Ghanekar","phone":"9930783633","email":"ghanekar.amruta@gmail.com"},
        {"name":"Sophia Opitz","phone":"+918800395676","email":"e_doo11@hotmail.com"},
        {"name":"keshwani12","phone":"","email":"keshwani12@gmail.com"},
        {"name":"Rajender Kumar Jain","phone":"9810025990","email":"paperpulp4@gmail.com"},
        {"name":"tanvi sonawane","phone":"919168136333","email":"tanvi2017@gmail.com"},
        {"name":"jabish","phone":"9871866090","email":"jabish.gohlyan1@gmail.com"},
        {"name":"Ritambhara Chaturvedi","phone":"9533906633","email":"ritambharasona@gmail.com"},
        {"name":"Gunmeen Kohli","phone":"09821878015","email":"kohligunmeen@gmail.com"},
        {"name":"Vidur Bhatia","phone":"9899951341","email":"singhalisha1989@gmail.com"},
        {"name":"nikeshbiswal1","phone":"","email":"nikeshbiswal1@gmail.com"},
        {"name":"Pradyumna Kejriwal","phone":"9919977444","email":"pradyumnakejriwal@outlook.com"},
        {"name":"Ashit Parikh","phone":"09824024094","email":"ashitp@yahoo.com"},
        {"name":"AMARENDRA MULYE","phone":"9821152080","email":"amarendramulye@rediffmail.com"},
        {"name":"asdas","phone":"","email":"asdas@sadrsf.com"},
        {"name":"Anjana Pant","phone":"9822208718","email":"amrita.pant@gmail.com"},
        {"name":"kediastuti","phone":"","email":"kediastuti@hotmail.com"},
        {"name":"Sargam Bhatia","phone":"9892611550","email":"sargamb11@hotmail.com"},
        {"name":"Snehal Khemani","phone":"09833206884","email":"snehal_khemani@hotmail.com"},
        {"name":"gopinath mahanti","phone":"+919583050015","email":"gopimahanti13@rediffmail.com"},
        {"name":"vunu.pulapaka","phone":"","email":"vunu.pulapaka@gmail.com"},
        {"name":"hemant patel","phone":"9825042336","email":"hemantmri@gmail.com"},
        {"name":"Satyam Dave","phone":"09925553034","email":"satyam.k.dave@gmail.com"},
        {"name":"Bhaskar Varun Prasad K","phone":"9899853965","email":"varunvp.c1@gmail.com"},
        {"name":"chombel tsering","phone":"+919717650934","email":"chombel4@gmail.com"},
        {"name":"Aadhya","phone":"9910172792","email":"aadhyaaggarwal09@gmail.com"},
        {"name":"Arun Narayanan","phone":"9980022446","email":"ren.narayanan@gmail.com"},
        {"name":"ruchi","phone":"09778718320","email":"msinghruchi@gmail.com"},
        {"name":"moses.allison","phone":"","email":"moses.allison@gmail.com"},
        {"name":"malvika","phone":"09873020800","email":"malvikagrover@gmail.com"},
        {"name":"rakesh.khapre","phone":"","email":"rakesh.khapre@gmail.com"},
        {"name":"Bakul Kampani","phone":"9811181675","email":"kampani.bakul@gmail.com"},
        {"name":"Purva Gahlot","phone":"8800556775","email":"purva.gahlot@rediffmail.com"},
        {"name":"Nishi Shah","phone":"9825464846","email":"nishishah9@gmail.com"},
        {"name":"Dhaval Mudgal","phone":"09810105735","email":"dhavalmudgal@gmail.com"},
        {"name":"Lvanika","phone":"09899980116","email":"lvanika@gmail.com"},
        {"name":"Jayanthi","phone":"9739950077","email":"jayanthi.srini@gmail.com"},
        {"name":"Ishita Mathur","phone":"8698202406","email":"mathur_ishita@hotmail.com"},
        {"name":"Gitanjali","phone":"9019401531","email":"gitanjali.made@gmail.com"},
        {"name":"Dinesh Sharma","phone":"9871652441","email":"erdsharma@gmail.com"},
        {"name":"Niladri Basu","phone":"07838660427","email":"hi.niladri@gmail.com"},
        {"name":"Atul Unadkat","phone":"9978937977","email":"vishasuchde@gmail.com"},
        {"name":"Shruti Sanghani","phone":"9611122282","email":"shrutisanghani@gmail.com"},
        {"name":"rohitkhare84","phone":"","email":"rohitkhare84@gmail.com"},
        {"name":"DrBharati G Gajjar","phone":"09426201201","email":"drbharatigajjar02@gmail.com"},
        {"name":"Aman Chhabra","phone":"9478564008","email":"sachin_khurana@hotmail.com"},
        {"name":"Mansi mehta","phone":"9920085587","email":"Mansi.mehta.rb@gmail.com"},
        {"name":"Shruti P","phone":"9910271839","email":"psarathy.shruti@gmail.com"},
        {"name":"Anirudh Gupta","phone":"9999999354","email":"buzz@funaster.com"},
        {"name":"ruchi m singh","phone":"9778718320","email":"lyfz.freak@gmail.com"},
        {"name":"Anandi Bharwani","phone":"9748150569","email":"anandib123@gmail.com"},
        {"name":"Kanchan joshi","phone":"9810973349","email":"kanchan.joshi@gmail.com"},
        {"name":"Pooja Bhushan","phone":"9717953312","email":"poojabhushan13@yahoo.in"},
        {"name":"amruta thamke","phone":"8380808743","email":"khushithamke25@gmail.com"},
        {"name":"Nimish","phone":"9404969241","email":"marathenimish@gmail.com"},
        {"name":"alishaarora9921","phone":"","email":"alishaarora9921@gmail.com"},
        {"name":"Deepti Nath","phone":"9886243144","email":"deepti.nath@gmail.com"},
        {"name":"MALYASHREE SRIDHARAN","phone":"9810654644","email":"malyashrees@gmail.com"},
        {"name":"rajatmittal01","phone":"","email":"rajatmittal01@gmail.com"},
        {"name":"misra.shreya","phone":"","email":"misra.shreya@gmail.com"},
        {"name":"Sonali Batra","phone":"9873097575","email":"sonali@oncourseglobal.com"},
        {"name":"Priti Kataria ","phone":"+919820292216","email":"priti.kataria@kalpataru.com"},
        {"name":"Manoj Chhaya","phone":"9898306067","email":"manoj_chhaya@rediffmail.com"},
        {"name":"Vritul Bajaj","phone":"9910195390","email":"vritul.bajaj@gmail.com"},
        {"name":"Nikitajain1104","phone":"","email":"Nikitajain1104@gmail.com"},
        {"name":"Harbir Singh Virk","phone":"+919216288373","email":"hsvirk@effluentindia.com"},
        {"name":"Achala bahl","phone":"+919810209172","email":"bahls05@gmail.com"},
        {"name":"Aileen Chatterjee","phone":"9831161995","email":"topofdaworld@gmail.com"},
        {"name":"gaurav kumar","phone":"9810036237","email":"gauravk3@gmail.com"},
        {"name":"Arunima Takiar","phone":"919892375523","email":"arunima.takiar@gmail.com"},
        {"name":"Eeshan Chandra Singh","phone":"7838660427","email":"eeshan.bits@gmail.com"},
        {"name":"pooja gupta","phone":"9810795881","email":"gupta.pooja44@gmail.com"},
        {"name":"vicky jain","phone":"+919866366266","email":"vky@live.in"},
        {"name":"Aileen Chatterjee","phone":"09731096580","email":"aileenchatterjee@gmail.com"},
        {"name":"Divish Sabhlok","phone":"9899793431","email":"divishsabhlok14@gmail.com"},
        {"name":"Chirag Thaker","phone":"9740120461","email":"chiragathaker@gmail.com"},
        {"name":"Jaideep Singh Sachdev","phone":"9986429444","email":"jaideepsingh.sachdev@gmail.com"},
        {"name":"sirjanwalia","phone":"","email":"sirjanwalia@gmail.com"},
        {"name":"Ankur Bansal","phone":"9891716202","email":"sankss@gmail.com"},
        {"name":"simran warsi","phone":"9810122235","email":"simran_warsi@hotmail.com"},
        {"name":"nipun reddy","phone":"500009","email":"nipungreddy@yahoo.com"},
        {"name":"Laxmi Jain","phone":"9810223088","email":"laxmi_j@yahoo.com"},
        {"name":"rajiv isaac","phone":"8017001011","email":"risaacs@rediffmail.com"},
        {"name":"Chetna Parekh","phone":"+919953608785","email":"chetna.parekh88@gmail.com"},
        {"name":"Sukriti Grover","phone":"9711184434","email":"kritisu@gmail.com"},
        {"name":"Tanya Dhall","phone":"9582827828","email":"tanyadhall@gmail.com"},
        {"name":"Rahul Sheel","phone":"8294011409","email":"rahul.sheel@gmail.com"},
        {"name":"harshmeet singh","phone":"9988165557","email":"harshmeet.28@gmail.com"},
        {"name":"Nikitha Shenoy","phone":"9177485805","email":"nikithashenoy93@gmail.com"},
        {"name":"Madhavi Sethupathi","phone":"09884123464","email":"madhavi.sethupathi@gmail.com"},
        {"name":"Mehak chowdhary","phone":"00919999100814","email":"Mehak.chow@gmail.com"},
        {"name":"Hemant Patel","phone":"9825042336","email":"hemntmri@gmail.com"},
        {"name":"oscvid_57","phone":"","email":"oscvid_57@yahoo.com"},
        {"name":"Arvind Sundararajan","phone":"04222546046","email":"arvindappuser@gmail.com"},
        {"name":"Sanjana Saksena ","phone":"9560090223","email":"sanjanasaksena23@gmail.com"},
        {"name":"Raghav Nanda","phone":"9958716723","email":"raghav1508@gmail.com"},
        {"name":"swatee Chaturvedi","phone":"09818241418","email":"Swatee06@gmail.com"},
        {"name":"Kavitha Chetana  Didugu","phone":"09662026299","email":"kavithacd@iimahd.ernet.in"},
        {"name":"Mamta Sanjeev Dubey","phone":"9793362666","email":"dubey_ms@yahoo.co.in"},
        {"name":"Manali","phone":"8509724434","email":"manali.cse@gmail.com"},
        {"name":"Ritika Gupta","phone":"9958395345","email":"ritikagupta89@gmail.com"},
        {"name":"anupama.mundhra","phone":"","email":"anupama.mundhra@gmail.com"},
        {"name":"priti","phone":"9537755526","email":"prit@ymail.com"},
        {"name":"navroz mahudawala","phone":"9820409164","email":"navroz.mahudawala@candleadvisors.com"},
        {"name":"Abhishek Gupta","phone":"+919820611061","email":"ag.abhishekgupta@gmail.com"},
        {"name":"Rashi Dubey","phone":"09727388112","email":"rashi16_d@yahoo.co.in"},
        {"name":"supriya.amr","phone":"","email":"supriya.amr@gmail.com"},
        {"name":"Ridhi Verma","phone":"9810147760","email":"ridhi1455@gmail.com"},
        {"name":"Alka Vaid","phone":"9810125820","email":"Alkavaid26@gmail.com"},
        {"name":"Ashish Mankad","phone":"+919898248414","email":"pratichi86@hotmail.com"},
        {"name":"Neetu Guron","phone":"09872446884","email":"neetu.guron@gmail.com"},
        {"name":"prafulfoodmall","phone":"","email":"prafulfoodmall@yahoo.com"},
        {"name":"Shreya Singh","phone":"8447925292","email":"shreya.carleton@gmail.com"},
        {"name":"shilpa sharma","phone":"9958475808","email":"shilpa_florals@yahoo.co.in"},
        {"name":"Shaurya Singh","phone":"9810036125","email":"iitabbyii98@gmail.com"},
        {"name":"jinurajan","phone":"","email":"jinurajan@in.com"},
        {"name":"Ram","phone":"9176012414","email":"riyer0019@gmail.com"},
        {"name":"Somya Dimri","phone":"09886101743","email":"swaatimehta@gmail.com"},
        {"name":"Somya Dimri","phone":"09886101743","email":"somyadimri@gmail.com"},
        {"name":"Jisha John","phone":"9502791005","email":"jisha.of.j@gmail.com"},
        {"name":"Nidhi Kalra","phone":"8141802746","email":"nidhie.01@gmail.com"},
        {"name":"naina sah","phone":"8951873771","email":"nainasah91@gmail.com"},
        {"name":"anthea.ws","phone":"","email":"anthea.ws@gmail.com"},
        {"name":"Harish KM","phone":"09620202233","email":"rudra2255@gmail.com"},
        {"name":"Saurabh Rajpal","phone":"9811988854","email":"saurabh@npglobal.in"},
        {"name":"Shallu ","phone":"+919888017677","email":"itsmeshallu@ymail.com"},
        {"name":"Amrapali Dhavare","phone":"9972006321","email":"Amrapali.dhaware@gmail.com"},
        {"name":"Anmol Bhagat","phone":"9999103178","email":"anmol_bhagat@hotmail.com"},
        {"name":"gunsnroses2183","phone":"","email":"gunsnroses2183@gmail.com"},
        {"name":"Ankush Nanda","phone":"9810968029","email":"21.ankush@gmail.com"},
        {"name":"Huzefa talib","phone":"+919886285727","email":"huzefa.smb@gmail.com"},
        {"name":"Schadda7","phone":"","email":"Schadda7@gmail.com"},
        {"name":"ashish.johry","phone":"","email":"ashish.johry@gmail.com"},
        {"name":"gurmahima ","phone":"9478824252","email":"gurmahima.sethi@gmail.com"},
        {"name":"Rutvik","phone":"9998750601","email":"rutviksagar@yahoo.com"},
        {"name":"Annu Singh Kundu","phone":"9830059795","email":"annu_kundu@yahoo.com"},
        {"name":"ANJALI GADA","phone":"9820466041","email":"anjali.gada@gmail.com"},
        {"name":"Shruti Sagar","phone":"9953156464","email":"shrutisagar_13@yahoo.in"},
        {"name":"Anjali Chahal","phone":"9818376585","email":"anjalikchahal@gmail.com"},
        {"name":"Sanjeev Gupta","phone":"9989100277","email":"soffice276@gmail.com"},
        {"name":"Aalok Wadhwa","phone":"09810084220","email":"aalokwadhwa@gmail.com"},
        {"name":"Surekha","phone":"8750952585","email":"ydv_suru@yahoo.co.in"},
        {"name":"Barbara Walter","phone":"8527184792","email":"barbara.walter@hotmail.de"},
        {"name":"manasi","phone":"9930039898","email":"manasi.kadne@gmail.com"},
        {"name":"ruchi m singh","phone":"9778718320","email":"lyz.freak@gmail.com"},
        {"name":"Srishti Sett","phone":"+919590501021","email":"srishti.sett@gmail.com"},
        {"name":"Jyoti Amba","phone":"9845170286","email":"jyoti2amba@gmail.com"},
        {"name":"Darshan dave","phone":"9726319777","email":"Dr.Darshandave@gmail.com"},
        {"name":"Medha Jolly","phone":"9810199116","email":"medhaj31@gmail.com"},
        {"name":"Avirudh Kaushik","phone":"9971160550","email":"akbond27@gmail.com"},
        {"name":"MUKESH ANAND","phone":"08941085555","email":"anandmegh@yahoo.co.in"},
        {"name":"Shireen luthra","phone":"9818738366","email":"luthrashireen@gmail.com"},
        {"name":"Suchet chugh","phone":"9899880778","email":"Suchet_chugh@live.in"},
        {"name":"Atul K Bansal","phone":"9871290044","email":"akbansal1255@gmail.com"},
        {"name":"Vanita bhola ","phone":"8860237870","email":"Vanitabhola@hotmail.com"},
        {"name":"Arjun Kakker","phone":"9720969779","email":"arjun.kakker@gmail.com"},
        {"name":"akhilm1991","phone":"","email":"akhilm1991@gmail.com"},
        {"name":"psidhu05","phone":"","email":"psidhu05@gmail.com"},
        {"name":"Sabeeha Khan","phone":"8393888377","email":"khansabeeha@gmail.com"},
        {"name":"ravinderjakhoda","phone":"","email":"ravinderjakhoda@gmail.com"},
        {"name":"Karan Saraf","phone":"9886797340","email":"karansaraf@live.in"},
        {"name":"aminpratik10","phone":"","email":"aminpratik10@gmail.com"},
        {"name":"Sohrab Bhujwala","phone":"9899867379","email":"crazyballz@hotmail.com"},
        {"name":"Serva Associates","phone":"9711081114","email":"akshit_kapoor@hotmail.com"},
        {"name":"Sheena Shah","phone":"9960146476","email":"sheena.s.shah@gmail.com"},
        {"name":"Sriram Anne","phone":"8500118238","email":"mathdude2@gmail.com"},
        {"name":"Anuradha Sood","phone":"9839221802","email":"Anuradha.sood@yahoo.com"},
        {"name":"vermsumit","phone":"","email":"vermsumit@gmail.com"},
        {"name":"Vijay Bhaskar Mishra","phone":"9810211460","email":"vijaybhaskarmishra@gmail.com"},
        {"name":"anoop4840","phone":"","email":"anoop4840@gmail.com"},
        {"name":"shreyakalra89","phone":"","email":"shreyakalra89@gmail.com"},
        {"name":"Auarunarun3","phone":"","email":"Auarunarun3@gmail.com"},
        {"name":"Neha Arora","phone":"09717077155","email":"neha.arora27@gmail.com"},
        {"name":"nruthya","phone":"9480252538","email":"nruthyasachin@gmail.com"},
        {"name":"Joylita Saldanha","phone":"09916604985","email":"joylitas@gmail.com"},
        {"name":"difficulttolive","phone":"","email":"difficulttolive@gmail.com"},
        {"name":"Harish G Bhat","phone":"9008005791","email":"harishgbhat@outlook.com"},
        {"name":"Sinith Gopinathan","phone":"8605005034","email":"sinith.g@gmail.com"},
        {"name":"kekal patel","phone":"9879329115","email":"kekal0911@hotmail.com"},
        {"name":"Kailash Gandhi","phone":"9820129992","email":"kgee2011@gmail.com"},
        {"name":"BALAR BHAVESH","phone":"9825132162","email":"balar0707@yahoo.com"},
        {"name":"A K Khare","phone":"09453614810","email":"akharester@gmail.com"},
        {"name":"sonalee tomar","phone":"9810205827","email":"sonalee.tomar@gmail.com"},
        {"name":"Shivani seth","phone":"9810007286","email":"Shivaniv02@gmail.com"},
        {"name":"Vikas Gupta","phone":"+918054075623","email":"vikas357in@gmail.com"},
        {"name":"Samuel ","phone":"9008235987","email":"gsamuelsanjay@gmail.com"},
        {"name":"abhay.loke","phone":"","email":"abhay.loke@gmail.com"},
        {"name":"Gayatri Iyer","phone":"9599938480","email":"gayatri30@gmail.com"},
        {"name":"Sakshi Galada","phone":"9940020864","email":"galadasakshi96@gmail.com"},
        {"name":"ABHISHEKPARMAR07","phone":"","email":"ABHISHEKPARMAR07@GMAIL.COM"},
        {"name":"Vishal Gupta","phone":"09535499886","email":"vhgupta@gmail.com"},
        {"name":"sadhna gupta","phone":"09873713103","email":"Sadhna1960@gmail.com"},
        {"name":"SHAZIA GOWARIKER","phone":"9821118881","email":"shazia.g@mac.com"},
        {"name":"Anoop Nair","phone":"8884772424","email":"nairanoopr@gmail.com"},
        {"name":"Aleena Nazar","phone":"8971316060","email":"aleenanazar92@gmail.com"},
        {"name":"Rubani","phone":"9930520690","email":"rubanibhatia7@gmail.com"},
        {"name":"Rubani bhatia","phone":"9818679702","email":"Ginni36@gmail.com"},
        {"name":"I Bezembinder","phone":"+911244069950","email":"j.vonk@kpnplanet.nl"},
        {"name":"mili gupta","phone":"09810611677","email":"mgmiligupta@gmail.com"},
        {"name":"Umesh Kandoi","phone":"+919610272727","email":"kandoiumesh@gmail.com"},
        {"name":"nazimsaifi81","phone":"","email":"nazimsaifi81@gmail.com"},
        {"name":"Keshav Anand ","phone":"9910929279","email":"keshav.anand90@gmail.com"},
        {"name":"smriti gupta","phone":"919711779925","email":"gupta1910@gmail.com"},
        {"name":"Neelima ","phone":"9810820877","email":"Tyagineelu@Gmail.Com"},
        {"name":"laksh","phone":"","email":"laksh@nuhydro.com"},
        {"name":"shatyaki chatterjee","phone":"9163993120","email":"Shatyakichatterjee@yahoo.in"},
        {"name":"Mayuri Majumdar","phone":"09051737091","email":"majumdar.ria@gmail.com"},
        {"name":"Anchal Bahl","phone":"9873283696","email":"anch_libra@hotmail.com"},
        {"name":"Nileshkaria","phone":"9993919999","email":"Knileshkumar@gmail.com"},
        {"name":"Vishal Agrawal","phone":"9987055990","email":"vishal@tlclegal.in"},
        {"name":"GAURAV KUMAMR","phone":"9837177710","email":"gkkumar80@gmail.com"},
        {"name":"Pushkar Raj","phone":"09542455188","email":"kopalpushkar@yahoo.com"},
        {"name":"vyshali","phone":"9496404908","email":"vyshaliprakash@gmail.com"},
        {"name":"Sana Chauhan","phone":"9811557404","email":"keshav@kkexporters.net"},
        {"name":"Siddharth deol","phone":"8527535007","email":"siddharthdeol86@gmail.com"},
        {"name":"suresh","phone":"9587412563","email":"peter@gmail.com"},
        {"name":"Harsh Goel","phone":"7680967900","email":"harsh_goel2016@isb.edu"},
        {"name":"janak","phone":"9408415205","email":"janaknh@gmail.com"},
        {"name":"Sneh Desai","phone":"9574477777","email":"Snehdesai_mindpower@yahoo.co.in"},
        {"name":"Murtaza Ezzy","phone":"9820036752","email":"murtazaezzy5@gmail.com"},
        {"name":"Nasir Tyabji","phone":"9968007300","email":"ntyabji@gmail.com"},
        {"name":"vivek_falod","phone":"","email":"vivek_falod@hotmail.com"},
        {"name":"KOMAL ANAND","phone":"9654143305","email":"komal77nirankari@gmail.com"},
        {"name":"Jay Oza","phone":"+919930917724","email":"jayoza@gmail.com"},
        {"name":"ppal5783","phone":"","email":"ppal5783@gmail.com"},
        {"name":"sdrg","phone":"+918800793642","email":"vipulsharma@wingreens.in"},
        {"name":"Sakshi Ranka","phone":"9923139380","email":"sak208@gmail.com"},
        {"name":"Simran Kalra","phone":"9818648353","email":"Kalra.simran@gmail.com"},
        {"name":"Mukul Dutt","phone":"9810771057","email":"Vandana@esvasa.com"},
        {"name":"Kartik Balu","phone":"8939621436","email":"div.passi@gmail.com"},
        {"name":"Kishori Tikmani","phone":"9973917157","email":"tikmani.kishori@gmail.com"},
        {"name":"Priyanka Rai singh","phone":"07709380902","email":"Priyankaraisingh@gmail.com"},
        {"name":"Amit Agarwal","phone":"+919313880389","email":"SPORTIVE@GMAIL.COM"},
        {"name":"Pallak","phone":"9650082015","email":"Pallakarora@gmail.com"},
        {"name":"Aditi Birla","phone":"9829387116","email":"aditi.birla@gmail.com"},
        {"name":"Sharad Jhunjhunwala","phone":"9431011461","email":"handpumps@gmail.com"},
        {"name":"Tanvi Agarwal","phone":"8130208525","email":"tanvi1996@gmail.com"},
        {"name":"sonali.gaur0369","phone":"","email":"sonali.gaur0369@yahoo.com"},
        {"name":"sb","phone":"12345678","email":"123@gmail.com"},
        {"name":"Meenal ","phone":"7899810361","email":"Meenaluppal@yahoo.co.in"},
        {"name":"INDRANIL DATTA ","phone":"+919911322002","email":"Indranil100@gmail.com"},
        {"name":"surbhi.lata","phone":"","email":"surbhi.lata@gmail.com"},
        {"name":"Jagrat Rawal","phone":"9711662052","email":"jrextra14@gmail.com"},
        {"name":"skhit140","phone":"","email":"skhit140@gmail.com"},
        {"name":"Insiyah Contractor","phone":"9618680632","email":"insiyah.c@gmail.com"},
        {"name":"rituparna paul","phone":"07506259223","email":"ritzee08@gmail.com"},
        {"name":"Aanchal vij","phone":"9811982868","email":"dhawanaanchal@gmail.com"},
        {"name":"Job Abraham","phone":"00918943303928","email":"jobabraham88@gmail.com"},
        {"name":"Srividya Easwaran","phone":"09833751429","email":"srividya.easwaran@gmail.com"},
        {"name":"Piyush Gosain","phone":"9999258655","email":"kritika.gosain@bankofamerica.com"},
        {"name":"sahiba singh","phone":"9810618987","email":"sahiba47@hotmail.com"},
        {"name":"rahul_jacob","phone":"","email":"rahul_jacob@ymail.com"},
        {"name":"ritwikbhattacharya","phone":"","email":"ritwikbhattacharya@nls.ac.in"},
        {"name":"Yash Majithia","phone":"9920362861","email":"majithia3@gmail.com"},
        {"name":"Snigdha puri","phone":"9999979805","email":"Snigdhapuri@gmail.com"},
        {"name":"Priyanka yadav","phone":"7838111585","email":"yadav.priyanka0585@yahoo.com"},
        {"name":"a","phone":"","email":"a@gmail.com"},
        {"name":"mail.manojverma","phone":"","email":"mail.manojverma@gmail.com"},
        {"name":"Daman Singhal","phone":"99999969713","email":"singhal.madhav@yahoo.com"},
        {"name":"Sunand Sharma","phone":"9810011549","email":"sunand@vsnl.com"},
        {"name":"Gautam Arora","phone":"9811327680","email":"gautam.ucms@gmail.com"},
        {"name":"Heeren A Karia","phone":"9821474000","email":"heerenkaria@yahoo.com"},
        {"name":"julick isaiah","phone":"9941091220","email":"julickisaiah@gmail.com"},
        {"name":"Nivedita","phone":"9962605682","email":"nivedita.narendar@gmail.com"},
        {"name":"shipra.s.0512","phone":"","email":"shipra.s.0512@gmail.com"},
        {"name":"Aarushi Gupta","phone":"9988899513","email":"gupta_aarushi13@hotmail.com"},
        {"name":"Piyush jain","phone":"9910001601","email":"Abhinandanjewellers@gmail.com"},
        {"name":"Sahil","phone":"+918860389716","email":"sahil.miglani@ymail.com"},
        {"name":"nandini0489","phone":"","email":"nandini0489@gmail.com"},
        {"name":"Munish Chadha","phone":"981009893","email":"munishmytechbays@gmail.com"},
        {"name":"pammi","phone":"9814063350","email":"pammi.mehta@yahoo.com"},
        {"name":"Amrita Menon","phone":"9920954695","email":"zeishajaffer@gmail.com"},
        {"name":"archanapremjit","phone":"","email":"archanapremjit@yahoo.com"},
        {"name":"Mihika Sahgal","phone":"9711924882","email":"mihikasahgal123@gmail.com"},
        {"name":"Richa Gupta Johar","phone":"9818209653","email":"rg.richa@gmail.com"},
        {"name":"Bhavana Pant","phone":"9873322113","email":"bhavnapant21@gmail.com"},
        {"name":"Nidhi Gupta","phone":"9821990060","email":"nidhi0188@gmail.com"},
        {"name":"majid nazir","phone":"9906899231","email":"majid_nazir@rediffmail.com"},
        {"name":"Saurabh Gupta","phone":"+919560399352","email":"Sgupta@srinathji.Co.in"},
        {"name":"shael.khandelwal","phone":"","email":"shael.khandelwal@gmail.com"},
        {"name":"kavita verma","phone":"9811060905","email":"kavitaverma@vsnl.net"},
        {"name":"Sparsh Agarwalla","phone":"+919702213140","email":"agarwalla.sparsh@gmail.com"},
        {"name":"Gunjan chowdhry","phone":"9811148208","email":"Gunjanchowdhry@hotmail.com"},
        {"name":"Sumit Chaterjee","phone":"9717991706","email":"sumit400@yahoo.com"},
        {"name":"sonali","phone":"9250002215","email":"rahul26285@gmail.com"},
        {"name":"Anuj Mankar","phone":"9703666693","email":"Anuj_mankar@hotmail.com"},
        {"name":"Saloni brar","phone":"9876198222","email":"Sal_brar@yahoo.co.in"},
        {"name":"naani8","phone":"","email":"naani8@hotmail.com"},
        {"name":"Amogh Kakirde","phone":"8450913133","email":"akakirde9@gmail.com"},
        {"name":"praveens8","phone":"","email":"praveens8@gmail.com"},
        {"name":"Jay Jogi","phone":"9920969617","email":"jay.jogi@gmail.com"},
        {"name":"jasjivan ahluwalia","phone":"9741908729","email":"jasjivan@gmail.com"},
        {"name":"Haresh Balani","phone":"9900030247","email":"haresh.balani@gmail.com"},
        {"name":"imad rais","phone":"9833198330","email":"imadrais1@gmail.com"},
        {"name":"Radhika","phone":"09845296319","email":"radhika.shekar@gmail.com"},
        {"name":"Neeta bhardwaj","phone":"9899869971","email":"drneeta4@gmail.com"},
        {"name":"C Zobiakhlui","phone":"8800182414","email":"zobiakhlui@gmail.com"},
        {"name":"arpantib","phone":"","email":"arpantib@gmail.com"},
        {"name":"Khushboo Aggarwal ","phone":"9780416882","email":"khush237@yahoo.co.in"},
        {"name":"Sunil Beri","phone":"7710085888","email":"sunil0090@gmail.com"},
        {"name":"Charmaine Paul","phone":"9873740825","email":"charmaine@riverindia.co.in"},
        {"name":"aishwarya.j.parakh","phone":"","email":"aishwarya.j.parakh@gmail.com"},
        {"name":"Kushagra sharma","phone":"9560209531","email":"shmedhasharma1303@gmail.com"},
        {"name":"shashank","phone":"9148004419","email":"shashankgowda9999@gmail.com"},
        {"name":"sachin.lv23","phone":"","email":"sachin.lv23@gmail.com"},
        {"name":"jmendonce","phone":"","email":"jmendonce@gmail.com"},
        {"name":"geetika","phone":"9818254376","email":"geeticakapoor@gmail.com"},
        {"name":"Geetanjali ","phone":"9599705559","email":"geet0602@yahoo.in"},
        {"name":"parveen.rawat","phone":"","email":"parveen.rawat@yahoo.co.in"},
        {"name":"Sajid Khan","phone":"9004476474","email":"sajidsiddiqkhan@gmail.com"},
        {"name":"Ipsa Mehta","phone":"9996200569","email":"ipsa.mehta@gmail.com"},
        {"name":"deepak","phone":"9482767374","email":"deepsan2011.dk@gmail.com"},
        {"name":"Dipti","phone":"7829673562","email":"ms.diptikamath@gmail.com"},
        {"name":"cshachi25","phone":"","email":"cshachi25@gmail.com"},
        {"name":"Gayatri Iyer","phone":"9599938480","email":"psitsgayatri@gmsil.com"},
        {"name":"richa","phone":"9873923359","email":"richajaiswal10@gmail.com"},
        {"name":"AS","phone":"3812727727","email":"sarin.amol@gmail.com"},
        {"name":"Pradeep Kapoor","phone":"9810693844","email":"pradeepkapoor5@gmail.com"},
        {"name":"Anjali Rajashekar","phone":"07411001741","email":"anjali.raj8@gmail.com"},
        {"name":"ankushm999","phone":"","email":"ankushm999@rediffmail.com"},
        {"name":"dhriti.chawla","phone":"","email":"dhriti.chawla@gmail.com"},
        {"name":"Amin_chivilkar","phone":"","email":"Amin_chivilkar@yahoo.com"},
        {"name":"Sowmya ","phone":"9597588098","email":"msowmya095@gmail.com"},
        {"name":"Grishma Patel ","phone":"9898520957","email":"grishma71@gmail.com"},
        {"name":"rpisipaty","phone":"","email":"rpisipaty@deloitte.com"},
        {"name":"div_sri1993","phone":"","email":"div_sri1993@yahoo.in"},
        {"name":"Karanjeet Singh","phone":"9999680474","email":"jasleenn@gmail.com"},
        {"name":"manjula.bt23","phone":"","email":"manjula.bt23@gmail.com"},
        {"name":"rajgupta.may","phone":"","email":"rajgupta.may@gmail.com"},
        {"name":"Joel Almeida","phone":"9819137133","email":"joel.almeida@gmail.com"},
        {"name":"pulkit","phone":"+919999962559","email":"pulkit.punj@gmail.com"},
        {"name":"Vybhavi","phone":"8050900993","email":"nr_angel85@yahoo.com"},
        {"name":"SIMI KALSI","phone":"9501876860","email":"simi.kalsi@rocketmail.com"},
        {"name":"blackjo1996","phone":"","email":"blackjo1996@gmail.com"},
        {"name":"Rishabh Aggarwal","phone":"+919560168950","email":"rishabhaggarwal2003@gmail.com"},
        {"name":"Atul Jain ","phone":"7042656650","email":"tljn1990@gmail.com"},
        {"name":"S Nagarajan","phone":"9841096892","email":"jyothsna.priya@gmail.com"},
        {"name":"Namrata basu","phone":"9674752129","email":"namrulz@gmail.com"},
        {"name":"Ekta Pujari","phone":"9619908211","email":"ekta.pujari@sequoiacap.com"},
        {"name":"Abhishek Ghate","phone":"8971359332","email":"abhishekghate@gmail.com"},
        {"name":"juhi882","phone":"","email":"juhi882@yahoo.com"},
        {"name":"dr.vrindasingla","phone":"","email":"dr.vrindasingla@gmail.com"},
        {"name":"amrithias94","phone":"","email":"amrithias94@gmail.com"},
        {"name":"Nikita khurana","phone":"9619069721","email":"khurana.niki16@gmail.com"},
        {"name":"nancy.ahuja6","phone":"","email":"nancy.ahuja6@gmail.com"},
        {"name":"annagupta07","phone":"","email":"annagupta07@gmail.com"},
        {"name":"Ashish Bhala","phone":"999999999","email":"ashish@cabhala.com"},
        {"name":"dr hemal nayak","phone":"9879795922","email":"drhemalnayak@gmail.com"},
        {"name":"Maheshwar Singh","phone":"9841040066","email":"maheshwar.esingh@gmail.com"},
        {"name":"ery","phone":"+918800793642","email":"vsjdsd@klshdf.com"},
        {"name":"wef","phone":"9760187187","email":"freshneasydehradun@gmailc.om"},
        {"name":"test","phone":"1234567","email":"test@test.com"},
        {"name":"Nayana Dutta","phone":"9435712271","email":"nayana1amulet@gmail.com"},
        {"name":"Nivedita Poonekar","phone":"+919000683123","email":"nivi_9685@yahoo.com"},
        {"name":"Saba khurana","phone":"9811965435","email":"khurana.saba@gmail.com"},
        {"name":"Aarav punia","phone":"9582251270","email":"Sneha.dhankher@gmail.com"},
        {"name":"Annaika Ahuja ","phone":"9910899622","email":"annaikaahuja123@hotmail.com"},
        {"name":"MRS JAYSHREE TRIPATHI","phone":"9910276244","email":"jmtri@rediffmail.com"},
        {"name":"11preetu","phone":"","email":"11preetu@gmail.com"},
        {"name":"prerna.kaushal","phone":"","email":"prerna.kaushal@payu.in"},
        {"name":"pasaripratiksha2009","phone":"","email":"pasaripratiksha2009@gmail.com"},
        {"name":"Atul Mittal","phone":"09711978907","email":"mittal19atul@gmail.com"},
        {"name":"sharad kumar","phone":"99962025070","email":"rsharadkumar@outloook.com"},
        {"name":"Rashmi Sarraf","phone":"8294186081","email":"rashmi.sarraf31@gmail.con"},
        {"name":"Akriti jain","phone":"09971642604","email":"akratyjain21@gmail.com"},
        {"name":"Edha Singh","phone":"8447012721","email":"Singh.harsh93@yahoo.co.in"},
        {"name":"Mohd Rizwan Ansari","phone":"9811558992","email":"mrizwanansari@gmail.com"},
        {"name":"Shivangi Chaturvedi","phone":"08976064987","email":"shivangi51986@yahoo.com"},
        {"name":"Abhimanyu Cherukupalli","phone":"9717694467","email":"abhimanyu.cherukupalli@hotmail.com"},
        {"name":"cbpattanayak","phone":"","email":"cbpattanayak@gmail.com"},
        {"name":"Shivi Srivastava","phone":"7718860080","email":"shivisrivastav@gmail.com"},
        {"name":"Luvee","phone":"","email":"Luvee@intnet.mu"},
        {"name":"Madhav Soi","phone":"9833352024","email":"soi.madhav@gmail.com"},
        {"name":"rashi khemka ","phone":"9764441136","email":"khemka.rashi94@gmail.com"},
        {"name":"premspinto","phone":"","email":"premspinto@gmail.com"},
        {"name":"Kavita","phone":"9311008801","email":"kj1971jaihind@gmail.com"},
        {"name":"Shreyaans Jain","phone":"9311008801","email":"shreyjainaa@gmail.com"},
        {"name":"Akansha Bhatnagar","phone":"8860354483","email":"ms.akanshabhatnagar@gmail.com"},
        {"name":"anuradha chowdhary","phone":"+919619294757","email":"anuradha_chowdhary16@hotmail.com"},
        {"name":"mohammad fahad ahamad","phone":"8860499028","email":"fahad.ahmed007@gmail.com"},
        {"name":"Neha Bahadur","phone":"09880299976","email":"Neha.bahadur@gmail.com"},
        {"name":"Sheena Chopra","phone":"8447235352","email":"sheenachopra17@gmail.com"},
        {"name":"Rohit Singh","phone":"9891967170","email":"dr.rohitsingh@outlook.com"},
        {"name":"neerajld92","phone":"","email":"neerajld92@gmail.com"},
        {"name":"Suren","phone":"9884661481","email":"rubecon.suren@gmail.com"},
        {"name":"Manoj  Purohit","phone":"9823039873","email":"manoj@radiancehospital.com"},
        {"name":"Rekha bhandari","phone":"09755553030","email":"Shradz204@gmail.com"},
        {"name":"Purva Chugh","phone":"9899037517","email":"purvachugh@hotmail.com"},
        {"name":"Ishani Rege","phone":"08408944581","email":"ishani@live.co.uk"},
        {"name":"nancygirotra","phone":"","email":"nancygirotra@gmail.com"},
        {"name":"aastha","phone":"9711208448","email":"aastha.nayyar@gmail.com"},
        {"name":"aiyarradhika1987","phone":"","email":"aiyarradhika1987@rediffmail.com"},
        {"name":"Rdiwanmail","phone":"","email":"Rdiwanmail@gmail.com"},
        {"name":"Tanu garg","phone":"9560999400","email":"tanugarggarg@gmail.com"},
        {"name":"Kanupriya ","phone":"9910649899","email":"Kanz_verma@yahoo.co.in"},
        {"name":"Varahagiri Vinay Giri","phone":"9845006231","email":"vvinaygiri@hotmail.com"},
        {"name":"Nikhil Parmar ","phone":"9702333399","email":"nik_par@yahoo.com"},
        {"name":"Pragati Shukla","phone":"9845006231","email":"pragshukla@gmail.com"},
        {"name":"Abhinandita","phone":"9820176554","email":"abhinandita@sequeira.co"},
        {"name":"bishtamrita","phone":"","email":"bishtamrita@gmail.com"},
        {"name":"Ankit gourisaria","phone":"9304801013","email":"jollybhimsaria@gmail.com"},
        {"name":"Sagar_realestates","phone":"","email":"Sagar_realestates@yahoo.com"},
        {"name":"Apoorv Malhotra","phone":"9810893010","email":"apoorvm40@gmail.com"},
        {"name":"Arundhati Dhuru","phone":"99919664444","email":"arundhatidhuru@gmail.com"},
        {"name":"ajesh","phone":"","email":"ajesh@elymer.com"},
        {"name":"pankaj","phone":"+919413060676","email":"pankajsuri_suri@gmail.com"},
        {"name":"simrat marwah","phone":"8800377774","email":"marwah.simrat@gmail.com"},
        {"name":"hackfun4me","phone":"","email":"hackfun4me@gmail.com"},
        {"name":"vaibhav suranaa","phone":"8130432999","email":"vaibhus@mail.com"},
        {"name":"Parmeet shah ","phone":"7506039829","email":"kadvakarela13@gmail.com"},
        {"name":"Renu Sethi Sharma","phone":"9810046235","email":"sethi_renu@hotmail.com"},
        {"name":"Mairu Jain","phone":"09599118710","email":"mairujain19@gmail.com"},
        {"name":"vaibhav suranaa","phone":"8130432999","email":"vaibhus@iin.com"},
        {"name":"wefewf","phone":"324234223","email":"asas@dfsd.sdf"},
        {"name":"Devvrath Arora","phone":"9999029340","email":"devvrath07@gmail.com"},
        {"name":"Rohit chawla","phone":"9899948814","email":"chawlaroohit@gmail.com"},
        {"name":"Aditya Thosar","phone":"09899679658","email":"adithosar8@gmail.com"},
        {"name":" Deepali Tezad ","phone":"09867409305","email":"dtezad@gmail.com"},
        {"name":"Simran Singh Khara","phone":"9582612474","email":"simransinghkhara@gmail.com"},
        {"name":"Sahil Miglani","phone":"+918860389716","email":"sahil.miglani96@gmail.com"},
        {"name":"Anshula","phone":"9910603489","email":"ab.doc.may@gmail.com"},
        {"name":"Mahima Mathur ","phone":"9769326177","email":"mahimamathur.mm@gmail.com"},
        {"name":"aditi sharma","phone":"09899049495","email":"aditisharma06@yahoo.com"},
        {"name":"Tithi","phone":"9694222222","email":"tithi1001@gmail.com"},
        {"name":"M Murthy","phone":"9717509604","email":"mithileshmurthy@yahoo.com"},
        {"name":"Gaurav Tuli","phone":"9999244906","email":"gt.gauravtuli@gmail.com"},
        {"name":"Rithika V Khanna","phone":"9840020320","email":"rithikavkhanna@gmail.com"},
        {"name":"ishpatnaik","phone":"","email":"ishpatnaik@gmail.com"},
        {"name":"SAURABH MAGON","phone":"9999099422","email":"magon_saurabh@ymail.com"},
        {"name":"tashakoshi","phone":"","email":"tashakoshi@gmail.com"},
        {"name":"contactdravi","phone":"","email":"contactdravi@gmail.com"},
        {"name":"D Chakravorty","phone":"09664333488","email":"dev@order-zapp.com"},
        {"name":"vipulkhera17","phone":"","email":"vipulkhera17@yahoo.com"},
        {"name":"Vivek Sehtia","phone":"9811042626","email":"vsehtia@gmail.com"},
        {"name":"Deepa sesuraj","phone":"9884463611","email":"graceland1981@hotmail.com"},
        {"name":"Krishna Dev Singh Rathore","phone":"9413509387","email":"kdbanna@gmail.com"},
        {"name":"Navjeet Chadha","phone":"9814103335","email":"arjan_chadha@hotmail.com"},
        {"name":"Pranav","phone":"9811817475","email":"pranav.suneja@gmail.com"},
        {"name":"chandrikachauhan","phone":"","email":"chandrikachauhan@hotmail.com"},
        {"name":"SHIVAANI AGARWAL","phone":"9845610221","email":"shivaaniblr@yahoo.co.in"},
        {"name":"Ankit Rathore","phone":"9655416392","email":"03051994pooja@gmail.com"},
        {"name":"abhinav sinha","phone":"9871403814","email":"a.s.rex2006@gmail.com"},
        {"name":"aditya nohria","phone":"9716861111","email":"aditya_nohria@yahoo.com"},
        {"name":"gourab singh","phone":"7838073545","email":"gourabgnit@gmail.com"},
        {"name":"sukritikapur1","phone":"","email":"sukritikapur1@gmail.com"},
        {"name":"Ruchi Thakkar","phone":"09930880479","email":"rt050291@gmail.com"},
        {"name":"Romit Patel","phone":"9825096464","email":"patelromit999@gmail.com"},
        {"name":"Madhav Narayan","phone":"9999031444","email":"bbhalla93@gmail.com"},
        {"name":"Swaroopa","phone":"9845496303","email":"sroopa.b@gmail.com"},
        {"name":"vasudhaa","phone":"8373987317","email":"vasudhaa95@gmail.com"},
        {"name":"ishita.gogia1997","phone":"","email":"ishita.gogia1997@gmail.com"},
        {"name":"panyalaaparna","phone":"","email":"panyalaaparna@gmail.com"},
        {"name":"Dinesh kumar Behani","phone":"9614891075","email":"lachhmi_malda@yahoo.co.in"},
        {"name":"Nishi Saksena","phone":"9810090963","email":"nssaksena@gmail.com"},
        {"name":"fghdfg","phone":"54775645675","email":"contact@wingreensfarms.com"},
        {"name":"Raman deep","phone":"9999899046","email":"shawarmawala@gmail.com"},
        {"name":"Raj Khosla","phone":"9818336894","email":"rajkhosla145@gmail.com"},
        {"name":"Soma Surana","phone":"7073885976","email":"johnysurana@gmail.com"},
        {"name":"Anurag Gehlot","phone":"9717496215","email":"anurag.gehlot79@gmail.com"},
        {"name":"Kls.sinha.divya","phone":"","email":"Kls.sinha.divya@gmail.com"},
        {"name":"anurag.icubes","phone":"","email":"anurag.icubes@gmail.com"},
        {"name":"VIVEK GUPTA","phone":"9815090009","email":"VIVEKG@ALASKAFLEXO.COM"},
        {"name":"spam","phone":"","email":"spam@spam.org"},
        {"name":"Aaditya kumar","phone":"0122307875","email":"kaaditya018@gmail.com"},
        {"name":"abd","phone":"","email":"abd@gmail.com"},
        {"name":"mattjones1848","phone":"","email":"mattjones1848@gmail.com"},
        {"name":"Tejasvinee Nair","phone":"9820631706","email":"vineenair@gmail.com"},
        {"name":"Naini Suri","phone":"9811464707","email":"surinaini@gmail.com"},
        {"name":"Vivek Anand Oberoi ","phone":"9870145551","email":"rockstarvo@gmail.com"},
        {"name":"Smit shah","phone":"8511059005","email":"smitshah9770@gmail.com"},
        {"name":"Anubhav","phone":"9560854619","email":"anubhav.ninjastar95@gmail.com"},
        {"name":"Atish Tak","phone":"09987118708","email":"aatish.tak@gmail.com"},
        {"name":"deepak ","phone":"9999105708","email":"deepak_g81@yahoo.com"},
        {"name":"hitesh_rao403","phone":"","email":"hitesh_rao403@hotmail.com"},
        {"name":"Aakar miglani","phone":"9888689850","email":"aakarmiglani1@gmail.cim"},
        {"name":"Aakar miglani","phone":"9888689850","email":"aakarmiglani1@gmail.com"},
        {"name":"Radhika Mehtani ","phone":"9999677464","email":"radds99@yahoo.com"},
        {"name":"Parampreet Singh","phone":"8195997770","email":"parampreet.mib2011@gmail.com"},
        {"name":"eshank jain","phone":"9837883311","email":"jain1.eshank@gmail.com"},
        {"name":"Shatej Sharma","phone":"9932932456","email":"satejsharma@yahoo.com"},
        {"name":"Carmen Jain","phone":"08485828544","email":"Krishna_carmen@hotmail.com"},
        {"name":"Sukriti Grover","phone":"9711184434","email":"su_kriti@hotmail.com"},
        {"name":"Akshay Sapra","phone":"+919811040604","email":"akshaysapra@gmail.com"},
        {"name":"Nandini Bhola","phone":"9837099460","email":"nandini_bhola@yahoo.co.in"},
        {"name":"AnupChandra SHIVAMURTHY","phone":"09591981035","email":"anupchandra_s@hotmail.com"},
        {"name":"Malvika ","phone":"9731356940","email":"malvika.0286@gmail.com"},
        {"name":"Purnima P Bhardwaj","phone":"9899669360","email":"purnima.pips@gmail.com"},
        {"name":"Basim Baqui","phone":"9884068384","email":"basimshahid@gmail.com"},
        {"name":"nikita.mehrotra","phone":"","email":"nikita.mehrotra@sc.com"},
        {"name":"Shweta Rao","phone":"9811183554","email":"raoshweta1978@gmail.com"},
        {"name":"Gabriel William","phone":"9894310972","email":"gabiejed007@gmail.com"},
        {"name":"Arpita Bhardwaj","phone":"+919654598313","email":"arpita0447@gmail.com"},
        {"name":"pranveerc","phone":"","email":"pranveerc@gmail.com"},
        {"name":"Shrruti Singhi","phone":"9820867051","email":"shrruti83@gmail.com"},
        {"name":"belani","phone":"","email":"belani@unhcr.org"},
        {"name":"vemareddyg","phone":"","email":"vemareddyg@gmail.com"},
        {"name":"Kartik Tandon","phone":"9910505050","email":"kartiktandon1@gmail.com"},
        {"name":"Jai Chary","phone":"9701866901","email":"jaichari@gmail.com"},
        {"name":"Sangeetha","phone":"9886411733","email":"sangeethavema@gmail.com"},
        {"name":"Akshata Majrekar","phone":"9820416065","email":"akshatamajrekar@gmail.com"},
        {"name":"lubna","phone":"09977004689","email":"lubna.warsi@gmail.com"},
        {"name":"kaustubh.v.chaudhary","phone":"","email":"kaustubh.v.chaudhary@gmail.com"},
        {"name":"Renu Gupta","phone":"9810614067","email":"rgrenugupta9@gmail.com"},
        {"name":"Venkatesh B","phone":"9945166988","email":"venkvb28@gmail.com"},
        {"name":"nupursuri.91","phone":"","email":"nupursuri.91@gmail.com"},
        {"name":"Harshit Murarka","phone":"9830794959","email":"harshitmurarka01@gmail.com"},
        {"name":"Sarthak Mehrotra","phone":"9810201411","email":"sarthakmehrotra@gmail.com"},
        {"name":"Rahul","phone":"9925017192","email":"rahoul.sajnani@yahoo.com"},
        {"name":"Rajat Arora","phone":"9999270572","email":"rajatkarora21@gmail.com"},
        {"name":"arohi j patel","phone":"08866007979","email":"arohi_patel@hotmail.co.uk"},
        {"name":"Emmanuel Bisi","phone":"+919008800500","email":"emmanuel.bisi@gmail.com"},
        {"name":"Varishensagar Shah","phone":"+919825800092","email":"varishen@gmail.com"},
        {"name":"mrunali chavhan","phone":"7875726666","email":"chavhanmrunali@gmail.com"},
        {"name":"Mani","phone":"9876631839","email":"chadhamandeep@yahoo.com"},
        {"name":"Amber ghallot","phone":"9999159310","email":"amberghallof@gmail.com"},
        {"name":"fghdfg","phone":"7657657657657","email":"mitesh_rami@yahoo.com"},
        {"name":"R Roslin mary","phone":"9443988070","email":"roslin.mary@gmail.com"},
        {"name":"IRFAN ULLAH","phone":"7417781832","email":"pwi.lko.nr@gmail.com"},
        {"name":"Amber ghallot","phone":"9999159310","email":"amberghallot@gmail.com"},
        {"name":"rahultina_d","phone":"","email":"rahultina_d@yahoo.co.in"},
        {"name":"Raghav Khanna","phone":"9872232222","email":"raghavkhanna@gmail.com"},
        {"name":"Megha Sharma ","phone":"9870064009","email":"templeofstyle@gmail.com"},
        {"name":"Richa Malhotra","phone":"9899090381","email":"richammalhotra@gmail.com"},
        {"name":"Foram Vora","phone":"9820330299","email":"advforamvora@gmail.com"},
        {"name":"Anjali","phone":"9811018956","email":"Anjali.gulliya@yahoo.com"},
        {"name":"Swaralee Khedkar","phone":"9096667675","email":"swarali.khedkar@yahoo.co.in"},
        {"name":"Sunil Raghav","phone":"9717832420","email":"sunny.raghav@gmail.com"},
        {"name":"Abha Mahajan","phone":"9888016606","email":"abhamahajan@gmail.com"},
        {"name":"Nishmeet arora","phone":"9540000554","email":"nish.arora1000@gmail.com"},
        {"name":"Shslu","phone":"9873337649","email":"Shalu.pratiek@gmail.com"},
        {"name":"ramkishan114","phone":"","email":"ramkishan114@gmail.com"},
        {"name":"Archana Dhawan","phone":"9899997507","email":"darushiarchana@gmail.com"},
        {"name":"Pranita Biyani","phone":"8377955496","email":"praneeta.biyani@gmail.com"},
        {"name":"ankita sahay","phone":"9910107785","email":"ankitasahaii@yahoo.co.in"},
        {"name":"Yamini MAHADEVAN","phone":"9840673664","email":"yamini.mahadevan@gmail.com"},
        {"name":"DHRUV NARAYAN","phone":"8800331717","email":"class190029@gmail.com"},
        {"name":"Tripti","phone":"99999815644","email":"Tripti.singhal.90@gmail.com"},
        {"name":"Parul kataria","phone":"9999772903","email":"parul.dbs@gmail.com"},
        {"name":"Kajal","phone":"8743001190","email":"Kajal004sharma@gmail.com"},
        {"name":"HIMANI ARORA","phone":"9582569103","email":"ARORAHIMANI816@GMAIL.COM"},
        {"name":"Disha Ahuja","phone":"9910449952","email":"disha.ahuja611@gmail.com"},
        {"name":"sarabkour","phone":"","email":"sarabkour@yahoo.com"},
        {"name":"Sharik Malhotra","phone":"9899202950","email":"Sharik.malhotra@gmail.com"},
        {"name":"Gulshan Babbar","phone":"9871803008","email":"babbar.gulshan@gmail.com"},
        {"name":"Swati Maheshwari ","phone":"05122381842","email":"shashim2608@icloud.com"},
        {"name":"Vatsal Jain","phone":"8948524346","email":"vatsaljain00@gmail.com"},
        {"name":"Gayatri Bhatia","phone":"9821551986","email":"gayatri04@gmail.com"},
        {"name":"Sneha Chandy","phone":"9500254092","email":"kuruvilasneha@gmail.com"},
        {"name":"Saumya Shree","phone":"8860227385","email":"saumya1008@gmail.com"},
        {"name":"ROSE LIMA BARA","phone":"7415218160","email":"jhn.brit14@gmail.com"},
        {"name":"Rajesh Bonagiri","phone":"9160644244","email":"rajesh.bonagiri82@gmail.com"},
        {"name":"Priyanka Gandhi ","phone":"9819230008","email":"gandhi.priyanka7@gmail.com"},
        {"name":"mjeena","phone":"","email":"mjeena@gmail.com"},
        {"name":"myconfessionsofglutoony","phone":"","email":"myconfessionsofglutoony@gmail.com"},
        {"name":"SHOBHANA CHITALE","phone":"2224381925","email":"shobhanachitale@rediffmail.com"},
        {"name":"Devika Kapur","phone":"8800722225","email":"devika1327@gmail.com"},
        {"name":"Priyanka","phone":"09845543534","email":"Priyankadevaiah@yahoo.com"},
        {"name":"rahul","phone":"9999576452","email":"rahuljainkl2006@gmail.com"},
        {"name":"Tluangi Sailo","phone":"9436154010","email":"sparkler.ts@gmail.com"},
        {"name":"Tanishka Rai","phone":"9818041990","email":"tanishkarai42@gmail.com"},
        {"name":"Dalreen Ann Miranda","phone":"9819929185","email":"Dalreenannmiranda@gmail.com"},
        {"name":"Archna Dalvi","phone":"02228414969","email":"archnadalvi@yahoo.co.in"},
        {"name":"Faiz Khan","phone":"9880517515","email":"mfk7515@gmail.com"},
        {"name":"om prakash singh","phone":"9811894973","email":"om.prakash@live.co.uk"},
        {"name":"zoha chaudhri","phone":"9810318032","email":"zoha@wingreens.in"},
        {"name":"v","phone":"+919830817341","email":"vedika_26192@hotmail.com"},
        {"name":"Ambika Sankar Mishra","phone":"9438011554","email":"ambikamails@gmail.com"},
        {"name":"Jaspreet Waraich ","phone":"9815426688","email":"jaspreetwaraich@gamil.com"},
        {"name":"devika","phone":"9888909950","email":"devikaa@icloud.com"},
        {"name":"Kandarp Patel","phone":"8758967201","email":"kandarp.mail@gmail.com"},
        {"name":"asdflkj","phone":"9875641230","email":"assfd@gmail.com"},
        {"name":"Ben Sullivan","phone":"9899202950","email":"zohachaudhri@gmail.com"},
        {"name":"Mani Raj","phone":"9899144134","email":"abhishek.chib@gmail.com"},
        {"name":"Sarthak Mehrotra","phone":"9810201411","email":"sarthakmehrotra@hotmail.com"},
        {"name":"amal_2009","phone":"","email":"amal_2009@hotmail.com"},
        {"name":"abhishek ","phone":"5333634546","email":"abhishektyagi877@gmail.com"},
        {"name":"Vivek aggarwal","phone":"8588808428","email":"Vaggarwal02@gmail.com"},
        {"name":"Preetam Bhupatiraju","phone":"9619312364","email":"preetambhupathi@gmail.com"},
        {"name":"Rahul Negi","phone":"8527548679","email":"rahul.negi@hotmail.com"},
        {"name":"Shiny Varghese","phone":"9967514669","email":"shinyf@gmail.com"},
        {"name":"Abhishek Mahajan","phone":"9815331293","email":"vanshika.saini13@bimtech.ac.in"},
        {"name":"mayurasaranathan@gmail.com","phone":"","email":"mayurasaranathan@gmail.com"},
        {"name":"Dr Ambika Sankar Mishra","phone":"9438011554","email":"ambikasankar@cutm.ac.in"},
        {"name":"Amit agrawal","phone":"9045400119","email":"agrawal.adesh@gmail.com"},
        {"name":"Anirban Sengupta","phone":"8860601040","email":"ani120973@gmail.com"},
        {"name":"Amit agrawal","phone":"9045400119","email":"aaamit95@gmail.com"},
        {"name":"Himani Lohia","phone":"9958126128","email":"hl.himanilohia@gmail.com"},
        {"name":"Preeti","phone":"9814010616","email":"preeti.pasricha616@gmail.com"},
        {"name":"Sarabjit","phone":"9820842031","email":"sarabjit@angelbroking.com"},
        {"name":"gopala goyal","phone":"7017006537","email":"goyalgopala@gmail.com"},
        {"name":"Pranita Agarwal","phone":"9820876008","email":"rakhi.garg1@gmail.com"},
        {"name":"Moumita Gupta ","phone":"8826714277","email":"momitagupta007@gmail.com"},
        {"name":"Munish Mann","phone":"8860527181","email":"deepikamann25@gmail.com"},
        {"name":"Granville Dsouza","phone":"9619052928","email":"granville.dsouza21@gmail.com"},
        {"name":"Blanche Dsouza","phone":"9930041065","email":"b4dsouza@gmail.com"},
        {"name":"b4dsouza","phone":"","email":"b4dsouza@gmail.com"},
        {"name":"Reeti Kohli","phone":"9891678130","email":"reeti.ch@gmail.com"},
        {"name":"sudbela","phone":"","email":"sudbela@gmail.com"},
        {"name":"Naomi Gonsalves Pereira","phone":"7798321326","email":"naomator@gmail.com"},
        {"name":"brevathi870","phone":"","email":"brevathi870@gmail.com"},
        {"name":"Yatin Mistry","phone":"9699921292","email":"yatin.mistry4u@icloud.com"},
        {"name":"pallavi trehan ","phone":"9958481884","email":"pallavi6584@Gmail.con"},
        {"name":"swati.katyal","phone":"","email":"swati.katyal@gmail.com"},
        {"name":"Niraj Agrawal","phone":"9904168800","email":"agrawal.niraj@hotmail.com"},
        {"name":"Anna Schwarz","phone":"9740600344","email":"aaa.schwarz@gmail.com"},
        {"name":"mugdha kalra","phone":"7760171775","email":"mugdhakalra@gmail.com"},
        {"name":"Sanjeev ","phone":"9891582062","email":"Viney30m@gmail.com"},
        {"name":"Ritika Talwar Singh","phone":"9814977700","email":"ritikakaramsingh@gmail.com"},
        {"name":"Shreya ","phone":"9711995777","email":"shreyanarula1420@gmail.com"},
        {"name":"malkarroma","phone":"","email":"malkarroma@gmail.com"},
        {"name":"aishgoyal","phone":"","email":"aishgoyal@yahoo.com"},
        {"name":"Urvashi Matta","phone":"9811757816","email":"urvashimatta94@gmail.com"},
        {"name":"Subramanian ","phone":"9910100430","email":"India50@gmail.com"},
        {"name":"Rohit ","phone":"9810321213","email":"ettuvij@hotmail.com"},
        {"name":"hemluk2","phone":"","email":"hemluk2@gmail.com"},
        {"name":"Aneesh Chengappa","phone":"9501634567","email":"aneeshchengi@gmail.com"},
        {"name":"ankur1622","phone":"","email":"ankur1622@gmail.com"},
        {"name":"himani saxena","phone":"9560447798","email":"himani89saxena@gmail.com"},
        {"name":"Sanchana Menon","phone":"8008123905","email":"sanchana.murali@hotmail.com"}]

        _.each(data,function(val,index){
            try {
                //console.log(val)
                id = Accounts.createUser({
                    password: "qwerty@#143",
                    email: val.email,
                    profile: {
                        name : val.name,
                        phone : val.phone
                    }
                });
                Roles.addUsersToRoles(id, ['user'])
                Meteor.users.update({_id: id}, {
                    $set: {
                        'profile.name': val.name,
                        'profile.phone': val.phone,
                    }
                })
            }catch(e){
                console.log(e);
            }
        })
        
    }
});
