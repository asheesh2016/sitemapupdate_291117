import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
export default class UserLayout extends TrackerReact(Component) {
    constructor(){
        super();
        this.state ={
            subcription: {
                background: Meteor.subscribe('Background'),
            }
        }
         Tracker.autorun(() =>{
            if(this.state.subcription.background.ready()){
            let img = Background.findOne({"tag":"bg"});
               // Session.set("bac1",img.featuredImage)
             }
        });
    }
    backgroundContent(){
           // return Session.get("bac1");
    }
    render() {
        const divStyle = {
          //background: 'url(' + backgroundContent() + ')' + ' no-repeat',
          backgroundSize: 'cover',
          backgroundAttachment: 'fixed'
        };
        return (
            <div className="newbody" style={divStyle}>
                {this.props.header}
                <section className="innerbody_info">
                    <div className="container_info">
                        <div className="innerpage_bg">
                            <div className="innerheight_info">
                            {this.props.content}
                            </div>
                            {this.props.footer}
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export  class UserLayoutHome extends TrackerReact(Component) {
    constructor(){
        super();
        this.state ={
            subcription: {
                background: Meteor.subscribe('Background'),
            }
        }
         Tracker.autorun(() =>{
            if(this.state.subcription.background.ready()){
            let img = Background.findOne({"tag":"bg"});
                //Session.set("bac1",img.featuredImage)
             }
        });
    }
    
      backgroundContent(){
           // return Session.get("bac1");
    }
    render() {
        const divStyle = {
         // background: 'url(' + backgroundContent() + ')' + ' no-repeat',
          backgroundSize: 'cover !important',
          backgroundAttachment: 'fixed !important'
        };
        return (
            <div className="newbody homebody" style={divStyle}>
                {this.props.header}
                <section className="innerbody_info">
                    <div className="container_info">
                            {this.props.content}
                            {this.props.footer}
                    </div>
                </section>
            </div>
        )
    }
}
export  class UserFooter extends TrackerReact(Component) {
    constructor(){
        super();
        this.state = {
        }
        this.state ={
            subcription: {
                footer: Meteor.subscribe('Footer'),
            }
        }
    }
    componentWillUnmount() {
        this.state.subscription.footer.stop();
    }
    componentWillMount() {
        var self = this;
        Tracker.autorun(function () {
            FlowRouter.watchPathChange();
            if(!_.isUndefined(self.state.subscription)){
                self.state.subscription.footer.stop();
            }
            self.setState({subscription: {
                footer: Meteor.subscribe('Footer'),
            }})
        });

    }  
    componentDidMount(){
       
    }     
    footerContent(){
        return Footer.findOne({});
    }    
    render() {
        return (
            <div>
                <footer dangerouslySetInnerHTML={{__html: this.footerContent() && this.footerContent().content}}></footer>
            </div>
        )
    }
}

export  class UserHeader extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
        }
        this.search = this.search.bind(this);
    }
    carts(){
        return Carts.findOne();
    }
    showSearch(event){
        event.preventDefault();
        if($('#search').css('display') == 'none'){
            $('#search').css('display','inline-block');
        }else{
            $('#search').css('display','none');
        }
    }
    search(event){
        event.preventDefault();
        let search = $('#search').val();
        Session.set('search',search);
        FlowRouter.go('/search');
    }
    componentWillMount(){
        var self = this;
        Tracker.autorun(() => {
            if(!Meteor.userId()){
                if(Meteor.isClient){
                    if(!Meteor.userId() && Session.equals('guestId', undefined)){
                        var deviceId = amplify.store("guestId");
                       
                        if(!deviceId){
                            deviceId = Random.id();
                            amplify.store("guestId", deviceId);
                        }
                        Session.set('guestId', deviceId);
                    }
                    self.setState({
                        subscription: {
                            carts: Meteor.subscribe('Carts',Session.get('guestId'))
                        }
                    })
                }
            }else{
                if(Meteor.isClient){
                    Meteor.call('modifyCart',Session.get('guestId'),function(err){
                        if(!err){
                            Session.set('guestId',undefined)
                            self.setState({
                                subscription: {
                                    carts: Meteor.subscribe('Carts',Session.get('guestId'))
                                }
                            })
                        }
                    });
                }
            }
        })
    }
    render() {
        let link,notify,cart;
        let count = () => {
            if(_.isUndefined(this.carts())){
                return (
                    <span className="badge new_badge">0</span>
                )
            }else{
                return (
                    <span className="badge new_badge">{_.reduce(this.carts().items,function(memo,number){return memo + number.count},0)}</span>
                )
            }
        }
        if(Meteor.userId()){
            link = (
                <li>
                    <UserInfoNav />
                </li>
            );
            cart = (
                <li>
                    <a href="/cart"><img src="/client/images/cart_icon.png" alt="Wingreens Farms Cart"/>{count()}</a>
                </li>
            );
            notify = "";
        }else{
            link = (
                <li>
                    <LoginSignUp />
                </li>
            )
            cart = (
                <li>
                    <a href="/cart"><img src="/client/images/cart_icon.png" alt="Wingreens Farms Cart"/>{count()}</a>
                </li>
            )
            notify = '';
        }
        return (
            <header className="header">
                <div className="container_info">
                    <div className="header_bg">
                        <div className="row logo_row">
                            <div className="col-xs-4 header_left">
                                <div className="logo"><a href="/"><img src="/client/images/wingreen_logo.png" alt="Wingreens Farms Logo"/></a></div>
                            </div>
                            <div className="col-xs-8 header_right">
                                <div className="top_menu">
                                    <ul>
                                        <li>
                                            <form onSubmit={this.search} style={{padding:0}} className="account_form">
                                                <input type="text" className="form-control" placeholder="Search" id='search' name="search" style={{width: '300px',display: 'none',borderBottom:'1px solid #349e49',paddingTop:0}}/>
                                                <button type="button" className="btn btn-default" style={{display:'none'}}>Action</button>
                                                <a href="javascript:void(0)" style={{display: 'inline-block'}} onClick={this.showSearch.bind(this)}><img src="/client/images/search_icon.png" alt="Wingreens Farms Search"/></a>
                                            </form>
                                        </li>
                                        {link}
                                        {notify}
                                        {cart}
                                    </ul>
                                </div>
                            </div>
                            <div className="col-xs-12 menu_info">
                                <NavBar />
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}
class UserInfoNav extends Component{
    logout(event){
        event.preventDefault();
        Bert.alert('logout successful', 'success', 'growl-top-right');
        Meteor.logout();
        FlowRouter.go('/');
    }
    render(){
        return (
            <div className="dropdown">
                <a href="javascript:void(0)" className="dropdown-toggle" data-toggle="dropdown"><img src="/client/images/user_icon.png" alt="Wingreens Farms User"/></a>
                <div className="dropdown-menu">
                    <ul>
                        <li><a href="/account-information">Account Information</a></li>
                        <li><a href="/order-history">Order History</a></li>
                        <li><a href="/reviews-ratings">Reviews & Ratings</a></li>
                        <li><a href="javascript:void(0)" onClick={this.logout}>Logout</a></li>
                    </ul>
                </div>
            </div>
        )
    }
}
class LoginSignUp extends Component{
    constructor() {
        super();
        this.signUp = this.signUp.bind(this);
        this.login = this.login.bind(this);
        this.forgotpassword = this.forgotpassword.bind(this);
    }
    login(event){
        event.preventDefault();
        let email = $(event.target).find("[name=email]").val();
        let password = $(event.target).find("[name=password]").val();
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
        }
        if(!email){
            Bert.alert('Email cannot be empty', 'danger', 'growl-top-right');
        }else if(!isEmail(email)){
            Bert.alert('Email is not valid', 'danger', 'growl-top-right');
        }else if(!password){
            Bert.alert('Password cannot be empty', 'danger', 'growl-top-right');
        }else if(password.length > 25){
            Bert.alert('Password length cannot be more than 25', 'danger', 'growl-top-right');
        }else{
            Meteor.loginWithPassword(email, password, function (error) {
                if (!error) {
                    Bert.alert('Login Successful', 'success', 'growl-top-right');
                    FlowRouter.go('/');
                }else{
                    Bert.alert('Wrong Email or Password', 'danger', 'growl-top-right');
                }
            })
            $(event.target).find("[name=email]").val('');
            $(event.target).find("[name=password]").val('');
        }
    }
    loginFacebook(event){
        event.preventDefault();
        if(Meteor.isClient){
            Meteor.loginWithFacebook({requestPermissions: ['email'],loginHint:"test"}, function(error){
                if(!error){
                    Bert.alert('Login Successful', 'success', 'growl-top-right');
                    FlowRouter.go('/');
                }
            });
        }
    }
    loginGoogle(event){
        event.preventDefault();
        if(Meteor.isClient){
            Meteor.loginWithGoogle({requestPermissions: ['email'],loginHint:"test"}, function(error){
                if(!error){
                    Bert.alert('Login Successful', 'success', 'growl-top-right');
                    FlowRouter.go('/');
                }
            });
        }
    }
    signUp(event){
        event.preventDefault();
        let email = $(event.target).find("[name=email]").val();
        let password = $(event.target).find("[name=password]").val();
        let re_password = $(event.target).find("[name=re-password]").val();
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
        }
        function isPassword(password){
            var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/;
            return regex.test(password);
        }
        if(re_password != password){
            Bert.alert('Password not Matching', 'danger', 'growl-top-right');
            return false;
        }else if(!isPassword(password)){
            Bert.alert('Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character', 'danger', 'growl-top-right');
            return false;
        }else if(!email){
            Bert.alert('Email cannot be empty', 'danger', 'growl-top-right');
            return false;
        }else if(!isEmail(email)){
            Bert.alert('Email is not valid', 'danger', 'growl-top-right');
            return false;
        }else{
            data = {
                email: email,
                password: password,
            }
            Meteor.call('createAccount', data, function (error) {
                if (!error) {
                    Bert.alert('Account Created', 'success', 'growl-top-right');
                    Meteor.loginWithPassword(email, password, function (error) {
                        if (!error) {
                            Bert.alert('Login Successful', 'success', 'growl-top-right');
                            FlowRouter.go('/');
                        }
                    })
                }else{
                    Bert.alert('Email already exist', 'danger', 'growl-top-right');
                }
            });
            $(event.target).find("[name=email]").val('');
            $(event.target).find("[name=password]").val('');
            $(event.target).find("[name=re-password]").val('');
        }
        
    }
    forgot(event) {
       event.preventDefault();
       if(jQuery('.sg').css('display') == 'none'){
           jQuery('.sl').hide();
           jQuery('.sf').hide();
           jQuery('.sg').show();
       }
   }
   toggle(event){
       if(jQuery('.sl').css('display') == 'none'){
           jQuery('.sl').show();
           jQuery('.sf').hide();
           jQuery('.sg').hide();
       }else{
           jQuery('.sf').show();
           jQuery('.sl').hide();
           jQuery('.sg').hide();
       }
   }
   forgotpassword(event) {
       event.preventDefault();
       let email = $(event.target).find("[name=email1]").val();
       if(_.isEmpty(email)){
           Bert.alert('Please enter your Email Address.', 'danger', 'growl-top-right');
           return true;
       }
       data = {
           email: email
       }

        Bert.alert('Please wait we are sending reset password link', 'info', 'growl-top-right');
       Accounts.forgotPassword(data, function (error) {
           if (!error) {
               Bert.alert('Please check your Email for reset password link.', 'success', 'growl-top-right');
           }else{
               Bert.alert('Email is not registered with WinGreensFarm', 'danger', 'growl-top-right');
           }
       });
       $(event.target).find("[name=email1]").val('');
   }
    render(){      
        return (
            <div className="dropdown">
                <a href="javascript:void(0)" className="dropdown-toggle" data-toggle="dropdown"><img src="/client/images/user_icon.png" alt='Wingreens Farms User'/></a>
                <div className="dropdown-menu">
                    <form onSubmit={this.login} className="sl">
                       <ul>
                           <li><a href="javascript:void(0)">Hi, Happy shopping</a></li>
                           <li>
                               <div className="form-groups">
                                   <input type="email" className="form-control" name="email" placeholder="Email Address" />
                               </div>
                           </li>
                           <li>
                               <div className="form-groups">
                                   <input type="password" className="form-control" name="password" placeholder="Password" />
                               </div>
                           </li>
                           <li>
                               <div className="form-groups">
                                   <button type="submit" className="login_btn">Login</button>
                               </div>
                           </li>
                           <li><span>(or)</span></li>
                           <li className="btgap"><span><a href="javascript:void(0)" onClick={this.loginFacebook.bind(this)}><img src="/client/images/fbnav_icon.png" alt="Wingreens Farms Facebook"/> Login with Facebook</a></span></li>
                           <li className="btgap"><span><a href="javascript:void(0)" onClick={this.loginGoogle.bind(this)}><img src="/client/images/google_icon.png" alt="Wingreens Farms Google"/> Login with Google</a></span></li>
                           <li onClick={this.toggle.bind(this)} className="text-center"><span>Sign Up</span></li>
                           <li onClick={this.forgot.bind(this)} className="text-center"><span style={{margin:0}}>Forgot Password</span></li>
                       </ul>
                   </form>
                    <form onSubmit={this.signUp} className="sf" style={{display:'none'}}>
                       <ul>
                           <li><a href="javascript:void(0)">Hi, Happy shopping</a></li>
                           <li>
                               <div className="form-groups">
                                   <input type="email" className="form-control" name="email" placeholder="Email Address" />
                               </div>
                           </li>
                           <li>
                               <div className="form-groups">
                                   <input type="password" className="form-control" name="password" placeholder="Password" />
                               </div>
                           </li>
                           <li>
                               <div className="form-groups">
                                   <input type="password" className="form-control" name="re-password" placeholder="Confirm Password" />
                               </div>
                           </li>
                           <li>
                               <div className="form-groups">
                                   <button type="submit" className="signup_btn">Sign Up</button>
                               </div>
                           </li>
                           <li><span>(or)</span></li>
                           <li className="btgap"><span><a href="javascript:void(0)" onClick={this.loginFacebook.bind(this)}><img src="/client/images/fbnav_icon.png"  alt="Wingreens Farms Facebook"/> SignUp with Facebook</a></span></li>
                           <li className="btgap"><span><a href="javascript:void(0)" onClick={this.loginGoogle.bind(this)}><img src="/client/images/google_icon.png" alt="Wingreens Farms Google"/> SignUp with Google</a></span></li>
                           <li onClick={this.toggle.bind(this)} className="text-center"><span>Sign In</span></li>
                           <li onClick={this.forgot.bind(this)} className="text-center"><span style={{margin:0}}>Forgot Password</span></li>                            
                       </ul>
                   </form>
                   <form onSubmit={this.forgotpassword} className="sg" style={{display:'none'}}>
                       <ul>
                           <li><a href="javascript:void(0)">Hi, Happy shopping</a></li>
                           <li>
                               <div className="form-groups">
                                   <input type="email" className="form-control" name="email1" placeholder="Email Address" />
                               </div>
                           </li>
                           <li>
                               <div className="form-groups">
                                   <button type="submit" className="signup_btn">Submit</button>
                               </div>
                           </li>
                           <li><span>(or)</span></li>
                           <li className="btgap"><span><a href="javascript:void(0)" onClick={this.loginFacebook.bind(this)}><img src="/client/images/fbnav_icon.png"  alt="Wingreens Farms Facebook"/> SignUp with Facebook</a></span></li>
                           <li className="btgap"><span><a href="javascript:void(0)" onClick={this.loginGoogle.bind(this)}><img src="/client/images/google_icon.png"  alt="Wingreens Farms Google"/> SignUp with Google</a></span></li>
                           <li onClick={this.toggle.bind(this)} className="text-center"><span>Sign In</span></li>
                           <li onClick={this.toggle.bind(this)} className="text-center"><span>Sign Up</span></li>
                       </ul>
                   </form>
                </div>
            </div>
        )
    }
}
class NavBar extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            subscription: {
                subcat: Meteor.subscribe('SubCategoriesHome'),
                categories: Meteor.subscribe('Categories'),
                header: Meteor.subscribe('Header'),  
                sliders:Meteor.subscribe('Sliders'),              
            }
        }
    }
    header() {
        return Header.find({}).fetch()
    }
    sub(slug){
        cat = Categories.findOne({slug:slug});
        if(!_.isUndefined(cat)){
            return SubCategories.find({cat_id:cat._id}).fetch();
        }else{
            return []
        }
    }
    displayNone(){
        try{
            jQuery('#navbar').removeClass('in');
        }catch(e){
            //console.log("jQuery in server");
        }
    }
    render(){
        let subcat = (slug) =>{
            var subl = this.sub(slug); 
            if(!_.isUndefined(subl)){
                if(!_.isEmpty(subl)){
                    return (
                        <ul>
                        {subl.map(function(val){
                            return <li key={val._id}><a href={'/subcat/'+val.slug}>{val.name}</a></li> 
                        })}
                        </ul>
                    )
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }
        return (
            <nav className="navbar navbar-defaults navbar-static-top menu_bg">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" className="navbar-collapse collapse">
                    <ul className="nav navbar-nav">
                        {this.header().map((val) => {
                            return (
                                <li key={val._id}>
                                    <a href={val.type == 'cat' ? '/category/'+val.slug:'/'+val.slug} onClick={this.displayNone()}>{val.name}</a>
                                    { val.type == 'cat' ? subcat(val.slug):''}
                                </li>
                            )
                        })}
                    </ul>
                </div>
            </nav>
        )
    }
}