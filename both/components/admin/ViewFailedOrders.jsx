import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import DatePicker from 'react-datepicker';
export default class ViewFailedOrders extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            q : '',
            startDate : moment('2016-06-01'),
            endDate : moment(),
            subscription: {
                OrderHistoryFailedAdmin : Meteor.subscribe('OrderHistoryFailedAdmin'),
            }
        }
        this.q = this.q.bind(this);
    }
    orders(){
        if(this.state.q == ''){
            return Orders.find({status : 0},{sort:{createdAt:-1}}).fetch()
        }else{
            var regExp = this.buildRegExp(this.state.q);
            return Orders.find({$or: [
                    {email: regExp},
                    {name: regExp},
                    {_id: regExp},
                    {phone: regExp},
                    {couponcode : regExp}
                ],status : 0},{sort:{createdAt:-1}}).fetch()
        }     
    }
    componentWillUnmount(){
        this.state.subscription.OrderHistoryFailedAdmin.stop();
    }
    componentDidMount(){
    
    }
    viewOrder(id){
        FlowRouter.go('/admin/view-order/'+id);
    }
    buildRegExp(searchText) {
      var words = searchText.trim().split(/[ \-\:]+/);
      var exps = _.map(words, function(word) {
        return "(?=.*" + word + ")";
      });
      var fullExp = exps.join('') + ".+";
      return new RegExp(fullExp, "i");
    }
    deleteOrder(id){
        //console.log(id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('deleteOrder',id,function (error) {
                    if(!error){
                        swal("Deleted!", "Your Order has been deleted.", "success");
                    }
                });
            } else {
                swal("Cancelled", "Your Order is safe :)", "error");
            }
        });
    }
    q(event){
        this.setState({q:event.target.value});
    }
    print(){
        window.print();
    }
    render() {
        DocHead.setTitle('Order Failed List');
        let date = (createdAt) => {
            return moment(createdAt).format('DD-MM-YYYY hh:mm');
        }
        let status = (status) => {
            if(status == 0){
                return (
                    <span className="label label-info">In Complete</span>
                );
            }
        }
        let type = (type) =>{
            if(type == '0'){
                return 'Flat Discount';
            }else if(type == '1'){
                return 'X % of cart total';
            }else{
                return '';
            }
        }
        return (
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <div className="box">
                            <div className="box-body" id="dvData">
                                <div className="input-group" style={{'marginBottom':'20px'}}>
                                  <input type="text" name="q" className="form-control" placeholder="Search..." value={this.state.q} onChange={this.q}/>
                                      <span className="input-group-btn">
                                        <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
                                        </button>
                                      </span>
                                </div>
                                <table className="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th style={{width: '10px'}}>Order ID</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Total</th>
                                        <th>Shipping Charge</th>
                                        <th>Discount</th>
                                        <th>Coupon Code</th>
                                        <th>Coupon Type</th>
                                        <th style={{display:'none'}}>Address</th>
                                        <th style={{display:'none'}}>City</th>
                                        <th style={{display:'none'}}>State</th>
                                        <th style={{display:'none'}}>Country</th>
                                        <th style={{display:'none'}}>Zipcode</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th style={{width: '70px'}}>Action</th>
                                    </tr>
                                    {this.orders().map((val)=>{
                                        return (
                                            <tr key={val._id}>
                                                <td><a href="javascript:;" onClick={this.viewOrder.bind(this,val._id)}>{val._id}</a></td>
                                                <td>{val.name}</td>
                                                <td>{val.phone}</td>
                                                <td>{val.email}</td>
                                                <td>{val.total}</td>
                                                <td>{val.ship}</td>
                                                <td>{val.discount}</td>
                                                <td>{val.couponcode}</td>
                                                <td>{type(val.type)}</td>
                                                <td style={{display:'none'}}>{_.isUndefined(val.address) ? '' : val.address.address }</td>
                                                <td style={{display:'none'}}>{_.isUndefined(val.address) ? '' : val.address.city }</td>
                                                <td style={{display:'none'}}>{_.isUndefined(val.address) ? '' : val.address.state }</td>
                                                <td style={{display:'none'}}>{_.isUndefined(val.address) ? '' : val.address.country }</td>
                                                <td style={{display:'none'}}>{_.isUndefined(val.address) ? '' : val.address.zipcode }</td>
                                                <td>{status(val.status)}</td>
                                                <td>{date(val.createdAt)}</td>
                                                <td>
                                                    <button type="button" className="btn btn-primary btn-xs" style={{float:'left'}} onClick={this.viewOrder.bind(this,val._id)}>
                                                        <i className="fa fa-eye"></i>
                                                    </button>
                                                    {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="button" className="btn btn-danger btn-xs" style={{float:'right'}} onClick={this.deleteOrder.bind(this,val._id)}><i className="fa fa-trash"></i></button>) : ''}
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row no-print">
                    <div className="col-xs-12">
                        <button className="btn btn-default" onClick={() => {this.print()}} style={{marginRight:'10px'}}><i className="fa fa-print"></i> Print</button>
                    </div>
                </div>
            </section>
        )
    }
}

export {
    ViewFailedOrders
}
