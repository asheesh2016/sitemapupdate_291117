import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class AdminProfile extends Component{
    render(){
        DocHead.setTitle('Profile');
        return (
            <section className="content">
                <div className="row">
                    <EditProfile />
                    <ChangePassword />
                </div>
            </section>
        )
    }
}

class EditProfile extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            fname : '',
            lname : '',
            avatar: ''
        }
        this.fname = this.fname.bind(this);
        this.lname = this.lname.bind(this);
        this.handleSubmitProfile = this.handleSubmitProfile.bind(this);
    }
    fname(event){
        this.setState({fname: event.target.value});
    }
    lname(event){
        this.setState({lname: event.target.value});
    }
    componentWillMount(){
        this.setState({fname: Meteor.user() && Meteor.user().profile.fname});
        this.setState({lname: Meteor.user() && Meteor.user().profile.lname});
    }
    handleSubmitProfile(event){
        event.preventDefault();
        let uploader = new Slingshot.Upload("myDefinedDirective");
        if($(event.target).find("[name=avatar]")[0].files[0]){
            uploader.send($(event.target).find("[name=avatar]")[0].files[0], (error, downloadUrl) => {
                if (error) {
                    Bert.alert('Error uploading', 'danger', 'growl-top-right');
                    data = {
                        fname: this.state.fname,
                        lname: this.state.lname
                    }
                    Meteor.call('updateProfile',data,function (error) {
                        if(error){
                            Bert.alert('Error Updating', 'danger', 'growl-top-right');
                        }else{
                            Bert.alert('Updated first name and last name', 'success', 'growl-top-right');
                        }
                    })
                }
                else {
                    this.setState({avatar: downloadUrl});
                    data = {
                        fname: this.state.fname,
                        lname: this.state.lname,
                        avatar: this.state.avatar
                    }
                    Meteor.call('updateProfile',data,function (error) {
                        if(error){
                            Bert.alert('Error Updating', 'danger', 'growl-top-right');
                        }else{
                            Bert.alert('Profile Updated', 'success', 'growl-top-right');
                        }
                    })
                }
            });
        }else{
            data = {
                fname: this.state.fname,
                lname: this.state.lname
            }
            Meteor.call('updateProfile',data,function (error) {
                if(error){
                    Bert.alert('Error Updating', 'danger', 'growl-top-right');
                }else{
                    Bert.alert('Profile Updated', 'success', 'growl-top-right');
                }
            })
        }
    }
    render(){
        let avatar;
        if(this.state.avatar){
            avatar = (
                <img src={this.state.avatar} />
            )
        }else{
            if(!_.isUndefined(Meteor.user())){
                if(!_.isUndefined(Meteor.user() && Meteor.user().profile.avatar)){
                    avatar = (
                        <img src={Meteor.user() && Meteor.user().profile.avatar} />
                    )
                }else{
                    avatar = "";
                }
            }else{
                avatar = "";
            }
        }
        return (
            <div className="col-md-6">
                <div className="box box-primary">
                    <div className="box-header with-border">
                        <h3 className="box-title">Edit Profile</h3>
                    </div>
                    <form onSubmit={this.handleSubmitProfile}>
                        <div className="box-body">
                            <div className="form-group">
                                <label>First Name</label>
                                <input
                                    className="form-control"
                                    type="text"
                                    placeholder="First Name"
                                    name="fname"
                                    value={this.state.fname}
                                    onChange={this.fname}
                                />
                            </div>
                            <div className="form-group">
                                <label>Last Name</label>
                                <input
                                    className="form-control"
                                    type="text"
                                    placeholder="Last Name"
                                    name="lname"
                                    value={this.state.lname}
                                    onChange={this.lname}
                                />
                            </div>
                            <div className="form-group">
                                <label>Avatar</label>
                                <input type="file" name="avatar"/>
                                {avatar}
                            </div>
                        </div>
                        <div className="box-footer">
                            <div className="form-group">
                                <button className="btn btn-primary">Edit Profile</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

class ChangePassword extends Component{
    constructor() {
        super();
        this.handleSubmitChangePasword = this.handleSubmitChangePasword.bind(this);
    }
    handleSubmitChangePasword(event){
        event.preventDefault();
        var cpass = $(event.target).find("[name=cpass]").val(),
            npass = $(event.target).find("[name=npass]").val(),
            repass = $(event.target).find("[name=repass]").val();

        if(_.isEmpty(cpass) || _.isEmpty(npass) || _.isEmpty(repass)){
            Bert.alert('Fields not be empty', 'danger', 'growl-top-right');
            return true;
        }
        if(npass.length < 6){
            Bert.alert('Password Length must be more than 6 and equal to 6', 'danger', 'growl-top-right');
            return true;
        }
        if(npass != repass){
            Bert.alert('Confirm Password is not Matching', 'danger', 'growl-top-right');
            return true;
        }
        Accounts.changePassword(cpass, npass, function (error) {
            if(error){
                Bert.alert('Current Password is Wrong', 'danger', 'growl-top-right');
            }else{
                Bert.alert('Password changed Successfully ', 'success', 'growl-top-right');
            }
        })
        $(event.target).find("[name=cpass]").val('');
        $(event.target).find("[name=npass]").val('');
        $(event.target).find("[name=repass]").val('');
    }
    render(){
        return (
            <div className="col-md-6">
                <div className="box box-primary">
                    <div className="box-header with-border">
                        <h3 className="box-title">Change Password</h3>
                    </div>
                    <form onSubmit={this.handleSubmitChangePasword}>
                        <div className="box-body">
                            <div className="form-group">
                                <label>Current Password</label>
                                <input
                                    className="form-control"
                                    type="password"
                                    placeholder="Current Password"
                                    name="cpass"
                                />
                            </div>
                            <div className="form-group">
                                <label>New Password</label>
                                <input
                                    className="form-control"
                                    type="password"
                                    placeholder="New Password"
                                    name="npass"
                                />
                            </div>
                            <div className="form-group">
                                <label>Confirm Password</label>
                                <input
                                    className="form-control"
                                    type="password"
                                    placeholder="Confirm Password"
                                    name="repass"
                                />
                            </div>
                        </div>
                        <div className="box-footer">
                            <div className="form-group">
                                <button className="btn btn-primary">Change Password</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}
export {
    AdminProfile
}
