import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import Dropzone from 'react-dropzone';
export default class EditProduct extends TrackerReact(Component) {
    
    constructor() {
        super();
        this.state = {
            slug: '',
            slugAvailable : true,
            quantity: 0,
            price: 0,
            status : "1",
            title: '',
            seotitle: '',
            content: '',
            excerpt: '',
            featuredImage: '',
            images: [],
            sku: '',
            weight: '',
            note: '',
            meta_title: '',
            meta_description : '',
            meta_keyword : '',
            selectedCat: [],
            selectedSubCat: [],
            categories: [],
            subcategories: [],
            catRadios: '',
            subcatRadios: '',
            subscription: {
                categories: Meteor.subscribe('Categories'),
                SubCategoriesHome : Meteor.subscribe('SubCategoriesHome'),
                singleProduct: Meteor.subscribe('SingleProduct',FlowRouter.getParam('id'))
            }
        };
        this.title = this.title.bind(this);
        this.seotitle = this.seotitle.bind(this);
        this.status = this.status.bind(this);
        this.pslug = this.pslug.bind(this);
        this.excerpt = this.excerpt.bind(this);
        this.sku =  this.sku.bind(this);
        this.quantity = this.quantity.bind(this);
        this.price = this.price.bind(this);
        this.weight = this.weight.bind(this);
        this.note = this.note.bind(this);
        this.catRadios =  this.catRadios.bind(this);
        this.subcatRadios = this.subcatRadios.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onDropSingle = this.onDropSingle.bind(this);
        this.onDrop = this.onDrop.bind(this);
        this.meta_title = this.meta_title.bind(this);
        this.meta_description = this.meta_description.bind(this);
        this.meta_keyword = this.meta_keyword.bind(this);
    }
    categories() {
        return Categories.find({}, {sort: {createdAt: 1}}).fetch()
    }
    subcategories(id) {
        return SubCategories.find({cat_id:id},{sort: {createdAt:1}}).fetch()
    }
    singleProduct(){
        return Products.findOne(FlowRouter.getParam('id'));
    }
    productSlug(){
        return Products.find({}).fetch();
    }
    categoryslug(product){
        pdata = Products.findOne({_id:product});
        id = "";
        if(pdata){
            id = pdata.category[0];
        }else{
            id = "";
        }
        if(pdata){
            sid = pdata.subcategory[0];
            if(sid){
                if(!_.isUndefined(SubCategories.findOne({_id:sid}))){
                    
                    if(id){
                    return Categories.findOne({_id:id}).slug +'/'+ SubCategories.findOne({_id:sid}).slug+'/';
                    }else{
                    return "";
                    }
                   
                }else{
                    if(id){
                    return Categories.findOne({_id:id}).slug +'/';
                    }else{
                    return "";
                    }
                }
            }else{
                if(id){
                return Categories.findOne({_id:id}).slug + '/';
               }else{
               return "";
               }
            }
        }else{
            return "";
        }
    }
    componentWillMount(){
        var self = this;
        Tracker.autorun(() =>{
            if(!_.isUndefined(self.singleProduct())){
                try{
                    //console.log(self.singleProduct())
                    this.setState({
                        title : this.singleProduct() && this.singleProduct().title,
                        seotitle : this.singleProduct() && this.singleProduct().seotitle,
                        pslug : this.singleProduct() && this.singleProduct().title,
                        slug : this.singleProduct() && this.singleProduct().slug,
                        status : this.singleProduct() && this.singleProduct().status,
                        excerpt : this.singleProduct() && this.singleProduct().excerpt,
                        content : this.singleProduct() && this.singleProduct().content,
                        quantity : this.singleProduct() && this.singleProduct().quantity,
                        price : this.singleProduct() && this.singleProduct().price,
                        featuredImage : this.singleProduct() && this.singleProduct().featuredImage,
                        images : this.singleProduct() && this.singleProduct().images,
                        weight : this.singleProduct() && this.singleProduct().weight,
                        selectedCat : this.singleProduct() && this.singleProduct().category,
                        selectedSubCat : !_.isUndefined(this.singleProduct() && this.singleProduct().subcategory) ? this.singleProduct() && this.singleProduct().subcategory:'',
                        note : this.singleProduct() && this.singleProduct().note,
                        sku : this.singleProduct() && this.singleProduct().sku,
                        meta_title : !_.isUndefined(this.singleProduct() && this.singleProduct().meta_title) ? this.singleProduct() && this.singleProduct().meta_title:'',
                        meta_description : !_.isUndefined(this.singleProduct() && this.singleProduct().meta_description) ? this.singleProduct() && this.singleProduct().meta_description:'',
                        meta_keyword : !_.isUndefined(this.singleProduct() && this.singleProduct().meta_keyword) ? this.singleProduct() && this.singleProduct().meta_keyword:''
                    });
                    try {
                        jQuery(document).ready(function () {
            
                            let mysummer = jQuery('#product-content');
                            mysummer.summernote({
                                height: 300,                 // set editor height
                                minHeight: null,             // set minimum height of editor
                                maxHeight: null,             // set maximum height of editor
                                focus: false,                  // set focus to editable area after initializing summernote
                            });
                            mysummer.summernote('destroy')
                            mysummer.summernote('enable')
                        });
                    }catch(e){
                        console.log(e);
                    }
                }catch(e){
                    console.log(e);
                }
            }
        });
        //console.log(this.state.title);
    }
    componentWillUnmount(){
        this.state.subscription.SubCategoriesHome.stop();
        this.state.subscription.categories.stop();
        this.state.subscription.singleProduct.stop();
    }
    componentDidMount() {
        let self = this;
        //if(this.singleProduct()){
       
        jQuery(document).ready(function(){
            jQuery('#product-excerpt').textcounter({
                type: "character",
                max: 160,
                countDown: true,
                stopInputAtMaximum: true
            });
        });
                
        //}
    }
    title(event) {
        let convertToSlug = (Text) => {
            return Text
                .toLowerCase()
                .replace(/ /g, '-')
                .replace(/[^\w-]+/g, '');
        }
        // if (event.target.value) {
        //     this.setState({slug: convertToSlug(event.target.value)});
        //     this.setState({pslug: event.target.value});
        //     this.setState({slugAvailable : !_.contains(_.pluck(this.productSlug(),'slug'),this.state.slug)});
        // } else {
        //     this.setState({pslug: ''});
        //     this.setState({slug: ''});
        //     this.setState({slugAvailable : !_.contains(_.pluck(this.productSlug(),'slug'),this.state.slug)});
        // }
        //this.setState({pslug: event.target.value});
       // this.setState({slug: convertToSlug(event.target.value)});
        this.setState({title: event.target.value});
       // this.setState({slugAvailable : !_.contains(_.pluck(this.productSlug(),'slug'),convertToSlug(event.target.value))});
    }
    pslug(event){
        let convertToSlug = (Text) => {
            return Text
                .toLowerCase()
                .replace(/ /g, '-')
                .replace(/[^\w-]+/g, '');
        }
        if(event.target.value){
            this.setState({slug: convertToSlug(event.target.value)});
        }else{
            this.setState({slug: ''});
        }
        this.setState({slugAvailable : !_.contains(_.pluck(this.productSlug(),'slug'),this.state.slug)});
        this.setState({pslug: event.target.value});
    }
    seotitle(event){
        this.setState({seotitle:event.target.value});
    }
    sku(event){
        this.setState({sku:event.target.value});
    }
    status(event){
        this.setState({status:event.target.value});
    }
    excerpt(event) {
        this.setState({excerpt: event.target.value});
    }
    quantity(event) {
        this.setState({quantity: event.target.value})
    }
    price(event) {
        this.setState({price: event.target.value})
    }
    weight(event){
        this.setState({weight: event.target.value});
    }
    note(event){
        this.setState({note: event.target.value});
    }
    meta_title(event){
        this.setState({meta_title: event.target.value});
    }
    meta_description(event){
        this.setState({meta_description: event.target.value});
    }
    meta_keyword(event){
        this.setState({meta_keyword: event.target.value});
    }
    handleSubmit(event) {
        event.preventDefault();
        if(_.isEmpty(this.state.title)){
            Bert.alert('Please enter the title', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.seotitle)){
            Bert.alert('Please enter the SEO title', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.excerpt)){
            Bert.alert('Please enter the excerpt', 'danger', 'growl-top-right');
            return false;
        }
        if(!this.state.slugAvailable){
            Bert.alert('Please slug is already exist', 'danger', 'growl-top-right');
            return false;
        }
         if(_.isEmpty(this.state.selectedCat)){
            Bert.alert('Please select Category', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.featuredImage)){
            Bert.alert('Please enter the featured Image', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.weight)){
            Bert.alert('Please enter the weight', 'danger', 'growl-top-right');
            return false;
        }
        data = {
            id : FlowRouter.current().params.id,
            set : {
                title : this.state.title,
                seotitle : this.state.seotitle,
                slug : this.state.slug,
                status : this.state.status,
                excerpt : this.state.excerpt,
                content : $("#product-content").summernote('code'),
                quantity : parseInt(this.state.quantity),
                price : Number(this.state.price),
                featuredImage : this.state.featuredImage,
                sku : this.state.sku,
                weight : this.state.weight,
                note : this.state.note,
                category : this.state.selectedCat,
                subcategory : this.state.selectedSubCat,
                images : this.state.images,
                meta_title  : this.state.meta_title,
                meta_description : this.state.meta_description,
                meta_keyword : this.state.meta_keyword,
            }
        }
        Meteor.call('EditProduct',data,function (error) {
            if(!error){
                FlowRouter.go('/admin/view-products');
            }else{
                Bert.alert('Product add problem', 'danger', 'growl-top-right');
            }
        })
    }
    catRadios(event){
        if(typeof this.state.selectedCat != 'string'){
            var cat = this.state.selectedCat;
            if(!_.contains(this.state.selectedCat,event.target.value)){
                cat.push(event.target.value);
            }else{
                cat = _.without(this.state.selectedCat,event.target.value);
            }
            this.setState({selectedCat: cat});
        }else{
            this.setState({selectedCat: []});
            var cat = this.state.selectedCat;
            if(!_.contains(this.state.selectedCat,event.target.value)){
                cat.push(event.target.value);
            }else{
                cat = _.without(this.state.selectedCat,event.target.value);
            }
            this.setState({selectedCat: cat});
        }
        //console.log(cat);
        
    }
    subcatRadios(event){
        if(typeof this.state.selectedSubCat != 'string'){
            var subcat = this.state.selectedSubCat;
            if(!_.contains(this.state.selectedSubCat,event.target.value)){
                subcat.push(event.target.value);
            }else{
                subcat = _.without(this.state.selectedSubCat,event.target.value);
            }
            this.setState({selectedSubCat: subcat});
        }else{
            this.setState({selectedSubCat: []});
            var subcat = this.state.selectedSubCat;
            if(!_.contains(this.state.selectedSubCat,event.target.value)){
                subcat.push(event.target.value);
            }else{
                subcat = _.without(this.state.selectedSubCat,event.target.value);
            }
            this.setState({selectedSubCat: subcat});
        }
        //console.log(subcat);
        
    }
    onDropSingle(files){
        let self = this;
        if(files[0].size/1024 > 200){
            Bert.alert('File Size is more than 200KB', 'danger', 'growl-top-right');
        }else{
            let uploader = new Slingshot.Upload("myDefinedDirective");
            uploader.send(files[0], (error, downloadUrl) => {
                if (error) {
                    Bert.alert('Error in file upload', 'danger', 'growl-top-right');
                }
                else {
                    console.log(downloadUrl)
                    self.setState({featuredImage: downloadUrl});
                }
            });
        }
    }
    onDrop(files){
        let self = this;
        console.log('files',files);
        _.each(files,function (file) {
            if(file.size/1024 > 200){
                Bert.alert('File Size is more than 200KB', 'danger', 'growl-top-right');
            }else{
                let uploader = new Slingshot.Upload("myDefinedDirective");
                uploader.send(file, (error, downloadUrl) => {
                    if (error) {
                        Bert.alert('Error in file upload', 'danger', 'growl-top-right');
                    }
                    else {
                        console.log(downloadUrl)
                        let images = self.state.images;
                        images.push(downloadUrl);
                        self.setState({images: images});
                    }
                });
            }
        })
    }
    removeImage(val){
        let images = this.state.images;
        // Find and remove item from an array
        var i = images.indexOf(val);
        if(i != -1) {
            images.splice(i, 1);
        }
        this.setState({images: images});
    }
    removeImageFeatured(){
        this.setState({featuredImage: ''});
    }

    render() {
        DocHead.setTitle('Product Edit');
        let slug,featureImageBox;
        if (this.state.slug) {
            if(this.state.slugAvailable){
                slug = (
                    <div className="form-group">
                        <label>Slug</label>
                        <input type="text" className="form-control" id="product-slug" value={this.state.pslug}
                               onChange={this.pslug}/>
                        <a href={Meteor.absoluteUrl()+'product/'+this.categoryslug(FlowRouter.getParam('id'))+this.state.slug}
                           style={{display:'block',color:'blue'}}>{Meteor.absoluteUrl() + 'product/' +this.categoryslug(FlowRouter.getParam('id'))+ this.state.slug}</a>
                    </div>
                )
            }else{
                slug = (
                    <div className="form-group">
                        <label>Slug</label>
                        <input type="text" className="form-control" id="product-slug" value={this.state.pslug}
                               onChange={this.pslug}/>
                        <a href={Meteor.absoluteUrl()+'product/'+this.categoryslug(FlowRouter.getParam('id'))+this.state.slug}
                           style={{display:'block',color:'red'}}>{Meteor.absoluteUrl() + 'product/' +this.categoryslug(FlowRouter.getParam('id'))+this.state.slug}</a>
                    </div>
                )
            }
        } else {
            slug = ''
        }
        if(this.state.featuredImage){
            featureImageBox = (
                <div className="col-sm-12" style={{position:'relative'}}>
                    <img className="img-responsive" src={this.state.featuredImage} alt="Photo" />
                    <button type="button" className="btn btn-danger btn-xs" style={{position:'absolute',top:5,right:20}} onClick={this.removeImageFeatured.bind(this)}><i className="fa fa-trash"></i></button>
                </div>
            )
        }else{
            featureImageBox = (
                <Dropzone onDrop={this.onDropSingle} multiple={false} accept="image/*" style={{border:'none',height:200,width:'100%',background:'#ccc',display:'flex',alignItems:'center',justifyContent:'center'}}>
                    <div style={{color:'#fff',textAlign:'center'}}>Try dropping some file here, or click to select file to upload.</div>
                </Dropzone>
            )
        }
        let imageBox;
        if(this.state.images){
            imageBox = this.state.images.map((val) => {
                                                return (
                                                <div key={val} className="col-sm-3" style={{position:'relative'}}>
                                                    <img className="img-responsive" src={val} alt="Photo" />
                                                    <button type="button" className="btn btn-danger btn-xs" style={{position:'absolute',top:5,right:20}} onClick={this.removeImage.bind(this,val)}><i className="fa fa-trash"></i></button>
                                                </div>
                                            )
                                        })
            
        }else{
            imageBox = null;
        }
        return (
            <section className="content">
                <div className="row">
                    <form role="form" onSubmit={this.handleSubmit}>
                        <div className="col-md-9">
                            <div className="box box-primary">
                                <div className="box-body">
                                    <div className="form-group">
                                        <label> Product Title </label>
                                        <input type="text" className="form-control" id="product-title"
                                               value={this.state.title} onChange={this.title}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Product SEO Title </label>
                                        <input type="text" className="form-control" id="product-seotitle" 
                                               value={this.state.seotitle} onChange={this.seotitle}/>
                                    </div>
                                    {slug}
                                    <div className="form-group">
                                        <label> Product Content </label>
                                        <textarea rows="10" value={this.state.content} className="form-control" id="product-content"
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label> Product Excerpt </label>
                                        <textarea rows="4" value={this.state.excerpt} className="form-control" id="product-excerpt"
                                                  onChange={this.excerpt}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Product Image (image dimensions 300px x 300px) </label>
                                        <div className="row margin-bottom">
                                            {imageBox}
                                            <div className="col-sm-3">
                                                <Dropzone onDrop={this.onDrop} accept="image/*" style={{border:'none',height:200,width:'100%',background:'#ccc',display:'flex',alignItems:'center',justifyContent:'center'}}>
                                                    <div style={{color:'#fff',textAlign:'center'}}>Try dropping some files here, or click to select files to upload.</div>
                                                </Dropzone>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Title </label>
                                        <input type="text" value={this.state.meta_title} className="form-control" id="meta_title"
                                               onChange={this.meta_title}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Description </label>
                                        <textarea rows="10" value={this.state.meta_description} className="form-control" id="meta_description"
                                               onChange={this.meta_description}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Keyword </label>
                                        <input type="text" value={this.state.meta_keyword} className="form-control" id="meta_keyword"
                                               onChange={this.meta_keyword}/>
                                    </div>
                                </div>
                                <div className="box-footer">
                                    <div className="form-group">
                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary"> Update Product</button>) : '' }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="box box-primary">
                                <div className="box-body">
                                    <div className="form-group">
                                        <label> Product Status </label>
                                        <select className="form-control" id="product-status" value={this.state.status} onChange={this.status}>
                                            <option value="1" >Active</option>
                                            <option value="0" >In Active</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label> Product Price </label>
                                        <input type="text" className="form-control" id="product-price"
                                            value={this.state.price} onChange={this.price}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Product SKU </label>
                                        <input type="text" value={this.state.sku} className="form-control" id="product-sku"
                                               onChange={this.sku}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Product Weight </label>
                                        <input type="text" value={this.state.weight} className="form-control" id="product-weight"
                                               onChange={this.weight}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Product Note </label>
                                        <input type="text" value={this.state.note} className="form-control" id="product-note"
                                               onChange={this.note}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Product Quantity </label>
                                        <input type="number" className="form-control" id="product-quantity" min="0"
                                               value={this.state.quantity} onChange={this.quantity}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Categories </label>
                                        {this.categories().map((val) => {
                                            return (
                                                <div className="radio" key={val._id}>
                                                    <div className="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="catRadios" value={val._id} checked={_.contains(this.state.selectedCat,val._id) ? 'checked':''} onChange={this.catRadios}/>
                                                            {val.name}
                                                        </label>
                                                    </div>
                                                    <div className="form-group" style={{'marginLeft': '20px'}}>
                                                        {this.subcategories(val._id).map((val1) => {
                                                            return (
                                                                <div className="radio" key={val1._id}>
                                                                    <label>
                                                                        <input style={{'marginRight':'10px'}} type="checkbox" name="subcatRadios" value={val1._id} checked={_.contains(this.state.selectedSubCat,val1._id) ? 'checked':''} onChange={this.subcatRadios}/>
                                                                        {val1.name}
                                                                    </label>
                                                                </div>
                                                            )
                                                        })}
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                    <div className="form-group">
                                        <label> Product Featured Image (image dimensions 300px x 300px)</label>
                                        {featureImageBox}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        )
    }
}

export {
    EditProduct
}
