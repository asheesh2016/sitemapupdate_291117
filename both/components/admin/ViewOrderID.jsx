import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class ViewOrderID extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            subscription: {
                OrderHistoryAdminWithID : Meteor.subscribe('OrderHistoryAdminWithID',FlowRouter.current().params.id),
            }
        }
        this.tracking_mumber = this.tracking_mumber.bind(this);
        this.tracking_mumber_slug = this.tracking_mumber_slug.bind(this);
        this.changeStatus = this.changeStatus.bind(this);
        this.changename = this.changename.bind(this);
        this.changephone = this.changephone.bind(this);
        this.changeemail = this.changeemail.bind(this);
        this.changeaddress = this.changeaddress.bind(this);
        this.changecity = this.changecity.bind(this);
        this.changestate = this.changestate.bind(this);
        this.changecountry = this.changecountry.bind(this);
        this.changezipcode = this.changezipcode.bind(this);
    }
    orders(){
        return Orders.findOne({_id:FlowRouter.current().params.id})
    }
    product(id){
        return Products.findOne(id);
    }
    tracking_mumber(event){
        this.setState({tracking_mumber:event.target.value});
    }
    tracking_mumber_slug(event){
        this.setState({tracking_mumber_slug : event.target.value});
    }
    changeStatus(event){
        this.setState({status:event.target.value});
    }
    changename(event){
        this.setState({name:event.target.value});
    }
    changephone(event){
        this.setState({phone:event.target.value});
    }
    changeemail(event){
        this.setState({email:event.target.value});
    }
    changeaddress(event){
        this.setState({address:event.target.value});
    }
    changecity(event){
        this.setState({city:event.target.value});
    }
    changestate(event){
        this.setState({state:event.target.value});
    }
    changecountry(event){
        this.setState({country:event.target.value});
    }
    changezipcode(event){
        this.setState({zipcode:event.target.value});
    }
    componentWillUnmount(){
        this.state.subscription.OrderHistoryAdminWithID.stop();
    }
    componentDidMount(){
        $(document).ready(function () {
            function exportTableToCSV($table, filename) {
                var $headers = $table.find('tr:has(th)')
                    ,$rows = $table.find('tr:has(td)')

                    // Temporary delimiter characters unlikely to be typed by keyboard
                    // This is to avoid accidentally splitting the actual contents
                    ,tmpColDelim = String.fromCharCode(11) // vertical tab character
                    ,tmpRowDelim = String.fromCharCode(0) // null character

                    // actual delimiter characters for CSV format
                    ,colDelim = '","'
                    ,rowDelim = '"\r\n"';

                    // Grab text from table into CSV formatted string
                    var csv = '"';
                    csv += formatRows($headers.map(grabRow));
                    csv += rowDelim;
                    csv += formatRows($rows.map(grabRow)) + '"';

                    // Data URI
                    var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                // For IE (tested 10+)
                if (window.navigator.msSaveOrOpenBlob) {
                    var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
                        type: "text/csv;charset=utf-8;"
                    });
                    navigator.msSaveBlob(blob, filename);
                } else {
                    $(this)
                        .attr({
                            'download': filename
                            ,'href': csvData
                            //,'target' : '_blank' //if you want it to open in a new window
                    });
                }

                //------------------------------------------------------------
                // Helper Functions 
                //------------------------------------------------------------
                // Format the output so it has the appropriate delimiters
                function formatRows(rows){
                    return rows.get().join(tmpRowDelim)
                        .split(tmpRowDelim).join(rowDelim)
                        .split(tmpColDelim).join(colDelim);
                }
                // Grab and format a row from the table
                function grabRow(i,row){
                     
                    var $row = $(row);
                    //for some reason $cols = $row.find('td') || $row.find('th') won't work...
                    var $cols = $row.find('td'); 
                    if(!$cols.length) $cols = $row.find('th');  

                    return $cols.map(grabCol)
                                .get().join(tmpColDelim);
                }
                // Grab and format a column from the table 
                function grabCol(j,col){
                    var $col = $(col),
                        $text = $col.text();

                    return $text.replace('"', '""'); // escape double quotes

                }
            }


            // This must be a hyperlink
            $("#export").click(function (event) {
                // var outputFile = 'export'
                var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
                outputFile = outputFile.replace('.csv','') + '.csv'
                 
                // CSV
                exportTableToCSV.apply(this, [$('#dvData > table'), outputFile]);
                
                // IF CSV, don't do event.preventDefault() or return false
                // We actually need this to be a typical hyperlink
            });

            $(document).on('click','#changeStatus',function(event){
                event.preventDefault();
                data = {
                    id : FlowRouter.current().params.id,
                    status : parseInt($('input[name=status]:checked').val())
                }
                console.log(data);
                swal({
                    title: "Are you sure?",
                    text: "You can change the order status!!!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, change it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                        Meteor.call('changeStatus',data,function (error) {
                            if(!error){
                                swal("Status!", "Your Order status has been changed.", "success");
                            }
                        });
                    } else {
                        swal("Cancelled", "Your Order status is not changed :)", "error");
                    }
                });
            })

            $(document).on('click','#changeToAll',function(event){
                event.preventDefault();
                function isPhone(phone){
                    var regex = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
                    return regex.test(phone);
                }
                function isFullName(name){
                    var regex = /^[A-Za-z\s]{3,30}$/;
                    return regex.test(name);
                }
                function isEmpty(str){
                    return !str.replace(/^\s+/g, '').length; // boolean (`true` if field is empty)
                }
                if(_.isEmpty($('#name').val()) || isEmpty($('#name').val())){
                    event.preventDefault();
                    Bert.alert('Please enter the valid name', 'danger', 'growl-top-right');
                    return false;
                }else if(!isFullName($('#name').val())){
                    event.preventDefault();
                    Bert.alert('Please enter the valid name', 'danger', 'growl-top-right');
                    return false;
                }
                
                if(_.isEmpty($('#email').val()) || isEmpty($('#email').val())){
                    Bert.alert('Please enter the valid email', 'danger', 'growl-top-right');
                    return false;
                }else{
                    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
                    if (!$('#email').val().match(mailformat)){
                        Bert.alert('Please enter valid email', 'danger', 'growl-top-right');
                        return false;
                    }
                }
                if(_.isEmpty($('#phone').val()) || isEmpty($('#phone').val())){
                    event.preventDefault();
                    Bert.alert('Please enter the valid phone', 'danger', 'growl-top-right');
                    return false;
                }else if(!isPhone($('#phone').val())){
                    event.preventDefault();
                    Bert.alert('Please enter the valid phone', 'danger', 'growl-top-right');
                    return false;
                }

                if(_.isEmpty($('#address').val()) || isEmpty($('#address').val())){
                    Bert.alert('Please enter the valid address', 'danger', 'growl-top-right');
                    return false;
                }
                if(_.isEmpty($('#city').val()) || !isFullName($('#city').val()) || isEmpty($('#city').val())){
                    Bert.alert('Please enter the valid city', 'danger', 'growl-top-right');
                    return false;
                }
                if(_.isEmpty($('#state').val()) || !isFullName($('#state').val()) || isEmpty($('#state').val())){
                    Bert.alert('Please enter the valid states', 'danger', 'growl-top-right');
                    return false;
                }
                if(_.isEmpty($('#country').val()) || !isFullName($('#country').val()) || isEmpty($('#country').val())){
                    Bert.alert('Please enter the valid country', 'danger', 'growl-top-right');
                    return false;
                }
                if(_.isEmpty($('#zipcode').val()) || isEmpty($('#zipcode').val())){
                    Bert.alert('Please enter the valid zipcode', 'danger', 'growl-top-right');
                    return false;
                }
                if(isNaN($('#zipcode').val()) && ($('#zipcode').val().length > 6 || $('#zipcode').val().length <6)){
                    Bert.alert('Please enter the valid zipcode', 'danger', 'growl-top-right');
                    return false;
                }
                data = {
                    id : FlowRouter.current().params.id,
                    name : $('#name').val(),
                    phone : $('#phone').val(),
                    email : $('#email').val(),
                    address : {
                        address : $('#address').val(),
                        city : $('#city').val(),
                        state : $('#state').val(),
                        country : $('#country').val(),
                        zipcode : $('#zipcode').val()
                    }
                }
                
                swal({
                    title: "Are you sure?",
                    text: "You can change the order status!!!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, change it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                        Meteor.call('changeToAll',data,function (error) {
                            if(!error){
                                swal("Status!", "Your Order status has been changed.", "success");
                            }
                        });
                    } else {
                        swal("Cancelled", "Your Order status is not changed :)", "error");
                    }
                });
            })

            $(document).on('click','#track',function(event){
                event.preventDefault();
                data = {
                    id : FlowRouter.current().params.id,
                    tracking_mumber : $('#tracking_mumber').val(),
                    tracking_mumber_slug : $('#tracking_mumber_slug').val(),
                }
                swal({
                    title: "Are you sure?",
                    text: "Are you sure tracking mumber correct!!!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, change it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                        Meteor.call('changeTacking',data,function (error,result) {
                            if(!error){
                                console.log(result);
                                swal("Status!", "Your Order Tacking has been added.", "success");
                            }else{
                                var message = error.error.response.data.meta.message;
                                swal("Cancelled", message, "error");
                                //swal("Cancelled", error.error.response.meta.message, "error");
                            }

                        });
                    } else {
                        swal("Cancelled", "Your Order status is not changed :)", "error");
                    }
                });
            })
        });
        var self = this;
        Tracker.autorun(function () {
            if(self.orders()){
                self.setState({
                    status: self.orders() && self.orders().status,
                    name : self.orders() && self.orders().name,
                    phone : self.orders() && self.orders().phone,
                    email : self.orders() && self.orders().email,
                    address : self.orders() && self.orders().address.address,
                    city : self.orders() && self.orders().address.city,
                    state : self.orders() && self.orders().address.state,
                    country : self.orders() && self.orders().address.country,
                    zipcode : self.orders() && self.orders().address.zipcode,
                    tracking_mumber : self.orders() && self.orders().tracking_mumber,
                    tracking_mumber_slug : self.orders() && self.orders().tracking_mumber_slug,
                });
            }
        })
    }
    render(){
        DocHead.setTitle('Order ID: '+FlowRouter.current().params.id);
        let type = (type) =>{
            if(type == '0'){
                return 'Flat Discount';
            }else if(type == '1'){
                return 'X % of cart total';
            }else{
                return '';
            }
        }
        return (
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <div className="box">
                            <div className="box-body" id="dvData">
                                <table className="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th style={{width: '10px'}}>ID</th>
                                            <th>Image</th>
                                            <th>Product Name</th>
                                            <th>Product Quantity</th>
                                            <th>Price</th>
                                        </tr>
                                        {this.orders() && this.orders().cart.items.map((item)=> {
                                            return (
                                                <tr key={item.id}>
                                                    <td><a href={'/admin/edit-product/'+item.id}>{item.id}</a></td>
                                                    <td><a href={'/admin/edit-product/'+item.id}><img
                                                        src={this.product(item.id) && this.product(item.id).featuredImage}
                                                        width="100"/></a></td>
                                                    <td>{this.product(item.id) && this.product(item.id).title}</td>
                                                    <td>{item.count}</td>
                                                    <td>{item.count * (this.product(item.id) && this.product(item.id).price)}</td>
                                                </tr>
                                            )
                                        })} 
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Discount :</td>
                                            <td>{this.orders() && this.orders().discount}</td>
                                        </tr>
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Coupon Code :</td>
                                            <td>{this.orders() && this.orders().couponcode}</td>
                                        </tr>
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Coupon Type :</td>
                                            <td>{type(this.orders() && this.orders().type)}</td>
                                        </tr>
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Shipping :</td>
                                            <td>{this.orders() && this.orders().ship}</td>
                                        </tr>
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Total :</td>
                                            <td>{this.orders() && this.orders().total}</td>
                                        </tr>
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Name :</td>
                                            <td>{this.orders() && this.orders().name}</td>
                                        </tr>
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Email :</td>
                                            <td>{this.orders() && this.orders().email}</td>
                                        </tr>
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Phone :</td>
                                            <td>{this.orders() && this.orders().phone}</td>
                                        </tr>
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Address :</td>
                                            <td>{this.orders() && !_.isUndefined(this.orders().address) ? this.orders().address.address:''}</td>
                                        </tr>
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>City :</td>
                                            <td>{this.orders() && !_.isUndefined(this.orders().address) ? this.orders().address.city :''}</td>
                                        </tr>
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>State :</td>
                                            <td>{this.orders() && !_.isUndefined(this.orders().address) ? this.orders().address.state :''}</td>
                                        </tr>
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Country :</td>
                                            <td>{this.orders() && !_.isUndefined(this.orders().address) ? this.orders().address.country :''}</td>
                                        </tr>
                                        <tr style={{display:'none'}}>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Zip :</td>
                                            <td>{this.orders() && !_.isUndefined(this.orders().address) ? this.orders().address.zipcode :''}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div className="row" style={{marginTop : '20px'}}>
                                    <div className="col-md-3">
                                        <p><b>Discount :</b> <span>{this.orders() && this.orders().discount}</span></p>
                                        <p><b>Coupon Code :</b> <span>{this.orders() && this.orders().couponcode}</span></p>
                                        <p><b>Coupon Type :</b> <span>{type(this.orders() && this.orders().type)}</span></p>
                                        <p><b>Shipping :</b> <span>{this.orders() && this.orders().ship}</span></p>
                                        <p><b>Total :</b> <span>{this.orders() && this.orders().total}</span></p>
                                    </div>
                                    <div className="col-md-3">
                                        <p><b>Name :</b> <br /><input name="text" className="form-control" id="name" value={this.state.name} onChange={this.changename}/></p>
                                        <p><b>Phone :</b> <br /><input name="text" className="form-control" id="phone" value={this.state.phone} onChange={this.changephone}/></p>
                                        <p><b>Email :</b> <br /><input name="text" className="form-control" id="email" value={this.state.email} onChange={this.changeemail}/></p>
                                        <p><b>Address :</b> <br /><input name="text" className="form-control" id="address" value={this.state.address} onChange={this.changeaddress}/></p>
                                        <p><b>City :</b> <br /><input name="text" className="form-control" id="city" value={this.state.city} onChange={this.changecity}/></p>
                                        <p><b>State :</b> <br /><input name="text" className="form-control" id="state" value={this.state.state} onChange={this.changestate}/></p>
                                        <p><b>Country :</b> <br /><input name="text" className="form-control" id="country" value={this.state.country} onChange={this.changecountry}/></p>
                                        <p><b>Zip :</b> <br /><input name="text" className="form-control" id="zipcode" value={this.state.zipcode} onChange={this.changezipcode}/></p>
                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" id="changeToAll" className="btn btn-primary btn-block" style={{marginBottom : '20px'}}> Update </button>) :''}
                                    </div>
                                    <div className="col-md-3">
                                        <form>
                                            <div className="form-group">
                                                <label>Order Status</label><br/>
                                                <input type="radio" name="status" value="2"  checked={this.state.status == 2} onChange={this.changeStatus}
/> Processing <br/>
                                                <input type="radio" name="status" value="3"  checked={this.state.status == 3} onChange={this.changeStatus}
/> Shipping <br/>
                                                <input type="radio" name="status" value="4"  checked={this.state.status == 4} onChange={this.changeStatus}
/> Completed <br/>
                                                <input type="radio" name="status" value="5"  checked={this.state.status == 5}onChange={this.changeStatus}
/> Cancelled <br/>
                                                {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button className="btn btn-primary btn-block" id="changeStatus">Change Status</button>) :''}
                                            </div>
                                        </form>
                                    </div>
                                    <div className="col-md-3">
                                        <p><b>Tracking Number :</b> <br /><input name="text" className="form-control" id="tracking_mumber" value={this.state.tracking_mumber} onChange={this.tracking_mumber}/></p>
                                        <p><b>Tracking Number :</b> <br />
                                            <select name="text" className="form-control" id="tracking_mumber_slug" value={this.state.tracking_mumber_slug} onChange={this.tracking_mumber_slug}>
                                                <option value="first-flight">First Flight Couriers</option>
                                                <option value="xpressbees">XpressBees</option>
                                                <option value="delhivery">Delhivery</option>
                                                <option value="india-post">India Post Domestic</option>
                                                <option value="india-post-int">India Post International</option>
                                                <option value="bluedart">Bluedart</option>
                                                <option value="usps">USPS</option>
                                                <option value="ups">UPS</option>
                                                <option value="fedex">FedEx</option>
                                                <option value="dhl">DHL Express</option>
                                            </select>
                                        </p>
                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" id="track" className="btn btn-primary btn-block" style={{marginBottom : '20px'}}> Send to Aftership </button>) :''}
                                    </div>
                                    <div className="col-md-12">
                                        <a href="#" id ="export" className="btn btn-default btn-block" role='button'><i className="fa fa-download"></i> Download</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
