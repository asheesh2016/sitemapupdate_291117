import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class ViewCoupons extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            subscription: {
                CouponsList : Meteor.subscribe('CouponsList'),
            }
        }
    }
    coupon(){
        return Coupons.find({});
    }
    openEdit(id){
        FlowRouter.go('/admin/edit-coupons/'+id);
    }
    deleteCoupon(id){
        //console.log(id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('deleteCoupon',id,function (error) {
                    if(!error){
                        swal("Deleted!", "Your Coupon has been deleted.", "success");
                    }
                });
            } else {
                swal("Cancelled", "Your Coupon is safe :)", "error");
            }
        });
    }
    render() {
        let type = (type) =>{
            if(type == 0){
                return 'Flat Discount';
            }else{
                return 'X % of cart total';
            }
        }
        let status = (type) =>{
            if(type == 0){
                return 'In Active';
            }else{
                return 'Active';
            }
        }
        DocHead.setTitle('Coupons List');
        return (
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <div className="box">
                            <div className="box-body">
                                <table className="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th>Code</th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th style={{width: '70px'}}>Action</th>
                                    </tr>
                                    {this.coupon().map((val) =>{
                                        return (
                                            <tr key={val._id}>
                                                <td>{val.code}</td>
                                                <td>{type(val.type)}</td>
                                                <td>{val.amount}</td>
                                                <td>{status(val.status)}</td>
                                                <td>
                                                    <button type="button" className="btn btn-primary btn-xs" style={{float:'left'}} onClick={this.openEdit.bind(this,val._id)}>
                                                        <i className="fa fa-edit"></i>
                                                    </button>
                                                    {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="button" className="btn btn-danger btn-xs" style={{float:'right'}} onClick={this.deleteCoupon.bind(this,val._id)}>
                                                        <i className="fa fa-trash"></i>
                                                    </button>) : ''}
                                                </td>
                                            </tr>
                                        )
                                    })}

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export {
    ViewCoupons
}