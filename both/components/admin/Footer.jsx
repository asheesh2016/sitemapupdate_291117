import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class AdminFooterList extends TrackerReact(Component) {
	constructor() {
		super();
		this.state = {
            id:'',
            content : '',            
			subscription : {
				Footer : Meteor.subscribe('Footer'),
			}
		};
        this.handleSubmit = this.handleSubmit.bind(this);
	}
    componentWillUnmount(){
        this.state.subscription.Footer.stop();
    }
    componentWillMount(){
        this.setState({
            id: this.footerEdit() && this.footerEdit()._id,
            content : this.footerEdit() && this.footerEdit().content
        })
    }
    componentDidMount() {
        let self = this;
        jQuery(document).ready(function () {
            let mysummer = jQuery('#footer-content');
            mysummer.summernote({
                height: 300,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: false,                  // set focus to editable area after initializing summernote
            });
        });
    }
    footerEdit(){
        return Footer.find({tag:'footer'}).fetch();
    }
    handleSubmit(event){
        event.preventDefault();
        data = {
            content : $("#footer-content").summernote('code'),
            id : this.footerEdit() && this.footerEdit()[0]._id,
        }
        Meteor.call('footerContent', data, function (error) {
            if(!error){
                Bert.alert('Footer edited successfully', 'success', 'growl-top-right');
            }else{
                Bert.alert('Footer edit problem', 'danger', 'growl-top-right');
            }            
        });
    }
    render(){
        DocHead.setTitle('Footer');
    	return (
            <section className="content">
                <div className="row">
                    <form role="form" onSubmit={this.handleSubmit}>
                        {this.footerEdit().map((val) =>{
                            return(
                                <div className="col-md-12" key={val._id}>
                                    <div className="box box-primary">
                                        <div className="box-header with-border">
                                            <h3 className="box-title">Edit Footer</h3>
                                        </div>
                                        <div className="box-body">
                                            <div className="form-group">
                                                <label> Footer Content </label>
                                                <textarea rows="10" className="form-control" id="footer-content" defaultValue={val.content}/>
                                            </div>
                                        </div>
                                        <div className="box-footer">
                                            <div className="form-group">
                                                {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary btn-block"> Edit Footer</button>) : '' }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </form>
                </div>
            </section>            
    	)
    }
}
export{
	AdminFooterList
}
