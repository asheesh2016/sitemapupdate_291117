import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
export default class SeoPage extends TrackerReact(Component){
	constructor(){
		super();
		this.state = {
            url : '',
            meta_title: '',
            meta_description : '',
            meta_keyword : '',
            subscription: {
                seo: Meteor.subscribe('Seo','/'),
            }
		}
        this.handleSubmit = this.handleSubmit.bind(this);
        this.meta_title = this.meta_title.bind(this);
        this.meta_description = this.meta_description.bind(this);
        this.meta_keyword = this.meta_keyword.bind(this);
	}
    seo(){
        return Seo.findOne({url:'/'});
    }
    componentWillUnmount(){
        this.state.subscription.seo.stop();
    }
    componentWillMount(){
        var self = this;
        Tracker.autorun(() =>{
            if(!_.isUndefined(this.seo())){
                try{
                    this.setState({
                        meta_title : !_.isUndefined(this.seo() && this.seo().meta_title) ? this.seo() && this.seo().meta_title:'',
                        meta_description : !_.isUndefined(this.seo() && this.seo().meta_description) ? this.seo() && this.seo().meta_description:'',
                        meta_keyword : !_.isUndefined(this.seo() && this.seo().meta_keyword) ? this.seo() && this.seo().meta_keyword:''
                               
                    })
                }catch(e){
                    console.log(e);
                }
            }
        });
    }
    meta_title(event){
        this.setState({meta_title: event.target.value});
    }
    meta_description(event){
        this.setState({meta_description: event.target.value});
    }
    meta_keyword(event){
        this.setState({meta_keyword: event.target.value});
    }
    handleSubmit(event){
    	event.preventDefault();
        data = {
            url : '/',
            set : {
                meta_title  : this.state.meta_title,
                meta_description : this.state.meta_description,
                meta_keyword : this.state.meta_keyword,          
            }
        }
        Meteor.call('editSeo',data,function (error) {
            if(!error){
                Bert.alert('Seo edit successfully', 'success', 'growl-top-right');
                FlowRouter.go('/admin/seo');
            }else{
                Bert.alert('Seo edit problem', 'danger', 'growl-top-right');
            }
        })
    }    	
	render(){
        DocHead.setTitle('Home Seo');
        return (
            <section className="content">
                <div className="row">
                    <form role="form" onSubmit={this.handleSubmit}>
                        <div className="col-md-12">
                            <div className="box box-primary">
                                <div className="box-body">
                                    <div className="form-group">
                                        <label> Meta Title </label>
                                        <input type="text" value={this.state.meta_title} className="form-control" id="meta_title"
                                               onChange={this.meta_title}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Description </label>
                                        <textarea rows="10" value={this.state.meta_description} className="form-control" id="meta_description"
                                               onChange={this.meta_description}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Keyword </label>
                                        <input type="text" value={this.state.meta_keyword} className="form-control" id="meta_keyword"
                                               onChange={this.meta_keyword}/>
                                    </div>
                                </div>
                                <div className="box-footer">
                                    <div className="form-group">
                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary btn-block"> Edit Page</button>) : ''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        )
	}
}

export{
    SeoPage
}