import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import Dropzone from 'react-dropzone';
export default class AdminBackground extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
            id:'',
            featuredImage : '',            
            subscription : {
                Background : Meteor.subscribe('Background'),
            }
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onDropSingle = this.onDropSingle.bind(this);
    }
    componentWillUnmount(){
        this.state.subscription.Background.stop();
    }
    componentWillMount(){
        var self = this;
        Tracker.autorun(() =>{
            if(!_.isUndefined(self.backgroundEdit())){
                try{
                    self.setState({
                        id: self.backgroundEdit() && self.backgroundEdit()[0]._id,
                        featuredImage : self.backgroundEdit() && self.backgroundEdit()[0].featuredImage
                    })
                }catch(e){
                        //console.log(e);
                }
            }
        });
    }
    componentDidMount() {
        let self = this;
        
    }
    backgroundEdit(){
        return Background.find({tag:'bg'}).fetch();
    }
    onDropSingle(files){
        let self = this;
        if(files[0].size/1024 > 4096){
            Bert.alert('File Size is more than 4MB', 'danger', 'growl-top-right');
        }else{
            let uploader = new Slingshot.Upload("myDefinedDirective");
            uploader.send(files[0], (error, downloadUrl) => {
                if (error) {
                    Bert.alert('Error in file upload', 'danger', 'growl-top-right');
                }
                else {
                    console.log(downloadUrl)
                    self.setState({featuredImage: downloadUrl});
                }
            });
        }
    }
    handleSubmit(event){
        event.preventDefault();
        data = {
            featuredImage : this.state.featuredImage,
            id : this.backgroundEdit() && this.backgroundEdit()[0]._id,
        }
        Meteor.call('backgroundContent', data, function (error) {
            if(!error){
                Bert.alert('Background edited successfully', 'success', 'growl-top-right');
            }else{
                Bert.alert('Background edit problem', 'danger', 'growl-top-right');
            }            
        });
    }
    removeImageFeatured() {
        this.setState({featuredImage: ''});
    }
    render(){
        let featureImageBox;
        if(this.state.featuredImage){
            featureImageBox = (
                <div className="col-sm-12" style={{position:'relative'}}>
                    <img className="img-responsive" src={this.state.featuredImage} alt="Photo" />
                    <button type="button" className="btn btn-danger btn-xs" style={{position:'absolute',top:5,right:20}} onClick={this.removeImageFeatured.bind(this)}><i className="fa fa-trash"></i></button>
                </div>
            )
        }else{
            featureImageBox = (
                <Dropzone onDrop={this.onDropSingle} multiple={false} accept="image/*" style={{border:'none',height:200,width:'100%',background:'#ccc',display:'flex',alignItems:'center',justifyContent:'center'}}>
                    <div style={{color:'#fff',textAlign:'center'}}>Try dropping some file here, or click to select file to upload.</div>
                </Dropzone>
            )
        }
        DocHead.setTitle('Background');
        return (
            <section className="content">
                <div className="row">
                    <form role="form" onSubmit={this.handleSubmit}>
                        {this.backgroundEdit().map((val) =>{
                            return(
                                <div className="col-md-12" key={val._id}>
                                    <div className="box box-primary">
                                        <div className="box-header with-border">
                                            <h3 className="box-title">Edit Background</h3>
                                        </div>
                                        <div className="form-group col-md-6">
                                            <label> Slider Image (Please Add more than 2000 width Image but less than 4 MB)</label>
                                            {featureImageBox}
                                        </div>
                                        <div className="box-footer">
                                            <div className="form-group">
                                                {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary btn-block"> Edit Background</button>) : '' }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </form>
                </div>
            </section>            
        )
    }
}
export{
    AdminBackground
}
