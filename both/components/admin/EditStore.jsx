import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class EditStore extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            address:'',
            city:'',
            lat:'',
            lng:'',
            subscription: {
                singleStore : Meteor.subscribe('singleStore',FlowRouter.current().params.id),
            }
        };
        this.address = this.address.bind(this);
        this.city = this.city.bind(this);
        this.lat = this.lat.bind(this);
        this.lng = this.lng.bind(this);        
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    singleStore(){
        return Stores.findOne(FlowRouter.current().params.id);
    }
    componentWillMount(){
        this.setState({
            address : this.singleStore() && this.singleStore().address,
            city : this.singleStore() && this.singleStore().city,
            lat : this.singleStore() && this.singleStore().lat,
            lng : this.singleStore() && this.singleStore().lng,
        })
    }
    handleSubmit(event){
        event.preventDefault();
        if(_.isEmpty(this.state.address)){  
            Bert.alert('Please enter the address', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.city)){
            Bert.alert('Please enter the city', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.lat)){
            Bert.alert('Please enter the lat', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.lng)){
            Bert.alert('Please enter the lng', 'danger', 'growl-top-right');
            return false;
        }
        data = {
            id : FlowRouter.current().params.id,
            set : {
                address : this.state.address,
                city : this.state.city,
                lat : this.state.lat,
                lng : this.state.lng,
            },
        }
        //console.log(data);
        Meteor.call('editStore',data,function (error) {
            if(!error){
                Bert.alert('Store edited successfully', 'success', 'growl-top-right');
                FlowRouter.go('/admin/storelist')
            }else{
                Bert.alert('Something went wrong. Try again!!', 'danger', 'growl-top-right');
            }
        })
    }
    address(event){
        this.setState({address:event.target.value});
    }
    city(event){
        this.setState({city:event.target.value});
    }
    lat(event){
        this.setState({lat:event.target.value});
    }
    lng(event){
        this.setState({lng:event.target.value});
    }
    render(){
        DocHead.setTitle('Edit Store');
        return (
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <div className="box">
                            <form role="form" onSubmit={this.handleSubmit}>
                                <div className="box-body">
                                    <div className="form-group">
                                        <label> Address </label>
                                        <input type="textarea" className="form-control" value={this.state.address} id="address" onChange={this.address}/>
                                    </div>
                                    <div className="form-group">
                                        <label> City </label>
                                        <select className="form-control" id="city" value={this.state.city} onChange={this.city}>
                                            <option value="Mumbai" selected={this.state.city == 'Mumbai' ? 'selected':''}>Mumbai</option>
                                            <option value="Delhi" selected={this.state.city == 'Delhi' ? 'selected':''}>Delhi</option>
                                            <option value="Bangalore" selected={this.state.city == 'Bangalore' ? 'selected':''}>Bangalore</option>
                                            <option value="Chennai" selected={this.state.city == 'Chennai' ? 'selected':''}>Chennai</option>
                                            <option value="Hyderabad" selected={this.state.city == 'Hyderabad' ? 'selected':''}>Hyderabad</option>
                                            <option value="Ahmedabad" selected={this.state.city == 'Ahmedabad' ? 'selected':''}>Ahmedabad</option>
                                            <option value="Kolkata" selected={this.state.city == 'Kolkata' ? 'selected':''}>Kolkata</option>
                                            <option value="Surat" selected={this.state.city == 'Surat' ? 'selected':''}>Surat</option>
                                            <option value="Pune" selected={this.state.city == 'Pune' ? 'selected':''}>Pune</option>
                                            <option value="Jaipur" selected={this.state.city == 'Jaipur' ? 'selected':''}>Jaipur</option>
                                            <option value="Lucknow" selected={this.state.city == 'Lucknow' ? 'selected':''}>Lucknow</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label> Latitude </label>
                                        <input type="text" className="form-control" value={this.state.lat} id="lat" onChange={this.lat}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Longitude </label>
                                        <input type="text" className="form-control" value={this.state.lng} id="lng" onChange={this.lng}/>
                                    </div>
                                    <div className="box-footer">
                                        <div className="form-group">
                                            {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary"> Edit Store</button>) : ''}
                                        </div>
                                    </div>
                                </div>    
                            </form>    
                        </div>
                    </div>
                </div>
            </section>
        )
    }   
}