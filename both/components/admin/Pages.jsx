import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class CreatePages extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            slug: '',
            pslug: '',
            slugAvailable : false,
            title: '',
            content: '',
            meta_title: '',
            meta_description : '',
            meta_keyword : '',
            subscription: {
                pageSlug: Meteor.subscribe('PageSlug'),
            }            
        };
        this.title = this.title.bind(this);
        this.pslug = this.pslug.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.meta_title = this.meta_title.bind(this);
        this.meta_description = this.meta_description.bind(this);
        this.meta_keyword = this.meta_keyword.bind(this);
    }
    pageSlug(){
    	return Pages.find({}).fetch();
    }
    componentWillUnmount(){
        this.state.subscription.pageSlug.stop();
    }  
    componentDidMount() {
        let self = this;
        jQuery(document).ready(function () {
            console.log("SUMMERNOTE SHIT HERE")
            let mysummer = jQuery('#page-content');
            mysummer.summernote({
                height: 300,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: false,                  // set focus to editable area after initializing summernote
                fontNames: ['GillesComicFont', 'UrbanPaints', 'Roboto Light', 'dk_lemon_yellow_sunregular', 'Helvetica', 'Arial', 'Comic Sans MS', 'Courier New']
            });
            jQuery('#product-excerpt').textcounter({
                type: "character",
                max: 160,
                countDown: true,
                stopInputAtMaximum: true
            });
        });
    }    
    title(event) {
        let convertToSlug = (Text) => {
            return Text
                .toLowerCase()
                .replace(/ /g, '-')
                .replace(/[^\w-]+/g, '');
        }
        if (event.target.value) {
            this.setState({slug: convertToSlug(event.target.value)}); 
            this.setState({pslug: event.target.value});
            this.setState({slugAvailable : !_.contains(_.pluck(this.pageSlug(),'slug'),this.state.slug)});
        } else {
            this.setState({pslug: ''});
            this.setState({slug: ''});
            this.setState({slugAvailable : !_.contains(_.pluck(this.pageSlug(),'slug'),this.state.slug)});
        }
        this.setState({title: event.target.value});
    }      
    pslug(event){
        let convertToSlug = (Text) => {
            return Text
                .toLowerCase()
                .replace(/ /g, '-')
                .replace(/[^\w-]+/g, '');	
        }        
        if(event.target.value){
            this.setState({slug: convertToSlug(event.target.value)});
        }else{
            this.setState({slug: ''});
        }
        this.setState({slugAvailable : !_.contains(_.pluck(this.pageSlug(),'slug'),this.state.slug)});
        this.setState({pslug: event.target.value}); 
    }
    meta_title(event){
        this.setState({meta_title: event.target.value});
    }
    meta_description(event){
        this.setState({meta_description: event.target.value});
    }
    meta_keyword(event){
        this.setState({meta_keyword: event.target.value});
    }    
    handleSubmit(event) {
        event.preventDefault();       
        if(_.isEmpty(this.state.title)){
            Bert.alert('Please enter the title', 'danger', 'growl-top-right');
            return false;
        }
        if(!this.state.slugAvailable){
            Bert.alert('Slug already exists', 'danger', 'growl-top-right');
            return false;
        }        
        data = {
            title : this.state.title,
            slug : this.state.slug,
            content : $("#page-content").summernote('code'),
            meta_title  : this.state.meta_title,
            meta_description : this.state.meta_description,
            meta_keyword : this.state.meta_keyword,
            createdAt : new Date(),
        }
        Meteor.call('AddPage',data,function (error) {
            if(!error){
                Bert.alert('Page added', 'success', 'growl-top-right');
                FlowRouter.go('/admin/list-pages');
            }else{
                Bert.alert('Page cannot be added', 'danger', 'growl-top-right');
            }
        })        
    }    
	render(){
        DocHead.setTitle('Create Page');
        let slug; 
        if (this.state.slug) {
            if(this.state.slugAvailable){
                slug = (
                    <div className="form-group">
                        <label>Slug</label>
                        <input type="text" className="form-control" id="page-slug" value={this.state.pslug}
                               onChange={this.pslug}/>
                        <a href={Meteor.absoluteUrl()+'/'+this.state.slug}
                           style={{display:'block',color:'blue'}}>{Meteor.absoluteUrl() + this.state.slug}</a>
                    </div>
                )
            }else{
                slug = (
                    <div className="form-group">
                        <label>Slug</label>
                        <input type="text" className="form-control" id="page-slug" value={this.state.pslug}
                               onChange={this.pslug}/>
                        <a href={Meteor.absoluteUrl()+'/'+this.state.slug}
                           style={{display:'block',color:'red'}}>{Meteor.absoluteUrl() + this.state.slug}</a>	
                    </div>
                )
            }
        } else {
            slug = ''
        }		
        return (
            <section className="content">
                <div className="row">
                    <form role="form" onSubmit={this.handleSubmit}>
                        <div className="col-md-12">
                            <div className="box box-primary">
                                <div className="box-body">
                                    <div className="form-group">
                                        <label> Page Title </label>
                                        <input type="text" className="form-control" id="product-title"
                                               onChange={this.title}/>
                                    </div>
                                    {slug}
                                    <div className="form-group">
                                        <label> Page Content </label>
                                        <textarea rows="10" className="form-control" id="page-content" />
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Title </label>
                                        <input type="text" className="form-control" id="meta_title"
                                               onChange={this.meta_title}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Description </label>
                                        <textarea rows="10" className="form-control" id="meta_description"
                                               onChange={this.meta_description}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Keyword </label>
                                        <input type="text" className="form-control" id="meta_keyword"
                                               onChange={this.meta_keyword}/>
                                    </div>
                                </div>
                                
                                <div className="box-footer">
                                    <div className="form-group">
                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary btn-block"> Add Page</button>) : ''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        )
	}
}
export  class ListPages extends TrackerReact(Component){
	constructor(){
		super();
		this.state = {
			subscription:{
				pages: Meteor.subscribe('Pages'),
			}
		}
	}
    pagesList(){
    	return Pages.find({}).fetch();
    }
    componentWillUnmount(){
        this.state.subscription.pages.stop();
    }
    openEdit(id){
        FlowRouter.go('/admin/list-pages/edit/'+id);
    }      
    deletePage(id){
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('deletePage',id,function (error) {
                    if(!error){
                        swal("Deleted!", "Your Page has been deleted.", "success");
                    }
                });
            } else {
                swal("Cancelled", "Your Page is safe :)", "error");
            }
        });
    }    	
	render(){
        DocHead.setTitle('Pages Lists');
		return(
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <div className="box">
                            <div className="box-header with-border">
                                <h3 className="box-title">Page List</h3>
                            </div>
                            <div className="box-body">
                                <table className="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>Title</th>
                                            <th>Slug</th>
                                            <th style={{width: '70px'}}>Action</th>
                                        </tr>
                                        {this.pagesList().map((val) => {
                                            return (
                                                <tr key={val._id}>
                                                    <td>{val.title}</td>
                                                    <td>{val.slug}</td>
                                                    <td>
                                                        <button type="button" className="btn btn-primary btn-xs" style={{float:'left'}} onClick={this.openEdit.bind(this,val._id)}><i className="fa fa-edit"></i></button>
                                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="button" className="btn btn-danger btn-xs" style={{float:'right'}} onClick={this.deletePage.bind(this,val._id)}><i className="fa fa-trash"></i></button>) : ''}
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
		)
	}
}
export  class ListPagesEdit extends TrackerReact(Component){
	constructor(){
		super();
		this.state = {
            title : '',
            content : '',
            meta_title: '',
            meta_description : '',
            meta_keyword : '',
            subscription: {
                page: Meteor.subscribe('pageWithId', FlowRouter.current().params.id),
            }
		}
        this.title = this.title.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.meta_title = this.meta_title.bind(this);
        this.meta_description = this.meta_description.bind(this);
        this.meta_keyword = this.meta_keyword.bind(this);
	}
    page(){
        return Pages.findOne(FlowRouter.current().params.id);
    }
    componentDidMount() {
        let self = this;
        console.log("SUMMERSHITHERE")
        jQuery(document).ready(function () {
            let mysummer = jQuery('#page-content');
            mysummer.summernote({
                height: 300,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: false,                  // set focus to editable area after initializing summernote
                fontNames: ['GillesComicFont', 'UrbanPaints', 'Roboto Light', 'dk_lemon_yellow_sunregular', 'Helvetica', 'Arial', 'Comic Sans MS', 'Courier New']
            });
            jQuery('#product-excerpt').textcounter({
                type: "character",
                max: 160,
                countDown: true,
                stopInputAtMaximum: true
            });
        });
    }
    componentWillUnmount(){
        this.state.subscription.page.stop();
    }
    componentWillMount(){
        this.setState({
            title : this.page() && this.page().title,
            content : this.page() && this.page().content,
            meta_title : !_.isUndefined(this.page() && this.page().meta_title) ? this.page() && this.page().meta_title:'',
            meta_description : !_.isUndefined(this.page() && this.page().meta_description) ? this.page() && this.page().meta_description:'',
            meta_keyword : !_.isUndefined(this.page() && this.page().meta_keyword) ? this.page() && this.page().meta_keyword:''
                   
        })
    }
    title(event){
        this.setState({title: event.target.value});
    }
    meta_title(event){
        this.setState({meta_title: event.target.value});
    }
    meta_description(event){
        this.setState({meta_description: event.target.value});
    }
    meta_keyword(event){
        this.setState({meta_keyword: event.target.value});
    }
    handleSubmit(event){
    	event.preventDefault();
        data = {
            id : FlowRouter.current().params.id,
            set : {
                title : this.state.title,
                content : $("#page-content").summernote('code'), 
                meta_title  : this.state.meta_title,
                meta_description : this.state.meta_description,
                meta_keyword : this.state.meta_keyword,          
            }
        }
        Meteor.call('editPage',data,function (error) {
            if(!error){
                Bert.alert('Page edit successfully', 'success', 'growl-top-right');
                FlowRouter.go('/admin/list-pages');
            }else{
                Bert.alert('Page edit problem', 'danger', 'growl-top-right');
            }
        })
    }    	
	render(){
        DocHead.setTitle('Edit Page');
        return (
            <section className="content">
                <div className="row">
                    <form role="form" onSubmit={this.handleSubmit}>
                        <div className="col-md-12">
                            <div className="box box-primary">
                                <div className="box-body">
                                    <div className="form-group">
                                        <label> Page Title </label>
                                        <input type="text" className="form-control" id="product-title"
                                               defaultValue={this.state.title} onChange={this.title}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Page Content </label>
                                        <textarea rows="10" className="form-control" id="page-content" defaultValue={this.state.content}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Title </label>
                                        <input type="text" value={this.state.meta_title} className="form-control" id="meta_title"
                                               onChange={this.meta_title}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Description </label>
                                        <textarea rows="10" value={this.state.meta_description} className="form-control" id="meta_description"
                                               onChange={this.meta_description}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Keyword </label>
                                        <input type="text" value={this.state.meta_keyword} className="form-control" id="meta_keyword"
                                               onChange={this.meta_keyword}/>
                                    </div>
                                </div>
                                <div className="box-footer">
                                    <div className="form-group">
                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary btn-block"> Edit Page</button>) :''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        )
	}
}