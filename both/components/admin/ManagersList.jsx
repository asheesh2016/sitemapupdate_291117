import React ,{Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
export default class ManagersList extends TrackerReact(Component) {

    constructor() {
        super();
        this.state = {
            q : '',
            subscription: {
                userslist: Meteor.subscribe('userslist', 'manager'),
            }
        }
        this.q = this.q.bind(this);
    }
    componentWillUnmount() {
        this.state.subscription.userslist.stop();
    }
    usersList() {
        if(this.state.q == ''){
            return Meteor.users.find({roles: 'manager'}).fetch();
        }else{
            var regExp = this.buildRegExp(this.state.q);
            var userIds = _.map(shipping,function(val){ return val.userId});
            return Meteor.users.find({$or: [
                    {'emails.address': regExp},
                    {'profile.name': regExp},
                    {'profile.phone': regExp},
                ],roles: 'manager'}).fetch()
        }
    }
    deleteProfile(id){
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('deleteAccount',id,function (error) {
                    if(!error){
                        swal("Deleted!", "Account has been deleted.", "success");
                    }
                });
            } else {
                swal("Cancelled", "Account is safe :)", "error");
            }
        });
    }
    buildRegExp(searchText) {
      var words = searchText.trim().split(/[ \-\:]+/);
      var exps = _.map(words, function(word) {
        return "(?=.*" + word + ")";
      });
      var fullExp = exps.join('') + ".+";
      return new RegExp(fullExp, "i");
    }
    q(event){
        this.setState({q:event.target.value});
    }
    render() {
        DocHead.setTitle('Managers List');
        return (
            <section className="content">
                <div className="box">
                    <div className="box-header with-border">
                        <h3 className="box-title">Managers</h3>  
                    </div>

                    <div className="box-body">
                        <div className="col-md-12">
                            <a href="/admin/create-manager" className="btn btn-primary btn-block" style={{'marginBottom':'20px'}}>Create Manager</a>
                        </div>
                        <div className="col-md-12">
                            <div className="input-group" style={{'marginBottom':'20px'}}>
                                <input type="text" name="q" className="form-control" placeholder="Search... " value={this.state.q} onChange={this.q}/>
                                  <span className="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
                                    </button>
                                  </span>
                            </div>
                            <table className="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.usersList().map((val,index)=> {
                                        return (
                                            <tr key={val._id}>
                                                <td>{index + 1}</td>
                                                <td>{_.isUndefined(val.profile) ? '':val.profile.name}</td>
                                                <td>{_.isUndefined(val.emails) ? '' : val.emails[0].address}</td>
                                                <td>{_.isUndefined(val.profile) ? '' : val.profile.phone}</td>
                                                <td>
                                                    {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="button" className="btn btn-danger btn-xs" style={{float:'left'}} onClick={this.deleteProfile.bind(this,val._id)}>
                                                        <i className="fa fa-trash"></i>
                                                    </button>) : ''}
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="box-footer">
                    </div>
                </div>
            </section>
        )
    }
}