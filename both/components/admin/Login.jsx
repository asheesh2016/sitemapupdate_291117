import React ,{Component} from 'react';
import {ReactMeteorData} from 'meteor/react-meteor-data';

export default class Login extends Component{
    handleLogin(event){
        event.preventDefault();
        let eu = $('#eu').val(),
            password = $('#pwd').val();
        Meteor.loginWithPassword(eu, password, function (error) {
            if (error) {
                Bert.alert('Username or Password is wrong', 'danger', 'growl-top-right');
            } else {
                FlowRouter.go('/admin/dashboard')
            }
        })
    }
    render(){
        DocHead.setTitle('Signin');
        return (
            <div className="containers">
                <div className="login-boxs">
                    <div className="login-box-bodys">
                        <p className="login-box-msg">Sign in to start your session</p>
                        <form role="form" className="AdminSignin" onSubmit={this.handleLogin}>
                            <div className="form-group">
                                <label htmlFor="text">Email address:</label>
                                <input type="text" className="form-control" id="eu" placeholder="Email / Username"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="pwd">Password:</label>
                                <input type="password" className="form-control" id="pwd" placeholder="Password"/>
                            </div>
                            <button type="submit" className="btn btn-primary btn-block btn-flat">Signin</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export {
    Login
}












