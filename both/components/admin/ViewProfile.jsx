import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class ViewProfile extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            subscription: {
                OrderHistoryAdmin : Meteor.subscribe('OrderHistoryAdmin'),
            }
        }
    }
    orders(){
        return Orders.find({createdBy:FlowRouter.current().params.id},{sort:{createdAt:-1}}).fetch()
    }
    componentWillUnmount(){
        this.state.subscription.OrderHistoryAdmin.stop();
    }
    viewOrder(id){
        FlowRouter.go('/admin/view-order/'+id);
    }
    deleteOrder(id){
        //console.log(id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('deleteOrder',id,function (error) {
                    if(!error){
                        swal("Deleted!", "Your Order has been deleted.", "success");
                    }
                });
            } else {
                swal("Cancelled", "Your Order is safe :)", "error");
            }
        });
    }
    render() {
        DocHead.setTitle('Order List');
        let date = (createdAt) => {
            return moment(createdAt).format('DD-MM-YYYY hh:mm');
        }
        let status = (status) => {
            if(status == 2){
                return (
                    <span className="label label-info">Processing</span>
                );
            }else if(status == 3){
                return (
                    <span className="label label-warning">Shipped</span>
                );
            }else if(status == 4){
                return (
                    <span className="label label-success">Completed</span>
                );
            }else if(status == 5){
                 return (
                    <span className="label label-danger">Cancelled</span>
                );
            }
        }
        return (
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <h3>User Order Detail</h3>
                    </div>
                    <div className="col-md-12">
                        <div className="box">
                            <div className="box-body">
                                <table className="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th style={{width: '10px'}}>Order ID</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Total</th>
                                        <th>Shipping Charge</th>
                                        <th>Discount</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th style={{width: '70px'}}>Action</th>
                                    </tr>
                                    {this.orders().map((val)=>{
                                        return (
                                            <tr key={val._id}>
                                                <td><a href="javascript:;" onClick={this.viewOrder.bind(this,val._id)}>{val._id}</a></td>
                                                <td>{val.name}</td>
                                                <td>{val.phone}</td>
                                                <td>{val.total}</td>
                                                <td>{val.ship}</td>
                                                <td>{val.discount}</td>
                                                <td>{status(val.status)}</td>
                                                <td>{date(val.createdAt)}</td>
                                                <td>
                                                    <button type="button" className="btn btn-primary btn-xs" style={{float:'left'}} onClick={this.viewOrder.bind(this,val._id)}>
                                                        <i className="fa fa-eye"></i>
                                                    </button>
                                                    <button type="button" className="btn btn-danger btn-xs" style={{float:'right'}} onClick={this.deleteOrder.bind(this,val._id)}><i className="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export {
    ViewProfile
}
