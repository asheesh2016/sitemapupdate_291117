import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
export default class ViewProducts extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
            subscription: {
                ProductList : Meteor.subscribe('ProductList'),
            }
        }
    }
    products(){
        return Products.find({}).fetch()
    }
    categories() {
        return Categories.find({}).fetch()
    }
    openEdit(id){
        FlowRouter.go('/admin/edit-product/'+id);
    }
    deleteProduct(id){
        //console.log(id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('deleteProduct',id,function (error) {
                    if(!error){
                        swal("Deleted!", "Your Product has been deleted.", "success");
                    }
                });
            } else {
                swal("Cancelled", "Your Product is safe :)", "error");
            }
        });
    }
    
    render() {
        DocHead.setTitle('Product List');
        let catIdToText = (catId) =>{
            if(typeof catId != 'string'){
                return _.map(catId,function(val){
                    return (
                        <p>{Categories.findOne(val) ? Categories.findOne(val).name : ''}</p>
                    )
                })
            }
            else{
                return Categories.findOne(catId) ? Categories.findOne(catId).name : ''
            }
        }
        return (
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <div className="box">
                            <div className="box-body">
                                <table className="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th style={{width: '10px'}}>#</th>
                                            <th>Title</th>
                                            <th style={{width:'150px'}}>Image</th>
                                            <th>Category</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Rating</th>
                                            <th style={{width: '70px'}}>Action</th>
                                        </tr>
                                        {this.products().map((val) => {
                                            return (
                                                <tr key={val._id} valign="center">
                                                    <td style={{verticalAlign: 'middle'}}><a href={'/admin/edit-product/'+val._id}>{val._id}</a></td>
                                                    <td style={{verticalAlign: 'middle'}}>{val.title}</td>
                                                    <td style={{verticalAlign: 'middle'}}><img src={val.featuredImage} width="150"/></td>
                                                    <td style={{verticalAlign: 'middle'}}>{catIdToText(val.category)}</td>
                                                    <td style={{verticalAlign: 'middle'}}>{val.price}</td>
                                                    <td style={{verticalAlign: 'middle'}}>{val.quantity}</td>
                                                    <td style={{verticalAlign: 'middle'}}>{val.rating}</td>
                                                    <td style={{verticalAlign: 'middle'}}>
                                                        <button type="button" className="btn btn-primary btn-xs" style={{float:'left'}} onClick={this.openEdit.bind(this,val._id)}><i className="fa fa-edit"></i></button>
                                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="button" className="btn btn-danger btn-xs" style={{float:'right'}} onClick={this.deleteProduct.bind(this,val._id)}><i className="fa fa-trash"></i></button>) : '' }
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export {
    ViewProducts
}
