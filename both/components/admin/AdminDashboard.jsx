import React ,{Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class AdminDashboard extends TrackerReact(Component){
    constructor() {
    	super();
    	this.state = {
    		sucscription: {
    			all: Meteor.subscribe('All'),
    		}
    	}
    }
    componentWillUnmount(){
    	this.state.sucscription.all.stop();
    }
    users(){
    	return Meteor.users.find({"roles":'user'}).count();
    }
    products(){
    	return Products.find().count();
    }
    pages(){
    	return Pages.find().count();
    }
    cats(){
    	return Categories.find().count();
    }
    orders(){
    	return Orders.find({$or :[{status:2},{status:3},{status:4},{status:5}]}).count();
    }
    coupons(){
    	return Coupons.find().count();
    }
    render(){
        DocHead.setTitle('Dashboard');
        return (
			<section className="content">
			    <div className="row">
			        <div className="col-lg-2 col-xs-3">
			          <div className="small-box bg-aqua">
			            <div className="inner">
			              <h3>{this.users()}</h3>
			              <p>Users</p>
			            </div>
			            <div className="icon">
			              <i className="ion ion-bag"></i>
			            </div>
			            <a href="/admin/users" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
			          </div>
			        </div>
			        <div className="col-lg-2 col-xs-3">
			          <div className="small-box bg-green">
			            <div className="inner">
			              <h3>{this.products()}</h3>
			              <p>Products</p>
			            </div>
			            <div className="icon">
			              <i className="ion ion-stats-bars"></i>
			            </div>
			            <a href="/admin/view-products" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
			          </div>
			        </div>
			        <div className="col-lg-2 col-xs-3">
			          <div className="small-box bg-yellow">
			            <div className="inner">
			              <h3>{this.pages()}</h3>
			              <p>Pages</p>
			            </div>
			            <div className="icon">
			              <i className="ion ion-person-add"></i>
			            </div>
			            <a href="/admin/list-pages" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
			          </div>
			        </div>
			        <div className="col-lg-2 col-xs-3">
			          <div className="small-box bg-red">
			            <div className="inner">
			              <h3>{this.cats()}</h3>
			              <p>Categories</p>
			            </div>
			            <div className="icon">
			              <i className="ion ion-pie-graph"></i>
			            </div>
			            <a href="/admin/categories" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
			          </div>
			        </div>
			        <div className="col-lg-2 col-xs-3">
			          <div className="small-box bg-navy">
			            <div className="inner">
			              <h3>{this.orders()}</h3>
			              <p>Orders</p>
			            </div>
			            <div className="icon">
			              <i className="ion ion-pie-graph"></i>
			            </div>
			            <a href="/admin/view-orders" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
			          </div>
			        </div>
			        <div className="col-lg-2 col-xs-3">
			          <div className="small-box bg-purple">
			            <div className="inner">
			              <h3>{this.coupons()}</h3>
			              <p>Coupons</p>
			            </div>
			            <div className="icon">
			              <i className="ion ion-pie-graph"></i>
			            </div>
			            <a href="/admin/view-coupons" className="small-box-footer">More info <i className="fa fa-arrow-circle-right"></i></a>
			          </div>
			        </div>
			    </div>
			</section>  
        )

    }
}

export {
    AdminDashboard
}
