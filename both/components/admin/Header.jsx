import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class AdminHeaderList extends TrackerReact(Component) {
	constructor() {
		super();
		this.state = {
			subscription : {
				categories : Meteor.subscribe('Categories'),
				pages : Meteor.subscribe('Pages'),
				header : Meteor.subscribe('Header'),
			}
		};	
	}
    componentWillUnmount(){
        this.state.subscription.categories.stop();
        this.state.subscription.pages.stop();
        this.state.subscription.header.stop();
    }	
    catList(){
    	return Categories.find({}).fetch();
    }
    pageList(){
    	return Pages.find({}).fetch();
    }
    headerList(){
    	return Header.find({}).fetch();
    }    
    selectedItem(slug,name,type,event){
    	event.preventDefault();
    	//console.log(slug);
    	data = {
    		slug : slug,
    		name : name,
    		type : type
    	}
    	Meteor.call('AddRemoveHeader',data,function(error){
    		if(!error){
    			Bert.alert('Header Updated successfully', 'success', 'growl-top-right');
    		}
    	})
    }
    render(){
    	let check = (slug) =>{
    		count = Header.find({slug:slug}).count();
    		if(count == 0){
    			return false;
    		}else{
    			return true;
    		}
    	}
        DocHead.setTitle('Header');
    	return (
			<section className="content">
				<div className="row">
					<div className="col-md-6">
						<div className="box box-primary">
                            <div className="box-header with-border">
                                <h3 className="box-title">Choose Header</h3>
                            </div>
                            <div className="box-body">
                            	{Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<ul className="list-group">
									{this.catList().map((val) => { 
										return (
											<a href='#' className={check(val.slug) ? 'list-group-item active':'list-group-item'} key={val._id} onClick={this.selectedItem.bind(this,val.slug,val.name,'cat')}>
												{val.name}
											</a>
										)
									})}
									{this.pageList().map((val) => { 
										return (
											<a href='#' className={check(val.slug) ? 'list-group-item active':'list-group-item'} key={val._id} onClick={this.selectedItem.bind(this,val.slug,val.title,'page')}>
												{val.title}
											</a>
										)
									})}
								</ul>) : ''}
							</div>
						</div>								
					</div>
					<div className="col-md-6">
						<div className="box box-primary">
                            <div className="box-header with-border">
                                <h3 className="box-title">Choosed Header</h3>
                            </div>
                            <div className="box-body">
                            	{Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<ul className="list-group">
									{this.headerList().map((val) => { 
										return (
											<li className="list-group-item" key={val._id}>{val.slug}</li>
										)
									})}
								</ul>) : ''}
							</div>
						</div>
					</div>
				</div>
			</section>
    	)
    }
}
export{
	AdminHeaderList
}