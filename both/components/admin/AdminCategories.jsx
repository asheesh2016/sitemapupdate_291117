import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
export default class AdminCategories extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
            category: '',
            subcategory: '',
            selectedCat: '',
            selectedCatId: '',
            currentUser: Meteor.user(),
            subscription: {
                categories: Meteor.subscribe('Categories'),
            }
        }
        this.category = this.category.bind(this);
        this.addCategory = this.addCategory.bind(this);
        this.addSubCat = this.addSubCat.bind(this);
        this.deleteCat = this.deleteCat.bind(this);
        this.deleteSubCat = this.deleteSubCat.bind(this);
        this.setectCat = this.setectCat.bind(this);
        this.subCategory = this.subCategory.bind(this);
        this.editCat = this.editCat.bind(this);
        this.editSubCat = this.editSubCat.bind(this);

    }
    categories() {
        return Categories.find({}, {sort: {createdAt: -1}}).fetch()
    }

    subcat() {
        return SubCategories.find().fetch();
    }
    componentWillUnmount() {
        if (!_.isUndefined(this.state.subscription.subcategory)) {
            this.state.subscription.subcategory.stop();
        }
        if (!_.isUndefined(this.state.subscription.categories)) {
            this.state.subscription.categories.stop();
        }
    }
    category(event) {
        this.setState({category: event.target.value});
    }
    addCategory(event) {
        event.preventDefault();
        if (_.isEmpty(this.state.category)) {
            Bert.alert('Fields not be empty', 'danger', 'growl-top-right');
            return true;
        }
        Meteor.call('AddCategory', this.state.category, function (error) {
            if (error) {
                Bert.alert('Category exists already', 'danger', 'growl-top-right');
            } else {
                Bert.alert('Category add successfully ', 'success', 'growl-top-right');
            }
        })
        $(event.target).find("[name=acat]").val('');
    }

    addSubCat(event) {
        event.preventDefault();
        if (_.isEmpty(this.state.selectedCatId)) {
            Bert.alert('Please select the category', 'danger', 'growl-top-right');
            return true;
        }
        if (_.isEmpty(this.state.subcategory)) {
            Bert.alert('Fields not be empty', 'danger', 'growl-top-right');
            return true;
        }
        let data = {
            cat_id: this.state.selectedCatId,
            name: this.state.subcategory
        }
        console.log(data);
        Meteor.call('AddSubCategory', data, function (error) {
            if (error) {
                Bert.alert('Sub Category exists already', 'danger', 'growl-top-right');
            } else {
                Bert.alert('Sub Category add successfully ', 'success', 'growl-top-right');
            }
        })
        $(event.target).find("[name=scat]").val('');
    }

    deleteCat(id) {
        Meteor.call('DeleteCategory', id, function (error) {
            if (error) {
                Bert.alert('Not able to delete category', 'danger', 'growl-top-right');
            } else {
                Bert.alert('Category deleted successfully ', 'success', 'growl-top-right');
            }
        })
    }

    deleteSubCat(id) {
        Meteor.call('DeleteSubCategory', id, function (error) {
            if (error) {
                Bert.alert('Not able to delete category', 'danger', 'growl-top-right');
            } else {
                Bert.alert('Sub-Category deleted successfully ', 'success', 'growl-top-right');
            }
        })
    }

    setectCat(id) {
        this.setState({'selectedCatId': id});
        this.setState({'selectedCat': Categories.findOne(id).name});
        let self = this;
        if (!_.isUndefined(self.state.subscription.subcategory)) {
            self.state.subscription.subcategory.stop();
            self.setState({subscription: {subcategory: Meteor.subscribe('SubCategories', id)}})
        }else{
            self.setState({subscription: {subcategory: Meteor.subscribe('SubCategories', id)}})
        }
    }

    subCategory(event) {
        this.setState({subcategory: event.target.value});
    }
    editCat(id){
        FlowRouter.go('/admin/categories/edit/'+id);
    }
    editSubCat(id){
        FlowRouter.go('/admin/subcategories/edit/'+id);
    }
    render() {
        let catList, subcatList, subcatform;
        if (_.size(this.categories()) > 0) {
            catList = (
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Slug</th>
                        <th><em className="fa fa-cog"></em></th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.categories().map((val)=> {
                        return (
                            <tr key={val._id} onClick={this.setectCat.bind(this,val._id)}>
                                <td>{val.name}</td>
                                <td>{val.slug}</td>
                                <td>
                                    {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<a className="btn btn-danger"
                                                      onClick={this.deleteCat.bind(this,val._id)} style={{marginRight:'10px'}}><em
                                    className="fa fa-trash"></em></a>) : ''}
                                    <a className="btn btn-success"
                                                      onClick={this.editCat.bind(this,val._id)}><em
                                    className="fa fa-pencil"></em></a>
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
            )
        } else {
            catList = ''
        }
        if (_.size(this.subcat()) > 0) {
            subcatList = (
                <div className="panel panel-default panel-table">
                    <div className="panel-heading">
                        <div className="row">
                            <div className="col col-xs-12 text-center">
                                <h1 className="panel-title">List Sub-Categories of {this.state.selectedCat}</h1>
                            </div>
                        </div>
                    </div>
                    <table className="table table-striped table-bordered table-list">
                        <thead>
                        <tr>
                            <th>Categories</th>
                            <th>Sub Categories Name</th>
                            <th>Slug</th>
                            <th><em className="fa fa-cog"></em></th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.subcat().map((val)=> {
                            return (
                                <tr key={val._id}>
                                    <td>{this.state.selectedCat}</td>
                                    <td>{val.name}</td>
                                    <td>{val.slug}</td>
                                    <td>
                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<a 
                                            className="btn btn-danger"
                                            onClick={this.deleteSubCat.bind(this,val._id)} style={{marginRight:'10px'}}>
                                            <em className="fa fa-trash"></em>
                                        </a>) : ''}
                                        <a  className="btn btn-success"
                                            onClick={this.editSubCat.bind(this,val._id)}>
                                            <em className="fa fa-pencil"></em>
                                        </a>
                                    </td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                </div>
            )
        } else {
            subcatList = ''
        }
        if (this.state.selectedCat) {
            subcatform = (
                <div className="box box-primary">
                    <div className="box-header with-border">
                        <h3 className="box-title">Add Sub-Category for {this.state.selectedCat}</h3>
                    </div>
                    <div className="box-body">
                        <form style={{marginBottom : '20px'}} onSubmit={this.addSubCat}>
                            <div className="form-group">
                                {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<input type="text" className="form-control" placeholder="Enter Sub Category" name="scat"
                                       onChange={this.subCategory}/>) : '' }
                            </div>
                        </form>
                        {subcatList}
                    </div>
                </div>
            )
        } else {
            subcatform = "";
        }
        DocHead.setTitle('Categories');
        return (
            <section className="content">
                <div className="row">
                    <div className="col-md-6">
                        <div className="box box-primary">
                            <div className="box-header with-border">
                                <h3 className="box-title">Add Category</h3>
                            </div>
                            <div className="box-body">
                                <form style={{marginBottom : '20px'}} onSubmit={this.addCategory}>
                                    <div className="form-group">
                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<input type="text" className="form-control" placeholder="Enter Category"
                                               name="acat" onChange={this.category}/>) : ''}
                                    </div>
                                </form>
                                {catList}
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        {subcatform}
                    </div>
                </div>
            </section>
        )
    }
}

export  class EditCategory extends TrackerReact(Component){
    constructor(){
        super();
        this.state = {
            currentUser: Meteor.user(),
            subscription: {
                categories: Meteor.subscribe('Categories'),
            }
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.name = this.name.bind(this);
        this.slug = this.slug.bind(this);
        this.meta_title = this.meta_title.bind(this);
        this.meta_description = this.meta_description.bind(this);
        this.meta_keyword = this.meta_keyword.bind(this);
    }
    componentWillUnmount(){
        this.state.subscription.categories.stop();
    }
    category(){
        return Categories.findOne(FlowRouter.current().params.id);
    }
    componentWillMount(){
        this.setState({
            name : this.category() && this.category().name,
            slug : this.category() && this.category().slug,
            meta_title : !_.isUndefined(this.category() && this.category().meta_title) ? this.category() && this.category().meta_title:'',
            meta_description : !_.isUndefined(this.category() && this.category().meta_description) ? this.category() && this.category().meta_description:'',
            meta_keyword : !_.isUndefined(this.category() && this.category().meta_keyword) ? this.category() && this.category().meta_keyword:''
                   
        })
    }
    name(event){
        this.setState({name: event.target.value});
    }
    slug(event){
        this.setState({slug: event.target.value});
    }
    meta_title(event){
        this.setState({meta_title: event.target.value});
    }
    meta_description(event){
        this.setState({meta_description: event.target.value});
    }
    meta_keyword(event){
        this.setState({meta_keyword: event.target.value});
    }
    handleSubmit(event){
        event.preventDefault();
        data = {
            id : FlowRouter.current().params.id,
            set : {
                name : this.state.name,
                slug : this.state.slug,
                meta_title  : this.state.meta_title,
                meta_description : this.state.meta_description,
                meta_keyword : this.state.meta_keyword,          
            }
        }
        Meteor.call('EditCategory',data,function (error) {
            if(!error){
                Bert.alert('Edit successfully', 'success', 'growl-top-right');
                FlowRouter.go('/admin/categories');
            }else{
                Bert.alert('Edit problem', 'danger', 'growl-top-right');
            }
        })
    }       
    render(){
        DocHead.setTitle('Edit Category '+this.state.name);
        return (
            <section className="content">
                <div className="row">
                    <form role="form" onSubmit={this.handleSubmit}>
                        <div className="col-md-12">
                            <div className="box box-primary">
                                <div className="box-body">
                                    <div className="form-group">
                                        <label>Name </label>
                                        <input type="text" value={this.state.name} className="form-control" id="name"
                                               onChange={this.name}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Slug </label>
                                        <input type="text" value={this.state.slug} className="form-control" id="slug"
                                               onChange={this.slug}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Title </label>
                                        <input type="text" value={this.state.meta_title} className="form-control" id="meta_title"
                                               onChange={this.meta_title}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Description </label>
                                        <textarea rows="10" value={this.state.meta_description} className="form-control" id="meta_description"
                                               onChange={this.meta_description}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Keyword </label>
                                        <input type="text" value={this.state.meta_keyword} className="form-control" id="meta_keyword"
                                               onChange={this.meta_keyword}/>
                                    </div>
                                </div>
                                <div className="box-footer">
                                    <div className="form-group">
                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary btn-block"> Edit Category</button>) :''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        )
    }
}

export  class EditSubCategory extends TrackerReact(Component){
    constructor(){
        super();
        this.state = {
            currentUser: Meteor.user(),
            subscription: {
                subcategories: Meteor.subscribe('SubCategories',FlowRouter.current().params.id),
            }
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.name = this.name.bind(this);
        this.slug = this.slug.bind(this);
        this.meta_title = this.meta_title.bind(this);
        this.meta_description = this.meta_description.bind(this);
        this.meta_keyword = this.meta_keyword.bind(this);
    }
    componentWillUnmount(){
        this.state.subscription.subcategories.stop();
    }
    subcategories(){
        return SubCategories.findOne(FlowRouter.current().params.id);
    }
    componentWillMount(){
        this.setState({
            name : this.subcategories() && this.subcategories().name,
            slug : this.subcategories() && this.subcategories().slug,
            meta_title : !_.isUndefined(this.subcategories() && this.subcategories().meta_title) ? this.subcategories() && this.subcategories().meta_title:'',
            meta_description : !_.isUndefined(this.subcategories() && this.subcategories().meta_description) ? this.subcategories() && this.subcategories().meta_description:'',
            meta_keyword : !_.isUndefined(this.subcategories() && this.subcategories().meta_keyword) ? this.subcategories() && this.subcategories().meta_keyword:''
                   
        })
    }
    name(event){
        this.setState({name: event.target.value});
    }
    slug(event){
        this.setState({slug: event.target.value});
    }
    meta_title(event){
        this.setState({meta_title: event.target.value});
    }
    meta_description(event){
        this.setState({meta_description: event.target.value});
    }
    meta_keyword(event){
        this.setState({meta_keyword: event.target.value});
    }
    handleSubmit(event){
        event.preventDefault();
        data = {
            id : FlowRouter.current().params.id,
            set : {
                name : this.state.name,
                slug : this.state.slug,
                meta_title  : this.state.meta_title,
                meta_description : this.state.meta_description,
                meta_keyword : this.state.meta_keyword,          
            }
        }
        Meteor.call('EditSubCategory',data,function (error) {
            if(!error){
                Bert.alert('Edit successfully', 'success', 'growl-top-right');
                FlowRouter.go('/admin/categories');
            }else{
                Bert.alert('Edit problem', 'danger', 'growl-top-right');
            }
        })
    }       
    render(){
        DocHead.setTitle('Edit Sub Category '+this.state.name);
        return (
            <section className="content">
                <div className="row">
                    <form role="form" onSubmit={this.handleSubmit}>
                        <div className="col-md-12">
                            <div className="box box-primary">
                                <div className="box-body">
                                    <div className="form-group">
                                        <label>Name </label>
                                        <input type="text" value={this.state.name} className="form-control" id="name"
                                               onChange={this.name}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Slug </label>
                                        <input type="text" value={this.state.slug} className="form-control" id="slug"
                                               onChange={this.slug}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Title </label>
                                        <input type="text" value={this.state.meta_title} className="form-control" id="meta_title"
                                               onChange={this.meta_title}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Description </label>
                                        <textarea rows="10" value={this.state.meta_description} className="form-control" id="meta_description"
                                               onChange={this.meta_description}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Meta Keyword </label>
                                        <input type="text" value={this.state.meta_keyword} className="form-control" id="meta_keyword"
                                               onChange={this.meta_keyword}/>
                                    </div>
                                </div>
                                <div className="box-footer">
                                    <div className="form-group">
                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary btn-block"> Edit Category</button>) : ''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        )
    }
}
