import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class StoreList extends TrackerReact(Component){
	constructor() {
        super();
        this.state = {
            subscription: {
                StoreList : Meteor.subscribe('StoreList'),
            }
        }
    }
    stores(){
        return Stores.find({},{sort:{createdAt:-1}}).fetch();
    }
    componentWillUnmount(){
        this.state.subscription.StoreList.stop();
    }
    editStore(id){
        FlowRouter.go('/admin/editstore/'+id);
    }
    deleteStore(id){
        //console.log(id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('deleteStore',id,function (error) {
                    if(!error){
                        swal("Deleted!", "Your Store has been deleted.", "success");
                    }
                });
            } else {
                swal("Cancelled", "Your Store is safe :)", "error");
            }
        });
    }
    render(){
    	return (
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <a href="/admin/addstore" className="btn btn-primary btn-block" style={{'marginBottom':'20px'}}>Add Store</a>
                    </div>
                    <div className="col-md-12">
                        <div className="box">
                            <div className="box-body">
                                <table className="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th style={{width: '10px'}}>Store ID</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>latitude</th>
                                        <th>longitude</th>
                                        <th style={{width: '70px'}}>Action</th>
                                    </tr>
                                    {this.stores().map((val)=>{
                                        return (
                                            <tr key={val._id}>
                                                <td>{val._id}</td>
                                                <td>{val.address}</td>
                                                <td>{val.city}</td>
                                                <td>{val.lat}</td>
                                                <td>{val.lng}</td>
                                                <td>
                                                    <button type="button" className="btn btn-primary btn-xs" style={{float:'left'}} onClick={this.editStore.bind(this,val._id)}>
                                                        <i className="fa fa-edit"></i>
                                                    </button>
                                                    {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="button" className="btn btn-danger btn-xs" style={{float:'right'}} onClick={this.deleteStore.bind(this,val._id)}><i className="fa fa-trash"></i></button>) : ''}
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
