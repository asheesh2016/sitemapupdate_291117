import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import DatePicker from 'react-datepicker';
export default class ProductReports extends TrackerReact(Component){
    constructor(){
        super();
        this.state = {
            q : '',
            productId: '',
            startDate : moment().subtract(7,'d'),
            endDate : moment(),
            selected : 'All',
    		subscription: {
    		    reports: Meteor.subscribe('Reports'),
                ProductList : Meteor.subscribe('ProductList'),
    		}
        }
        this.q = this.q.bind(this);
        this.handleChangeEnd = this.handleChangeEnd.bind(this);
        this.handleChangeStart = this.handleChangeStart.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.getProduct = this.getProduct.bind(this);
        this.reports =  this.reports.bind(this);
        this.productChange = this.productChange.bind(this);
    }
    componentWillUnmount(){
        this.state.subscription.reports.stop();
        this.state.subscription.ProductList.stop();
    }
    componentDidMount(){
        $(document).ready(function () {

            console.log("HELLO")
            function exportTableToCSV($table, filename) {
                var $headers = $table.find('tr:has(th)')
                    ,$rows = $table.find('tr:has(td)')

                    // Temporary delimiter characters unlikely to be typed by keyboard
                    // This is to avoid accidentally splitting the actual contents
                    ,tmpColDelim = String.fromCharCode(11) // vertical tab character
                    ,tmpRowDelim = String.fromCharCode(0) // null character

                    // actual delimiter characters for CSV format
                    ,colDelim = '","'
                    ,rowDelim = '"\r\n"';

                    // Grab text from table into CSV formatted string
                    var csv = '"';
                    csv += formatRows($headers.map(grabRow));
                    csv += rowDelim;
                    csv += formatRows($rows.map(grabRow)) + '"';

                    // Data URI
                    var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                // For IE (tested 10+)
                if (window.navigator.msSaveOrOpenBlob) {
                    var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
                        type: "text/csv;charset=utf-8;"
                    });
                    navigator.msSaveBlob(blob, filename);
                } else {
                    $(this)
                        .attr({
                            'download': filename
                            ,'href': csvData
                            //,'target' : '_blank' //if you want it to open in a new window
                    });
                }

                //------------------------------------------------------------
                // Helper Functions 
                //------------------------------------------------------------
                // Format the output so it has the appropriate delimiters
                function formatRows(rows){
                    return rows.get().join(tmpRowDelim)
                        .split(tmpRowDelim).join(rowDelim)
                        .split(tmpColDelim).join(colDelim);
                }
                // Grab and format a row from the table
                function grabRow(i,row){
                     
                    var $row = $(row);
                    //for some reason $cols = $row.find('td') || $row.find('th') won't work...
                    var $cols = $row.find('td'); 
                    if(!$cols.length) $cols = $row.find('th');  

                    return $cols.map(grabCol)
                                .get().join(tmpColDelim);
                }
                // Grab and format a column from the table 
                function grabCol(j,col){
                    var $col = $(col),
                        $text = $col.text();

                    return $text.replace('"', '""'); // escape double quotes

                }
            }


            // This must be a hyperlink
            $("#export").click(function (event) {
                // var outputFile = 'export'
                var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
                outputFile = outputFile.replace('.csv','') + '.csv'
                 
                // CSV
                exportTableToCSV.apply(this, [$('#dvData > table'), outputFile]);
                
                // IF CSV, don't do event.preventDefault() or return false
                // We actually need this to be a typical hyperlink
            });
        });
    }
    buildRegExp(searchText) {
      var words = searchText.trim().split(/[ \-\:]+/);
      var exps = _.map(words, function(word) {
        return "(?=.*" + word + ")";
      });
      var fullExp = exps.join('') + ".+";
      return new RegExp(fullExp, "i");
    }
    q(event){
        this.setState({q:event.target.value});
    }
    products(id){
        return Products.findOne({_id:id})
    }
    products_list(){
        return Products.find().fetch()
    }
    reports(){
        items = []
        if(this.state.q == ''){
            return Orders.find({createdAt: {$gte: this.state.startDate.toDate(), $lt: this.state.endDate.toDate()}}).fetch()
        }else{
            var regExp = this.buildRegExp(this.state.q);
            return Orders.find({$or: [
                    {email: regExp},
                    {name: regExp},
                    {_id: regExp},
                    {phone: regExp}
                ]},{sort:{createdAt:-1}}).fetch()
        }
    }
    print(){
        window.print();
    }
    productList(){
        return Products.find({}).fetch();
    }
    getProduct(event) {
        this.setState({productId: event.target.value});
        console.log(this.state.productId);
        // return Orders.find({}).fetch();
    }
    handleChange({ startDate, endDate}) {
        startDate = startDate || this.state.startDate
        endDate = endDate || this.state.endDate
        if (startDate.isAfter(endDate)) {
            var temp = startDate
                startDate = endDate
                endDate = temp
        }
        this.setState({ startDate, endDate })
    }
    handleChangeStart(startDate) {
        this.handleChange({ startDate })
    }
    handleChangeEnd(endDate) {
        this.handleChange({ endDate })
    }
    productChange(event){
        this.setState({selected:event.target.value});
    }
    render(){
        DocHead.setTitle('Reports');
        let date = (createdAt) => {
            return moment(createdAt).format('DD-MM-YYYY hh:mm');
        }
        let subtotal = () => {
            return _.reduce(this.reports(),function(memo,num){ return memo + Number(num.total)},0) - _.reduce(this.reports(),function(memo,num){ return memo + Number(num.ship)},0) - _.reduce(this.reports(),function(memo,num){ return memo +Number(num.discount)},0);
        }
        let shipping = () => {
            return _.reduce(this.reports(),function(memo,num){return memo + Number(num.ship)},0)
        }
        let discount = () => {
            return _.reduce(this.reports(),function(memo,num){return memo + Number(num.discount)},0)
        }
        let total = () =>{
            return _.reduce(this.reports(),function(memo,num){return memo + Number(num.total)},0)
        }
        let today = () =>{
            return moment().format('DD-MM-YYYY');
        }
        let loop = (val) => {
            return val.cart.items.map((i,x)=>{
                return (
                    <tr key={i}>
                        <td><a href="#">{i}</a></td>
                    </tr>
                )
            })
        }
        return (
            <section className="invoice">
                <div className="row">
                    <div className="col-xs-12">
                        <h2 className="page-header">
                            <i className="fa fa-globe"></i> Wingreens
                            <small className="pull-right">Date: {today()}</small>
                        </h2>
                    </div>
                </div>
                <div className="input-group" style={{'marginBottom':'20px'}}>
                    <input type="text" name="q" className="form-control" placeholder="Search... " value={this.state.q} onChange={this.q}/>
                      <span className="input-group-btn">
                        <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
                        </button>
                      </span>
                </div>
                <div className="row invoice-info" style={{'marginBottom':'20px'}}>
                    <div className="col-sm-4 invoice-col">
                        <span>From</span>
                        <DatePicker
                            selected={this.state.startDate}
                            startDate={this.state.startDate}
                            endDate={this.state.endDate}
                            onChange={this.handleChangeStart} />
                    </div>
                    <div className="col-sm-4 invoice-col">
                        <span>To</span>
                        <DatePicker
                            selected={this.state.endDate}
                            startDate={this.state.startDate}
                            endDate={this.state.endDate}
                            onChange={this.handleChangeEnd} />
                    </div>
                    <div className="col-sm-4 invoice-col">
                        <select className="form-control" onChange={this.productChange}>
                            <option value="All">All</option>
                            {this.products_list().map((val) => {
                                return (
                                    <option value={val._id} key={val._id}>{val.title}</option>
                                )
                            })}
                        </select>
                    </div>                  
                </div>
                <div className="row">
                    <div className="col-xs-12 table-responsive" id="dvData">
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th style={{width: '10px'}}>Order ID</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Date</th>
                                    <th>Product</th>
                                    <th>Count</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.reports().map((val,index)=>{
                                    return (
                                        val.cart.items.map((i,x)=>{
                                            if(i.id == this.state.selected){
                                                return (
                                                    <tr key={val._id+index+i.id}>
                                                        <td><a href={'/admin/view-order/'+val._id}>{val._id}</a></td>
                                                        <td>{val.name}</td>
                                                        <td>{val.phone}</td>
                                                        <td>{date(val.createdAt)}</td>
                                                        <td>{this.products(i.id) && this.products(i.id).title}</td>
                                                        <td>{i.count}</td>
                                                    </tr>
                                                )
                                            }else{
                                                if(this.state.selected == 'All'){
                                                    return (
                                                        <tr key={val._id+index+i.id}>
                                                            <td><a href={'/admin/view-order/'+val._id}>{val._id}</a></td>
                                                            <td>{val.name}</td>
                                                            <td>{val.phone}</td>
                                                            <td>{date(val.createdAt)}</td>
                                                            <td>{this.products(i.id) && this.products(i.id).title}</td>
                                                            <td>{i.count}</td>
                                                        </tr>
                                                    )
                                                }
                                            }
                                        })
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="row no-print">
                    <div className="col-xs-12">
                        <button className="btn btn-default" onClick={() => {this.print()}} style={{marginRight:'10px'}}><i className="fa fa-print"></i> Print</button>
                        <a href="#" id ="export" className="btn btn-default" role='button'><i className="fa fa-download"></i> Download</a>
                    </div>
                </div>
            </section>
        )
    }
}
