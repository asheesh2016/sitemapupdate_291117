import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class AddStore extends TrackerReact(Component){
	constructor() {
        super();
        this.state = {
        	address:'',
        	city:'Mumbai',
        	lat:'',
        	lng:'',
            subscription: {
                StoreList : Meteor.subscribe('StoreList'),
            }
        };
		this.address = this.address.bind(this);
		this.city = this.city.bind(this);
		this.lat = this.lat.bind(this);
		this.lng = this.lng.bind(this);        
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(event){
    	event.preventDefault();
    	if(_.isEmpty(this.state.address)){	
            Bert.alert('Please enter the address', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.city)){
            Bert.alert('Please enter the city', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.lat)){
            Bert.alert('Please enter the lat', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.lng)){
            Bert.alert('Please enter the lng', 'danger', 'growl-top-right');
            return false;
        }
    	data = {
			address : this.state.address,
			city : this.state.city,
			lat : this.state.lat,
			lng : this.state.lng,
        }
        //console.log(data);
        Meteor.call('createStore',data,function (error) {
            if(!error){
                Bert.alert('Store created successfully', 'success', 'growl-top-right');
                FlowRouter.go('/admin/storelist')
            }else{
                Bert.alert('Store already exists', 'danger', 'growl-top-right');
            }
        })
    }
	address(event){
		this.setState({address:event.target.value});
	}
	city(event){
		this.setState({city:event.target.value});
	}
	lat(event){
		this.setState({lat:event.target.value});
	}
	lng(event){
		this.setState({lng:event.target.value});
	}
    render(){
    	DocHead.setTitle('Create Store');
    	return (
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <div className="box">
                    		<form role="form" onSubmit={this.handleSubmit}>
	                            <div className="box-body">
		                            <div className="form-group">
		                                <label> Address </label>
		                                <input type="textarea" className="form-control" id="address" onChange={this.address}/>
		                            </div>
		                            <div className="form-group">
		                                <label> City </label>
                                        <select className="form-control" id="city" onChange={this.city}>
                                            <option value="Mumbai">Mumbai</option>
                                            <option value="Delhi">Delhi</option>
                                            <option value="Bangalore">Bangalore</option>
                                            <option value="Chennai">Chennai</option>
                                            <option value="Hyderabad">Hyderabad</option>
                                            <option value="Ahmedabad">Ahmedabad</option>
                                            <option value="Kolkata">Kolkata</option>
                                            <option value="Surat">Surat</option>
                                            <option value="Pune">Pune</option>
                                            <option value="Jaipur">Jaipur</option>
                                            <option value="Lucknow">Lucknow</option>
                                        </select>
		                            </div>
		                            <div className="form-group">
		                                <label> Latitude </label>
		                                <input type="text" className="form-control" id="lat" onChange={this.lat}/>
		                            </div>
		                            <div className="form-group">
		                                <label> Longitude </label>
		                                <input type="text" className="form-control" id="lng" onChange={this.lng}/>
		                            </div>
		                            <div className="box-footer">
	                                    <div className="form-group">
	                                        <button type="submit" className="btn btn-primary"> Add Store</button>
	                                    </div>
	                                </div>
		                        </div>    
	                		</form>    
                        </div>
                    </div>
                </div>
            </section>
        )
    }	
}