import React ,{Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
export default class UsersList extends TrackerReact(Component) {

    constructor() {
        super();
        this.state = {
            q : '',
            skip : 0,
            limit : 50,
            selected : 0,
            subscription: {
                userslist: Meteor.subscribe('userslist', 'user'),
                ShippingAll : Meteor.subscribe('ShippingAll'),
            }
        }
        this.q = this.q.bind(this);
    }
    componentWillUnmount() {
        this.state.subscription.userslist.stop();
        this.state.subscription.ShippingAll.stop();
    }
    usersList() {
        if(this.state.q == ''){
            return Meteor.users.find({roles: 'user'},{skip: this.state.skip, limit: this.state.limit}).fetch();
        }else{
            var regExp = this.buildRegExp(this.state.q);
            var shipping = Shipping.find({$or : [
                {city : regExp},
                {state : regExp},
                {address : regExp},
                {zipcode : regExp},
            ]}).fetch()
            var userIds = _.map(shipping,function(val){ return val.userId});
            return Meteor.users.find({$or: [
                    {'emails.address': regExp},
                    {'profile.name': regExp},
                    {'profile.phone': regExp},
                    {"_id": { "$in": userIds }}
                ],roles: 'user'},{limit : 50}).fetch()
        }
    }
    count() { 
        var count = Meteor.users.find({roles: 'user'}).count()
        var page = parseInt(count/50) + ((count%50) != 0 ? 1 : 0);
        return page;
    }
    next(item){
        this.setState({skip:item*50,limit:50});
        this.setState({selected : item});
    }
    address(id){
        return Shipping.find({userId:id}).fetch();
    }
    viewProfile(id){
        FlowRouter.go('/admin/view-profile/'+id);
    }
    buildRegExp(searchText) {
      var words = searchText.trim().split(/[ \-\:]+/);
      var exps = _.map(words, function(word) {
        return "(?=.*" + word + ")";
      });
      var fullExp = exps.join('') + ".+";
      return new RegExp(fullExp, "i");
    }
    q(event){
        this.setState({q:event.target.value});
    }
    render() {
        DocHead.setTitle('Users List');
        let self = this;
        let pagegination = () => {
            return _.map(_.range(this.count()),function(val,index){
                return (<li key={val} className={self.state.selected == val ? 'active':''}><a href="javascript:void(0)" onClick={self.next.bind(self,val)}>{val+1}</a></li>)
            })
        }
        return (
            <section className="content">
                <div className="box">
                    <div className="box-header with-border">
                        <h3 className="box-title">Users</h3>  
                    </div>
                    <div className="box-body">
                        <div className="col-md-12">
                            <div className="input-group" style={{'marginBottom':'20px'}}>
                                <input type="text" name="q" className="form-control" placeholder="Search... " value={this.state.q} onChange={this.q}/>
                                  <span className="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
                                    </button>
                                  </span>
                            </div>
                            <ul className="pagination pagination-sm pull-right" style={{'marginBottom':'20px !important'}}>
                                {this.state.q != '' ? '' : pagegination()}
                            </ul>
                            <table className="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>Country</th>
                                        <th>Zipcode</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.usersList().map((val,index)=> {
                                        return (
                                            <tr key={val._id}>
                                                <td>{this.state.q != '' ? (index + 1): (50*(this.state.selected))+index + 1}</td>
                                                <td>{_.isUndefined(val.profile) ? '':val.profile.name}</td>
                                                <td>{_.isUndefined(val.emails) ? '' : val.emails[0].address}</td>
                                                <td>{_.isUndefined(val.profile) ? '' : val.profile.phone}</td>
                                                <td>
                                                    {this.address(val._id).map((a,i)=> {
                                                        return (
                                                            <p key={a._id}>{a.address}</p>
                                                        )
                                                    })}
                                                </td>
                                                <td>
                                                    {this.address(val._id).map((a,i)=> {
                                                        return (
                                                            <p key={a._id}>{a.city}</p>
                                                        )
                                                    })}
                                                </td>
                                                <td>
                                                    {this.address(val._id).map((a,i)=> {
                                                        return (
                                                            <p key={a._id}>{a.state}</p>
                                                        )
                                                    })}
                                                </td>
                                                <td>
                                                    {this.address(val._id).map((a,i)=> {
                                                        return (
                                                            <p key={a._id}>{a.country}</p>
                                                        )
                                                    })}
                                                </td>
                                                <td>
                                                    {this.address(val._id).map((a,i)=> {
                                                        return (
                                                            <p key={a._id}>{a.zipcode}</p>
                                                        )
                                                    })}
                                                </td>
                                                <td>
                                                    <button type="button" className="btn btn-primary btn-xs" style={{float:'left'}} onClick={this.viewProfile.bind(this,val._id)}>
                                                        <i className="fa fa-eye"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="box-footer">
                        <ul className="pagination pagination-sm no-margin pull-right">
                            {this.state.q != '' ? '' : pagegination()}
                        </ul>
                    </div>
                </div>
            </section>
        )
    }
}