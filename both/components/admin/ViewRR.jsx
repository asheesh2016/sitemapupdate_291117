import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class ViewRR extends TrackerReact(Component){
    constructor(){
        super();
        this.state = {
            subscription : {
                rr : Meteor.subscribe('RatingByAdmin')
            }
        }
    }
    componentWillUnmount(){
        this.state.subscription.rr.stop();
    }
    user(id){
        return Meteor.users.findOne({_id:id});
    }
    product(id){
        return Products.findOne({_id:id});
    }
    reviews(){
        return Reviews.find().fetch();
    }
    deleteReview(id){
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('deleteReview',id,function (error) {
                    if(!error){
                        swal("Deleted!", "Review has been deleted.", "success");
                    }
                });
            } else {
                swal("Cancelled", "Your Review is safe :)", "error");
            }
        });
    }
    render(){
        DocHead.setTitle('Reviews');
        return ( 
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <div className="box">
                            <div className="box-body">
                                <table className="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>User</th>
                                            <th>Phone</th>
                                            <th>Product</th>
                                            <th>Review</th>
                                            <th>Rating</th>
                                            <th style={{width: '70px'}}>Action</th>
                                        </tr>
                                        {this.reviews().map((val) => {
                                            return (
                                                <tr key={val._id} valign="center">
                                                    <td style={{verticalAlign: 'middle'}}>{this.user(val.userId) && this.user(val.userId).profile.name}</td>
                                                    <td style={{verticalAlign: 'middle'}}>{this.user(val.userId) && this.user(val.userId).profile.phone}</td>
                                                    <td style={{verticalAlign: 'middle'}} style={{width:'150px'}}><a href={'/admin/edit-product/'+ (this.product(val.productId) && this.product(val.productId)._id)}><img src={this.product(val.productId) && this.product(val.productId).featuredImage} width="150"/></a></td>
                                                    <td style={{verticalAlign: 'middle'}}>{val.review}</td>
                                                    <td style={{verticalAlign: 'middle'}}>{val.rating}</td>
                                                    <td style={{verticalAlign: 'middle'}}>
                                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="button" className="btn btn-danger btn-xs" style={{float:'right'}} onClick={this.deleteReview.bind(this,val._id)}><i className="fa fa-trash"></i></button>) : ''}
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export {
    ViewRR
}
