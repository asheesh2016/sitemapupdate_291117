import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import Dropzone from 'react-dropzone';

export default class AdminSliders extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            subscription: {
                sliders: Meteor.subscribe('Sliders'),
            }            
        };
    }
    componentWillUnmount() {
        this.state.subscription.sliders.stop();
    }    
    slidersList(){
        return Sliders.find({}).fetch();
    }
    addSlider(event){
        FlowRouter.go('/admin/addslider');
    }
    editSlider(id){ 
    	FlowRouter.go('/admin/editslider/'+id);
    }
    deleteSlider(id){
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('deleteSlider',id,function (error) {
                    if(!error){
                        swal("Deleted!", "Your Slider has been deleted.", "success");
                    }
                });
            } else {
                swal("Cancelled", "Your Slider is safe :)", "error");
            }
        });
    }
    render(){
        DocHead.setTitle('Home Page Sliders List');
        return(
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <div className="box">
                            <div className="box-header with-border">
                                <h3 className="box-title">Sliders List</h3>
                                <button className="btn btn-sm btn-primary btn-space" name="add" onClick={this.addSlider.bind(this)} style={{marginLeft:'10px'}}> Add Slider</button>
                            </div>
                            <div className="box-body">
                                <table className="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>Content</th>
                                            <th>Image</th>	
                                            <th>Link</th>
                                            <th>Order</th>
                                            <th style={{width: '70px'}}>Action</th>
                                        </tr>
                                        {this.slidersList().map((val) => {
                                            return (
                                                <tr key={val._id}>
                                                    <td>{val.content}</td>
                                                    <td>{val.featuredImage}</td>
                                                    <td>{val.link}</td>
                                                    <td>{val.order}</td>
                                                    <td>
                                                        <button type="button" className="btn btn-primary btn-xs" style={{float:'left'}} onClick={this.editSlider.bind(this,val._id)}><i className="fa fa-edit"></i></button>
                                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="button" className="btn btn-danger btn-xs" style={{float:'right'}} onClick={this.deleteSlider.bind(this,val._id)}><i className="fa fa-trash"></i></button>) : '' }
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>        
        )
    }
}
export  class AdminAddSlider extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            featuredImage: '',
        };
        this.onDropSingle = this.onDropSingle.bind(this);
        this.addSlider = this.addSlider.bind(this);
    }  
    addSlider(event){
        event.preventDefault();
        if(_.isEmpty(this.state.featuredImage)){
            Bert.alert('Please enter the featured Image', 'danger', 'growl-top-right');
            return false;
        }
		data = {
            content : $("#slider-content").val(),
            link : $("#slider-link").val(),
            featuredImage : this.state.featuredImage,
            order : $('#slider-order').val(),
            sliderAltTag :$("#slider-alttag").val(),
            createdAt : new Date(),
        }  
        Meteor.call('addSlider', data, function(error) {
            if(error) {
                Bert.alert('Error Adding Slider', 'danger', 'growl-top-right');
            } else {
                FlowRouter.go('/admin/sliders');
                Bert.alert('Slider Added Successfully', 'success', 'growl-top-right');
            }
        });
    }
    onDropSingle(files){
        let self = this;
        if(files[0].size/1024 > 2048){
            Bert.alert('File Size is more than 2MB', 'danger', 'growl-top-right');
        }else{
            let uploader = new Slingshot.Upload("myDefinedDirective");
            uploader.send(files[0], (error, downloadUrl) => {
                if (error) {
                    Bert.alert('Error in file upload', 'danger', 'growl-top-right');
                }
                else {
                    console.log(downloadUrl)
                    self.setState({featuredImage: downloadUrl});
                }
            });
        }
    }
    removeImageFeatured(){
        this.setState({featuredImage: ''});
    }
    render(){
    	DocHead.setTitle('Add Slider');
        let featureImageBox;
        if(this.state.featuredImage){
            featureImageBox = (
                <div className="col-sm-12" style={{position:'relative'}}>
                    <img className="img-responsive" src={this.state.featuredImage} alt="Photo" />
                    <button type="button" className="btn btn-danger btn-xs" style={{position:'absolute',top:5,right:20}} onClick={this.removeImageFeatured.bind(this)}><i className="fa fa-trash"></i></button>
                </div>
            )
        }else{
            featureImageBox = (
                <Dropzone onDrop={this.onDropSingle} multiple={false} accept="image/*" style={{border:'none',height:200,width:'100%',background:'#ccc',display:'flex',alignItems:'center',justifyContent:'center'}}>
                    <div style={{color:'#fff',textAlign:'center'}}>Try dropping some file here, or click to select file to upload.</div>
                </Dropzone>
            )
        }        
        return (
            <section className="content">
                <div className="box">
                    <form onSubmit={this.addSlider}>
                        <div className="box-body">
                            <div className="form-group col-md-12">
                                <label> Link </label>
                                <input className="form-control" id="slider-link" />
                            </div>
                             <div className="form-group col-md-12">
                                <label> Slider Alt Tag </label>
                                <input className="form-control" id="slider-alttag" />
                            </div>
                            <div className="form-group col-md-12">
                                <label> Slider Content </label>
                                <input className="form-control" id="slider-content"	/>
                            </div>
                            <div className="form-group col-md-12">
                                <label> Slider Order </label>
                                <input className="form-control" id="slider-order" />
                            </div>
                            <div className="form-group col-md-6">
                                <label> Slider Image (Please Add 1200x800 Image)</label>
                                {featureImageBox}
                            </div>
                        </div>
                        <div className="box-footer">
                            <div className="form-group">
                                {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary">Create Slider</button>) : '' }
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        )
    }
}
export  class AdminEditSlider extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            featuredImage: '',
            content:'',
            link: '',
            sliderAltTag:'',
            order: '',
            subscription:{
            	slider: Meteor.subscribe('SingleSlider',FlowRouter.current().params.id),
            }
        };
        this.content = this.content.bind(this);
        this.link = this.link.bind(this);
        this.sliderAltTag =this.sliderAltTag.bind(this);
        this.order = this.order.bind(this);
        this.onDropSingle = this.onDropSingle.bind(this);
        this.editSlider = this.editSlider.bind(this);
    } 
    componentWillUnmount() {
        this.state.subscription.slider.stop();
    }  
    edit() {
    	return Sliders.findOne(FlowRouter.current().params.id);
    }
    componentWillMount() {
        var self = this;
        self.setState({
            content : self.edit() && self.edit().content,
            sliderAltTag : self.edit() && self.edit().sliderAltTag,
            featuredImage: self.edit() && self.edit().featuredImage,
            link: self.edit() && self.edit().link,
            order : self.edit() && self.edit().order,
        });
    }     
    content(event) {
    	this.setState({content:event.target.value});
    }
    link(event) {
        this.setState({link:event.target.value});
    }
    order(event){
        this.setState({order:event.target.value});
    }
    sliderAltTag(event){
        this.setState({sliderAltTag:event.target.value});
    }
    editSlider(event) {
        event.preventDefault();
        if(_.isEmpty(this.state.featuredImage)){
            Bert.alert('Please enter the featured Image', 'danger', 'growl-top-right');
            return false;
        }
		if(_.isEmpty(this.state.content)){
            Bert.alert('Please enter the Content for Slider', 'danger', 'growl-top-right');
            return false;
        }        
		data = {
			id : FlowRouter.current().params.id,
			set :{
	            content :  this.state.content,
                link : this.state.link,
	            featuredImage : this.state.featuredImage,
                sliderAltTag :this.state.sliderAltTag,
                order : this.state.order,
	            createdAt : new Date(),
	        }
        }  
        Meteor.call('editSlider', data, function(error) {
            if(error) {
                Bert.alert('Error Editing Slider', 'danger', 'growl-top-right');
            } else {
                FlowRouter.go('/admin/sliders');
                Bert.alert('Slider Edited Successfully', 'success', 'growl-top-right');
            }
        });
    }
    onDropSingle(files) {
        let self = this;
        if(files[0].size/1024 > 2048){
            Bert.alert('File Size is more than 2MB', 'danger', 'growl-top-right');
        }else{
            let uploader = new Slingshot.Upload("myDefinedDirective");
            uploader.send(files[0], (error, downloadUrl) => {
                if (error) {
                    Bert.alert('Error in file upload', 'danger', 'growl-top-right');
                }
                else {
                    console.log(downloadUrl)
                    self.setState({featuredImage: downloadUrl});
                }
            });
        }
    }
    removeImageFeatured() {
        this.setState({featuredImage: ''});
    }
    render(){
        let featureImageBox;
        if(this.state.featuredImage){
            featureImageBox = (
                <div className="col-sm-12" style={{position:'relative'}}>
                    <img className="img-responsive" src={this.state.featuredImage} alt="Photo" />
                    <button type="button" className="btn btn-danger btn-xs" style={{position:'absolute',top:5,right:20}} onClick={this.removeImageFeatured.bind(this)}><i className="fa fa-trash"></i></button>
                </div>
            )
        }else{
            featureImageBox = (
                <Dropzone onDrop={this.onDropSingle} multiple={false} accept="image/*" style={{border:'none',height:200,width:'100%',background:'#ccc',display:'flex',alignItems:'center',justifyContent:'center'}}>
                    <div style={{color:'#fff',textAlign:'center'}}>Try dropping some file here, or click to select file to upload.</div>
                </Dropzone>
            )
        }
        DocHead.setTitle('Edit Slider');
        return (
            <section className="content">
                <div className="box">
                    <form onSubmit={this.editSlider}>
                        <div className="box-body">
                            <div className="form-group col-md-12">
                                <label> Slider Link </label>
                                <input className="form-control" id="slider-content" value={this.state.link} onChange={this.link}/>
                            </div>
                             <div className="form-group col-md-12">
                                <label> Slider Alt Tag </label>
                                <input className="form-control" id="slider-alttag" value={this.state.sliderAltTag} onChange={this.sliderAltTag}/>
                            </div>
                            <div className="form-group col-md-12">
                                <label> Slider Content </label>
                                <input className="form-control" id="slider-content"	value={this.state.content} onChange={this.content}/>
                            </div>
                            <div className="form-group col-md-12">
                                <label> Slider Order </label>
                                <input className="form-control" id="slider-content" value={this.state.order} onChange={this.order}/>
                            </div>
                            <div className="form-group col-md-6">
                                <label> Slider Image (Please Add 1200x800 Image)</label>
                                {featureImageBox}
                            </div>
                        </div>
                        <div className="box-footer">
                            <div className="form-group">
                                {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary">Edit Slider</button>) : ''}
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        )
    }
}