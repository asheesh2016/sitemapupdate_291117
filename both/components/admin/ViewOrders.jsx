import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import DatePicker from 'react-datepicker';
export default class ViewOrders extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            q : '',
            startDate : moment().subtract(7,'d'),
            endDate : moment(),
            subscription: {
                OrderHistoryAdmin : Meteor.subscribe('OrderHistoryAdmin'),
            }
        }
        this.q = this.q.bind(this);
        this.handleChangeEnd = this.handleChangeEnd.bind(this);
        this.handleChangeStart = this.handleChangeStart.bind(this);
    }
    orders(){
        if(this.state.q == ''){
            return Orders.find({createdAt: {$gte: this.state.startDate.toDate(), $lt: this.state.endDate.toDate()}},{sort:{createdAt:-1}}).fetch()
        }else if(this.state.q == '1' ){
            return Orders.find({status : 2},{sort:{createdAt:-1}}).fetch()
        }else if(this.state.q == '2' ){
            return Orders.find({status : 3},{sort:{createdAt:-1}}).fetch()
        }else if(this.state.q == '3'){
            return Orders.find({status : 4},{sort:{createdAt:-1}}).fetch()
        }else if(this.state.q == '4'){
            return Orders.find({status : 5},{sort:{createdAt:-1}}).fetch()
        }else{
            var regExp = this.buildRegExp(this.state.q);
            return Orders.find({$or: [
                    {email: regExp},
                    {name: regExp},
                    {_id: regExp},
                    {phone: regExp},
                    {couponcode : regExp}
                ]},{sort:{createdAt:-1}}).fetch()
        }     
    }
    componentWillUnmount(){
        this.state.subscription.OrderHistoryAdmin.stop();
    }
    componentDidMount(){
        $(document).ready(function () {

            function exportTableToCSV($table, filename) {
                var $headers = $table.find('tr:has(th)')
                    ,$rows = $table.find('tr:has(td)')

                    // Temporary delimiter characters unlikely to be typed by keyboard
                    // This is to avoid accidentally splitting the actual contents
                    ,tmpColDelim = String.fromCharCode(11) // vertical tab character
                    ,tmpRowDelim = String.fromCharCode(0) // null character

                    // actual delimiter characters for CSV format
                    ,colDelim = '","'
                    ,rowDelim = '"\r\n"';

                    // Grab text from table into CSV formatted string
                    var csv = '"';
                    csv += formatRows($headers.map(grabRow));
                    csv += rowDelim;
                    csv += formatRows($rows.map(grabRow)) + '"';

                    // Data URI
                    var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                // For IE (tested 10+)
                if (window.navigator.msSaveOrOpenBlob) {
                    var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
                        type: "text/csv;charset=utf-8;"
                    });
                    navigator.msSaveBlob(blob, filename);
                } else {
                    $(this)
                        .attr({
                            'download': filename
                            ,'href': csvData
                            //,'target' : '_blank' //if you want it to open in a new window
                    });
                }

                //------------------------------------------------------------
                // Helper Functions 
                //------------------------------------------------------------
                // Format the output so it has the appropriate delimiters
                function formatRows(rows){
                    return rows.get().join(tmpRowDelim)
                        .split(tmpRowDelim).join(rowDelim)
                        .split(tmpColDelim).join(colDelim);
                }
                // Grab and format a row from the table
                function grabRow(i,row){
                     
                    var $row = $(row);
                    //for some reason $cols = $row.find('td') || $row.find('th') won't work...
                    var $cols = $row.find('td'); 
                    if(!$cols.length) $cols = $row.find('th');  

                    return $cols.map(grabCol)
                                .get().join(tmpColDelim);
                }
                // Grab and format a column from the table 
                function grabCol(j,col){
                    var $col = $(col),
                        $text = $col.text();

                    return $text.replace('"', '""'); // escape double quotes

                }
            }


            // This must be a hyperlink
            $("#export").click(function (event) {
                // var outputFile = 'export'
                var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
                outputFile = outputFile.replace('.csv','') + '.csv'
                 
                // CSV
                exportTableToCSV.apply(this, [$('#dvData > table'), outputFile]);
                
                // IF CSV, don't do event.preventDefault() or return false
                // We actually need this to be a typical hyperlink
            });
        });
    }
    viewOrder(id){
        FlowRouter.go('/admin/view-order/'+id);
    }
    buildRegExp(searchText) {
      var words = searchText.trim().split(/[ \-\:]+/);
      var exps = _.map(words, function(word) {
        return "(?=.*" + word + ")";
      });
      var fullExp = exps.join('') + ".+";
      return new RegExp(fullExp, "i");
    }
    handleChange({ startDate, endDate}) {
        startDate = startDate || this.state.startDate
        endDate = endDate || this.state.endDate
        if (startDate.isAfter(endDate)) {
            var temp = startDate
                startDate = endDate
                endDate = temp
        }
        this.setState({ startDate, endDate })
    }
    handleChangeStart(startDate) {
        this.handleChange({ startDate })
    }
    handleChangeEnd(endDate) {
        this.handleChange({ endDate })
    }
    deleteOrder(id){
        //console.log(id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('deleteOrder',id,function (error) {
                    if(!error){
                        swal("Deleted!", "Your Order has been deleted.", "success");
                    }
                });
            } else {
                swal("Cancelled", "Your Order is safe :)", "error");
            }
        });
    }
    q(event){
        this.setState({q:event.target.value});
    }
    print(){
        window.print();
    }
    render() {
        DocHead.setTitle('Order List');
        let date = (createdAt) => {
            return moment(createdAt).format('DD-MM-YYYY hh:mm');
        }
        let status = (status) => {
            if(status == 2){
                return (
                    <span className="label label-info">Processing</span>
                );
            }else if(status == 3){
                return (
                    <span className="label label-warning">Shipped</span>
                );
            }else if(status == 4){
                return (
                    <span className="label label-success">Completed</span>
                );
            }else if(status == 5){
                 return (
                    <span className="label label-danger">Cancelled</span>
                );
            }
        }
        let type = (type) =>{
            if(type == '0'){
                return 'Flat Discount';
            }else if(type == '1'){
                return 'X % of cart total';
            }else{
                return '';
            }
        }
        return (
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <a href="/admin/create-order" className="btn btn-primary btn-block" style={{'marginBottom':'20px'}}>Create Order</a>
                    </div>
                    <div className="col-md-12">
                        <div className="box">
                            <div className="box-body" id="dvData">
                                <div className="input-group" style={{'marginBottom':'20px'}}>
                                  <input type="text" name="q" className="form-control" placeholder="Search... type 1 for Processing , 2 for Shipping , 3 for Completed, 4 for Cancelled" value={this.state.q} onChange={this.q}/>
                                      <span className="input-group-btn">
                                        <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
                                        </button>
                                      </span>
                                </div>
                                <div className="row invoice-info"  style={{'marginBottom':'20px'}}>
                                    <div className="col-sm-4 invoice-col">
                                        <span>From</span>
                                        <DatePicker
                                            selected={this.state.startDate}
                                            startDate={this.state.startDate}
                                            endDate={this.state.endDate}
                                            onChange={this.handleChangeStart} />
                                    </div>
                                    <div className="col-sm-4 invoice-col">
                                        <span>To</span>
                                        <DatePicker
                                            selected={this.state.endDate}
                                            startDate={this.state.startDate}
                                            endDate={this.state.endDate}
                                            onChange={this.handleChangeEnd} />
                                    </div>
                                </div>
                                <table className="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th style={{width: '10px'}}>Order ID</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Total</th>
                                        <th>Shipping Charge</th>
                                        <th>Discount</th>
                                        <th>Coupon Code</th>
                                        <th>Coupon Type</th>
                                        <th style={{display:'none'}}>Address</th>
                                        <th style={{display:'none'}}>City</th>
                                        <th style={{display:'none'}}>State</th>
                                        <th style={{display:'none'}}>Country</th>
                                        <th style={{display:'none'}}>Zipcode</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th style={{width: '70px'}}>Action</th>
                                    </tr>
                                    {this.orders().map((val)=>{
                                        return (
                                            <tr key={val._id}>
                                                <td><a href="javascript:;" onClick={this.viewOrder.bind(this,val._id)}>{val._id}</a></td>
                                                <td>{val.name}</td>
                                                <td>{val.phone}</td>
                                                <td>{val.email}</td>
                                                <td>{val.total}</td>
                                                <td>{val.ship}</td>
                                                <td>{val.discount}</td>
                                                <td>{val.couponcode}</td>
                                                <td>{type(val.type)}</td>
                                                <td style={{display:'none'}}>{_.isUndefined(val.address) ? '' : val.address.address }</td>
                                                <td style={{display:'none'}}>{_.isUndefined(val.address) ? '' : val.address.city }</td>
                                                <td style={{display:'none'}}>{_.isUndefined(val.address) ? '' : val.address.state }</td>
                                                <td style={{display:'none'}}>{_.isUndefined(val.address) ? '' : val.address.country }</td>
                                                <td style={{display:'none'}}>{_.isUndefined(val.address) ? '' : val.address.zipcode }</td>
                                                <td>{status(val.status)}</td>
                                                <td>{date(val.createdAt)}</td>
                                                <td>
                                                    <button type="button" className="btn btn-primary btn-xs" style={{float:'left'}} onClick={this.viewOrder.bind(this,val._id)}>
                                                        <i className="fa fa-eye"></i>
                                                    </button>
                                                    {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="button" className="btn btn-danger btn-xs" style={{float:'right'}} onClick={this.deleteOrder.bind(this,val._id)}><i className="fa fa-trash"></i></button>) : ''}
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row no-print">
                    <div className="col-xs-12">
                        <button className="btn btn-default" onClick={() => {this.print()}} style={{marginRight:'10px'}}><i className="fa fa-print"></i> Print</button>
                        <a href="#" id ="export" className="btn btn-default" role='button'><i className="fa fa-download"></i> Download</a>
                    </div>
                </div>
            </section>
        )
    }
}

export {
    ViewOrders
}
