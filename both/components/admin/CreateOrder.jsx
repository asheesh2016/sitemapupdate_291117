import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class CreateOrder extends TrackerReact(Component) {
	constructor() {
        super();
        this.state = {
        	name : '',
			email : '',
			phone : '',
			address : '',
			city : '',
			states : '',
			country : '',
			zipcode : '',
			subtotal : '',
			discount : '',
			ship : '',
			total : '',
			count : 1,
			subscription: {
                ProductList : Meteor.subscribe('ProductList'),
            }
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.add = this.add.bind(this);
        this.name = this.name.bind(this);
		this.email = this.email.bind(this);
		this.phone = this.phone.bind(this);
		this.address = this.address.bind(this);
		this.city = this.city.bind(this);
		this.states = this.states.bind(this);
		this.country = this.country.bind(this);
		this.zipcode = this.zipcode.bind(this);
		this.subtotal = this.subtotal.bind(this);
		this.discount = this.discount.bind(this);
		this.ship = this.ship.bind(this);
		this.total = this.total.bind(this);
    }
    products(){
        return Products.find({}).fetch()
    } 
    name(event){
    	this.setState({name:event.target.value});
    }
	email(event){
		this.setState({email:event.target.value});
	}
	phone(event){
		this.setState({phone:event.target.value});
	}
	address(event){
		this.setState({address:event.target.value});
	}
	city(event){
		this.setState({city:event.target.value});
	}
	states(event){
		this.setState({states:event.target.value});
	}
	country(event){
		this.setState({country:event.target.value});
	}
	zipcode(event){
		this.setState({zipcode:event.target.value});
	}
	subtotal(event){
		this.setState({subtotal:event.target.value});
	}
	discount(event){
		this.setState({discount:event.target.value});
	}
	ship(event){
		this.setState({ship:event.target.value});
	}
	total(event){
		this.setState({total:event.target.value});
	}
    handleSubmit(event){
    	event.preventDefault();
    	product = []
    	count = []
        function isPhone(phone){
            var regex = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
            return regex.test(phone);
        }
        function isFullName(name){
            var regex = /^[A-Za-z\s]{3,30}$/;
            return regex.test(name);
        }
        function isEmpty(str){
            return !str.replace(/^\s+/g, '').length; // boolean (`true` if field is empty)
        }
    	if(_.isEmpty(this.state.name) || isEmpty(this.state.name)){
            event.preventDefault();
            Bert.alert('Please enter the valid name', 'danger', 'growl-top-right');
            return false;
        }else if(!isFullName(this.state.name)){
            event.preventDefault();
            Bert.alert('Please enter the valid name', 'danger', 'growl-top-right');
            return false;
        }

        if(_.isEmpty(this.state.email) || isEmpty(this.state.email)){
            Bert.alert('Please enter the valid email', 'danger', 'growl-top-right');
            return false;
        }else{
        	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
            if (!this.state.email.match(mailformat)){
                Bert.alert('Please enter valid email', 'danger', 'growl-top-right');
                return false;
            }
        }
        if(_.isEmpty(this.state.phone) || isEmpty(this.state.phone)){
            event.preventDefault();
            Bert.alert('Please enter the valid phone', 'danger', 'growl-top-right');
            return false;
        }else if(!isPhone(this.state.phone)){
            event.preventDefault();
            Bert.alert('Please enter the valid phone', 'danger', 'growl-top-right');
            return false;
        }

        if(_.isEmpty(this.state.address) || isEmpty(this.state.address)){
            Bert.alert('Please enter the valid address', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.city) || !isFullName(this.state.city) || isEmpty(this.state.city)){
            Bert.alert('Please enter the valid city', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.states) || !isFullName(this.state.states) || isEmpty(this.state.states)){
            Bert.alert('Please enter the valid states', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.country) || !isFullName(this.state.country) || isEmpty(this.state.country)){
            Bert.alert('Please enter the valid country', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.zipcode) || isEmpty(this.state.zipcode)){
            Bert.alert('Please enter the valid zipcode', 'danger', 'growl-top-right');
            return false;
        }
        if(isNaN(this.state.zipcode) && (this.state.zipcode.length > 6 || this.state.zipcode.length <6)){
            Bert.alert('Please enter valid zipcode', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.subtotal) || isEmpty(this.state.subtotal)){
            Bert.alert('Please enter the subtotal', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.discount) || isEmpty(this.state.discount)){
            Bert.alert('Please enter the discount', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.ship) || isEmpty(this.state.ship)){
            Bert.alert('Please enter the shipping', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.total) || isEmpty(this.state.total)){
            Bert.alert('Please enter the total', 'danger', 'growl-top-right');
            return false;
        }
        //if(_.isEmpty(this.state.total)){
        

    	//}
		$('[id^=products]').each(function(i, item) {
	         var p =  $(item).val();
	         product.push(p);
	    });
	    $('[id^=qty]').each(function(i, item) {
	         var p =  $(item).val();
	         count.push(p);
	    });
	    items = _.map(product,function(val,index){
	    	if(count[index]){
		    	return {
		    		id : val,
		    		count : count[index]
		    	}
		    }else{
		    	return {
		    		id : val,
		    		count : 1
		    	}
		    }
	    })
	    data = {
			  "cart": {
			    "_id": "admin",
			    "userId": "admin",
			    "items": items
			  },
			  "subtotal": this.state.subtotal,
			  "discount": this.state.discount,
			  "ship": this.state.ship,
			  "total": this.state.total,
			  "createdBy": "admin",
			  "createdAt" : new Date(),
			  "status": 2,
			  "name": this.state.name,
			  "phone": this.state.phone,
			  "email": this.state.email,
			  "address": {
			    "address": this.state.address,
			    "city": this.state.city,
			    "state": this.state.states,
			    "country": this.state.country,
			    "zipcode": this.state.zipcode,
			    "createdAt": new Date(),
			    "userId": "admin"
			  }
		}
		var flag = false;
		if(_.isEmpty(count)){
			_.each(count,function(val){
				if(_.isEmpty(val)){
					Bert.alert('Please enter the total', 'danger', 'growl-top-right');
					flag = true;
            		return false;
				}
			})
	    }
	    if(!flag){
	    	id = Meteor.call('adminOrder',data,function(err,data){
				FlowRouter.go('/admin/view-orders');
			})
			return true;
	    }
		
    }
    add(event){
    	var x = this.state.count;
    	this.setState({count:(x+1)})
    }
    render(){
    	return (
    		<section className="content">
                <div className="row">
                    <form role="form" onSubmit={this.handleSubmit}>
                        <div className="col-md-12">
                            <div className="box box-primary">
                                <div className="box-body">
                                	{_.map(_.range(this.state.count),(count,index)=>{
                                    	return (
                                    		<div key={'h'+count}>
			                                    <div className="form-group">
			                                        <label> Product </label>
			                                        <select className="form-control product" id={'products['+index+']'}>
			                                        	{this.products().map((val)=>{
			                                        		return (<option key={val._id} value={val._id}>{val.title}</option>)
			                                        	})}
			                                        </select>
			                                    </div>
			                                    <div className="form-group">
			                                        <label> Quantity </label>
			                                        <input type="text" className="form-control" id="product-title" id={'qty['+index+']'}/>
			                                    </div>
		                                    </div>
	                                    )
                                    })}
                                    <div className="form-group">
                                        <a href="javascript.void(0);" className="btn btn-primary" id="add" onClick={this.add}>Add</a>
                                    </div>
                                	<div className="form-group">
                                        <label> Name </label>
                                        <input type="text" className="form-control" id="product-title" onChange={this.name}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Email </label>
                                        <input type="text" className="form-control" id="product-title" onChange={this.email}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Phone </label>
                                        <input type="text" className="form-control" id="product-title" onChange={this.phone}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Address </label>
                                        <input type="text" className="form-control" id="product-title" onChange={this.address}/>
                                    </div>
                                    <div className="form-group">
                                        <label> City </label>
                                        <input type="text" className="form-control" id="product-title" onChange={this.city}/>
                                    </div>
                                    <div className="form-group">
                                        <label> State </label>
                                        <input type="text" className="form-control" id="product-title" onChange={this.states}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Country </label>
                                        <input type="text" className="form-control" id="product-title" onChange={this.country}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Zipcode </label>
                                        <input type="text" className="form-control" id="product-title" onChange={this.zipcode}/>
                                    </div>
                                    
                                    
                                    <div className="form-group">
                                        <label> Subtotal </label>
                                        <input type="text" className="form-control" id="product-title" onChange={this.subtotal}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Discount </label>
                                        <input type="text" className="form-control" id="product-title" onChange={this.discount}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Shipping </label>
                                        <input type="text" className="form-control" id="product-title" onChange={this.ship}/>
                                    </div>
                                    <div className="form-group">
                                        <label> Total </label>
                                        <input type="text" className="form-control" id="product-title" onChange={this.total}/>
                                    </div>
                                </div>
                                <div className="box-footer">
                                    <div className="form-group">
                                        <button type="submit" className="btn btn-primary">Create Order</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
    	)
    }
}

export {
    CreateOrder
}