import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class EditCoupons extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            code : '',
            type : '',
            amount : '',
            status : "1",
            subscription: {
                singleCoupon: Meteor.subscribe('singleCoupon',FlowRouter.current().params.id)
            }
        }
        this.status = this.status.bind(this);
        this.code = this.code.bind(this);
        this.type = this.type.bind(this);
        this.amount = this.amount.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    singleCoupon(){
        return Coupons.findOne(FlowRouter.current().params.id);
    }
    componentWillMount(){
        this.setState({
            code : this.singleCoupon().code,
            type : this.singleCoupon().type,
            amount : this.singleCoupon().amount,
            status : this.singleCoupon() && this.singleCoupon().status,
        })
    }
    code(event){
        this.setState({code:event.target.value})
    }
    type(event){
        this.setState({type:event.target.value})
    }
    amount(event){
        this.setState({amount:event.target.value})
    }
    status(event){
        this.setState({status:event.target.value});
    }
    handleSubmit(event){
        event.preventDefault();
        data = {
            id : FlowRouter.current().params.id,
            set : {
                code : (this.state.code).toUpperCase(),
                type : this.state.type,
                amount : this.state.amount,
                status : this.state.status,
            },
        }
        console.log(data);
        Meteor.call('editCoupon',data,function (error) {
            if(!error){
                Bert.alert('Coupon edited successfully', 'success', 'growl-top-right');
                FlowRouter.go('/admin/view-coupons')
            }else{
                Bert.alert('Coupon edit is already exists', 'danger', 'growl-top-right');
            }
        })
    }
    render() {
        DocHead.setTitle('Edit Coupon');
        return (
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <div className="box">
                            <form onSubmit={this.handleSubmit}>
                                <div className="box-body">
                                    <div className="form-group">
                                        <label> Coupon Status </label>
                                        <select className="form-control" id="product-status" value={this.state.status} onChange={this.status}>
                                            <option value="1" >Active</option>
                                            <option value="0" >In Active</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label>Coupon Code</label>
                                        <input type="text" className="form-control" value={this.state.code} placeholder="Coupon Code" onChange={this.code}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Select type of Coupon</label>
                                        <select className="form-control" id="exampleSelect1" onChange={this.type} value={this.state.type}>
                                            <option value="0">Flat Discount</option>
                                            <option value="1">X % of cart total</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label>Discount Amount</label>
                                        <input type="text" className="form-control" placeholder="Discount Amount" value={this.state.amount} onChange={this.amount}/>
                                    </div>
                                </div>
                                <div className="box-footer">
                                    <div className="form-group">
                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary">Generate Coupon</button>) : ''}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>

        )
    }
}

export {
    EditCoupons
}