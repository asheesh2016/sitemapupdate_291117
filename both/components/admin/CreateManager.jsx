import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class CreateManager extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            username : '',
            name : '',
            password : '',
            phone : '',
            email : '',
        }
        this.email = this.email.bind(this);
        this.username = this.username.bind(this);
        this.name = this.name.bind(this);
        this.password = this.password.bind(this);
        this.phone = this.phone.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    username(event){
        this.setState({username:event.target.value})
    }
    name(event){
        this.setState({name:event.target.value})
    }
    password(event){
        this.setState({password:event.target.value})
    }
    phone(event){
        this.setState({phone:event.target.value})
    }
    email(event){
        this.setState({email:event.target.value})
    }
    handleSubmit(event){
        event.preventDefault();
        data = {
            username : this.state.username,
            name : this.state.name,
            password : this.state.password,
            phone : this.state.phone,
            email : this.state.email
        }
        console.log(data);
        Meteor.call('createManagerAccount',data,function (error) {
            if(!error){
                Bert.alert('Account created successfully', 'success', 'growl-top-right');
                FlowRouter.go('/admin/managers')
            }else{
                Bert.alert('Account is already exists', 'danger', 'growl-top-right');
            }
        })
    }
    render() {
        DocHead.setTitle('Create Manager');
        return (
            <section className="content">
                <div className="row">
                    <div className="col-md-12">
                        <div className="box">
                            <form onSubmit={this.handleSubmit}>
                                <div className="box-body">
                                    <div className="form-group">
                                        <label>Username</label>
                                        <input type="text" className="form-control" placeholder="Username" onChange={this.username}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Email</label>
                                        <input type="email" className="form-control" placeholder="Email" onChange={this.email}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Name</label>
                                        <input type="text" className="form-control" placeholder="Name" onChange={this.name}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Password</label>
                                        <input type="password" className="form-control" placeholder="Password" onChange={this.password}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Phone</label>
                                        <input type="text" className="form-control" placeholder="Phone" onChange={this.phone}/>
                                    </div>
                                </div>
                                <div className="box-footer">
                                    <div className="form-group">
                                        {Roles.userIsInRole(Meteor.userId(), ['admin']) ? (<button type="submit" className="btn btn-primary">Create Manager</button>) : ''}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>

        )
    }
}

export {
    CreateManager
}