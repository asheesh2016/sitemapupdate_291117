import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import crypto from 'crypto';
import ReactMixin from "react-mixin";
export default class Checkout extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
            address: '',
            city: '',
            state : '',
            country : '',
            zipcode : '',
            name : '',
            phone : '',
            change : '1',
            selectedAddress : '',
            subscription : {
                OrdersData : Meteor.subscribe('OrdersData',FlowRouter.getParam('id')),
                ShippingDetails : Meteor.subscribe('ShippingDetails')
            }
        }
        this.address = this.address.bind(this);
        this.city = this.city.bind(this);
        this.stateName = this.stateName.bind(this);
        this.country = this.country.bind(this);
        this.zipcode = this.zipcode.bind(this);
        this.name = this.name.bind(this);
        this.phone = this.phone.bind(this);
        this.email = this.email.bind(this);
        this.goToPayment = this.goToPayment.bind(this);
        this.addShippingAddress = this.addShippingAddress.bind(this);
        this.changeDetail = this.changeDetail.bind(this);
        this.login = this.login.bind(this);
        this.signUp = this.signUp.bind(this);
        this.changeCheck = this.changeCheck.bind(this);
    }
    componentWillUnmount(){
        this.state.subscription.CartItems.stop();
        this.state.subscription.OrdersData.stop();
        this.state.subscription.ShippingDetails.stop();
    }
    componentWillMount(){
        var self = this;  
        Tracker.autorun(() => {
            if(!Meteor.userId()){
                if(Meteor.isClient){
                    self.setState({
                        subscription: {
                            CartItems: Meteor.subscribe('CartItems',Session.get('guestId')),
                            OrdersData : Meteor.subscribe('OrdersData',FlowRouter.getParam('id')),
                            ShippingDetails : Meteor.subscribe('ShippingDetails')
                        }
                    })
                }
            }else{
                if(Meteor.isClient){
                    self.setState({
                        subscription: {
                            CartItems: Meteor.subscribe('CartItems',Session.get('guestId')),
                            OrdersData : Meteor.subscribe('OrdersData',FlowRouter.getParam('id')),
                            ShippingDetails : Meteor.subscribe('ShippingDetails')
                        }
                    })
                }
            }
        })
    }
    address(event){
        this.setState({address:event.target.value});
    }
    city(event){
        this.setState({city:event.target.value});
    }
    stateName(event){
        this.setState({state:event.target.value});
    }
    country(event){
        this.setState({country:event.target.value});
    }
    zipcode(event){
        this.setState({zipcode:event.target.value});
    }
    carts(){
        return Carts.findOne()
    }
    OrdersData(){
        return Orders.findOne(FlowRouter.getParam('id'))
    }
    goToPayment(event){
        //event.preventDefault();
        function isPhone(phone){
            var regex = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
            return regex.test(phone);
        }
        function isFullName(name){
            var regex = /^[A-Za-z\s]{3,30}$/;
            return regex.test(name);
        }
        function isEmpty(str){
            return !str.replace(/^\s+/g, '').length; // boolean (`true` if field is empty)
        }
        if(_.isEmpty(this.state.name) || isEmpty(this.state.name)){
            event.preventDefault();
            Bert.alert('Please enter the valid name', 'danger', 'growl-top-right');
            return false;
        }else if(!isFullName(this.state.name)){
            event.preventDefault();
            Bert.alert('Please enter the valid name', 'danger', 'growl-top-right');
            return false;
        }

        if(!Meteor.userId()){
            if(_.isEmpty(this.state.email) || isEmpty(this.state.email)){
                event.preventDefault();
                Bert.alert('Please enter the valid email', 'danger', 'growl-top-right');
                return false;
            }else{
                
                //console.log(this.state.email)
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
                if (!this.state.email.match(mailformat)){
                    event.preventDefault();
                    Bert.alert('Please enter the valid email', 'danger', 'growl-top-right');
                    return false;
                }
            }
        }else{
            if(_.isEmpty(this.state.email) || isEmpty(this.state.email)){
                event.preventDefault();
                Bert.alert('Please enter the valid email', 'danger', 'growl-top-right');
                return false;
            }else{
                
                //console.log(this.state.email)
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
                if (!this.state.email.match(mailformat)){
                    event.preventDefault();
                    Bert.alert('Please enter the valid email', 'danger', 'growl-top-right');
                    return false;
                }
            } 
        }

        if(_.isEmpty(this.state.phone) || isEmpty(this.state.phone)){
            event.preventDefault();
            Bert.alert('Please enter the valid phone', 'danger', 'growl-top-right');
            return false;
        }else if(!isPhone(this.state.phone)){
            event.preventDefault();
            Bert.alert('Please enter the valid phone', 'danger', 'growl-top-right');
            return false;
        }

        if(_.isEmpty(this.state.selectedAddress)){
            event.preventDefault();
            Bert.alert('Please enter the address', 'danger', 'growl-top-right');
            return false;
        }else{
            if(_.isEmpty(this.state.selectedAddress.address) || isEmpty(this.state.selectedAddress.address)){
                event.preventDefault();
                Bert.alert('Please enter the address', 'danger', 'growl-top-right');
                return false;
            }
            if(_.isEmpty(this.state.selectedAddress.city) || isEmpty(this.state.selectedAddress.city)){
                event.preventDefault();
                Bert.alert('Please enter the city', 'danger', 'growl-top-right');
                return false;
            }
            if(_.isEmpty(this.state.selectedAddress.state) || isEmpty(this.state.selectedAddress.state)){
                event.preventDefault();
                Bert.alert('Please enter the state', 'danger', 'growl-top-right');
                return false;
            }
            if(_.isEmpty(this.state.selectedAddress.country) || isEmpty(this.state.selectedAddress.country)){
                event.preventDefault();
                Bert.alert('Please enter the country', 'danger', 'growl-top-right');
                return false;
            }
            if(_.isEmpty(this.state.selectedAddress.zipcode) || isEmpty(this.state.selectedAddress.zipcode)){
                event.preventDefault();
                Bert.alert('Please enter the zipcode', 'danger', 'growl-top-right');
                return false;
            }else{
                if(isNaN(this.state.zipcode) && (this.state.zipcode.length > 6 || this.state.zipcode.length <6)){
                    event.preventDefault();
                    Bert.alert('Please enter valid zipcode', 'danger', 'growl-top-right');
                    return false;
                }
            }
        }
        return false;
       
    }
    shipping(){
        return Shipping.find()
    }
    name(event){
        this.setState({name:event.target.value})
    }
    phone(event){
        this.setState({phone:event.target.value})
    }
    email(event){
        this.setState({email:event.target.value})
    }
    componentDidMount(){
        console.log("haha");
        // $(document).ready(function(){
        //     $('#xname').val(Meteor.user() && Meteor.user().profile.name);
        //     $('#xphone').val(Meteor.user() && Meteor.user().profile.phone);
        //     this.setState({
        //         name: Meteor.user() && Meteor.user().profile.name,
        //         phone: Meteor.user() && Meteor.user().profile.phone,
        //         email :  Meteor.user() && Meteor.user().emails[0].address
        //     });
        // });
        var self = this;
        Tracker.autorun(function () {
            if(Meteor.userId()){
                console.log(Meteor.user() && Meteor.user().profile.name)
                self.setState({
                    name: Meteor.user() && Meteor.user().profile.name,
                    phone: Meteor.user() && Meteor.user().profile.phone,
                    email :  Meteor.user() && Meteor.user().emails[0].address
                });
            }
        })
    }
    changeDetail(event){
        event.preventDefault();
        function isPhone(phone){
            var regex = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
            return regex.test(phone);
        }
        function isFullName(name){
            var regex = /^[A-Za-z\s]{3,30}$/;
            return regex.test(name);
        }
        function isEmpty(str){
            return !str.replace(/^\s+/g, '').length; // boolean (`true` if field is empty)
        }
        if(_.isEmpty(this.state.name) || isEmpty(this.state.name)){
            Bert.alert('Please enter the valid name', 'danger', 'growl-top-right');
            return false;
        }else if(!isFullName(this.state.name)){
            Bert.alert('Please enter the valid name', 'danger', 'growl-top-right');
            return false;
        }
        if(!Meteor.userId()){
            if(_.isEmpty(this.state.email)  || isEmpty(this.state.email)){
                Bert.alert('Please enter the email', 'danger', 'growl-top-right');
                return false;
            }else{
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
                if (!this.state.email.match(mailformat)){
                    Bert.alert('Please enter valid email', 'danger', 'growl-top-right');
                    return false;
                }
            }
        }else{
            if(_.isEmpty(this.state.email) || isEmpty(this.state.email)){
                Bert.alert('Please enter the email', 'danger', 'growl-top-right');
                return false;
            }else{
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
                if (!this.state.email.match(mailformat)){
                    Bert.alert('Please enter valid email', 'danger', 'growl-top-right');
                    return false;
                }
            }
        }
        if(_.isEmpty(this.state.phone) || isEmpty(this.state.phone)){
            Bert.alert('Please enter the phone', 'danger', 'growl-top-right');
            return false;
        }else if(!isPhone(this.state.phone)){
            Bert.alert('Please enter the valid phone', 'danger', 'growl-top-right');
            return false;
        }
        data = {
            name : this.state.name,
            phone : this.state.phone
        }
        if(Meteor.userId()){
            Meteor.call('updateUserBillProfile',data,function (error) {
                if(error){
                    Bert.alert('Error Updating', 'danger', 'growl-top-right');
                }else{
                    Bert.alert('Profile Updated', 'success', 'growl-top-right');
                }
            })
        }
    }
    selectAddress(id){
        if(Meteor.userId()){
            address = Shipping.findOne(id);
            this.setState({
                selectedAddress : address
            })
        }else{
            address = {
                address : this.state.address,
                city : this.state.city,
                state : this.state.state,
                country : this.state.country,
                zipcode : this.state.zipcode,
                createdAt : new Date(),
                userId : Session.get('guestId')
            }
            this.setState({
                selectedAddress : data
            })
        }  
        data = {
            id : FlowRouter.getParam('id'),
            name : this.state.name,
            phone : this.state.phone,
            email : this.state.email,
            address : address,
        }
        console.log(data);
        id = Meteor.call('proceedPayment',data,function(err,data){
            if(!err){
                console.log("done")
            }
        });    

    }
    addShippingAddress(event){
        event.preventDefault();
        function isFullName(name){
            var regex = /^[A-Za-z\s]{3,90}$/;
            return regex.test(name);
        }
        function isEmpty(str){
            return !str.replace(/^\s+/g, '').length; // boolean (`true` if field is empty)
        }
        if(_.isEmpty(this.state.address) || isEmpty(this.state.address)){
            Bert.alert('Please enter the valid address', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.city) || !isFullName(this.state.city) || isEmpty(this.state.city)){
            Bert.alert('Please enter the valid city', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.state) || !isFullName(this.state.state) || isEmpty(this.state.state)){
            Bert.alert('Please enter the valid state', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.country) || !isFullName(this.state.country) || isEmpty(this.state.country)){
            Bert.alert('Please enter the valid country', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty(this.state.zipcode) || isEmpty(this.state.zipcode)){
            Bert.alert('Please enter the valid zipcode', 'danger', 'growl-top-right');
            return false;
        }else{
            if(isNaN(this.state.zipcode) ||  (this.state.zipcode.length > 6 || this.state.zipcode.length <6)){
                Bert.alert('Please enter the valid zipcode', 'danger', 'growl-top-right');
                return false;
            }
        }
        if(Meteor.userId()){
            data = {
                address : this.state.address,
                city : this.state.city,
                state : this.state.state,
                country : this.state.country,
                zipcode : this.state.zipcode,
                createdAt : new Date(),
                userId : Meteor.userId()
            }
            Meteor.call('createShippingAddress',data,function (error) {
                if(!error){
                    $("input[name=address]").val('');
                    $("input[name=city]").val('');
                    $("input[name=state]").val('');
                    $("input[name=country]").val('');
                    $("input[name=zipcode]").val('');
                    
                    Bert.alert('Address Added Successfully', 'success', 'growl-top-right');
                }else{
                    Bert.alert('Something wrong try again', 'danger', 'growl-top-right');
                }
            });
            this.setState({address: '',city: '',state : '',country : '',zipcode : '',});
        }else{
            address = {
                address : this.state.address,
                city : this.state.city,
                state : this.state.state,
                country : this.state.country,
                zipcode : this.state.zipcode,
                createdAt : new Date(),
                userId : Session.get('guestId')
            }
            this.setState({
                selectedAddress : address
            })
            data1 = {
                id : FlowRouter.getParam('id'),
                name : this.state.name,
                phone : this.state.phone,
                email : this.state.email,
                address : address,
            }
            //console.log(data1);
            id = Meteor.call('proceedPayment',data1,function(err,data){
                if(!err){
                    console.log("done")
                }
            });  
        }
    }
    deleteAddress(id){
        Meteor.call('deleteShippingAddress',id,function (error) {
            if(!error){
                Bert.alert('Address Deleted Successfully', 'success', 'growl-top-right');
            }else{
                Bert.alert('Something wrong try again', 'danger', 'growl-top-right');
            }
        })
    }
    login(event){
        event.preventDefault();
        let email = $(event.target).find("[name=email]").val();
        let password = $(event.target).find("[name=password]").val();
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
        }
        if(!email){
            Bert.alert('Email cannot be empty', 'danger', 'growl-top-right');
        }else if(!isEmail(email)){
            Bert.alert('Email is not valid', 'danger', 'growl-top-right');
        }else if(!password){
            Bert.alert('Password cannot be empty', 'danger', 'growl-top-right');
        }else if(password.length > 25){
            Bert.alert('Password length cannot be more than 25', 'danger', 'growl-top-right');
        }else{
            Meteor.loginWithPassword(email, password, function (error) {
                if (!error) {
                    Bert.alert('Login Successful', 'success', 'growl-top-right');
                    FlowRouter.go('/checkout/'+FlowRouter.getParam('id'));
                }else{
                    Bert.alert('Wrong Email or Password', 'danger', 'growl-top-right');
                }
            })
            $(event.target).find("[name=email]").val('');
            $(event.target).find("[name=password]").val('');
        }
    }
    signUp(event){
        event.preventDefault();
        let email = $(event.target).find("[name=email]").val();
        let password = $(event.target).find("[name=password]").val();
        let re_password = $(event.target).find("[name=re-password]").val();
        console.log(email,password,re_password);
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
        }
        function isPassword(password){
            var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/;
            return regex.test(password);
        }
        if(re_password != password){
            Bert.alert('Password not Matching', 'danger', 'growl-top-right');
            return false;
        }else if(!isPassword(password)){
            Bert.alert('Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character', 'danger', 'growl-top-right');
            return false;
        }else if(!email){
            Bert.alert('Email cannot be empty', 'danger', 'growl-top-right');
            return false;
        }else if(!isEmail(email)){
            Bert.alert('Email is not valid', 'danger', 'growl-top-right');
            return false;
        }else{
            data = {
                email: email,
                password: password,
            }
            Meteor.call('createAccount', data, function (error) {
                if (!error) {
                    Bert.alert('Account Created', 'success', 'growl-top-right');
                    Meteor.loginWithPassword(email, password, function (error) {
                        if (!error) {
                            Bert.alert('Login Successful', 'success', 'growl-top-right');
                            FlowRouter.go('/checkout/'+FlowRouter.getParam('id'));
                        }
                    })
                }else{
                    Bert.alert('Email already exist', 'danger', 'growl-top-right');
                }
            });
            $(event.target).find("[name=email]").val('');
            $(event.target).find("[name=password]").val('');
            $(event.target).find("[name=re-password]").val('');
        }
    }
    changeCheck(event){
        this.setState({change:event.target.value});
    }
    loginFacebook(event){
        event.preventDefault();
        if(Meteor.isClient){
            Meteor.loginWithFacebook({requestPermissions: ['email'],loginHint:"test"}, function(error){
                if(!error){
                    Bert.alert('Login Successful', 'success', 'growl-top-right');
                    FlowRouter.go('/checkout/'+FlowRouter.getParam('id'));
                }
            });
        }
    }
    loginGoogle(event){
        event.preventDefault();
        if(Meteor.isClient){
            Meteor.loginWithGoogle({requestPermissions: ['email'],loginHint:"test"}, function(error){
                if(!error){
                    Bert.alert('Login Successful', 'success', 'growl-top-right');
                    FlowRouter.go('/checkout/'+FlowRouter.getParam('id'));
                }
            });
        }
    }
    render(){
        let cartDisplay,priceDisplay,selectedAddress;
        productImage = (id) =>{
            return Products.findOne(id) && Products.findOne(id).featuredImage;
        }
        productTitle = (id) =>{
            return Products.findOne(id) && Products.findOne(id).title;
        }
        productWeight = (id) =>{
            return Products.findOne(id) && Products.findOne(id).weight;
        }
        productPrice = (id) =>{
            return Products.findOne(id) && Products.findOne(id).price;
        }
        if(!_.isUndefined(this.carts())){
            if(!_.isUndefined(this.carts().items) && !_.isEmpty(this.carts().items)){
                cartDisplay = (
                    this.carts().items.map((val,index) => {
                        return (
                            <div className="cartpage_list" key={'index'+val.id+index}>
                                <div className="row cartpage_row">
                                    <div className="col-md-4 col-sm-6 col-xs-12 cartpage_lft">
                                        <div className="cartpage_img"> <a href="javascript:void(0)"><img src={productImage(val.id)} alt={productTitle(val.id)}/></a>
                                            <h5 className="text-center"><a href="javascript:void(0)">{productTitle(val.id)}</a><em>({productWeight(val.id)})</em></h5>
                                        </div>
                                    </div>
                                    <div className="col-md-4 col-sm-6 col-xs-12 cartpage_lft">
                                        <div className="carousel-search hidden-phone qty_info">
                                            <div className="btn-group">
                                                <a className="btn dropdown-toggle btn-select" data-toggle="dropdown" href="#">QTY {val.count}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 col-sm-6 col-xs-12 cartpage_rght">
                                        <div className="cartpage_price">
                                            <h4>INR  {val.count*productPrice(val.id)}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                )
            }else{
                cartDisplay = (
                    <h5 className="text-left">No Item in cart</h5>
                )
            }
        }else {
           cartDisplay = ( 
                <h5 className="text-left">No Item in cart</h5>
           )
        }
        if(this.state.selectedAddress){
            selectedAddress = (
                <div className="col-md-12 shipping_list">
                    <p>Shipping Address</p>
                    <p>{this.state.selectedAddress.address +' ,'+this.state.selectedAddress.city+' ,'+this.state.selectedAddress.state+' ,'+this.state.selectedAddress.country+' ,'+this.state.selectedAddress.zipcode}</p>
                </div>
            )
        }else{
            selectedAddress = ""
        }
        var f;
        if(Meteor.userId()){
            var  secret;
            secret = 'q0BEE9'+'|'+FlowRouter.getParam('id')+'|'+(this.OrdersData() && this.OrdersData().total)+'|'+'Wingreen product buy '+Meteor.userId()+'|'+(Meteor.user() && Meteor.user().profile.name)+'|'+(Meteor.user() && Meteor.user().emails[0].address)+'|||||||||||hBw9gyix';
            var hash = crypto.createHash('sha512').update(secret).digest('hex');
            f = (
                <div>
                <input type="hidden" name="firstname" value={Meteor.user() && Meteor.user().profile.name} />
                <input type="hidden" name="lastname" value="" />
                <input type="hidden" name="surl" value="http://www.wingreensfarms.com/payment/success" />
                <input type="hidden" name="phone" value={Meteor.user() && Meteor.user().profile.phone} />
                <input type="hidden" name="key" value="q0BEE9" />
                <input type="hidden" name="hash" value ={hash} />
                <input type="hidden" name="curl" value="http://www.wingreensfarms.com/payment/cancel" />
                <input type="hidden" name="furl" value="http://www.wingreensfarms.com/payment/error" />
                <input type="hidden" name="txnid" value={FlowRouter.getParam('id')} />
                <input type="hidden" name="drop_category" value="COD" />
                <input type="hidden" name="productinfo" value={'Wingreen product buy '+Meteor.userId()} />
                <input type="hidden" name="amount" value={this.OrdersData() && this.OrdersData().total} />
                <input type="hidden" name="email" value={Meteor.user() && Meteor.user().emails[0].address} />
                <input type="hidden" name="address1" value={this.state.selectedAddress.address} />
                <input type="hidden" name="city" value={this.state.selectedAddress.city} />
                <input type="hidden" name="state" value={this.state.selectedAddress.state} />
                <input type="hidden" name="country" value={this.state.selectedAddress.country} />
                <input type="hidden" name="zipcode" value={this.state.selectedAddress.zipcode} />
                </div>
            ) 
            // var  secret;
            // secret = 'gtKFFx'+'|'+FlowRouter.getParam('id')+'|'+(this.OrdersData() && this.OrdersData().total)+'|'+'Wingreen product buy '+Meteor.userId()+'|'+(Meteor.user() && Meteor.user().profile.name)+'|'+(Meteor.user() && Meteor.user().emails[0].address)+'|||||||||||eCwWELxi';
            // var hash = crypto.createHash('sha512').update(secret).digest('hex');
            // f = (
            //     <div>
            //     <input type="hidden" name="firstname" value={Meteor.user() && Meteor.user().profile.name} />
            //     <input type="hidden" name="lastname" value="" />
            //     <input type="hidden" name="surl" value="http://localhost:3000/payment/success" />
            //     <input type="hidden" name="phone" value={Meteor.user() && Meteor.user().profile.phone} />
            //     <input type="hidden" name="key" value="gtKFFx" />
            //     <input type="hidden" name="hash" value ={hash} />
            //     <input type="hidden" name="curl" value="http://localhost:3000/payment/cancel" />
            //     <input type="hidden" name="furl" value="http://localhost:3000/payment/error" />
            //     <input type="hidden" name="txnid" value={FlowRouter.getParam('id')} />
            //     <input type="hidden" name="drop_category" value="COD" />
            //     <input type="hidden" name="productinfo" value={'Wingreen product buy '+Meteor.userId()} />
            //     <input type="hidden" name="amount" value={this.OrdersData() && this.OrdersData().total} />
            //     <input type="hidden" name="email" value={Meteor.user() && Meteor.user().emails[0].address} />
            //     <input type="hidden" name="address1" value={this.state.selectedAddress.address} />
            //     <input type="hidden" name="city" value={this.state.selectedAddress.city} />
            //     <input type="hidden" name="state" value={this.state.selectedAddress.state} />
            //     <input type="hidden" name="country" value={this.state.selectedAddress.country} />
            //     <input type="hidden" name="zipcode" value={this.state.selectedAddress.zipcode} />
            //     </div>
            // )                    
        }else{
            var  secret;
            secret = 'q0BEE9'+'|'+FlowRouter.getParam('id')+'|'+(this.OrdersData() && this.OrdersData().total)+'|'+'Wingreen product buy guest'+'|'+this.state.name+'|'+this.state.email+'|||||||||||hBw9gyix';
            var hash = crypto.createHash('sha512').update(secret).digest('hex');
            f = (
                <div>
                <input type="hidden" name="firstname" value={this.state.name} />
                <input type="hidden" name="lastname" value="" />
                <input type="hidden" name="surl" value="http://www.wingreensfarms.com/payment/success" />
                <input type="hidden" name="phone" value={this.state.phone} />
                <input type="hidden" name="key" value="q0BEE9" />
                <input type="hidden" name="hash" value ={hash} />
                <input type="hidden" name="curl" value="http://www.wingreensfarms.com/payment/cancel" />
                <input type="hidden" name="furl" value="http://www.wingreensfarms.com/payment/error" />
                <input type="hidden" name="txnid" value={FlowRouter.getParam('id')} />
                <input type="hidden" name="drop_category" value="COD" />
                <input type="hidden" name="productinfo" value={'Wingreen product buy guest'} />
                <input type="hidden" name="amount" value={this.OrdersData() && this.OrdersData().total} />
                <input type="hidden" name="email" value={this.state.email} />
                <input type="hidden" name="address1" value={this.state.selectedAddress.address} />
                <input type="hidden" name="city" value={this.state.selectedAddress.city} />
                <input type="hidden" name="state" value={this.state.selectedAddress.state} />
                <input type="hidden" name="country" value={this.state.selectedAddress.country} />
                <input type="hidden" name="zipcode" value={this.state.selectedAddress.zipcode} />
                </div>
            )
            // var  secret;
            // secret = 'gtKFFx'+'|'+FlowRouter.getParam('id')+'|'+(this.OrdersData() && this.OrdersData().total)+'|'+'Wingreen product buy guest'+'|'+this.state.name+'|'+this.state.email+'|||||||||||eCwWELxi';
            // var hash = crypto.createHash('sha512').update(secret).digest('hex');
            // f = (
            //     <div>
            //     <input type="hidden" name="firstname" value={this.state.name} />
            //     <input type="hidden" name="lastname" value="" />
            //     <input type="hidden" name="surl" value="http://localhost:3000/payment/success" />
            //     <input type="hidden" name="phone" value={this.state.phone} />
            //     <input type="hidden" name="key" value="gtKFFx" />
            //     <input type="hidden" name="hash" value ={hash} />
            //     <input type="hidden" name="curl" value="http://localhost:3000/payment/cancel" />
            //     <input type="hidden" name="furl" value="http://localhost:3000/payment/error" />
            //     <input type="hidden" name="txnid" value={FlowRouter.getParam('id')} />
            //     <input type="hidden" name="drop_category" value="COD" />
            //     <input type="hidden" name="productinfo" value={'Wingreen product buy guest'} />
            //     <input type="hidden" name="amount" value={this.OrdersData() && this.OrdersData().total} />
            //     <input type="hidden" name="email" value={this.state.email} />
            //     <input type="hidden" name="address1" value={this.state.selectedAddress.address} />
            //     <input type="hidden" name="city" value={this.state.selectedAddress.city} />
            //     <input type="hidden" name="state" value={this.state.selectedAddress.state} />
            //     <input type="hidden" name="country" value={this.state.selectedAddress.country} />
            //     <input type="hidden" name="zipcode" value={this.state.selectedAddress.zipcode} />
            //     </div>
            // )
        }
        var u;
        if(Meteor.userId()){
            u = (
                <div>
                    <div className="form-group" style={{marginBottom:'0px'}}>
                        <input type="text" className="form-control" style={{marginBottom:'0px'}} name="name" id="xname" placeholder="Enter your Name" value={this.state.name} onChange={this.name}/>
                    </div>
                    <div className="form-group" style={{marginBottom:'0px'}}>
                        <input type="text" className="form-control" style={{marginBottom:'0px'}} name="email" placeholder="Enter your Email" value={this.state.email} onChange={this.email}/>
                    </div>
                    <div className="form-group" style={{marginBottom:'0px'}}>
                        <input type="text" className="form-control" style={{marginBottom:'0px'}} name="phone" id="xphone" placeholder="Enter your Phone" value={this.state.phone} onChange={this.phone}/>
                    </div>
                    <button type="submit" className="btn btn-default submit_btn">Apply</button>
                </div>
            )                  
        }else{
            u = (
                <div>
                    <div className="form-group" style={{marginBottom:'0px'}}>
                        <input type="text" className="form-control" style={{marginBottom:'0px'}} name="name" placeholder="Enter your Name" value={this.state.name} onChange={this.name}/>
                    </div>
                    <div className="form-group" style={{marginBottom:'0px'}}>
                        <input type="text" className="form-control" style={{marginBottom:'0px'}} name="email" placeholder="Enter your Email" value={this.state.email} onChange={this.email}/>
                    </div>
                    <div className="form-group" style={{marginBottom:'0px'}}>
                        <input type="text" className="form-control" style={{marginBottom:'0px'}} name="phone" placeholder="Enter your Phone" value={this.state.phone} onChange={this.phone}/>
                    </div>
                </div>
            )
        }
        var log,sp;
        if(this.state.change == "0"){
            sp = (
                <div>
                <h5>Sign Up</h5>
                <div className="account_form" style={{paddingTop:'0px'}}>
                    <form onSubmit={this.signUp}>
                        <div className="form-group" style={{marginBottom:'0px'}}>
                            <input type="email" className="form-control" style={{marginBottom:'0px'}} name="email" placeholder="Email Address" />
                        </div>
                        <div className="form-group" style={{marginBottom:'0px'}}>
                            <input type="password" className="form-control" style={{marginBottom:'0px'}} name="password" placeholder="Password"/>
                        </div>
                        <div className="form-group" style={{marginBottom:'0px'}}>
                            <input type="password" className="form-control" style={{marginBottom:'0px'}} name="re-password" placeholder="Confirm Password"/>
                        </div>
                        <div className="form-groups">
                            <button type="submit" className="btn btn-default submit_btn">Sign Up</button>
                        </div>
                    </form>
                </div>
                </div>
            )
        }else{
            sp = null;
        }
        if(!Meteor.userId()){
            log = (
                <div className="col-xs-12 shipping_list">
                    <div className="row">
                        <div className="col-md-4 col-xs-12 ">
                            <form>
                                <div className="form-group" style={{marginBottom:'0px'}}>
                                    <input type="radio" name="check" value="1" checked={this.state.change === "1" } onChange={this.changeCheck}/> <span className="check">Guest Checkout</span>
                                </div>
                                <div className="form-group" style={{marginBottom:'0px'}}>
                                    <input type="radio" name="check" value="0" checked={this.state.change === "0" } onChange={this.changeCheck}/> <span className="check">Create Account</span>
                                </div>
                            </form>
                        </div>
                        <div className="col-md-4 col-xs-12">
                            {sp}
                        </div>
                        <div className="col-md-4 col-xs-12">
                            <h5>Login</h5>
                            <div className="account_form sl" style={{paddingTop:'0px'}}>
                                <form onSubmit={this.login}>
                                    <div className="form-group" style={{marginBottom:'0px'}}>
                                        <input type="email" className="form-control" style={{marginBottom:'0px'}} name="email" placeholder="Email Address" />
                                    </div>
                                    <div className="form-group" style={{marginBottom:'0px'}}>
                                        <input type="password" className="form-control" style={{marginBottom:'0px'}} name="password" placeholder="Enter your password"/>
                                    </div>
                                    <div className="form-groups">
                                        <button type="submit" className="btn btn-default submit_btn">Login</button>
                                    </div>
                                    <span className="ss"> OR </span>
                                    <span><a href="javascript:void(0)" onClick={this.loginFacebook.bind(this)} className="signcheck"><img src="/client/images/fbnav_icon.png"/> Login with Facebook</a></span>
                                    <span><a href="javascript:void(0)" onClick={this.loginGoogle.bind(this)} className="signcheck"><img src="/client/images/google_icon.png"/> Login with Google</a></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                )
        }else{
            log = null;
        }
        if(!_.isUndefined(this.carts())){
        if(!_.isUndefined(this.carts().items) && !_.isEmpty(this.carts().items)){
            priceDisplay = (
                <div>
                    <div className="row totalrow">
                        <div className="total_list">
                            <div className="col-md-12 col-sm-12">
                                <div className="row ordersum_heading">
                                    {log}
                                    <div className="col-xs-12 shipping_list">
                                        <h5>Billing Details</h5>
                                        <div className="account_form" style={{paddingTop:'0px'}}>
                                            <form onSubmit={this.changeDetail}>
                                                {u}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-8 col-sm-8">
                                <div className="row ordersum_heading">
                                    <div className="col-xs-12">
                                        <h5>Shipping Details</h5>
                                    </div>
                                </div>
                                <div className="row shipping_row">
                                    <div className="shipping_details">
                                        {this.shipping().map((val) => {
                                            return (
                                                <div key={val._id} className="col-md-4 col-sm-6 col-xs-12 shipping_list selectlist" style={{position:'relative',padding:'10px'}}>
                                                    <p>{val.address}</p>
                                                    <p>{val.city}</p>
                                                    <p>{val.state}</p>
                                                    <p>{val.country}</p>
                                                    <p>{val.zipcode}</p>
                                                    <div className="overlay-ship">
                                                        <div className="button_box">
                                                            <button onClick={this.deleteAddress.bind(this,val._id)} className="delete"><i className="fa fa-trash"></i></button>
                                                            <button onClick={this.selectAddress.bind(this,val._id)} className="select"><i className="fa fa fa-check"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })}
                                        <div className="col-md-3 col-sm-6 col-xs-12 shipping_list">
                                            <div className="account_form" style={{paddingTop:'0px'}}>
                                                <form onSubmit={this.addShippingAddress} id="bill_add">
                                                    <div className="form-group" style={{marginBottom:'0px'}}>
                                                        <input type="text" className="form-control" style={{marginBottom:'0px'}} name="address" placeholder="Enter your address" onChange={this.address}/>
                                                    </div>
                                                    <div className="form-group" style={{marginBottom:'0px'}}>
                                                        <input type="text" className="form-control" style={{marginBottom:'0px'}} name="city" placeholder="Enter your city" onChange={this.city}/>
                                                    </div>
                                                    <div className="form-group" style={{marginBottom:'0px'}}>
                                                        <input type="text" className="form-control" style={{marginBottom:'0px'}} name="state" placeholder="Enter your state" onChange={this.stateName}/>
                                                    </div>
                                                    <div className="form-group" style={{marginBottom:'0px'}}>
                                                        <input type="text" className="form-control" style={{marginBottom:'0px'}} name="country" placeholder="Enter your country" onChange={this.country}/>
                                                    </div>
                                                    <div className="form-group" style={{marginBottom:'0px'}}>
                                                        <input type="text" className="form-control" style={{marginBottom:'0px'}} name="zipcode" placeholder="Enter your zipcode" onChange={this.zipcode}/>
                                                    </div>
                                                    <button type="submit" className="btn btn-default submit_btn">Apply</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-offset-md-8 col-sm-4 col-offset-sm-8 col-xs-12 total_price">
                                <ul>
                                    <li>Sub Total INR {this.OrdersData() && this.OrdersData().subtotal}</li>
                                    <li>Discount INR {this.OrdersData() && this.OrdersData().discount}</li>
                                    <li>Shipping INR {this.OrdersData() && this.OrdersData().ship}</li>
                                    <li>Total INR {this.OrdersData() && this.OrdersData().total}</li>
                                </ul>
                            </div>
                            {selectedAddress}
                        </div>
                    </div>
                    <div className="row checkoutbtn_info">
                        <div className="col-xs-12">
                            <form action='https://secure.payu.in/_payment' id="#target" method='post' onSubmit={this.goToPayment}>
                                {f}
                                <button className="checkout_btn" style={{border:'0px',padding:'16px 75px'}}>Submit</button>
                            </form> 
                        </div>
                    </div>
                </div>
            )
        }else{
            priceDisplay = ""
        }
        }else{
            priceDisplay = ""
        }
         DocHead.removeDocHeadAddedTags();
        let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            metaTSite,
            metaGeoPlacename,
            metaGeoPosition,
            metaGeoRegion,
            metaIcbm;

        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = "Checkout - Wingreens Farms";
        metaInfoDescription = {name: "description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: "Wingreens Farms - Traditionally handmade dips, spreads, bakery and teas"};
        metaOgDescription = {name: "og:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/checkout"};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: "Checkout - Wingreens Farms"};
        metaTTitle = {name: "twitter:title", content: "Checkout - Wingreens Farms"};
        metaTDescription = {name: "twitter:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        metaGeoPlacename={name: "geo.placename", content: "B19, 1, Info Technology Park, Sector 34, Gurgaon, Haryana - 122001, India"};
        metaGeoPosition={name: "geo.position", content: "28.430799;77.013499"};
        metaGeoRegion={name: "geo.region", content: "IN-Haryana"};
        metaIcbm={name: "ICBM", content: "28.430799, 77.013499"};
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);
        DocHead.addMeta(metaGeoPlacename);
        DocHead.addMeta(metaGeoPosition);
        DocHead.addMeta(metaGeoRegion);
        DocHead.addMeta(metaIcbm);



        let gaScript = 'https://www.google-analytics.com/analytics.js';
        let self =  this;
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');

            ga('require', 'ecommerce');

            ga('ecommerce:addTransaction', {
              'id': FlowRouter.getParam('id'),                     // Transaction ID. Required.
              'affiliation': 'Wingreens',   // Affiliation or store name.
              'revenue': self.OrdersData() && self.OrdersData().total,               // Grand Total.
              'shipping': self.OrdersData() && self.OrdersData().ship,                  // Shipping.
              'tax': 0                     // Tax.
            });
            if(!_.isUndefined(self.carts())){
                if(!_.isUndefined(self.carts().items) && !_.isEmpty(self.carts().items)){
                    _.each(self.carts().items,function(val){
                        ga('ecommerce:addItem', {
                          'id': FlowRouter.getParam('id'),                     // Transaction ID. Required.
                          'name': productTitle(val.id),    // Product name. Required.
                          'price': val.count*productPrice(val.id),                 // Unit price.
                          'quantity': val.count                  // Quantity.
                        });
                    })
                }
            }
            ga('ecommerce:send');                
        });
        return (
            <div>
                <div className="row history_info">
                    <div className="breadcrumb-info">
                        <ol className="breadcrumb">
                            <li><a href="/">Home</a></li>
                            <li><a href="/cart">Cart</a></li>
                            <li className="active">CheckOut</li>
                        </ol>
                    </div>
                    <div className="col-xs-12">
                        <h3 className="text-center">CheckOut</h3>
                    </div>
                </div>
                <div className="row ordersum_heading">
                    <div className="col-xs-12">
                        <h5>Order Summary</h5>
                    </div>
                </div>
                {cartDisplay}
                {priceDisplay}
            </div>
        )
    }
}


export {
    Checkout
}
