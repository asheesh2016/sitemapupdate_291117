import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactMixin from "react-mixin";


export default class NotFound extends TrackerReact(Component){
    render(){
        return(
            <div>
                <div className="row history_info ">
                    <div className="col-xs-12 center" >
                        <h3>404 Not-Found</h3>
                    </div>
                </div>
                <div className="static_cnt center">
                    <p>"The page you are looking for is not available.	"</p>
                </div>
            </div>
        )
    }
}

export{
    NotFound
}