import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactMixin from "react-mixin";


export default class PaySuccess extends TrackerReact(Component){
	constructor(){
		super();
	}
    componentDidMount(){
    	// data = {
    	// 	session : Session.get('guestId')
    	// }
     //    Meteor.call('successPayment',data,function(){
     //    	FlowRouter.go('/products');
     //    });
    }
	render(){
		let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
             metaTSite,
            metaGeoPlacename,
            metaGeoPosition,
            metaGeoRegion,
            metaIcbm;

        DocHead.removeDocHeadAddedTags();
        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = "Cart - Wingreens Farms";
        metaInfoDescription = {name: "description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: "Wingreens Farms - Traditionally handmade dips, spreads, bakery and teas"};
        metaOgDescription = {name: "og:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/payment/success"};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: "Cart - Wingreens Farms"};
        metaTTitle = {name: "twitter:title", content: "Cart - Wingreens Farms"};
        metaTDescription = {name: "twitter:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        metaGeoPlacename={name: "geo.placename", content: "B19, 1, Info Technology Park, Sector 34, Gurgaon, Haryana - 122001, India"};
        metaGeoPosition={name: "geo.position", content: "28.430799;77.013499"};
        metaGeoRegion={name: "geo.region", content: "IN-Haryana"};
        metaIcbm={name: "ICBM", content: "28.430799, 77.013499"};
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);
        DocHead.addMeta(metaGeoPlacename);
        DocHead.addMeta(metaGeoPosition);
        DocHead.addMeta(metaGeoRegion);
        DocHead.addMeta(metaIcbm);

        let gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });
		return(
			<div className="innerheight_info">
	            <div className="thanks_info">
		            <div className="thanks_msg">
		                <span className="checkmark_img"><img src="/client/images/checkmark.png"/></span>
		                <span>Thanks for shopping with us</span>
		                <div className="shoppingbtns_info">
		                	<a href="/products">Continue shopping</a>
		                </div>
		            </div>
	            </div>
            </div>
		)
	}
}

export  class PayCancel extends TrackerReact(Component){
	constructor(){
		super();
	}
	render(){
		let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            metaTSite;

        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = "Cart - Wingreens Farms";
        metaInfoDescription = {name: "description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: "Wingreens Farms - Traditionally handmade dips, spreads, bakery and teas"};
        metaOgDescription = {name: "og:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/payment/cancel"};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: "Cart - Wingreens Farms"};
        metaTTitle = {name: "twitter:title", content: "Cart - Wingreens Farms"};
        metaTDescription = {name: "twitter:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);



        let gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });
		return(
			<div className="innerheight_info">
	            <div className="cancel_info">
		            <div className="cancel_msg">
		                <span className="checkmark_img"><img src="/client/images/cancel_img.png"/></span>
		                <span>Payment was cancelled by you</span>
		                <div className="shoppingbtns_info">
		                	<a href="/products">Continue shopping</a>
		                </div>
		            </div>
	            </div>
            </div>
		)
	}
}

export  class PayError extends TrackerReact(Component){
	constructor(){
		super();
	}
	render(){
		let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            metaTSite;

        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = "Cart - Wingreens Farms";
        metaInfoDescription = {name: "description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: "Wingreens Farms - Traditionally handmade dips, spreads, bakery and teas"};
        metaOgDescription = {name: "og:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/payment/error"};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: "Cart - Wingreens Farms"};
        metaTTitle = {name: "twitter:title", content: "Cart - Wingreens Farms"};
        metaTDescription = {name: "twitter:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);



        let gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });
		return(
			<div className="innerheight_info">
	            <div className="cancel_info">
		            <div className="cancel_msg">
		                <span className="checkmark_img"><img src="/client/images/cancel_img.png"/></span>
		                <span>Error in Gatway Please try again</span>
		                <div className="shoppingbtns_info">
		                	<a href="/products">Continue shopping</a>
		                </div>
		            </div>
	            </div>
            </div>
		)
	}
}

