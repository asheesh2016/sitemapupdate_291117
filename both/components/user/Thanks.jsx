import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactMixin from "react-mixin";


export default class Thanks extends TrackerReact(Component){
    render(){
        return(
            <div>
                <div className="row history_info">
                    <div className="col-xs-12">
                        <h3>Thanks</h3>
                    </div>
                </div>
                <div className="static_cnt">
                    <p>"Thanks for the Order , we will notify you"</p>
                </div>
            </div>
        )
    }
}

export{
    Thanks
}