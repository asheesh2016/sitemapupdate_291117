import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactMixin from "react-mixin";
export default class ResetPassword extends TrackerReact(Component) {
    constructor() {
        super();
        this.resetpass = this.resetpass.bind(this);
    }
    resetpass(event){
        event.preventDefault();
        let token = FlowRouter.getParam('token');
        var npass = $(event.target).find("[name=npass]").val(),
            repass = $(event.target).find("[name=repass]").val();
        function isPassword(password){
            var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/;
            return regex.test(password);
        }
        if(_.isEmpty(npass) || _.isEmpty(repass)){
            Bert.alert('Fields not be empty', 'danger', 'growl-top-right');
            return false;
        }
        if(npass.length < 6){
            Bert.alert('Password Length must be more than 6 and equal to 6', 'danger', 'growl-top-right');
            return false;
        }
        if(npass != repass){
            Bert.alert('Confirm Password is not Matching', 'danger', 'growl-top-right');
            return false;
        } else if(!isPassword(npass)){
            Bert.alert('Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character', 'danger', 'growl-top-right');
            return false;
        }

        Accounts.resetPassword(token, npass, function(error){
            if (!error) {
                Bert.alert('Password changed Successfully. Please login with New Password.', 'success', 'growl-top-right');
                FlowRouter.go('/');
            }else{
                Bert.alert('Something went wrong! Please try again.', 'danger', 'growl-top-right');
            }
        });
    }
    render(){
        return (
            <section className="innerbody_info">
                <div className="container_info">
                    <div className="innerpage_bg">
                        <div className="innerheight_info">
                            <div className="row account_info">
                                <div className="col-xs-12">
                                    <h3 className="text-center">Reset Password</h3>
                                    <div className="reset_form">
                                        <form onSubmit={this.resetpass}>
                                            <div className="form-group">
                                                <input
                                                    className="form-control"
                                                    type="password"
                                                    placeholder="New Password"
                                                    name="npass"
                                                />
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    className="form-control"
                                                    type="password"
                                                    placeholder="Confirm Password"
                                                    name="repass"
                                                />
                                            </div>
                                            <button type="submit" className="btn btn-default submit_btn">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>    
        )
    }
}


export {
    ResetPassword
}
