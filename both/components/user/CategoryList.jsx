import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactMixin from "react-mixin";
import LazyLoad from 'react-lazy-load';

export default class CategoryList extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
            subscription: {
                ProductList : Meteor.subscribe('ProductList')
            }
        }
    }
    componentWillUnmount(){
        this.state.subscription.ProductList.stop();
    }
    componentWillMount(){
        let self = this;
    }
    category(){
        return Categories.findOne({slug:FlowRouter.getParam('slug')})
    }
    products(){
        var category = this.category() && this.category()._id;
        return Products.find({category: category,status : "1"}).fetch()
    }
    getaltag(txtdata){
       
       if(txtdata!=""){

          kdata=txtdata.split("|");

          return kdata[0];
      }else{
      return txtdata;
      }

    }

    carts(){
        return Carts.findOne();
    }
    categoryslug(product){
        pdata = Products.findOne({_id:product});
        id = "";
        if(pdata){
            id = pdata.category[0];
        }else{
            id = "";
        }
        if(pdata){
            sid = pdata.subcategory[0]
            if(sid){
                return Categories.findOne({_id:id}).slug +'/'+ SubCategories.findOne({_id:sid}).slug+'/';
            }else{
                return Categories.findOne({_id:id}).slug + '/';
            }
        }else{
            return "";
        }
    }
    addToCart(id,event){
        event.preventDefault();
        Meteor.call('AddToCart', {id:id,session:Session.get('guestId')}, function (error) {
            if (error) {
                Bert.alert('Error in connection', 'danger', 'growl-top-right');
            } else {
                Bert.alert('Item added to cart', 'success', 'growl-top-right');
            }
        })
    }
    render(){
        let cart = (id) =>{
            if(!_.isUndefined(this.carts())){
                if(_.findWhere(this.carts().items,{id:id})){
                    return 'Added to Cart';
                }else{
                    return 'Add to Cart';
                }
            }else{
                return 'Add to Cart';
            }
        }
        let showCart = (val) =>{
            if(val.quantity <= 0){
                return (
                    <a href="javascript:void(0)" className="addtocart_btn  outofstock_btn pull-left adtl">Out of Stock</a>
                )
            }else{
                return (
                    <a href="javascript:void(0)" className="addtocart_btn pull-left adtl" onClick={this.addToCart.bind(this,val._id)}>{cart(val._id)}</a>
                )
            }
        }
        DocHead.removeDocHeadAddedTags();
        let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            keywords,
            metaTSite,
            metaGeoPlacename,
            metaGeoPosition,
            metaGeoRegion,
            metaIcbm;

        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = (!_.isUndefined(this.category() && this.category().meta_title) ? this.category() && this.category().meta_title:'');
        keywords = {name:'keywords',content:(!_.isUndefined(this.category() && this.category().meta_keyword) ? this.category() && this.category().meta_keyword:'')};
        metaInfoDescription = {name: "description", content: (!_.isUndefined(this.category() && this.category().meta_description) ? this.category() && this.category().meta_description:'')};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: (!_.isUndefined(this.category() && this.category().meta_title) ? this.category() && this.category().meta_title:'')};
        metaOgDescription = {name: "og:description", content: (!_.isUndefined(this.category() && this.category().meta_description) ? this.category() && this.category().meta_description: '')};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/category/"+FlowRouter.getParam('slug')};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: (!_.isUndefined(this.category() && this.category().meta_title) ? this.category() && this.category().meta_title:'')};
        metaTTitle = {name: "twitter:title", content: (!_.isUndefined(this.category() && this.category().meta_title) ? this.category() && this.category().meta_title : '')};
        metaTDescription = {name: "twitter:description", content: (!_.isUndefined(this.category() && this.category().meta_description) ? this.category() && this.category().meta_description:'')};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        metaGeoPlacename={name: "geo.placename", content: "B19, 1, Info Technology Park, Sector 34, Gurgaon, Haryana - 122001, India"};
        metaGeoPosition={name: "geo.position", content: "28.430799;77.013499"};
        metaGeoRegion={name: "geo.region", content: "IN-Haryana"};
        metaIcbm={name: "ICBM", content: "28.430799, 77.013499"};
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);
        DocHead.addMeta(keywords);
        DocHead.addMeta(metaGeoPlacename);
        DocHead.addMeta(metaGeoPosition);
        DocHead.addMeta(metaGeoRegion);
        DocHead.addMeta(metaIcbm);


        let gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });
        return (
           <div>
               <div className="row history_info">
                    <div className="breadcrumb-info">
                        <ol className="breadcrumb">
                            <li><a href="/">Home</a></li>
                            <li className="active">{this.category() && this.category().name}</li>
                        </ol>
                    </div>
                   <div className="col-xs-12">
                       <h1>{this.category() && this.category().name}</h1>
                   </div>
               </div>
               <div className="row">
                   {this.products().map((val)=>{
                        return (
                            <div className="col-sm-6 col-md-4 col-xs-12 search_list gourmet_list" key={val._id}>
                                <div className="searchlist_img"> 
                                    <a href={'/product/'+(this.categoryslug(val._id))+ val.slug}>
                                        <LazyLoad height={250}>
                                            <img src={val.featuredImage} alt={this.getaltag(val.seotitle ? val.seotitle : val.title)}/>
                                        </LazyLoad>
                                    </a> 
                                </div>
                                <h5 className="text-center">
                                    <a href={'/product/'+(this.categoryslug(val._id))+ val.slug}>{val.title}</a> <span>INR  {val.price}</span>
                                </h5>
                                <div className="addtocart_info clearfix">
                                    {showCart(val)}
                                    <a href={'/product/'+(this.categoryslug(val._id))+ val.slug} className="viewdetails_btn adtr pull-right">View Details</a>
                                </div>
                            </div>
                       )
                   })}
               </div>
           </div>
       )
   }
}

export {
    CategoryList
}
