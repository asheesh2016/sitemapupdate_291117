import React ,{Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactMixin from "react-mixin";

export default class Home extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
            subscription:{
                sliders:Meteor.subscribe('Sliders'),
                seo: Meteor.subscribe('Seo','/'),
            }
        }
    }
    componentDidMount(){
        var swiper_home = new Swiper('.swiper-container-home', {
            spaceBetween: 0,
            autoplay : 5000,
            loop : true,
            slidesPerView:1,
            observer:true
        });
         FlowRouter.triggers.enter(function() {

    DocHead.removeDocHeadAddedTags();
      });
    }
    componentWillUnmount() {
        this.state.subscription.sliders.stop();
        this.state.subscription.seo.stop();
    }
    seo(){
        return Seo.findOne({url:'/'});
    }      
    sliderList(){
        return Sliders.find({},{sort: {order: 1}}).fetch();
    }
    render(){
       
        let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            keywords,
            metaCanonical,
            metaTSite,
            metaGeoPlacename,
            metaGeoPosition,
            metaGeoRegion,
            metaIcbm;

        DocHead.removeDocHeadAddedTags();
        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = (!_.isUndefined(this.seo() && this.seo().meta_title) ? this.seo() && this.seo().meta_title:'');
        keywords = {name:'keywords',content:(!_.isUndefined(this.seo() && this.seo().meta_keyword) ? this.seo() && this.seo().meta_keyword:'')};
        metaInfoDescription = {name: "description", content: (!_.isUndefined(this.seo() && this.seo().meta_description) ? this.seo() && this.seo().meta_description:'')};
        metaInfoRobots = {name: "robots", content: "noodp, noydir"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: (!_.isUndefined(this.seo() && this.seo().meta_title) ? this.seo() && this.seo().meta_title:'')};
        metaOgDescription = {name: "og:description", content: (!_.isUndefined(this.seo() && this.seo().meta_description) ? this.seo() && this.seo().meta_description: '')};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/"};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: (!_.isUndefined(this.seo() && this.seo().meta_title) ? this.seo() && this.seo().meta_title:'')};
        metaTTitle = {name: "twitter:title", content: (!_.isUndefined(this.seo() && this.seo().meta_title) ? this.seo() && this.seo().meta_title : '')};
        metaTDescription = {name: "twitter:description", content: (!_.isUndefined(this.seo() && this.seo().meta_description) ? this.seo() && this.seo().meta_description:'')};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        metaCanonical={rel: "canonical", href: "https://www.wingreensfarms.com"}
        metaGeoPlacename={name: "geo.placename", content: "B19, 1, Info Technology Park, Sector 34, Gurgaon, Haryana - 122001, India"}
        metaGeoPosition={name: "geo.position", content: "28.430799;77.013499"}
        metaGeoRegion={name: "geo.region", content: "IN-Haryana"}
        metaIcbm={name: "ICBM", content: "28.430799, 77.013499"}
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaCanonical);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);
        DocHead.addMeta(metaGeoPlacename);
        DocHead.addMeta(metaGeoPosition);
        DocHead.addMeta(metaGeoRegion);
        DocHead.addMeta(metaIcbm);
        DocHead.addMeta(keywords);
        var gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });

        return (
                    <div className="bannerslider_info">
                        <div className="swiper-container swiper-container-home">
                            <div className="swiper-wrapper banner_slider">
                                <div className="swiper-slide">
                                    <div className="banner_logoinfo">
                                        <img src="/client/images/banner_logo.png" alt="Wingreens Farms -Banner Logo"/>
                                    </div>
                                    <div className="homebanner_txt text-center"> <img src="/client/images/itsall_img.png" className="itsall_img" alt="Wingreens Farms - Banner"/>
                                    </div>
                                </div>
                                {this.sliderList().map((val) => {  
                                    return (
                                        
                                            <div className="swiper-slide" key={val._id}>
                                                <a href={val.link}>
                                                    <img src={val.featuredImage} alt={val.sliderAltTag} height="600px"/>
                                                    <h2>{val.content}</h2>
                                                </a>
                                            </div>
                                        
                                    )
                                })}
                            </div>
                        </div>
                    </div>
        )
    }
}

export {
    Home
}
