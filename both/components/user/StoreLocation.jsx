import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class StoreLocation extends TrackerReact(Component){
	constructor(){
		super();
		this.state ={
			loaded: false,
			city : 'All',
            q : '',
			subscription: {
                StoreList : Meteor.subscribe('StoreList'),
            }
		}
        this.q = this.q.bind(this);
	}
	stores(){
        if(this.state.q == ''){
            return Stores.find({},{sort:{createdAt:-1}}).fetch();
        }else{
            var regExp = this.buildRegExp(this.state.q);
             return Stores.find({$or: [
                    {city: regExp},
                    {address: regExp},
                ]},{sort:{createdAt:-1}}).fetch();
        }
  	}
    componentWillUnmount(){
        this.state.subscription.StoreList.stop();
    }
    q(event){
        this.setState({q:event.target.value});
    }
	
    buildRegExp(searchText) {
      var words = searchText.trim().split(/[ \-\:]+/);
      var exps = _.map(words, function(word) {
        return "(?=.*" + word + ")";
      });
      var fullExp = exps.join('') + ".+";
      return new RegExp(fullExp, "i");
    }
    render(){
    
        let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            metaTSite,
            metaGeoPlacename,
            metaGeoPosition,
            metaGeoRegion,
            metaIcbm;
         DocHead.removeDocHeadAddedTags();
        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = "Store Locator - Wingreens Farms";
        metaInfoDescription = {name: "description", content: "Find the closest store to you!"};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: "Wingreens Farms - Traditionally handmade dips, spreads, bakery and teas"};
        metaOgDescription = {name: "og:description", content: "Find the closest store to you!"};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/store-location"};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: "Store Locator - Wingreens Farms"};
        metaTTitle = {name: "twitter:title", content: "Store Locator - Wingreens Farms"};
        metaTDescription = {name: "twitter:description", content: "Find the closest store to you!"};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        metaGeoPlacename={name: "geo.placename", content: "B19, 1, Info Technology Park, Sector 34, Gurgaon, Haryana - 122001, India"};
        metaGeoPosition={name: "geo.position", content: "28.430799;77.013499"};
        metaGeoRegion={name: "geo.region", content: "IN-Haryana"};
        metaIcbm={name: "ICBM", content: "28.430799, 77.013499"};

        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);
        DocHead.addMeta(metaGeoPlacename);
        DocHead.addMeta(metaGeoPosition);
        DocHead.addMeta(metaGeoRegion);
        DocHead.addMeta(metaIcbm);



        let gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });
		return(
			<div className="innerheight_info">
                <div className="breadcrumb-info">
                    <ol className="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li className="active">Store Locator</li>
                    </ol>
                </div>
                <div className="input-group" style={{'marginBottom':'20px','marginTop':'20px'}}>
                  <input type="text" name="q" className="form-control" placeholder="Search..." value={this.state.q} onChange={this.q}/>
                      <span className="input-group-btn">
                        <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
                        </button>
                      </span>
                </div>
				<div className="row">
					{this.stores().map((val)=>{
						return (
							<div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 stores_list" key={val._id}>
			                	<h4>{val.address}</h4>
			                	<p>{val.city}
			                    <br /> <a href={'http://maps.google.com?q='+val.lat+','+val.lng} target="_blank"><i><img src="/client/images/viewmap_icon.png"/></i> Location</a></p>
			            	</div>
						)
					})}
                </div>
			</div>
		)
	}
}

export {
    StoreLocation
}