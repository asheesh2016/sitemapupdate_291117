import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { FacebookButton,GooglePlusButton, TwitterButton } from "react-social";
import Rating from "react-rating";
import ReactMixin from "react-mixin";
import LazyLoad from 'react-lazy-load';
export default class Product extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
            subscription: {
                reviewList : Meteor.subscribe('RatingProduct',FlowRouter.getParam('slug')), 
                ProductList : Meteor.subscribe('ProductList')
            }
        }
    }
    componentWillUnmount(){
        this.state.subscription.ProductList.stop();
        this.state.subscription.reviewList.stop();

    }
    componentWillMount(){
        let self = this;
        if(FlowRouter.current().route.name == 'Product1'){
            console.log("haha");
            FlowRouter.go('/product/'+this.categoryslug(this.product() && this.product()._id)+FlowRouter.getParam('slug'))
        }
    }
    product(){
        return Products.findOne({slug:FlowRouter.getParam('slug'),status : "1"})
    }
    category(){
        id = this.product() && this.product().category[0]
        return Categories.findOne({_id:id})
    }
    products(){
        var category = this.product() && this.product().category[0];
        id = this.product() && this.product()._id
        return Products.find({category:category,_id : {$nin:[id]},status : "1"}).fetch()
    }
    categoryslug(product){
        pdata = Products.findOne({_id:product});
        id = "";
        if(pdata){
            id = pdata.category[0];
        }else{
            id = "";
        }
        if(pdata){
            sid = pdata.subcategory[0]
            if(sid){
                return Categories.findOne({_id:id}).slug +'/'+ SubCategories.findOne({_id:sid}).slug+'/';
            }else{
                return Categories.findOne({_id:id}).slug + '/';
            }
        }else{
            return "";
        }
    }

    getaltTag(txt){
      pdata = this.product() && this.product().seotitle
      if(pdata){
       var kdata=pdata.split('|');
       return kdata[txt];
      }else{
      return pdata = this.product() && this.product().title;
      }
    }
    
    getaltTagSwiper(tdata){

     if(tdata!=""){
       var ndata=tdata.split("|");
       return ndata[0];
     }
     else{

         return "Product Details";
     }

    }

    componentDidMount(){
        let self = this;
            var swiper_image = new Swiper('.swiper-container-image', {
                spaceBetween: 0,
                slidesPerView:1,
                observer:true
            });

            var swiper_thumb = new Swiper('.swiper-container-thumb', {
                spaceBetween: 0,
                centeredSlides: true,
                slidesPerView: 1,
                slideToClickedSlide: true,
                observer:true,
                nextButton: '.thumbNext',
                prevButton: '.thumbPrev',
            });
            swiper_image.params.control = swiper_thumb;
            swiper_thumb.params.control = swiper_image;
            
            var swiper_related = new Swiper('.swiper-container-related', {
                nextButton: '.relatedNext',
                prevButton: '.relatedPrev',
                slidesPerView: 3,
                spaceBetween: 10,
                observer:true,
            });

            var swiper_review = new Swiper('.swiper-container-review', {
                nextButton: '.reviewNext',
                prevButton: '.reviewPrev',
                spaceBetween: 0,
                slidesPerView: 1,
                observer:true
            });
        if(FlowRouter.current().route.name == 'Product1'){
            console.log("haha");
            Meteor.call("categoryslug",FlowRouter.getParam('slug'),function(err,result){
                if(err){
                    FlowRouter.go('/');
                }else{
                    if(result == ''){
                        FlowRouter.go('/');
                    }else{
                        FlowRouter.go('/product/'+result+FlowRouter.getParam('slug'))
                    }
                }
            })
            
        } 
    }
    carts(){
        return Carts.findOne();
    }
    addToCart(id,event){
        event.preventDefault();
        Meteor.call('AddToCart', {id:id,session:Session.get('guestId')}, function (error) {
            if (error) {
                Bert.alert('Error in connection', 'danger', 'growl-top-right');
            } else {
                Bert.alert('Item added to cart', 'success', 'growl-top-right');
            }
        })
    }
    reviews(){
        return Reviews.find({}).fetch();
    }
    users(id){
        return Meteor.users.findOne(id) && Meteor.users.findOne(id).profile.name;
    }
    render(){
        let AvailableArea;
        let cart = (id) =>{
            if(!_.isUndefined(this.carts())){
                if(_.contains(this.carts().items,id)){
                    return 'Added to Cart';
                }else{
                    return 'Add to Cart';
                }
            }else{
                return 'Add to Cart';
            }
        }
        if(this.product()) {
            if (this.product().quantity <= 0) {
                AvailableArea = (
                    <div className="checkavbl_info text-right">
                        <a href="javascript:void(0)" className="checkavbl_btn" style={{color:'red',marginRight:'36px'}}>Out of Stock</a>
                    </div>
                )
            } else {
                AvailableArea = (
                    <div className="addtocart_info text-right">
                        <a href="javascript:void(0)" className="addtocart_btn" onClick={this.addToCart.bind(this,this.product() && this.product()._id)}>{cart(this.product() && this.product()._id)}</a>
                    </div>
                )
            }
        }
        let rating = (val) => {
            if(!_.isUndefined(val)){
                if(!_.isUndefined(val.rating)){
                    return (
                        <Rating
                            start={0}
                            stop={5}
                            step={1}
                            initialRate={val.rating}
                            readonly={true}
                            empty="fa fa-star-o fa-star-2x wincolor"
                            full="fa fa-star fa-star-2x wincolor"
                        />
                    )
                }else{
                    return (
                        <Rating
                            start={0}
                            stop={5}
                            step={1}
                            initialRate={0}
                            readonly={true}
                            empty="fa fa-star-o fa-star-2x wincolor"
                            full="fa fa-star fa-star-2x wincolor"
                        />
                    )
                }
            } else {  
                    return (
                        <Rating
                            start={0}
                            stop={5}
                            step={1}
                            initialRate={0}
                            readonly={true}
                            empty="fa fa-star-o fa-star-2x wincolor"
                            full="fa fa-star fa-star-2x wincolor"
                        />
                    )
            }
        }
        DocHead.removeDocHeadAddedTags();
        var link = Meteor.absoluteUrl()+'product/'+(this.categoryslug(this.product() && this.product()._id))+FlowRouter.getParam('slug'), facebookAppId = "134727616926860", message = this.product() && this.product().title;
        let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            keywords,
            metaTSite,
            metaGeoPlacename,
            metaGeoPosition,
            metaGeoRegion,
            metaIcbm;

        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = (!_.isUndefined(this.product() && this.product().meta_title) ? this.product() && this.product().meta_title:'');
        keywords = {name:'keywords',content:(!_.isUndefined(this.product() && this.product().meta_keyword) ? this.product() && this.product().meta_keyword:'')};
        metaInfoDescription = {name: "description", content: (!_.isUndefined(this.product() && this.product().meta_description) ? this.product() && this.product().meta_description:'')};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: (!_.isUndefined(this.product() && this.product().meta_title) ? this.product() && this.product().meta_title:'')};
        metaOgDescription = {name: "og:description", content: (!_.isUndefined(this.product() && this.product().meta_description) ? this.product() && this.product().meta_description: '')};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/product/"+(this.categoryslug(this.product() && this.product()._id))+FlowRouter.getParam('slug')};
        metaOgimage = {name: "og:image", content: this.product() && this.product().images[0]};
        metaOgSite_name = {name: "og:site_name", content: (!_.isUndefined(this.product() && this.product().meta_title) ? this.product() && this.product().meta_title:'')};
        metaTTitle = {name: "twitter:title", content: (!_.isUndefined(this.product() && this.product().meta_title) ? this.product() && this.product().meta_title : '')};
        metaTDescription = {name: "twitter:description", content: (!_.isUndefined(this.product() && this.product().meta_description) ? this.product() && this.product().meta_description:'')};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        metaGeoPlacename={name: "geo.placename", content: "B19, 1, Info Technology Park, Sector 34, Gurgaon, Haryana - 122001, India"};
        metaGeoPosition={name: "geo.position", content: "28.430799;77.013499"};
        metaGeoRegion={name: "geo.region", content: "IN-Haryana"};
        metaIcbm={name: "ICBM", content: "28.430799, 77.013499"};
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);
        DocHead.addMeta(keywords);
        DocHead.addMeta(metaGeoPlacename);
        DocHead.addMeta(metaGeoPosition);
        DocHead.addMeta(metaGeoRegion);
        DocHead.addMeta(metaIcbm);



        let gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });
        let reviewBox;
        if(_.size(this.reviews()) > 0){
            reviewBox = (
                <div className="reviews_row">
                    <h3>Reviews</h3>
                    <div className="swiper-container swiper-container-review">
                        <div className="swiper-wrapper review_slider">
                            {this.reviews().map((val)=>{
                                return (
                                    <div className="swiper-slide" key={val._id}><span>{this.users(val.userId)} :</span> {val.review} </div>
                                )
                            })}
                        </div>
                        <div className="swiper-button-next reviewNext"></div>
                        <div className="swiper-button-prev reviewPrev"></div>
                    </div>
                </div>
            )
        }else{
            reviewBox = null;
        }
        return (
            <div>
                <div className="breadcrumb-info">
                    <ol className="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li><a href={"/category/"+(this.category() && this.category().slug)}>{this.category() && this.category().name}</a></li>
                        <li className="active">{this.product() && this.product().title}</li>
                    </ol>
                </div>
                <div className="row seprateprod_row">
                    <div className="col-sm-6 col-md-4 col-xs-12 seprateprod_list">
                        <div className="swiper-container swiper-container-image">
                            <div className="swiper-wrapper">
                                {this.product() && this.product().images.map((val,key)=>{
                                    return (
                                        <div className="swiper-slide" key={val}><img src={val} alt={this.getaltTag(key)} width="300"/></div>
                                    )
                                })}
                            </div>
                        </div>
                        <div className="swiper-container swiper-container-thumb">
                            <div className="swiper-wrapper">
                                {this.product() && this.product().images.map((val,key)=>{
                                    return (
                                        <div className="swiper-slide" key={val}><img src={val} alt={this.getaltTag(key)} width="150"/></div>
                                    )
                                })}
                            </div>
                            <div className="swiper-button-next thumbNext"></div>
                            <div className="swiper-button-prev thumbPrev"></div>
                        </div>
                    </div>
                    <div className="col-md-8  col-sm-6  col-xs-12 seprateprod_list">
                        <h1 className="title">{this.product() && this.product().title} <span>({this.product() && this.product().weight})</span></h1>
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="rating_stars">
                                    {rating(this.product())}
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="social text-right">
                                    <FacebookButton  url={link} className="facebookc" appId={facebookAppId} message={message}>
                                        <i className="fa fa-facebook" aria-hidden="true"></i>
                                    </FacebookButton>
                                    <TwitterButton url={link} title="Share via Facebook" className="twiiterc" message={message}>
                                        <i className="fa fa-twitter" aria-hidden="true"></i>
                                    </TwitterButton>
                                </div>
                            </div>
                        </div>
                        <h4 className="price"><span>{'INR '+(this.product() && this.product().price)}</span></h4>
                        <div dangerouslySetInnerHTML={{__html: this.product() && this.product().content}}></div>
                        <p>{this.product() && this.product().note}</p>
                        {AvailableArea}
                    </div>
                </div>
                {reviewBox}
                <div className="alsotry_row hidden-xs">
                    <h3>Also try</h3>
                    <div className="swiper-container swiper-container-related">
                        <div className="swiper-wrapper alsotry_slider">
                            {this.products().map((val)=>{
                                return (
                                    <div className="swiper-slide srr" key={val._id}>
                                        <a href={'/product/'+(this.categoryslug(val._id))+val.slug}>
                                            <img src={val.featuredImage} alt={this.getaltTagSwiper(val.seotitle ? val.seotitle : val.title)}/>
                                            <h3>{val.title}</h3>
                                            <h5>{'INR '+val.price}</h5>
                                        </a>
                                    </div>
                                )
                            })}
                        </div>
                        <div className="swiper-button-next relatedNext"></div>
                        <div className="swiper-button-prev relatedPrev"></div>
                    </div>
                </div>
                <div className="alsotry_row visible-xs-block">
                    <h3>Also try</h3>
                    <div className="row">
                        {this.products().map((val)=>{
                            return (
                                <div className="col-sm-6 col-md-4 col-xs-12 search_list" key={val._id}>
                                    <div className="searchlist_img"> 
                                        <a href={'/product/'+(this.categoryslug(val._id))+val.slug}>
                                            <img src={val.featuredImage} alt={this.getaltTagSwiper(val.seotitle ? val.seotitle : val.title)}/>
                                            <h3>{val.title}</h3>
                                            <h5>{'INR '+val.price}</h5>
                                        </a>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

export {
    Product
}
