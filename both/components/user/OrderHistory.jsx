import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactMixin from "react-mixin";
export default class OrderHistory extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
            subscription: {
                OrderHistoryUser : Meteor.subscribe('OrderHistoryUser'),
            }
        }
    }
    orders(){
        return Orders.find({},{sort:{createdAt:-1}}).fetch()
    }
    product(id){
        return Products.findOne(id);
    }
    componentWillUnmount(){
        this.state.subscription.OrderHistoryUser.stop();
    }
    categoryslug(product){
        pdata = Products.findOne({_id:product});
        id = "";
        if(pdata){
            id = pdata.category[0];
        }else{
            id = "";
        }
        if(pdata){
            sid = pdata.subcategory[0]
            if(sid){
                return Categories.findOne({_id:id}).slug +'/'+ SubCategories.findOne({_id:sid}).slug+'/';
            }else{
                return Categories.findOne({_id:id}).slug + '/';
            }
        }else{
            return "";
        }
    }
    render(){
        let date = (createdAt) => {
            return moment(createdAt).format('DD-MM-YYYY');
        }
        let status = (status) => {
            if(status == 2){
                return 'processing';
            }else if(status == 3){
                return 'shipping';
            }else if(status == 4){
                return 'completed';
            }else if(status == 5) {
                return 'cancelled';
           }
        }
        let price_check = (id) => {
            if(_.isUndefined(this.product(id) && this.product(id).price)){
                return 0;
            }else{
                return this.product(id) && this.product(id).price;
            }
        }
        let orderBox;
        if(_.size(this.orders()) > 0){
            orderBox = this.orders().map((val) =>{
                            return (
                                <div className="row history_row" id={val._id} key={val._id}>
                                    {val.cart.items.map((item)=>{
                                        return (
                                            <div className="col-md-4 col-xs-12 history_list" key={item.id}>
                                                <div className="history_img">
                                                    <a href={'/product/'+(this.categoryslug(item.id))+ (this.product(item.id) && this.product(item.id).slug)}>
                                                        <img src={this.product(item.id) && this.product(item.id).featuredImage} alt={this.product(item.id) && this.product(item.id).title}/>
                                                    </a> 
                                                </div>
                                                <h5 className="text-center">
                                                    <a href={'/product/'+(this.categoryslug(item.id))+ (this.product(item.id) && this.product(item.id).slug)}>
                                                        {this.product(item.id) && this.product(item.id).title}
                                                    </a> 
                                                    <span>INR  {price_check(item.id)}</span>
                                                </h5>
                                                <ul>
                                                    <li>Order Date: {date(val.createdAt)}</li>
                                                    <li>Order Quantity: {item.count} </li>
                                                    <li>Order Number: {val._id}</li>
                                                    <li>Status: {status(val.status)}</li>
                                                    <li>Total Amount: {item.count * price_check(item.id)}</li>
                                                    <li className="text-center"><a href='/reviews-ratings'>Review this Product</a></li>
                                                </ul>
                                            </div>
                                        )
                                    })}
                                </div>
                            )
                        })
        }else{
            orderBox = (
                <div className="col-xs-12">
                    <h3>No Orders Yet</h3>
                </div>
            );
        }
        let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            metaTSite,
            metaGeoPlacename,
            metaGeoPosition,
            metaGeoRegion,
            metaIcbm;
         DocHead.removeDocHeadAddedTags();
        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = "Order History - Wingreens Farms";
        metaInfoDescription = {name: "description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: "Wingreens Farms - Traditionally handmade dips, spreads, bakery and teas"};
        metaOgDescription = {name: "og:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/order-history"};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: "Order History - Wingreens Farms"};
        metaTTitle = {name: "twitter:title", content: "Order History - Wingreens Farms"};
        metaTDescription = {name: "twitter:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        metaGeoPlacename={name: "geo.placename", content: "B19, 1, Info Technology Park, Sector 34, Gurgaon, Haryana - 122001, India"};
        metaGeoPosition={name: "geo.position", content: "28.430799;77.013499"};
        metaGeoRegion={name: "geo.region", content: "IN-Haryana"};
        metaIcbm={name: "ICBM", content: "28.430799, 77.013499"};
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);
        DocHead.addMeta(metaGeoPlacename);
        DocHead.addMeta(metaGeoPosition);
        DocHead.addMeta(metaGeoRegion);
        DocHead.addMeta(metaIcbm);



        let gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });
        return (
            <div>
                <div className="row history_info">
                    <div className="breadcrumb-info">
                        <ol className="breadcrumb">
                            <li><a href="/">Home</a></li>
                            <li className="active">Order History</li>
                        </ol>
                    </div>
                    <div className="col-xs-12">
                        <h3>Order History</h3>
                    </div>
                </div>
                {orderBox}
            </div>
        )
    }
}

export {
    OrderHistory
}
