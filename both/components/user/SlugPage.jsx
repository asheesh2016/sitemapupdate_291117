import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactMixin from "react-mixin";

export default class SlugPage extends TrackerReact(Component){
	constructor(){
		super();
		this.state ={
			subcription: {
				singlepage: Meteor.subscribe('SinglePage',FlowRouter.getParam('slug')),
			}
		}
	}
    componentWillUnmount() {
        this.state.subscription.singlepage.stop();
    }
    componentWillMount() {
        var self = this;
        Tracker.autorun(function () {
            FlowRouter.watchPathChange();
            if(!_.isUndefined(self.state.subscription)){
                self.state.subscription.singlepage.stop();
            }
            self.setState({subscription: {
                singlepage: Meteor.subscribe('SinglePage',FlowRouter.getParam('slug')),
            }})
        });

    }       
    page(){
        return Pages.findOne({slug:FlowRouter.getParam('slug')});
    }    	
    pageTitle(){
        var page =  Pages.findOne({slug:FlowRouter.getParam('slug')});
        if(page){
            return page.title;
        }else{
           return "404 Not-Found"; 
        }
    }
	pageContent(){
		var page =  Pages.findOne({slug:FlowRouter.getParam('slug')});
        if(page){
            return page.content;
        }else{
           return "<div><div className='row history_info '><div className='col-xs-12 center' style='text-align:center !important;'><h3>404 Not-Found</h3></div></div><div className='static_cnt center' style='text-align:center !important;'><p>The page you are looking for is not available.</p></div></div>"; 
        }
	}
	render(){
		let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            keywords,
            metaTSite,
            metaGeoPlacename,
            metaGeoPosition,
            metaGeoRegion,
            metaIcbm;

          DocHead.removeDocHeadAddedTags();
        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};

        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = (!_.isUndefined(this.page() && this.page().meta_title) ? this.page() && this.page().meta_title:'');
        keywords = {name:'keywords',content:(!_.isUndefined(this.page() && this.page().meta_keyword) ? this.page() && this.page().meta_keyword:'')};
        metaInfoDescription = {name: "description", content: (!_.isUndefined(this.page() && this.page().meta_description) ? this.page() && this.page().meta_description:'')};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: (!_.isUndefined(this.page() && this.page().meta_title) ? this.page() && this.page().meta_title:'')};
        metaOgDescription = {name: "og:description", content: (!_.isUndefined(this.page() && this.page().meta_description) ? this.page() && this.page().meta_description: '')};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/"+FlowRouter.getParam('slug')};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: (!_.isUndefined(this.page() && this.page().meta_title) ? this.page() && this.page().meta_title:'')};
        metaTTitle = {name: "twitter:title", content: (!_.isUndefined(this.page() && this.page().meta_title) ? this.page() && this.page().meta_title : '')};
        metaTDescription = {name: "twitter:description", content: (!_.isUndefined(this.page() && this.page().meta_description) ? this.page() && this.page().meta_description:'')};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        metaGeoPlacename={name: "geo.placename", content: "B19, 1, Info Technology Park, Sector 34, Gurgaon, Haryana - 122001, India"};
        metaGeoPosition={name: "geo.position", content: "28.430799;77.013499"};
        metaGeoRegion={name: "geo.region", content: "IN-Haryana"};
        metaIcbm={name: "ICBM", content: "28.430799, 77.013499"};
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);
        DocHead.addMeta(keywords);
        DocHead.addMeta(metaGeoPlacename);
        DocHead.addMeta(metaGeoPosition);
        DocHead.addMeta(metaGeoRegion);
        DocHead.addMeta(metaIcbm);


        let gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });
		return(
			<div>
                <div className="breadcrumb-info">
                    <ol className="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li className="active">{this.pageTitle()}</li>
                    </ol>
                </div>
				<div dangerouslySetInnerHTML={{__html: this.pageContent()}}></div>
			</div>
		)
	}
}

export{
	SlugPage
}
