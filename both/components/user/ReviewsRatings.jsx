import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import Rating from "react-rating";
import ReactMixin from "react-mixin";

export default class ReviewsRatings extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
            subscription: {
                OrderHistoryUser : Meteor.subscribe('OrderHistoryUser'),
                Rating : Meteor.subscribe('RatingByUser'),
            }
        }
        this.rating = 0;
        this.onChange = this.onChange.bind(this);
        this.submitRating = this.submitRating.bind(this);
    }
    orders(){
        return Orders.find({},{sort:{createdAt:1}}).fetch()
    }
    product(id){
        return Products.findOne(id);
    }
    reviews(id){
        return Reviews.findOne({userId:Meteor.userId(),productId:id})
    }
    onChange(rate){
        this.rating = rate;
        return rate;
    }
    submitRating(event,id){
        event.preventDefault();
        if(_.isEmpty($(event.target).find('textarea').val())){
            Bert.alert('Please enter the review', 'danger', 'growl-top-right');
            return false;
        }
        data = {
            productId : id,
            rating : this.rating,
            review : $(event.target).find('textarea').val(),
            userId : Meteor.userId()
        }
        Meteor.call('addReview',data,function (error) {
            if(!error){
                Bert.alert('Review added', 'success', 'growl-top-right');
                FlowRouter.go('/reviews-ratings')
            }
        })
    }
    componentWillUnmount(){
        this.state.subscription.OrderHistoryUser.stop();
    }
    categoryslug(product){
        pdata = Products.findOne({_id:product});
        id = "";
        if(pdata){
            id = pdata.category[0];
        }else{
            id = "";
        }
        if(pdata){
            sid = pdata.subcategory[0]
            if(sid){
                return Categories.findOne({_id:id}).slug +'/'+ SubCategories.findOne({_id:sid}).slug+'/';
            }else{
                return Categories.findOne({_id:id}).slug + '/';
            }
        }else{
            return "";
        }
    }
    render(){
        let checkRating = (id) =>{
            if(this.reviews(id)){
                return(
                    <div className="col-md-8 col-sm-6 col-xs-12 ratinglist_rght">
                        <div className="rating_stars">
                            <Rating
                                start={0}
                                stop={5}
                                step={1}
                                initialRate={this.reviews(id) && this.reviews(id).rating}
                                readOnly={true}
                                empty="fa fa-star-o fa-star-2x wincolor"
                                full="fa fa-star fa-star-2x wincolor"
                            />
                        </div>
                        <p>{this.reviews(id) && this.reviews(id).review}</p>
                    </div>
                )
            }else{
               return (
                   <div className="col-md-8 col-sm-6 col-xs-12">
                       <div className="rating_stars">
                           <Rating
                               start={0}
                               stop={5}
                               step={1}
                               empty="fa fa-star-o fa-star-2x wincolor"
                               full="fa fa-star fa-star-2x wincolor"
                               onChange = {(rate) => this.onChange(rate)}
                           />
                       </div>
                       <form onSubmit={(event) => {this.submitRating(event,id)}}>
                           <textarea rows="2" style={{width:'100%',fontSize:'28px',fontFamily:'none'}}></textarea>
                           <button className='submit_btn' style={{fontFamily:'none',fontSize:'none',background:"#349e49",padding:"6px 30px",border:'0px',color:'#fff'}}>Submit</button>
                       </form>
                   </div>
               )
            }
        }
        let orderBox;
        if(_.size(this.orders()) > 0){
            let items = _.union(_.flatten(_.map(this.orders(),function(val){return _.map(val.cart.items,function(item){return item.id})})))
            orderBox = items.map((item)=> {
                                        return (
                                            <div className="row rating_row" key={item}>
                                                <div className="col-md-4 col-sm-6 col-xs-12 ratinglist_lft text-center">
                                                    <div className="ratinglist_img">
                                                        <a href={'/product/'+(this.categoryslug(item))+ (this.product(item) && this.product(item).slug)}>
                                                            <img src={this.product(item) && this.product(item).featuredImage} alt={this.product(item) && this.product(item).title}/>
                                                        </a>
                                                        <h5 className="text-center">
                                                            <a href={'/product/'+(this.categoryslug(item))+ (this.product(item) && this.product(item).slug)}>
                                                                {this.product(item) && this.product(item).title}
                                                            </a>
                                                            <span>INR  {this.product(item) && this.product(item).price}</span>
                                                        </h5>
                                                    </div>
                                                </div>
                                                {checkRating(item)}
                                            </div>
                                        )
                                    })
        }else{
            orderBox = (
                <div className="col-xs-12">
                    <h1>Please place an order to review a product</h1>
                </div>
            )
        }
        let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            metaTSite,
            metaGeoPlacename,
            metaGeoPosition,
            metaGeoRegion,
            metaIcbm;
        DocHead.removeDocHeadAddedTags();
        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = "Reviews and Ratings - Wingreens Farms";
        metaInfoDescription = {name: "description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: "Wingreens Farms - Traditionally handmade dips, spreads, bakery and teas"};
        metaOgDescription = {name: "og:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/reviews-ratings"};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: "Reviews and Ratings - Wingreens Farms"};
        metaTTitle = {name: "twitter:title", content: "Reviews and Ratings - Wingreens Farms"};
        metaTDescription = {name: "twitter:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        metaGeoPlacename={name: "geo.placename", content: "B19, 1, Info Technology Park, Sector 34, Gurgaon, Haryana - 122001, India"};
        metaGeoPosition={name: "geo.position", content: "28.430799;77.013499"};
        metaGeoRegion={name: "geo.region", content: "IN-Haryana"};
        metaIcbm={name: "ICBM", content: "28.430799, 77.013499"};
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);
        DocHead.addMeta(metaGeoPlacename);
        DocHead.addMeta(metaGeoPosition);
        DocHead.addMeta(metaGeoRegion);
        DocHead.addMeta(metaIcbm);



        let gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });
        return (
            <div>
                <div className="row history_info">
                    <div className="breadcrumb-info">
                        <ol className="breadcrumb">
                            <li><a href="/">Home</a></li>
                            <li className="active">Reviews and Ratings</li>
                        </ol>
                    </div>
                    <div className="col-xs-12">
                        <h3>Reviews and Ratings</h3>
                    </div>
                </div>
                {orderBox}
            </div>
        )
    }
}

export {
    ReviewsRatings
}
