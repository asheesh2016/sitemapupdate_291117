import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactMixin from "react-mixin";
export default class Cart extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
            discount : 0,
            couponcode : '',
            type : '',
            coupon : ''
        }
        this.code = this.code.bind(this);
        this.applyCoupon = this.applyCoupon.bind(this);
        this.total = this.total.bind(this);
        this.totalSum = this.totalSum.bind(this);
        this.discountTotal =  this.discountTotal.bind(this);
        this.ship = this.ship.bind(this);
        this.goToCheckout = this.goToCheckout.bind(this);
    }
    componentWillUnmount(){
        this.state.subscription.CartItems.stop();
        this.state.subscription.CouponsList.stop();
    }
    componentDidMount(){
        $(".qty_info .dropdown-menu li a").on('click',function () {
            var selText = $(this).text();
            //console.log(selText);
            var count = Number(selText);
            //console.log(count)
            id = $(this).parent().parent().attr('id');
            data = {
                id : id,
                count : count,
                session : Session.get('guestId')
            }
            Meteor.call('changeItemCart',data,function (error) {
                if(!error){
                    Bert.alert('Quantity increased in cart', 'success', 'growl-top-right');
                }
            })
        });
    }
    componentWillMount(){
        var self = this;  
        Tracker.autorun(() => {
            if(!Meteor.userId()){
                if(Meteor.isClient){
                    self.setState({
                        subscription: {
                            CartItems: Meteor.subscribe('CartItems',Session.get('guestId')),
                            CouponsList : Meteor.subscribe('CouponsList')
                        }
                    })
                }
            }else{
                if(Meteor.isClient){
                    self.setState({
                        subscription: {
                            CartItems: Meteor.subscribe('CartItems',Session.get('guestId')),
                            CouponsList : Meteor.subscribe('CouponsList')
                        }
                    })
                }
            }
        })
    }
    CouponsList(code){
        return Coupons.findOne({'code':code,status:'1'})
    }
    code(event){
        this.setState({code:event.target.value})
    }
    carts(){
        return Carts.findOne()
    }
    categoryslug(product){
         pdata = Products.findOne({_id:product,category: { $exists: true, $ne: [] }});
       id = "";
		if(pdata){
			id = pdata.category[0];
		}else{
			id = "";
		}
    if(pdata){
	
        sid = pdata.subcategory[0];
		
		
		if(sid){
			if(sid != undefined){
            return Categories.findOne({_id:id}).slug +'/'+ SubCategories.findOne({_id:sid}).slug+'/';
			}
			
        }else{
			if(id != undefined){
            return Categories.findOne({_id:id}).slug + '/';
			}
			
        }
		
    }else{
            return " ";
        }
	
    }
    pullItemCart(id,event){
        Meteor.call('pullItemCart',{id:id,session:Session.get('guestId')},function (error) {
            if(!error){
                Bert.alert('Item removed from cart', 'success', 'growl-top-right');
            }
        })
    }
    total(){
        if(!_.isUndefined(this.carts())) {
            return _.reduce(this.carts().items,function (memo,val) {
                return memo +  productPrice(val.id) * val.count;
            },0);
        }else{
            return 0
        }
    }
    discountTotal(){
        if(!_.isUndefined(this.state.code)){
            code = (this.state.code).toUpperCase();
            if(!_.isUndefined(this.CouponsList(code))){
                if(this.CouponsList(code).type == 0){
                    //this.setState({discount:this.CouponsList(code).amount});
                    return this.CouponsList(code).amount
                }else{
                    //this.setState({discount: this.total()*this.CouponsList(code).amount/100});
                    return this.total()*this.CouponsList(code).amount/100
                }
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    ship(){
        return this.total() < 800 ? 50: 0;
    }
    totalSum(){
        if(!_.isUndefined(this.state.code)){
            code = (this.state.code).toUpperCase();
            if(!_.isUndefined(this.CouponsList(code))){
                if(this.CouponsList(code).type == 0){
                    if(this.total() + this.ship() >=  this.state.discount){
                        return this.total() + this.ship() - this.state.discount
                    }else{
                        return 0
                    }
                }else{
                    if(this.total() + this.ship() >=  this.state.discount){
                        return this.total() + this.ship() - this.state.discount
                    }else{
                        return 0
                    }
                }
            }else{
                return this.total() + this.ship()
            }
        }else{
            return this.total() + this.ship()
        }
    }
    applyCoupon(event){
        event.preventDefault();
        code = (this.state.code).toUpperCase();
        if(!_.isUndefined(this.CouponsList(code))){
            if(this.CouponsList(code).type == 0){
                this.setState({discount:this.CouponsList(code).amount,couponcode:code,type:this.CouponsList(code).type});
            }else{
                this.setState({discount: this.total()*this.CouponsList(code).amount/100,couponcode:code,type:this.CouponsList(code).type});
            }
        }else{
            this.setState({discount: 0,couponcode : '',type : ''});
            Bert.alert('Code not found', 'danger', 'growl-top-right');
        }
    }
    goToCheckout(event){
        event.preventDefault();
        if(Meteor.userId()){
            data = {
                cart : this.carts(),
                subtotal : this.total(),
                discount : this.discountTotal(),
                ship : this.ship(),
                total : this.totalSum(),
                couponcode : this.state.couponcode,
                type : this.state.type,
                createdBy : Meteor.userId(),
                createdAt : new Date(),
                status : 0,
            }
            Meteor.call('createOrder',data,function (error,result) {
                if(!error){
                    FlowRouter.go('/checkout/'+result);
                }else{
                    Bert.alert('Something wrong try again', 'danger', 'growl-top-right');
                }
            })
        }else{
            data = {
                cart : this.carts(),
                subtotal : this.total(),
                discount : this.discountTotal(),
                ship : this.ship(),
                total : this.totalSum(),
                couponcode : this.state.couponcode,
                type : this.state.type,
                createdBy : Session.get('guestId'),
                createdAt : new Date(),
                status : 0,
            }
            Meteor.call('createOrder',data,function (error,result) {
                if(!error){
                    FlowRouter.go('/checkout/'+result);
                }else{
                    Bert.alert('Something wrong try again', 'danger', 'growl-top-right');
                }
            })
        }
    }
    render(){
        let count = () => {
            if(_.isUndefined(this.carts())){
                return (
                    <h1 className="text-left">Cart <span>(0)</span></h1>
                )
            }else{
                return (
                    <h1 className="text-left">Cart <span>({_.reduce(this.carts().items,function(memo,number){return memo + number.count},0)})</span></h1>
                )
            }
        }
        productImage = (id) =>{
            return Products.findOne(id) && Products.findOne(id).featuredImage;
        }
        productTitle = (id) =>{
            return Products.findOne(id) && Products.findOne(id).title;
        }
        
        productWeight = (id) =>{
            return Products.findOne(id) && Products.findOne(id).weight;
        }
        productPrice = (id) =>{
            return Products.findOne(id) && Products.findOne(id).price;
        }
        let cartDisplay = () => {
        if(!_.isUndefined(this.carts())){
            if (_.size(this.carts().items) > 0) {
                return (
                    this.carts().items.map((val,index) => {
                        return (
                            <div className="cartpage_list" key={'index'+val.id+index}>
                                <div className="row cartpage_row">
                                    <div className="col-md-4 col-sm-6 col-xs-12 cartpage_lft">
                                        <div className="close_img">
                                            <a href="javascript:void(0)" onClick={this.pullItemCart.bind(this,val.id)}>
                                                <img src="/client/images/close_img.png"/>
                                            </a>
                                        </div>
                                        <div className="cartpage_img">
                                            <a href="javascript:void(0)">
                                                <img src={productImage(val.id)} alt={productTitle(val.id)}/>
                                            </a>
                                            <h5 className="text-center">
                                                <a href="javascript:void(0)">{productTitle(val.id)}</a><em>({productWeight(val.id)})</em>
                                            </h5>
                                        </div>
                                    </div>
                                    <div className="col-md-4 col-sm-6 col-xs-12 cartpage_lft">
                                        <div className="carousel-search hidden-phone qty_info">
                                            <div className="btn-group">
                                                <a className="btn dropdown-toggle btn-select"
                                                                          data-toggle="dropdown"
                                                                          href="#">{val.count}<span
                                                className="caret"></span> </a>
                                                <ul className="dropdown-menu" id={val.id}>
                                                    <li><a href="javascript:void(0)">1</a></li>
                                                    <li><a href="javascript:void(0)">2</a></li>
                                                    <li><a href="javascript:void(0)">3</a></li>
                                                    <li><a href="javascript:void(0)">4</a></li>
                                                    <li><a href="javascript:void(0)">5</a></li>
                                                    <li><a href="javascript:void(0)">6</a></li>
                                                    <li><a href="javascript:void(0)">7</a></li>
                                                    <li><a href="javascript:void(0)">8</a></li>
                                                    <li><a href="javascript:void(0)">9</a></li>
                                                    <li><a href="javascript:void(0)">10</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 col-sm-6 col-xs-12 cartpage_rght">
                                        <div className="cartpage_price">
                                            <h4>INR {val.count*productPrice(val.id)}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                )
            } else {
                return (
                    <h5 className="text-left">No Item in cart</h5>
                )
            }
        }else{
            return (
                <h5 className="text-left">No Item in cart</h5>
            )
        }
        }
        subTotal = () =>{
            if(!_.isUndefined(this.carts())) {
                return _.reduce(this.carts().items,function (memo,val) {
                    return memo +  productPrice(val.id) * val.count;
                },0);
            }else{
                return 0
            }
        }
        let priceDisplay = () =>{
        if(!_.isUndefined(this.carts())) {
            if (_.size(this.carts().items) > 0) {
                return (
                    <div>
                        <div className="row totalrow">
                            <div className="total_list">
                                <div className="col-md-8 col-sm-8">
                                    <div className="account_form account_info">
                                        <form onSubmit={this.applyCoupon}>
                                            <div className="form-group">
                                                <input type="text" className="form-control" name="code"
                                                       placeholder="Enter your coupon code" onChange={this.code}/>
                                            </div>
                                            <button type="submit" className="btn btn-default submit_btn">Apply</button>
                                        </form>
                                    </div>
                                </div>
                                <div
                                    className="col-md-4 col-offset-md-8 col-sm-4 col-offset-sm-8 col-xs-12 total_price">
                                    <ul>
                                        <li>Sub Total INR {this.total()}</li>
                                        <li>Discount INR {this.discountTotal()}</li>
                                        <li>Shipping INR {this.ship()}</li>
                                        <li>Total INR {this.totalSum()}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="row checkoutbtn_info">
                            <div className="col-xs-12">
                                <a href="javascript:void(0)" className="checkout_btn" onClick={this.goToCheckout}>Proceed</a>
                            </div>
                        </div>
                    </div>
                )
            } else {
                return null
            }
        }else{
            return null
        }
        }
        DocHead.removeDocHeadAddedTags();
        let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            metaTSite,
            metaGeoPlacename,
            metaGeoPosition,
            metaGeoRegion,
            metaIcbm;
        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = "Cart - Wingreens Farms";
        metaInfoDescription = {name: "description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: "Wingreens Farms - Traditionally handmade dips, spreads, bakery and teas"};
        metaOgDescription = {name: "og:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/cart"};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: "Cart - Wingreens Farms"};
        metaTTitle = {name: "twitter:title", content: "Cart - Wingreens Farms"};
        metaTDescription = {name: "twitter:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
         metaGeoPlacename={name: "geo.placename", content: "B19, 1, Info Technology Park, Sector 34, Gurgaon, Haryana - 122001, India"};
        metaGeoPosition={name: "geo.position", content: "28.430799;77.013499"};
        metaGeoRegion={name: "geo.region", content: "IN-Haryana"};
        metaIcbm={name: "ICBM", content: "28.430799, 77.013499"};
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);
        DocHead.addMeta(metaGeoPlacename);
        DocHead.addMeta(metaGeoPosition);
        DocHead.addMeta(metaGeoRegion);
        DocHead.addMeta(metaIcbm);



        let gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });
        return (
            <div>
                <div className="breadcrumb-info">
                    <ol className="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li className="active">Cart</li>
                    </ol>
                </div>
                <div className="row history_info cartpage_head">
                    <div className="col-xs-4">
                        {count()}
                    </div>
                    <div className="col-xs-4">
                        <h3 className="text-center">Quantity</h3>
                    </div>
                    <div className="col-xs-4">
                        <h3 className="text-right">Price</h3>
                    </div>
                </div>
                {cartDisplay()}
                {priceDisplay()}
                <CrossSell />
            </div>
        )
    }
}
class CrossSell extends Component{
    constructor() {
        super();
        this.state = {
        }
    }
    product(){
        if(!_.isUndefined(Carts.findOne())){
            if (!_.isUndefined(Carts.findOne().items) && !_.isEmpty(Carts.findOne().items)) {
                ids = _.map(Carts.findOne().items,function(val,index){
                    return val.id
                })
                gen = { '_id': { $nin: ids }};
                return Products.find(gen,{skip: (Math.random() * 10) + 1, limit: 10}).fetch();
            }
        }else{
            return Products.find({},{skip: (Math.random() * 10) + 1, limit: 10}).fetch();
        }
    }
    componentDidMount(){
        var swiper_related = new Swiper('.swiper-container-related', {
            nextButton: '.relatedNext',
            prevButton: '.relatedPrev',
            slidesPerView: 3,
            spaceBetween: 0,
            observer:true,
            breakpoints: {
                1024: {
                   slidesPerView: 3,
                   spaceBetween: 40
                },
                768: {
                   slidesPerView: 3,
                   spaceBetween: 30
                },
                700: {
                   slidesPerView: 2,
                   spaceBetween: 20
                },
                400: {
                   slidesPerView: 1,
                   spaceBetween: 10
                }
            }
        });
    }
    categoryslug(product){
         pdata = Products.findOne({_id:product,category: { $exists: true, $ne: [] }});
       id = "";
		if(pdata){
			id = pdata.category[0];
		}else{
			id = "";
		}
    if(pdata){
	
        sid = pdata.subcategory[0];
		
		
		if(sid){
			if(sid != undefined){
            return Categories.findOne({_id:id}).slug +'/'+ SubCategories.findOne({_id:sid}).slug+'/';
			}
			
        }else{
			if(id != undefined){
            return Categories.findOne({_id:id}).slug + '/';
			}
			
        }
		
    }else{
            return " ";
        }
    }
    alttagForSeo(sData){
         if(sData!=""){
        var altSplit=sData.split('|')
        return altSplit[0];
       }
    }
    render(){
        
        if(!_.isUndefined(this.product())){
            return (
                <div className="row">
                    <div className="alsotry_row hidden-xs">
                        <h3>Also try</h3>
                        <div className="swiper-container swiper-container-related">
                            <div className="swiper-wrapper alsotry_slider">
                                {this.product().map((val)=>{
                                    return (
                                        <div className="swiper-slide srr" key={val._id}>
                                            <a href={'/product/'+(this.categoryslug(val._id))+ val.slug}>
                                                <img src={val.featuredImage}   alt={this.alttagForSeo(val.seotitle ? val.seotitle : val.title)} />
                                                <h3>{val.title}</h3>
                                                <h5>{'INR '+val.price}</h5>
                                            </a>
                                        </div>
                                    )
                                })}
                            </div>
                            <div className="swiper-button-next relatedNext"></div>
                            <div className="swiper-button-prev relatedPrev"></div>
                        </div>
                    </div>
                    <div className="alsotry_row visible-xs-block">
                        <h3>Also try</h3>
                        <div className="row">
                            {this.product().map((val)=>{
                                return (
                                    <div className="col-sm-6 col-md-4 col-xs-12 search_list" key={val._id}>
                                        <div className="searchlist_img"> 
                                            <a href={'/product/'+(this.categoryslug(val._id))+ val.slug}>
                                                <img src={val.featuredImage} alt={this.alttagForSeo(val.seotitle ? val.seotitle : val.title)}/>
                                                <h3>{val.title}</h3>
                                                <h5>{'INR '+val.price}</h5>
                                            </a>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            )
        }else{
            return null
        }
    }
}

export {
    Cart
}
