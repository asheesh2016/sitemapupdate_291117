import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactMixin from "react-mixin";

export default class AccountInformation extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
            selectedAddress : '',
            subscription : {
                ShippingDetails : Meteor.subscribe('ShippingDetails')
            }
        }
        this.name = this.name.bind(this);
        this.dob = this.dob.bind(this);
        this.phone = this.phone.bind(this);
        this.handleSubmitChangePasword = this.handleSubmitChangePasword.bind(this);
        this.handleSubmitProfile = this.handleSubmitProfile.bind(this);
    }
    componentWillMount(){
        this.setState({
            name: Meteor.user() && Meteor.user().profile.name,
            dob: Meteor.user() && Meteor.user().profile.dob,
            phone: Meteor.user() && Meteor.user().profile.phone
        });
    }
    componentWillUnmount(){
        this.state.subscription.ShippingDetails.stop();
    }
    name(event){
        this.setState({name: event.target.value});
    }
    dob(event){
        this.setState({dob: event.target.value});
    }
    phone(event){
        this.setState({phone: event.target.value});
    }
    shipping(){
        return Shipping.find()
    }
    deleteAddress(id){
        Meteor.call('deleteShippingAddress',id,function (error) {
            if(!error){
                Bert.alert('Address Deleted Successfully', 'success', 'growl-top-right');
            }else{
                Bert.alert('Something wrong try again', 'danger', 'growl-top-right');
            }
        })
    }
    editAddress(id){
        sh = Shipping.findOne({_id:id});
        this.setState({'selectedAddress':Shipping.findOne({_id:id})})
        $('#address').val(sh.address);
        $('#city').val(sh.city);
        $('#state').val(sh.state);
        $('#country').val(sh.country);
        $('#zipcode').val(sh.zipcode);
        $('#id').val(id);
    }
    handleSubmitProfile(event){
        event.preventDefault();
        function isFullName(name){
            var regex = /^[A-Za-z\s]{3,30}$/;
            return regex.test(name);
        }
        function isPhone(phone){
            var regex = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
            return regex.test(phone);
        }
        function isEmpty(str){
            return !str.replace(/^\s+/g, '').length; // boolean (`true` if field is empty)
        }
        function isDate(dob){
            var formats = ['DD/MM/YYYY','DD-MM-YYYY']
            return moment(dob, formats).isValid() 
        }
        ;
        if(_.isEmpty(this.state.name) || isEmpty(this.state.name)){
            Bert.alert('Please enter the valid name', 'danger', 'growl-top-right');
            return false;
        }else if(!isFullName(this.state.name)){
            Bert.alert('Please enter the valid name', 'danger', 'growl-top-right');
            return false;
        }else if(_.isEmpty(this.state.dob) || isEmpty(this.state.dob)){
            Bert.alert('Please enter the valid dob', 'danger', 'growl-top-right');
            return false;
        }else if(!moment(this.state.dob, 'DD-MM-YYYY', true).isValid()){
            Bert.alert('Please enter valid dob in this format DD-MM-YYYY', 'danger', 'growl-top-right');
            return false;
        }else if(!isPhone(this.state.phone) || isEmpty(this.state.phone)){
            Bert.alert('Please enter the valid phone', 'danger', 'growl-top-right');
            return false;
        }else{
            data = {
                name : this.state.name,
                dob : this.state.dob,
                phone : this.state.phone
            }
            Meteor.call('updateUserProfile',data,function (error) {
                if(error){
                    Bert.alert('Error Updating', 'danger', 'growl-top-right');
                }else{
                    Bert.alert('Profile Updated', 'success', 'growl-top-right');
                }
            })
        }
        
    }
    handleSubmitChangePasword(event){
        event.preventDefault();
        var cpass = $(event.target).find("[name=cpass]").val(),
            npass = $(event.target).find("[name=npass]").val(),
            repass = $(event.target).find("[name=repass]").val();

        if(_.isEmpty(cpass) || _.isEmpty(npass) || _.isEmpty(repass)){
            Bert.alert('Fields not be empty', 'danger', 'growl-top-right');
            return true;
        }
        function isPassword(password){
            var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/;
            return regex.test(password);
        }

        if(!isPassword(npass)){
            Bert.alert('Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character', 'danger', 'growl-top-right');
            return true;
        }
        if(npass != repass){
            Bert.alert('Confirm Password is not Matching', 'danger', 'growl-top-right');
            return true;
        }
        Accounts.changePassword(cpass, npass, function (error) {
            if(error){
                Bert.alert('Current Password is Wrong', 'danger', 'growl-top-right');
            }else{
                Bert.alert('Password changed Successfully ', 'success', 'growl-top-right');
            }
        })
        $(event.target).find("[name=cpass]").val('');
        $(event.target).find("[name=npass]").val('');
        $(event.target).find("[name=repass]").val('');
    }
    subAddress(event){
        event.preventDefault();
        if(_.isEmpty($('#address').val())){
            Bert.alert('Please enter the address', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty($('#city').val())){
            Bert.alert('Please enter the city', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty($('#state').val())){
            Bert.alert('Please enter the state', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty($('#country').val())){
            Bert.alert('Please enter the country', 'danger', 'growl-top-right');
            return false;
        }
        if(_.isEmpty($('#zipcode').val())){
            Bert.alert('Please enter the zipcode', 'danger', 'growl-top-right');
            return false;
        }else{
            if(isNaN($('#zipcode').val()) ||  ($('#zipcode').val().length > 6 || $('#zipcode').val().length <6)){
                Bert.alert('Please enter valid zipcode', 'danger', 'growl-top-right');
                return false;
            }
        }
        data = {
            id : $('#id').val(),
            set : {
                address : $('#address').val(),
                city : $('#city').val(),
                state : $('#state').val(),
                country : $('#country').val(),
                zipcode : $('#zipcode').val(),
            }
        }
        Meteor.call('editShippingAddress',data,function(error){
            if(error){
                Bert.alert('Error Updating', 'danger', 'growl-top-right');
            }else{
                Bert.alert('Address is changed', 'success', 'growl-top-right');
            }
        })
    }
    render(){
        let c;

        if(!_.isUndefined(Meteor.user() && Meteor.user().services.password)){
            c = (
            <div className="account_form">
                <form onSubmit={this.handleSubmitChangePasword}>
                    <div className="form-group">
                        <input
                            className="form-control"
                            type="password"
                            placeholder="Current Password"
                            name="cpass"
                        />
                    </div>
                    <div className="form-group">
                        <input
                            className="form-control"
                            type="password"
                            placeholder="New Password"
                            name="npass"
                        />
                    </div>
                    <div className="form-group">
                        <input
                            className="form-control"
                            type="password"
                            placeholder="Confirm Password"
                            name="repass"
                        />
                    </div>
                    <button type="submit" className="btn btn-default submit_btn">Submit</button>
                </form>
            </div>
            )
        }
        
        let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            metaTSite,
            metaGeoPlacename,
            metaGeoPosition,
            metaGeoRegion,
            metaIcbm;

        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = "Account Information - Wingreens Farms";
        metaInfoDescription = {name: "description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: "Wingreens Farms - Traditionally handmade dips, spreads, bakery and teas"};
        metaOgDescription = {name: "og:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/account-information"};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: "Account Information - Wingreens Farms"};
        metaTTitle = {name: "twitter:title", content: "Account Information - Wingreens Farms"};
        metaTDescription = {name: "twitter:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        metaGeoPlacename={name: "geo.placename", content: "B19, 1, Info Technology Park, Sector 34, Gurgaon, Haryana - 122001, India"};
        metaGeoPosition={name: "geo.position", content: "28.430799;77.013499"};
        metaGeoRegion={name: "geo.region", content: "IN-Haryana"};
        metaIcbm={name: "ICBM", content: "28.430799, 77.013499"};
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);
        DocHead.addMeta(metaGeoPlacename);
        DocHead.addMeta(metaGeoPosition);
        DocHead.addMeta(metaGeoRegion);
        DocHead.addMeta(metaIcbm);


        let gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });
        return (
            <div className="row account_info">
                <div className="breadcrumb-info">
                    <ol className="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li className="active">Account Information</li>
                    </ol>
                </div>
                <div className="col-md-12">
                    <h3 className="text-center">Account Information</h3>
                    <p>Hello ,<br/>
                        Welcome to your Wingreens Farms account.</p>
                    <p>We strive to make your experience better on Wingreens Farms and hence request you to help us get know you better. Rest assured, this information will not be shared with anyone.</p>
                    <div className="row">
                        <div className="col-sm-6 col-xs-12">
                            <div className="account_form">
                                <form onSubmit={this.handleSubmitProfile}>
                                    <div className="form-group">
                                        <input type="text" className="form-control" placeholder="Full Name" name="name" onChange={this.name} value={this.state.name}/>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control" placeholder="Date Of Birth" name="dob" onChange={this.dob} value={this.state.dob}/>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control" placeholder="Mobile Number" name="phone" onChange={this.phone} value={this.state.phone}/>
                                    </div>
                                    <button type="submit" className="btn btn-default submit_btn">Submit</button>
                                </form>
                            </div>
                        </div>
                        <div className="col-sm-6 col-xs-12">
                            {c}
                        </div>
                    </div>
                </div>
                <div className="col-md-12 col-sm-12">
                    <div className="row ordersum_heading">
                        <div className="col-xs-12">
                            <h5>Shipping Details</h5>
                        </div>
                    </div>
                    <div className="row shipping_row">
                        <div className="shipping_details">
                            {this.shipping().map((val) => {
                                return (
                                    <div key={val._id} className="col-md-4 col-sm-6 col-xs-12 shipping_list selectlist" style={{position:'relative',padding:'10px'}}>
                                        <p>{val.address}</p>
                                        <p>{val.city}</p>
                                        <p>{val.state}</p>
                                        <p>{val.country}</p>
                                        <p>{val.zipcode}</p>
                                        <div className="overlay-ship">
                                            <div className="button_box">
                                                <button onClick={this.deleteAddress.bind(this,val._id)} className="delete"><i className="fa fa-trash"></i></button>
                                                <button onClick={this.editAddress.bind(this,val._id)} className="edit" data-toggle="modal" data-target="#myModal"><i className="fa fa-pencil"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}                            
                        </div>
                    </div>
                    <div id="myModal" className="modal fade" role="dialog">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <form onSubmit={this.subAddress}>
                                    <div className="modal-header">
                                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                                        <h4 className="modal-title">Edit Shipping Address</h4>
                                    </div>
                                    <div className="modal-body">
                                        <div className="form-group" style={{marginBottom:'0px'}}>
                                            <input type="text" className="form-control" style={{marginBottom:'10px'}} name="address" id="address" placeholder="Enter your address"/>
                                        </div>
                                        <div className="form-group" style={{marginBottom:'0px'}}>
                                            <input type="text" className="form-control" style={{marginBottom:'10px'}} name="city" id="city" placeholder="Enter your city"/>
                                        </div>
                                        <div className="form-group" style={{marginBottom:'0px'}}>
                                            <input type="text" className="form-control" style={{marginBottom:'10px'}} name="state" id="state" placeholder="Enter your state"/>
                                        </div>
                                        <div className="form-group" style={{marginBottom:'0px'}}>
                                            <input type="text" className="form-control" style={{marginBottom:'10px'}} name="country" id="country" placeholder="Enter your country"/>
                                        </div>
                                        <div className="form-group" style={{marginBottom:'0px'}}>
                                            <input type="text" className="form-control" style={{marginBottom:'10px'}} name="zipcode" id="zipcode" placeholder="Enter your zipcode"/>
                                            <input type="hidden" className="form-control" style={{marginBottom:'10px'}} name="id" id="id"/>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="submit" className="btn btn-default submit_btn">Submit</button>
                                        <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export {
    AccountInformation
}
