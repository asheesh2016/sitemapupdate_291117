import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactMixin from "react-mixin";

import LazyLoad from 'react-lazy-load';

export default class Search extends TrackerReact(Component) {
    constructor() {
        super();
    }
    componentWillUnmount(){
        this.state.subscription.ProductList.stop();
    }
    carts(){
        return Carts.findOne();
    }
    products(){
        return Products.find({status : "1"}).fetch()
    }
    addToCart(id,event){
        event.preventDefault();
        Meteor.call('AddToCart', {id:id,session:Session.get('guestId')}, function (error) {
            if (error) {
                Bert.alert('Error in connection', 'danger', 'growl-top-right');
            } else {
                Bert.alert('Item added to cart', 'success', 'growl-top-right');
            }
        })
    }
    componentDidMount(){
        let self = this;
        Tracker.autorun(() => {
            self.setState({subscription: {
                ProductList : Meteor.subscribe('ProductListWithSearch',Session.get('search')),
            }})
        });
    }
    searchKey(){
        if(Meteor.isClient){
            return Session.get('search')
        }else{
            return '';
        }
    }
    categoryslug(product){
        pdata = Products.findOne({_id:product});
        id = "";
        if(pdata){
            id = pdata.category[0];
        }else{
            id = "";
        }
        if(pdata){
            sid = pdata.subcategory[0]
            if(sid){
                return Categories.findOne({_id:id}).slug +'/'+ SubCategories.findOne({_id:sid}).slug+'/';
            }else{
                return Categories.findOne({_id:id}).slug + '/';
            }
        }else{
            return "";
        }
    }
     alttagForSeo(sData){
         if(sData!=""){
        var altSplit=sData.split('|')
        return altSplit[0];
       }
    }
    render(){
        let cart = (id) =>{
            if(!_.isUndefined(this.carts())){
                if(_.findWhere(this.carts().items,{id:id})){
                    return 'Added to Cart';
                }else{
                    return 'Add to Cart';
                }
            }else{
                return 'Add to Cart';
            }
        }
        let showCart = (val) =>{
            if(val.quantity == 0){
                return (
                    <a href="javascript:void(0)" className="addtocart_btn outofstock_btn adtl pull-left">Out of Stock</a>
                )
            }else{
                return (
                    <a href="javascript:void(0)" className="addtocart_btn adtl pull-left" onClick={this.addToCart.bind(this,val._id)}>{cart(val._id)}</a>
                )
            }
        }
        let title,
            metaInfoDescription,
            metaInfoRobots,
            metaOgLocal,
            metaOgType,
            metaOgTitle,
            metaOgDescription,
            metaOgUrl,
            metaOgSite_name,
            viewport,
            x,
            metaOgimage ,
            metaTTitle,
            metaTDescription,
            metaTcard,
            metaTSite,
            metaGeoPlacename,
            metaGeoPosition,
            metaGeoRegion,
            metaIcbm;
         DocHead.removeDocHeadAddedTags();
        let metaInfoGoogle = {name: "google-site-verification", content: "kjfvAoFsD5ghqNMBh8NA2KdRBvo1UTFRrgrwQtRz0rg"};
        DocHead.addMeta(metaInfoGoogle);
        let linkInfo = {rel: "icon", type: "image/png", href: "/fav.png"};
        let linkInfoA = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" };
        let linkInfoA1 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"76x76"};
        let linkInfoA2 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"120x120"};
        let linkInfoA3 = {rel: "apple-touch-icon", type: "image/png", href: "/fav.png" ,sizes:"152x152"};
        DocHead.addLink(linkInfo);
        DocHead.addLink(linkInfoA);
        DocHead.addLink(linkInfoA1);
        DocHead.addLink(linkInfoA2);
        DocHead.addLink(linkInfoA3);
        viewport = {name:"viewport",content:"width=device-width, initial-scale=1.0"};
        x = {"http-equiv":"Content-Type",content:"text/html",charset:"utf-8"};
        title = "Search - Wingreens Farms";
        metaInfoDescription = {name: "description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaInfoRobots = {name: "robots", content: "noodp"};
        metaOgLocal = {name: "og:locale", content: "en_US"};
        metaOgType = {name: "og:type", content: "website"};
        metaOgTitle = {name: "og:title", content: "Wingreens Farms - Traditionally handmade dips, spreads, bakery and teas"};
        metaOgDescription = {name: "og:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaOgUrl = {name: "og:url", content: "http://www.wingreensfarms.com/search"};
        metaOgimage = {name: "og:image", content: "http://www.wingreensfarms.com/client/images/wingreen-1200x630.jpg"};
        metaOgSite_name = {name: "og:site_name", content: "Search - Wingreens Farms"};
        metaTTitle = {name: "twitter:title", content: "Search - Wingreens Farms"};
        metaTDescription = {name: "twitter:description", content: "We're Wingreens Farms, a delicious, gourmet food company inspired by social good and environmental sustainability. Shop our farm fresh dips, spreads, bakery,  teas- all traditionally handmade. Delivery across India."};
        metaTcard = {name: "twitter:card", content: "summary_large_image"};
        metaTSite = {name: "twitter:site", content: "@WingreensFarms"};
        metaGeoPlacename={name: "geo.placename", content: "B19, 1, Info Technology Park, Sector 34, Gurgaon, Haryana - 122001, India"};
        metaGeoPosition={name: "geo.position", content: "28.430799;77.013499"};
        metaGeoRegion={name: "geo.region", content: "IN-Haryana"};
        metaIcbm={name: "ICBM", content: "28.430799, 77.013499"};
        DocHead.addMeta(metaInfoDescription);
        DocHead.addMeta(metaInfoRobots);
        DocHead.addMeta(metaOgLocal);
        DocHead.addMeta(metaOgType);
        DocHead.addMeta(metaOgTitle);
        DocHead.addMeta(metaOgDescription);
        DocHead.addMeta(metaOgUrl);
        DocHead.addMeta(metaOgSite_name);
        DocHead.addMeta(metaTTitle);
        DocHead.addMeta(metaTDescription);
        DocHead.addMeta(metaTcard);
        DocHead.addMeta(metaTSite);
        DocHead.addMeta(metaOgimage);
        DocHead.setTitle(title);
        DocHead.addMeta(x);
        DocHead.addMeta(viewport);
        DocHead.addMeta(metaGeoPlacename);
        DocHead.addMeta(metaGeoPosition);
        DocHead.addMeta(metaGeoRegion);
        DocHead.addMeta(metaIcbm);


        let gaScript = 'https://www.google-analytics.com/analytics.js';
        DocHead.loadScript(gaScript, function() {
            // Google Analytics loaded
            ga('create', 'UA-52496874-1', 'auto');
            ga('send', 'pageview');
        });
        return (
            <div>
                <div className="row history_info">
                    <div className="breadcrumb-info">
                        <ol className="breadcrumb">
                            <li><a href="/">Home</a></li>
                            <li className="active">Search</li>
                        </ol>
                    </div>
                    <div className="col-xs-12">
                        <h3>Search Results for {this.searchKey()}</h3>
                    </div>
                </div>
                <div className="row">
                    {this.products().map((val)=>{
                        return (
                            <div className="col-sm-6 col-md-4 col-xs-12 search_list gourmet_list" key={val._id}>
                                <div className="searchlist_img"> 
                                    <a href={'/product/'+(this.categoryslug(val._id))+ val.slug}>
                                        <LazyLoad height={250}>
                                            <img src={val.featuredImage} alt={this.alttagForSeo(val.seotitle ? val.seotitle : val.title)}/>
                                        </LazyLoad>
                                    </a> 
                                </div>
                                <h5 className="text-center">
                                    <a href={'/product/'+(this.categoryslug(val._id))+ val.slug}>{val.title}</a> <span>INR  {val.price}</span>
                                </h5>
                                <div className="addtocart_info clearfix">
                                    {showCart(val)}
                                    <a href={'/product/'+(this.categoryslug(val._id))+ val.slug} className="viewdetails_btn adtr pull-right">View Details</a>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}


export {
    Search
}
