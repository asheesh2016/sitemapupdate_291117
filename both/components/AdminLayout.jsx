import React ,{Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class AdminLayout extends TrackerReact(Component) {
    constructor() {
        super();
    }
    componentWillUnmount(){
        document.body.classList.remove('skin-blue', 'sidebar-mini','skin-blue','sidebar-mini');
    }
    componentWillMount(){
        if(Meteor.isClient){
            document.body.classList.remove('hold-transition', 'login-page');
            document.body.classList.add('skin-blue', 'sidebar-mini');
        }
    }
    render(){
        let title = DocHead.getTitle();
        let path = FlowRouter.current().route.name.split('.');
        return (
            <div className="wrapper">
                {this.props.header}
                <div className="content-wrapper">
                    <section className="content-header">
                        <h1>
                            {title}
                            <small>Admin</small>
                        </h1>
                        <ol className="breadcrumb">
                            <li><a href="#"><i className="fa fa-dashboard"></i> Home</a></li>
                            <li className="active">{path[path.length - 1]}</li>
                        </ol>
                    </section>
                    {this.props.content}
                </div>
                {this.props.footer}
            </div>
        )
    }
}

export  class AdminLoginLayout extends TrackerReact(Component) {
    constructor() {
        super();
    }
    componentWillUnmount(){
        document.body.classList.remove('skin-blue', 'sidebar-mini','skin-blue','sidebar-mini');
    }
    componentWillMount(){
        if(Meteor.isClient){
            document.body.classList.add('hold-transition', 'login-page');
            document.body.classList.remove('skin-blue', 'sidebar-mini');
            //document.body.classList.remove('hold-transition', 'login-page');
            //document.body.classList.add('skin-blue', 'sidebar-mini');
        }
    }
    render(){
        return (
            <div className="login-page">
                <div className="login-box">
                    <div className="login-logo">
                        <a href=""><b>Admin</b> Wingreens</a>
                    </div>
                    <div className="login-box-body">
                        {this.props.content}
                    </div>
                </div>
            </div>
        )
    }
}
export  class AdminHeader extends Component{
    render(){
        let button;
            button = (
                <div>
                    <Header/>
                    <SideNav />
                </div>
            )
        return button
    }
}
class Header extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            subscription: {
                ProductList : Meteor.subscribe('ProductList'),
            }
        }
        this.logout = this.logout.bind(this);
    }
    products(){
        return Products.find({quantity: { $lt: 10}}).fetch()
    }
    logout(event){
        event.preventDefault();
        Meteor.logout();
        FlowRouter.go('/admin');
    }
    render(){
        return (
            <header className="main-header">
                <a href="/admin/dashboard" className="logo">
                    <span className="logo-mini"><b>A</b>WG</span>
                    <span className="logo-lg"><b>Admin</b>WG</span>
                </a>
                <nav className="navbar navbar-static-top">
                    <a href="#" className="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span className="sr-only">Toggle navigation</span>
                    </a>
                    <div className="navbar-custom-menu">
                        <ul className="nav navbar-nav">
                            <li className="dropdown notifications-menu">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                                    <i className="fa fa-bell-o"></i>
                                    <span className="label label-warning">{this.products() && this.products().length}</span>
                                </a>
                                <ul className="dropdown-menu">
                                    <li className="header">You have {this.products() && this.products().length} notifications</li>
                                    <li>
                                        <ul className="menu">
                                            {this.products().map((val) => {
                                            return (
                                                <li key={val._id}>
                                                    <a href={'/admin/edit-product/'+val._id}>
                                                        <i className="fa fa-shopping-cart text-red"></i> {val.title}
                                                    </a>
                                                </li>
                                                )
                                            })}
                                        </ul>
                                    </li>
                                    <li className="footer"><a href="/admin/view-products">View all</a></li>
                                </ul>
                            </li>
                            <li className="dropdown user user-menu">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                                    <img src="/images/user2-160x160.jpg" className="user-image"
                                         alt="User Image"/>
                                    <span className="hidden-xs">{Meteor.user() && Meteor.user().username}</span>
                                </a>
                                <ul className="dropdown-menu">
                                    <li className="user-header">
                                        <img src="/images/user2-160x160.jpg" className="img-circle"
                                             alt="User Image"/>
                                        <p>
                                            {Meteor.user() && Meteor.user().username}
                                        </p>
                                    </li>
                                    <li className="user-footer">
                                        <div className="pull-left">
                                            <a href="/admin/profile" className="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div className="pull-right">
                                            <a href="javascript:;" onClick={this.logout}
                                               className="btn btn-default btn-flat">Logout</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
        )
    }
}
class SideNav extends TrackerReact(Component){
    constructor() {
        super();
        this.state = {
            currentUser : Meteor.user(),
        }
    }
    render(){
        let active = (routes) => {
            return FlowRouter.current().route.name === routes ? 'active' : ''
        }
        return (
            <aside className="main-sidebar">
                <section className="sidebar">
                    <div className="user-panel">
                        <div className="pull-left image">
                            <img src="/images/user2-160x160.jpg" className="img-circle" alt="User Image"/>
                        </div>
                        <div className="pull-left info">
                            <p>{Meteor.user() && Meteor.user().username}</p>
                            <a href="#"><i className="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <ul className="sidebar-menu">
                        <li className="header">MAIN NAVIGATION</li>
                        <li className={active('admin.users')}>
                            <a href="/admin/users">
                                <i className="fa fa-users"></i> <span>Users</span>
                            </a>
                        </li>
                        <li className={active('admin.managers')}>
                            <a href="/admin/managers">
                                <i className="fa fa-users"></i> <span>Managers</span>
                            </a>
                        </li>
                        <li className={'treeview ' + active('admin.createProduct') + active('admin.viewProducts')}>
                            <a href="#">
                                <i className="fa fa-cutlery"></i> <span>Products</span>
                                <i className="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul className="treeview-menu">
                                <li className={active('admin.createProduct')}><a href="/admin/create-product"><span>Add New Product</span></a></li>
                                <li className={active('admin.viewProducts')}><a href="/admin/view-products"><span>View / Edit Products</span></a></li>
                                <li className={active('admin.categories')}><a href="/admin/categories"><span>Categories</span></a></li>
                            </ul>
                        </li>
                        <li className={active('admin.viewOrders')}>
                            <a href="/admin/view-orders">
                                <i className="fa fa-car"></i> <span>View Orders</span>
                            </a>
                        </li>
                        <li className={active('admin.viewFailedOrders')}>
                            <a href="/admin/view-failed-orders">
                                <i className="fa fa-car"></i> <span>View Failed Orders</span>
                            </a>
                        </li>
                        <li className={'treeview ' + active('admin.createCoupon') + active('admin.viewCoupons')}>
                            <a href="#">
                                <i className="fa fa-gift"></i> <span>Coupons</span>
                                <i className="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul className="treeview-menu">
                                <li className={active('admin.createCoupon')}><a href="/admin/create-coupon"><span>Add New Coupon</span></a></li>
                                <li className={active('admin.viewCoupons')}><a href="/admin/view-coupons"><span>View / Edit Coupons</span></a></li>
                            </ul>
                        </li>
                        <li className={'treeview ' + active('admin.newpage') + active('admin.listpages')}>
                            <a href="#">
                                <i className="fa fa-file-text-o"></i> <span>Pages</span>
                                <i className="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul className="treeview-menu">
                                <li className={active('admin.newpage')}><a href="/admin/create-newpage"><span>Add New Page</span></a></li>
                                <li className={active('admin.listpages')}><a href="/admin/list-pages"><span>View / Edit Pages</span></a></li>
                            </ul>
                        </li>
                        <li className={active('admin.reviews')}>
                            <a href="/admin/reviews">
                                <i className="fa fa-bullhorn"></i> <span>Reviews</span>
                            </a>
                        </li>
                        <li className={active('admin.reports')}>
                            <a href="/admin/reports">
                                <i className="fa fa-line-chart"></i> <span>Customer/Order Reports</span>
                            </a>
                        </li>
                        <li className={active('admin.productReports')}>
                            <a href="/admin/product-reports">
                                <i className="fa fa-line-chart"></i> <span>ProductName Reports</span>
                            </a>
                        </li>
                        <li className={active('admin.StoreList')}>
                            <a href="/admin/storelist">
                                <i className="fa fa-map-marker"></i> <span>Store Locator</span>
                            </a>
                        </li>
                        <li className={'treeview ' + active('admin.sliders') + active('admin.headerlist') + active('admin.footerlist') + active('admin.seo')}>
                            <a href="#">
                                <i className="fa fa-wrench"></i> <span>Settings</span>
                                <i className="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul className="treeview-menu">
                                <li className={active('admin.sliders')}>
                                    <a href="/admin/sliders">
                                        <span>Home Page Sliders</span>
                                    </a>
                                </li>  
                                <li className={active('admin.headerlist')}>
                                    <a href="/admin/headerlist">
                                        <span>Header List</span>
                                    </a>
                                </li>  
                                <li className={active('admin.footerlist')}>
                                    <a href="/admin/footerlist">
                                        <span>Footer List</span>
                                    </a>
                                </li>
                                <li className={active('admin.background')}>
                                    <a href="/admin/background">
                                        <span>Background</span>
                                    </a>
                                </li>
                                <li className={active('admin.seo')}>
                                    <a href="/admin/seo">
                                        <span>SEO</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </section>
            </aside>
        )
    }
}
export  class AdminFooter extends Component {
    render(){
        return (
            <footer className="main-footer">
                <div className="pull-right hidden-xs">
                    <p className="pull-right">Powered By <a href="https://www.igenero.com" target="_blank">iGenero</a>
                    </p>
                </div>
                <strong>Copyright &copy; 2016 Wingreen</strong> All rights reserved.
            </footer>
        )
    }
}
