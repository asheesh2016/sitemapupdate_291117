import React from 'react';
import {mount} from 'react-mounter';

import AdminLayout,{AdminHeader,AdminFooter,AdminLoginLayout} from './components/AdminLayout';
import UserLayout,{UserLayoutHome,UserHeader,UserFooter} from './components/UserLayout';
import CreatePages,{ListPages,ListPagesEdit} from './components/admin/Pages';
import AdminSliders,{AdminAddSlider,AdminEditSlider} from './components/admin/Sliders';
import {AdminHeaderList} from './components/admin/Header';
import {AdminFooterList} from './components/admin/Footer';
import {AdminBackground} from './components/admin/Background';


import Login from './components/admin/Login';
import AdminDashboard from './components/admin/AdminDashboard';
import AdminProfile from './components/admin/AdminProfile';
import CreateProduct from './components/admin/CreateProduct';
import EditProduct from './components/admin/EditProduct';
import ViewProducts from './components/admin/ViewProducts';
import ViewCoupons from './components/admin/ViewCoupons';
import ViewOrders from './components/admin/ViewOrders';
import ViewFailedOrders from './components/admin/ViewFailedOrders';
import CreateOrder from './components/admin/CreateOrder';
import ViewOrderID from './components/admin/ViewOrderID';
import CreateCoupon from './components/admin/CreateCoupon';
import EditCoupons from './components/admin/EditCoupons';
import AdminCategories,{EditCategory,EditSubCategory} from './components/admin/AdminCategories';
import {SeoPage} from './components/admin/Seo';
import UsersList from './components/admin/UsersList';
import ManagersList from './components/admin/ManagersList';
import CreateManager from './components/admin/CreateManager';
import ViewProfile from './components/admin/ViewProfile';
import Reports from './components/admin/Reports';
import ProductReports from './components/admin/ProductReports';
import ViewRR from './components/admin/ViewRR';
import StoreList from './components/admin/StoreList';
import EditStore from './components/admin/EditStore';
import AddStore from './components/admin/AddStore';

import Home from './components/user/Home';
import Recommendations from './components/user/Recommendations';
import ReviewsRatings from './components/user/ReviewsRatings';
import OrderHistory from './components/user/OrderHistory';
import AccountInformation from './components/user/AccountInformation';
import Search from './components/user/Search';
import Cart from './components/user/Cart';
import NotFound from './components/user/NotFound';
import Checkout from './components/user/Checkout';
import CategoryList from './components/user/CategoryList';
import SubCatList from './components/user/SubCatList';
import Product from './components/user/Product';
import SlugPage from './components/user/SlugPage';
import AllProducts from './components/user/AllProducts';
import Thanks from './components/user/Thanks';
import StoreLocation from './components/user/StoreLocation';
import {PaySuccess,PayError,PayCancel} from './components/user/Payment';
import ResetPassword from './components/user/ResetPassword';

FlowRouter.triggers.enter(function() {
    DocHead.removeDocHeadAddedTags();
 });

let renderMainLayoutWith = (component) => {
   mount(AdminLayout, {
       header: <AdminHeader />,
       content: component,
       footer: <AdminFooter />
   });
}

let renderLoginLayoutWith = (component) => {
    mount(AdminLoginLayout, {
        header: <AdminHeader />,
        content: component,
        footer: <AdminFooter />
    });
}

let renderUserMainLayoutWith = (component) => {
    var metaInfo1 = {name: "robots", content: "noodp, noydir"};

    DocHead.addMeta(metaInfo1);

    mount(UserLayout, {
        header: <UserHeader />,
        content: component,
        footer: <UserFooter />
    });
}
let renderUserHomeLayoutWith = (component) => {
    
    var metaInfo = {rel: "canonical", href: "https://www.wingreensfarms.com"};
    DocHead.addMeta(metaInfo);

    mount(UserLayoutHome, {
        header: <UserHeader />,
        content: component,
        footer: <UserFooter />
    });
}

let renderUserLayout = (component) => {
    if(!Meteor.userId()){
        FlowRouter.go('/');
    }else{
        if(!Roles.userIsInRole(Meteor.userId(), ['admin','user','manager'])){
            FlowRouter.go('/');
        }else{
            renderUserMainLayoutWith(component)
        }
    }
}
FlowRouter.route("/", {
    name: 'Home',
    action() {
        renderUserHomeLayoutWith(<Home />)
    }
});
FlowRouter.route("/reset-password/:token", {
   name: 'Reset',
   action() {
       renderUserHomeLayoutWith(<ResetPassword />)
   }
}); 
FlowRouter.route("/recommendations", {
    name: 'Recommendations',
    action() {
        renderUserMainLayoutWith(<Recommendations />)
    }
});

FlowRouter.route("/reviews-ratings", {
    name: 'ReviewsRatings',
    action() {
        renderUserLayout(<ReviewsRatings />)
    }
});

FlowRouter.route("/order-history", {
    name: 'OrderHistory',
    action() {
        renderUserLayout(<OrderHistory />)
    }
});
FlowRouter.route("/account-information", {
    name: 'AccountInformation',
    action() {
        renderUserLayout(<AccountInformation />)
    }
});

FlowRouter.route("/search", {
    name: 'Search',
    action() {
        renderUserMainLayoutWith(<Search />)
    }
});

FlowRouter.route("/products", {
    name: 'Products',
    action() {
        renderUserMainLayoutWith(<AllProducts />)
    }
});

FlowRouter.route("/cart", {
    name: 'Cart',
    action() {
        renderUserMainLayoutWith(<Cart />)
    }
});

FlowRouter.route("/checkout/:id", {
    name: 'Checkout',
    action() {
        renderUserMainLayoutWith(<Checkout />)
    }
});
FlowRouter.route("/thanks", {
    name: 'Thanks',
    action() {
        renderUserMainLayoutWith(<Thanks />)
    }
});

FlowRouter.route("/product/:slug", {
    name: 'Product1',
    action() {
        renderUserMainLayoutWith(<Product />)
    },
});

FlowRouter.route("/product/:catslug/:slug", {
    name: 'Product',
    action() {
        renderUserMainLayoutWith(<Product />)
    }
});
FlowRouter.route("/product/:catslug/:subcatslug/:slug", {
    name: 'Product',
    action() {
        renderUserMainLayoutWith(<Product />)
    }
});
FlowRouter.route("/category/:slug", {
    name: 'CategoryList',
    action() {
        renderUserMainLayoutWith(<CategoryList />);
        
    }
     
});

FlowRouter.route("/subcat/:slug", {
    name: 'SubCatList',
    action() {
        renderUserMainLayoutWith(<SubCatList />)
    }
});

FlowRouter.route("/store-location",{
    name:"StoreLocation",
    action(){
        renderUserMainLayoutWith(<StoreLocation />)
    }
});


FlowRouter.route("/admin", {
   name: 'admin.login',
   action() {
       renderLoginLayoutWith(<Login />)
   }
});



// /* This is Most important for role based rendering*/
let renderAdminLayout = (component) => {
    if(!Meteor.userId()){
        FlowRouter.go('/admin');
    }else{
        if(!Roles.userIsInRole(Meteor.userId(), ['admin','manager'])){
            FlowRouter.go('/admin');
        }else{
            renderMainLayoutWith(component)
        }
    }
}

FlowRouter.route("/admin/dashboard", {
   name: 'admin.dashboard',
   action() {
       renderAdminLayout(<AdminDashboard />)
   }
});
FlowRouter.route("/admin/profile", {
   name: 'admin.profile',
   action() {
       renderAdminLayout(<AdminProfile />)
   }
});
FlowRouter.route("/admin/create-product", {
   name: 'admin.createProduct',
   action() {
       renderAdminLayout(<CreateProduct />)
   }
});
FlowRouter.route("/admin/edit-product/:id", {
    name: 'admin.editProduct',
    action() {
        renderAdminLayout(<EditProduct />)
    }
});
FlowRouter.route("/admin/view-products", {
   name: 'admin.viewProducts',
   action() {
       renderAdminLayout(<ViewProducts />)
   }
});
FlowRouter.route("/admin/view-orders", {
   name: 'admin.viewOrders',
   action() {
       renderAdminLayout(<ViewOrders />)
   }
});
FlowRouter.route("/admin/view-failed-orders", {
   name: 'admin.viewFailedOrders',
   action() {
       renderAdminLayout(<ViewFailedOrders />)
   }
});
FlowRouter.route("/admin/create-order",{
  name: 'admin.createOrder',
  action() {
      renderAdminLayout(<CreateOrder />)
  }
});
FlowRouter.route("/admin/view-order/:id", {
    name: 'admin.viewOrderId',
    action() {
        renderAdminLayout(<ViewOrderID />)
    }
});
FlowRouter.route("/admin/view-coupons", {
   name: 'admin.viewCoupons',
   action() {
       renderAdminLayout(<ViewCoupons />)
   }
});
FlowRouter.route("/admin/edit-coupons/:id", {
    name: 'admin.editCoupons',
    action() {
        renderAdminLayout(<EditCoupons />)
    }
});
FlowRouter.route("/admin/create-coupon", {
   name: 'admin.createCoupon',
   action() {
       renderAdminLayout(<CreateCoupon />)
   }
});
FlowRouter.route("/admin/categories", {
    name: 'admin.categories',
    action() {
        renderAdminLayout(<AdminCategories />)
    }
});
FlowRouter.route("/admin/categories/edit/:id", {
    name: 'admin.categories.edit',
    action() {
        renderAdminLayout(<EditCategory />)
    }
});
FlowRouter.route("/admin/subcategories/edit/:id", {
    name: 'admin.subcategories.edit',
    action() {
        renderAdminLayout(<EditSubCategory />)
    }
});
FlowRouter.route("/admin/create-newpage", {
   name: 'admin.newpage',
   action() {
       renderAdminLayout(<CreatePages />)
   }
});
FlowRouter.route("/admin/list-pages", {
   name: 'admin.listpages',
   action() {
       renderAdminLayout(<ListPages />)
   }
});
FlowRouter.route("/admin/list-pages/edit/:id", {
   name: 'admin.listpages.edit',
   action() {
       renderAdminLayout(<ListPagesEdit />)
   }
});
FlowRouter.route("/admin/sliders", {
   name: 'admin.sliders',
   action() {
       renderAdminLayout(<AdminSliders />)
   }
});
FlowRouter.route("/admin/addslider", {
   name: 'admin.addslider',
   action() {
       renderAdminLayout(<AdminAddSlider />)
   }
});
FlowRouter.route("/admin/editslider/:id", {
   name: 'admin.editslider',
   action() {
       renderAdminLayout(<AdminEditSlider />)
   }
});
FlowRouter.route("/admin/headerlist/", {
   name: 'admin.headerlist',
   action() {
       renderAdminLayout(<AdminHeaderList />)
   }
});
FlowRouter.route("/admin/seo/", {
   name: 'admin.seo',
   action() {
       renderAdminLayout(<SeoPage />)
   }
});
FlowRouter.route("/admin/footerlist/", {
   name: 'admin.footerlist',
   action() {
       renderAdminLayout(<AdminFooterList />)
   }
});
FlowRouter.route("/admin/background/", {
   name: 'admin.background',
   action() {
       renderAdminLayout(<AdminBackground />)
   }
});
FlowRouter.route("/admin/users", {
    name: 'admin.users',
    action() {
        renderAdminLayout(<UsersList />)
    }
});
FlowRouter.route("/admin/managers", {
    name: 'admin.managers',
    action() {
        renderAdminLayout(<ManagersList />)
    }
});
FlowRouter.route("/admin/create-manager",{
    name: 'admin.createmanager',
    action(){
      renderAdminLayout(<CreateManager />)
    }
})
FlowRouter.route("/admin/view-profile/:id", {
    name: 'admin.viewProfile',
    action() {
        renderAdminLayout(<ViewProfile />)
    }
});
FlowRouter.route("/admin/reports",{
    name:"admin.reports",
    action(){
        renderAdminLayout(<Reports />)
    }
});
FlowRouter.route("/admin/product-reports",{
    name:"admin.productReports",
    action(){
      renderAdminLayout(<ProductReports />)
    }
})
FlowRouter.route("/admin/reviews",{
    name:"admin.reviews",
    action(){
        renderAdminLayout(<ViewRR />)
    }
});

FlowRouter.route("/admin/storelist",{
    name:"admin.StoreList",
    action(){
        renderAdminLayout(<StoreList />)
    }
});

FlowRouter.route("/admin/editstore/:id",{
    name:"admin.EditStore",
    action(){
        renderAdminLayout(<EditStore />)
    }
});

FlowRouter.route("/admin/addstore",{
    name:"admin.AddStore",
    action(){
        renderAdminLayout(<AddStore />)
    }
});



FlowRouter.route("/payments/success",{
    name:"success",
    action(params,queryParams){
        console.log(params,queryParams)
        renderUserMainLayoutWith(<PaySuccess />)
    }
});
FlowRouter.route("/payments/error",{
    name:"success",
    action(params,queryParams){
        console.log(params,queryParams)
        renderUserMainLayoutWith(<PayError />)
    }
});
FlowRouter.route("/payments/cancel",{
    name:"success",
    action(params,queryParams){
        console.log(params,queryParams)
        renderUserMainLayoutWith(<PayCancel />) 
    }
});
FlowRouter.route("/:slug", {
    name:'pages',
    action() {
        renderUserMainLayoutWith(<SlugPage />)
    }
});


